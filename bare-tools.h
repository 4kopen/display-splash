/*
* This file is part of Splash Screen
*
* Copyright 2013, STMicroelectronics - All Rights Reserved
* Author(s): mohamed-ali.fodha@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* Splash Screen may alternatively be licensed under GPL V2 license from ST.
*/

#ifndef _BARE_TOOLS_H
#define _BARE_TOOLS_H

void *splash_memcpy(void *dest, const void * const src, const U32 len);
void* splash_memset(void *ptr, const int ch, const U32 len);
void* splash_malloc(const U32 size, const U16 align);

#endif /* _BARE_TOOLS_H */
