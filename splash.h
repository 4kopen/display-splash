/*
* This file is part of Splash Screen
*
* Copyright 2013, STMicroelectronics - All Rights Reserved
* Author(s): mohamed-ali.fodha@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* Splash Screen may alternatively be licensed under GPL V2 license from ST.
*/
#ifndef _SPLASH_H
#define _SPLASH_H

#include "sys.h"

/* CLUT needs to be 16 bytes aligned */
#define CLUT_GDP_ALIGNMENT       16
#define CLUT_ENTRIES             256
#define CLUT_SIZE               ((CLUT_ENTRIES + CLUT_GDP_ALIGNMENT) * (sizeof (U32)))
#define _ALIGN_UP(addr, size)   (((addr)+((size)-1))&(~((size)-1)))
#define SET_ARGB_PIXEL(a,r,g,b) (((a) << 24) | \
                                 ((r) << 16) | \
                                 ((g) << 8)  | \
                                 (b))

typedef enum stm_hdmi_sta_e {
  HOT_PLUG_STA,
  RX_SENSE_STA
} stm_hdmi_sta_t;

typedef enum stm_pixel_format_e {
  PIXEL_FORMAT_RGB_AUTO,
  PIXEL_FORMAT_CLUT8,
  PIXEL_FORMAT_RGB_BITFIELDS
} stm_pixel_format_t;

typedef struct stm_img_s {
  char *            fbuffer;
  U32*              Palette;
  stm_pixel_format_t pixel_format;
  U32               bits_per_pixel;
  int               height;
  int               width;
} stm_img_t;

typedef enum stm_wss_aspect_ratio_e
{
  STM_WSS_ASPECT_RATIO_DISABLED = 0,
  STM_WSS_ASPECT_RATIO_4_3,
  STM_WSS_ASPECT_RATIO_16_9,
  STM_WSS_ASPECT_RATIO_14_9,
  STM_WSS_ASPECT_RATIO_GT_16_9
} stm_wss_aspect_ratio_t;

typedef enum stm_wss_cgms_a_e
{
  STM_WSS_CGMS_A_DISABLED = 0,
  STM_WSS_CGMS_A_COPY_RESTRICTED,
  STM_WSS_CGMS_A_COPY_NOT_RESTRICTED
} stm_wss_cgms_a_t;

typedef struct stm_wss_config_s
{
  stm_wss_aspect_ratio_t  aspect_ratio;
  stm_wss_cgms_a_t        cgms_a;
} stm_wss_config_t;

typedef enum splash_modes_e{
  STM_PAL,
  STM_NTSC,
  STM_720X576P50,
  STM_720X480P60,
  STM_1280X720P60,
  STM_1280X720P50,
  STM_1920X1080P60,
  STM_1920X1080P50,
  STM_1920X1080I60,
  STM_1920X1080I50,

  STM_TIMING_MODES_NBR
} splash_modes_t;

extern int splash_get_hdmi_status(stm_hdmi_sta_t);
extern void splash_display_img(stm_img_t *imgInfo);
extern void splash_set_wss(stm_wss_config_t wss_config);
extern void splash_enable(splash_modes_t main_mode, splash_modes_t aux_mode, int enable, stm_img_t *imgInfo);
extern void do_splash_ext(int idx);
extern void do_splash(void);


#endif /* _SPLASH_H */
