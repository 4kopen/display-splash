/*
* This file is part of Splash Screen
*
* Copyright 2013, STMicroelectronics - All Rights Reserved
* Author(s): mohamed-ali.fodha@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* Splash Screen may alternatively be licensed under GPL V2 license from ST.
*/
#include "sys.h"
#include "bmp-decoder.h"
#include "bare-tools.h"

typedef struct BmpHeader_s
{
  U16 signature;
  U32 bmp_file_size;
  U16 reserved0;
  U16 reserved1;
  U32 offset_data;
  U32 size_bmp_header;
  int width;
  int height;
  U16 number_of_plane;
  U16 bits_per_pixel;
  U32 compression_type;
  U32 size_data;
  U32 horizontal_resolution;
  U32 vertical_resolution;
  U32 number_of_color;
  U32 number_of_important_color;
} BmpHeader_t;

typedef enum BmpCompression_e
{
  BI_RGB,
  BI_RLE8,
  BI_RLE4,
  BI_BITFILEDS
} BmpCompression_e;

typedef enum RleState_e
{
  RLE_NEXT = 0,
  RLE_END = 1,
  RLE_MOVE = 2
} RleState_e;

static void bmp_parse_header(U8 *bmp_data, BmpHeader_t * bmpHeader)
{
  bmpHeader->signature = bmp_data[0] | bmp_data[1] << 8;
  bmpHeader->bmp_file_size = bmp_data[2] | bmp_data[3] << 8 | bmp_data[4] << 16| bmp_data[5] << 24;
  bmpHeader->reserved0 = bmp_data[6] | bmp_data[7] << 8;
  bmpHeader->reserved1 = bmp_data[8] | bmp_data[9] << 8;
  bmpHeader->offset_data =  bmp_data[10] | bmp_data[11] << 8 | bmp_data[12] << 16| bmp_data[13] << 24;
  bmpHeader->size_bmp_header = bmp_data[14] | bmp_data[15] << 8 | bmp_data[16] << 16| bmp_data[17] << 24;
  bmpHeader->width = bmp_data[18] | bmp_data[19] << 8 | bmp_data[20] << 16| bmp_data[21] << 24;
  bmpHeader->height = bmp_data[22] | (bmp_data[23] << 8) | (bmp_data[24] << 16)| (bmp_data[25] << 24);
  bmpHeader->number_of_plane = bmp_data[26] | bmp_data[27] << 8;
  bmpHeader->bits_per_pixel = bmp_data[28] | bmp_data[29] << 8;
  bmpHeader->compression_type = bmp_data[30] | bmp_data[31] << 8 | bmp_data[32] << 16| bmp_data[33] << 24;
  bmpHeader->size_data = bmp_data[34] | bmp_data[35] << 8 | bmp_data[36] << 16| bmp_data[37] << 24;
  bmpHeader->horizontal_resolution = bmp_data[38] | bmp_data[39] << 8 | bmp_data[40] << 16| bmp_data[41] << 24;
  bmpHeader->vertical_resolution = bmp_data[42] | bmp_data[43] << 8 | bmp_data[44] << 16| bmp_data[45] << 24;
  bmpHeader->number_of_color = bmp_data[46] | bmp_data[47] << 8 | bmp_data[48] << 16| bmp_data[49] << 24;
  bmpHeader->number_of_important_color = bmp_data[50] | bmp_data[51] << 8 | bmp_data[52] << 16| bmp_data[53] << 24;

  splash_debug(" signature        = %d\n", bmpHeader->signature);
  splash_debug(" bmp_file_size    = %d\n", bmpHeader->bmp_file_size );
  splash_debug(" offset_data      = %d\n", bmpHeader->offset_data);
  splash_debug(" size_bmp_header  = %d\n", bmpHeader->size_bmp_header);
  splash_debug(" width            = %d\n", bmpHeader->width);
  splash_debug(" height           = %d\n", bmpHeader->height);
  splash_debug(" number_of_plane  = %d\n", bmpHeader->number_of_plane);
  splash_debug(" bits_per_pixel   = %d\n", bmpHeader->bits_per_pixel);
  splash_debug(" compression_type = %d\n", bmpHeader->compression_type);
  splash_debug(" size_data        = %d\n", bmpHeader->size_data);
  splash_debug(" vert_resolution  = %d\n", bmpHeader->vertical_resolution);
  splash_debug(" number_of_color  = %d\n", bmpHeader->number_of_color);
  splash_debug(" nb_imprtnt_color = %d\n", bmpHeader->number_of_important_color);
}

/*stm_pixel_format_t bmp_get_color_format(BmpHeader_t * bmpHeader)
{
  stm_pixel_format_t     ColorFormat = 0 ;
  if(bmpHeader->bits_per_pixel == 8)
      ColorFormat = SURF_CLUT8;
  return ColorFormat;
}*/

static void bmp_get_resolution(BmpHeader_t * bmpHeader, int * height, int * width)
{
  if(bmpHeader->height < 0)
  {
    *height = - bmpHeader->height;
  }
  else
  {
    *height = bmpHeader->height;
  }
  *width = bmpHeader->width;
}

static U32 bmp_get_buffer_size(BmpHeader_t * bmpHeader, int height, int width)
{
  if(bmpHeader->compression_type == BI_RLE8)
    return (height * width);
  else
    return (bmpHeader->bmp_file_size - bmpHeader->offset_data);
}

static U32 bmp_get_bits_per_pixel(BmpHeader_t * bmpHeader)
{
  return (bmpHeader->bits_per_pixel);
}

static char * bmp_decode_RLE8(U8 *bmp_data, BmpHeader_t * bmpHeader, int height)
{
  U8 byte1    = 0;
  U8 byte2    = 0;
  U32 i        = 0;
  U32 position = 0;

  char * fbuffer = 0;
  char * fbufferAddress = 0;
  fbuffer = (char *) splash_malloc((((((bmpHeader->width  * bmpHeader->bits_per_pixel + 7) >> 3) + 3) & ~3)*height), 16);
  if(!fbuffer)
  {
    return 0;
  }
  splash_memset(fbuffer, 0x00, ((((bmpHeader->width  * bmpHeader->bits_per_pixel + 7) >> 3) + 3) & ~3)*height);
  fbufferAddress = fbuffer;
  while(position <(bmpHeader->bmp_file_size-bmpHeader->offset_data-1))
  {
    byte1 = bmp_data[bmpHeader->offset_data+position];
    position++;
    byte2 = bmp_data[bmpHeader->offset_data+position];
    position++;

    if(byte1)
    {
       for(i=0;i<byte1;i++)
       {
         *fbuffer = byte2;
         fbuffer++;
       }
    }
    else
    {
      switch(byte2)
      {
        case RLE_NEXT:
        {
          break;
        }
        case RLE_END:
        {
          *fbuffer=byte1;
           fbuffer++;
          *fbuffer=byte2;
          break;
        }
        case RLE_MOVE:
        {
          position+=bmp_data[bmpHeader->offset_data+position+2]+(bmp_data[bmpHeader->offset_data+position+3]*bmpHeader->width);
          break;
        }
        default:
        {
           for(i=0;i<byte2;i++)
           {
             *fbuffer= bmp_data[bmpHeader->offset_data+position];
              fbuffer++;
             position++;
           }
           if(byte2 & 1)
            position++;
            break;
        }
      }
    }
  }
  return fbufferAddress;
}

static char * bmp_invert_buffer(char *decod_buffer, U32 size, U32 bits_per_pixel, U32 width, U32 height)
{
  U32 i = 0;
  U32 j = 0;
  U32 k = 0;

  char* fbuffer = 0;
  fbuffer = (char *) splash_malloc((((((width  * bits_per_pixel + 7) >> 3) + 3) & ~3)*height), 16);
  if(!fbuffer)
  {
    return 0;
  }
  splash_memset(fbuffer, 0x00, size);
  for (i=1; i <= height; i++)
  {
    for(k=0;k < (width*(bits_per_pixel/8));k++)
    {
      fbuffer[j] = decod_buffer[size - ((((width * bits_per_pixel + 7) >> 3) + 3) & ~3)*i +k];
      j++;
    }
  }
  return fbuffer;
}

void splash_get_bmp_info(U8 *bmpData, stm_img_t *imgInfo)
{
  int  i = 0;
  BmpHeader_t  bmpHeader;
  char *       decode_fbuffer = 0;
  U32          buffer_size    = 0;

  imgInfo->Palette = (U32*)splash_malloc(CLUT_SIZE, CLUT_GDP_ALIGNMENT);
  splash_memset(&bmpHeader,0, sizeof(BmpHeader_t));
  splash_memset(imgInfo->Palette, 0, CLUT_SIZE);

  bmp_parse_header(bmpData, &bmpHeader);

  bmp_get_resolution(&bmpHeader, &imgInfo->height, &imgInfo->width);
  buffer_size    = bmp_get_buffer_size(&bmpHeader, imgInfo->height, imgInfo->width);
  imgInfo->bits_per_pixel = bmp_get_bits_per_pixel(&bmpHeader);

  if (bmpHeader.compression_type == BI_RLE8)
  {
    U8 * palbmp = &bmpData[14 + bmpHeader.size_bmp_header];
    imgInfo->pixel_format = PIXEL_FORMAT_CLUT8;
    for(i=0;i<(bmpHeader.number_of_color);i++)
    {
       imgInfo->Palette[i] = SET_ARGB_PIXEL(255,palbmp[2],palbmp[1],palbmp[0]);
       palbmp += 4;
    }
    decode_fbuffer = bmp_decode_RLE8(bmpData, &bmpHeader, imgInfo->height);
  }
  else if (bmpHeader.compression_type == BI_RGB)
  {
    imgInfo->pixel_format = PIXEL_FORMAT_RGB_AUTO;
    decode_fbuffer = (char *) &bmpData[bmpHeader.offset_data];
  }
  else if (bmpHeader.compression_type == BI_BITFILEDS)
  {
    imgInfo->pixel_format = PIXEL_FORMAT_RGB_BITFIELDS;
    decode_fbuffer = (char *)(&bmpData[bmpHeader.offset_data]);
  }

  if (bmpHeader.height < 0)
    imgInfo->fbuffer = decode_fbuffer;
  else
    imgInfo->fbuffer = bmp_invert_buffer(decode_fbuffer, buffer_size, imgInfo->bits_per_pixel, imgInfo->width, imgInfo->height);
}
