/*
* This file is part of Splash Screen
*
* Copyright 2013, STMicroelectronics - All Rights Reserved
* Author(s): mohamed-ali.fodha@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* Splash Screen may alternatively be licensed under GPL V2 license from ST.
*/

#ifndef _BMP_MANAGER_H
#define _BMP_MANAGER_H

#include "sys.h"
#include "splash.h"

void splash_get_bmp_info(U8 *bmpData, stm_img_t *imgInfo);

#endif /* _BMP_MANAGER_H */
