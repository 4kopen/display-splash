/*
* This file is part of Splash Screen
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
* Author(s): mohamed-ali.fodha@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* Splash Screen may alternatively be licensed under GPL V2 license from ST.
*/

#ifndef _STIH418REG_H
#define _STIH418REG_H

/* STiH418 Base addresses for display IP ------------------------------------ */
#define STiH418_REGISTER_BASE          0x08D00000

/* STiH418 Base address size -------------------------------------------------*/
#define STiH418_REG_ADDR_SIZE          0x02000000

/*
 * The two TVOut register blocks:
 * TVOUT0 is TVOUT base address, TVOUT1 is TVOUT glue base address
 */
#define STiH418_TVOUT0_BASE             (STiH418_REGISTER_BASE + 0x01B00000)
#  define STiH418_DENC_BASE              (STiH418_TVOUT0_BASE + 0x0000)
#  define STiH418_VTG_AUX_BASE           (STiH418_TVOUT0_BASE + 0x0200)
#  define STiH418_FLEXDVO_BASE           (STiH418_TVOUT0_BASE + 0x0400)
#  define STiH418_HD_FORMATTER_BASE      (STiH418_TVOUT0_BASE + 0x2000)
#  define STiH418_HD_FORMATTER_AWG       (STiH418_HD_FORMATTER_BASE + 0x300)
#  define STiH418_VTG_MAIN_BASE          (STiH418_TVOUT0_BASE + 0x2800)
#  define STiH418_HDMI_BASE              (STiH418_TVOUT0_BASE + 0x4000)


#define STiH418_TVOUT1_BASE            (STiH418_REGISTER_BASE + 0x01C00000)
#  define STiH418_TVO_MAIN_PF_BASE       (STiH418_TVOUT1_BASE + 0x000)
#  define STiH418_TVO_AUX_PF_BASE        (STiH418_TVOUT1_BASE + 0x100)
#  define STiH418_TVO_VIP_DENC_BASE      (STiH418_TVOUT1_BASE + 0x200)
#  define STiH418_TVO_TTXT_BASE          (STiH418_TVOUT1_BASE + 0x300)
#  define STiH418_TVO_VIP_HDF_BASE       (STiH418_TVOUT1_BASE + 0x400)
#  define STiH418_TVO_VIP_HDMI_BASE      (STiH418_TVOUT1_BASE + 0x500)

/* The compositor (COMP) offset addresses ------------------------------------*/
#define STiH418_COMPOSITOR_BASE        (STiH418_REGISTER_BASE + 0x01900000)
#  define STiH418_GDP1_BASE              (STiH418_COMPOSITOR_BASE + 0x000000)
#  define STiH418_GDP6_BASE              (STiH418_COMPOSITOR_BASE + 0x050000)
#  define STiH418_MIXER1_BASE            (STiH418_COMPOSITOR_BASE + 0x100000)
#  define STiH418_MIXER2_BASE            (STiH418_COMPOSITOR_BASE + 0x110000)

/* SYSCFG --------------------------------------------------------------------*/
#define STiH418_SYSCFG_CORE             (STiH418_REGISTER_BASE + 0x005B0000)

/* Sysconfig Core 5131, Reset Generator control 0 ----------------------------*/
#define SYS_CFG5072                    0x120

#define SYS_CFG5131                    0x20c
#define SYS_CFG5131_RST_N_HDTVOUT        (1L<<0)
#define SYS_CFG5131_CLK_EN_HDTVOUT       (1L<<11)

#endif // _STIH418REG_H
