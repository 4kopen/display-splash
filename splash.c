/*
* This file is part of Splash Screen
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
* Author(s): mohamed-ali.fodha@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* Splash Screen may alternatively be licensed under GPL V2 license from ST.
*/

#include "sys.h"
#include "sti-reg.h"
#if defined(SPLASH_IMAGE_BMP)
#include "bmp-decoder.h"
#include "bmp.h"
#endif
#include "bare-tools.h"
#include "gdp-filters.h"
#include "vclk.h"
#include "modes.h"
#include "splash.h"

#ifdef TEST_ALL_FORMATS
#include "bmp_rgb565.h"
#include "bmp_rgb888.h"
#include "bmp_argb8888.h"
#include "bmp_argb1555.h"
#endif

extern int clk_get_clock_configuration(splash_modes_t main_mode, const stm_clock_t *clock_cfg);
extern int clk_set_clocks(const stm_clock_t* clock_cfg);
extern int clk_disable_clocks(void);

#ifdef USE_SPLASH_SHELL
extern void apply_hdmi_config();
extern void apply_config();
#endif

#ifdef STM_MAIN_MODE_1280X720P50
#define MAIN_MODE STM_1280X720P50
#elif STM_MAIN_MODE_1280X720P60
#define MAIN_MODE STM_1280X720P60
#elif STM_MAIN_MODE_720X480P60
#define MAIN_MODE STM_720X480P60
#elif STM_MAIN_MODE_720X576P50
#define MAIN_MODE STM_720X576P50
#elif STM_MAIN_MODE_1920X1080I60
#define MAIN_MODE STM_1920X1080I60
#elif STM_MAIN_MODE_1920X1080I50
#define MAIN_MODE STM_1920X1080I50
#elif STM_MAIN_MODE_1920X1080P60
#define MAIN_MODE STM_1920X1080P60
#elif STM_MAIN_MODE_1920X1080P50
#define MAIN_MODE STM_1920X1080P50
#else
#error Main timing mode not defined
#endif

#ifdef STM_AUX_MODE_PAL
#define AUX_MODE STM_PAL
#elif STM_AUX_MODE_NTSC
#define AUX_MODE STM_NTSC
#else
#error Auxilliary timing mode not defined
#endif

/* Minimal Vsync counter for Mixer/GDP registers latching is 2 Vsyncs */
#define MIXER_FETCH_VSYNC_COUNTER 2

/* NVN offset is not the same for GDP and GDPPlus IPs */
#define GDP_NVN_OFFSET            0x24
#define GDP_GQR_NVN_OFFSET        0x1c

#define GDP_CTL_ALPHA_RANGE_FULL  (1<<5)
#define GDP_CTL_EN_H_RESIZE       (1<<10)
#define GDP_CTL_EN_V_RESIZE       (1<<11)
#define GDP_VSRC_FILTER_EN        (1<<24)
#define GDP_HSRC_FILTER_EN        (1<<24)
#define GDP_CTL_ENA_CLUT_UPDATE   (1<<27)
#define GDP_CTL_EN_VFILTER_UPD    (1<<28)
#define GDP_CTL_EN_HFILTER_UPD    (1<<30)
#define GDP_CTL_WAIT_NEXT_VSYNC   (1<<31)

#define CLUT8                     (0x0b | GDP_CTL_ENA_CLUT_UPDATE)
#define ARGB8888                  (0x05 | GDP_CTL_ALPHA_RANGE_FULL)
#define RGB888                    (0x01)
#define RGB565                    (0x00)
#define ARGB1555                  (0x06)

#define HOTPLUG_STATUS_MASK       (1L<<4)
#define RXSENSE_STATUS_MASK       (1L<<6)

#define ASPECT_RATIO_MASK         (0xF0)
#define WSS_ENABLE                (1)
#define CGMS_A_MASK               (1<<5)
#define CGMS_A_ENABLE             (1<<5)

/* Used to choose RGB or YCbCr format for HDMI           */
#ifdef STM_HDMI_YCBCR_ENABLE
#define HDMI_YCBCR_ENABLE         (1L<<14)
#define HDMI_YCBCR_LIMITED_RANGE  (1L<<8)
#define HDMI_YCBCR_CONV_ENABLE    (0x01)
#define RGB_CHECKSUM_OFFSET       (0x00)
#else
#define HDMI_YCBCR_ENABLE         (0)
#define HDMI_YCBCR_LIMITED_RANGE  (0)
#define HDMI_YCBCR_CONV_ENABLE    (0x00)
#define RGB_CHECKSUM_OFFSET       (0x40)
#endif

#define GDP_NODE_ALIGNMENT        16
#define GDP_FILTERS_ALIGNMENT     16

/*********************************************************/
/* Strcut desclaration                                   */
/*********************************************************/

typedef struct
{
    U32 GDPn_CTL;   /* 0x00 control register                */
    U32 GDPn_AGC;   /* 0x04 alpha gain constant             */
    U32 GDPn_HSRC;  /* 0x08 H sample rate converter         */
    U32 GDPn_VPO;   /* 0x0C viewport offset                 */
    U32 GDPn_VPS;   /* 0x10 viewport stop                   */
    U32 GDPn_PML;   /* 0x14 pixmap memory location          */
    U32 GDPn_PMP;   /* 0x18 pixmap memory pitch             */
    U32 GDPn_SIZE;  /* 0x1C pixmap size                     */
    U32 GDPn_VSRC;  /* 0x20 V sample rate converter         */
    U32 GDPn_NVN;   /* 0x24 next viewport node              */
    U32 GDPn_KEY1;  /* 0x28 Color keying - lower limit      */
    U32 GDPn_KEY2;  /* 0x2C Color keying - upper limit      */
    U32 GDPn_HFP;   /* 0x30 horizontal filter pointer       */
    U32 GDPn_PPT;   /* 0x34 Properties register             */
    U32 GDPn_VFP;   /* 0x38 Vertical/flicker filter pointer */
    U32 GDPn_CML;   /* 0x3C Clut memory location            */
    U32 GDPn_CROP;  /* 0x40 H418 Horizontal Crop            */
    U32 GDPn_BT0;   /* 0x44 H418 BT2020_0 register          */
    U32 GDPn_BT1;   /* 0x48 H418 BT2020_0 register          */
    U32 GDPn_BT2;   /* 0x4C H418 BT2020_0 register          */
    U32 GDPn_BT3;   /* 0x50 H418 BT2020_0 register          */
    U32 GDPn_BT4;   /* 0x54 H418 BT2020_0 register          */
    U32 Reserved1;  /* 0x58 align to 128 bits               */
    U32 Reserved2;  /* 0x5C align to 128 bits               */
} GDP_Node_t;

/* GDPPlus struct definition */
typedef struct
{
    U32 GDPn_GQR_CTL;   /* 0x00 control register            */
    U32 GDPn_GQR_AGC;   /* 0x04 alpha gain constant         */
    U32 GDPn_GQR_VPO;   /* 0x08 viewport offset             */
    U32 GDPn_GQR_VPS;   /* 0x0C viewport stop               */
    U32 GDPn_GQR_PML;   /* 0x10 pixmap memory location      */
    U32 GDPn_GQR_PMP;   /* 0x14 pixmap memory pitch         */
    U32 GDPn_GQR_SIZE;  /* 0x18 pixmap size                 */
    U32 GDPn_GQR_NVN;   /* 0x1C next viewport node          */
    U32 GDPn_GQR_KEY1;  /* 0x20 Color keying - lower limit  */
    U32 GDPn_GQR_KEY2;  /* 0x24 Color keying - upper limit  */
    U32 GDPn_GQR_HFP;   /* 0x28 horizontal filter pointer   */
    U32 GDPn_GQR_PPT;   /* 0x2C Properties register         */
    U32 GDPn_GQR_VFP;   /* 0x30 Vertical filter pointer     */
    U32 GDPn_GQR_CML;   /* 0x34 Clut memory location        */
    U32 GDPn_GQR_CROP;  /* 0x38 H418 Horizontal Crop        */
    U32 GDPn_GQR_BT0;   /* 0x3C H418 BT2020_0 register      */
    U32 GDPn_GQR_BT1;   /* 0x40 H418 BT2020_0 register      */
    U32 GDPn_GQR_BT2;   /* 0x44 H418 BT2020_0 register      */
    U32 GDPn_GQR_BT3;   /* 0x48 H418 BT2020_0 register      */
    U32 GDPn_GQR_BT4;   /* 0x4C H418 BT2020_0 register      */
    U32 GDPn_GQR_HSRC;  /* 0x50 H sample rate converter     */
    U32 GDPn_GQR_HIP;   /* 0x54 horizontal initial phase    */
    U32 GDPn_GQR_HP1;   /* 0x58 horizontal param 1          */
    U32 GDPn_GQR_HP2;   /* 0x5C horizontal param 2          */
    U32 GDPn_GQR_VSRC;  /* 0x60 V sample rate converter     */
    U32 GDPn_GQR_VIP;   /* 0x64 vertical initial phase      */
    U32 GDPn_GQR_VP1;   /* 0x68 vertical param 1            */
    U32 GDPn_GQR_VP2;   /* 0x6C vertical param 2            */
} GDP_GQR_Node_t;

#ifndef stx418
static GDP_Node_t *gdp3_node1 = NULL, *gdp3_node2 = NULL;
#else
static GDP_GQR_Node_t *gdp3_node1 = NULL, *gdp3_node2 = NULL;
#endif
static GDP_Node_t *gdp4_node1 = NULL, *gdp4_node2 = NULL;
static U32         hfilters   = 0;
static U32         vfilters   = 0;
static splash_modes_t main_mode;
static splash_modes_t aux_mode;

U16 awg_ram[STM_TIMING_MODES_NBR] [64]= {
 /* STM_PAL, */  { },
 /* STM_NTSC, */ { },
 { /*STM_720X576P50, */
  0x0901, 0x0f1f, 0x013a, 0x1804, 0x0901, 0x0c3d, 0x003a, 0x1a6b },
 {/* STM_720X480P60, */
  0x0901, 0x0f18, 0x013a, 0x1805, 0x0901, 0x0c3d, 0x003a, 0x1a06 },
{ /* STM_1280X720P60, */
   0x0976, 0x0c26, 0x013d, 0x0cda, 0x0104, 0x0e7e, 0x0e7f, 0x013d,
   0x0c44, 0x0104, 0x1804, 0x0976, 0x0c26, 0x003d, 0x0f0f, 0x0f10,
   0x0104, 0x1ae8 },
{ /* STM_1280X720P50, */
   0x0976, 0x0c26, 0x013d, 0x0cda, 0x0104, 0x0e7e, 0x0e7f, 0x013d,
   0x0d8e, 0x0104, 0x1804, 0x0976, 0x0c26, 0x003d, 0x0fb4, 0x0fb5,
   0x0104, 0x1ae8 },
{ /* STM_1920X1080P60, */
   0x0971, 0x0c2a, 0x013b, 0x0c56, 0x0104, 0x0fdc, 0x0fdd, 0x013b,
   0x0c2a, 0x0104, 0x1804, 0x0971, 0x0c2a, 0x003b, 0x0ebe, 0x0ebf,
   0x0ebf, 0x0104, 0x1a2f, 0x1c4b, 0x1c52},
{ /* STM_1920X1080P50, */
   0x0971, 0x0c2a, 0x013b, 0x0c56, 0x0104, 0x0fdc, 0x0fdd, 0x013b,
   0x0de2, 0x0104, 0x1804, 0x0971, 0x0c2a, 0x003b, 0x0f51, 0x0f51,
   0x0f52, 0x0104, 0x1a2f, 0x1c4b, 0x1c52 },
{ /* STM_1920X1080I60, */
   0x0576, 0x0c2a, 0x013d, 0x0c56, 0x0104, 0x0f6e, 0x013d, 0x0c2a,
   0x0104, 0x0c29, 0x1409, 0x0576, 0x0c2a, 0x013d, 0x0ff2, 0x0104,
   0x0c29, 0x1401, 0x0976, 0x0c2a, 0x003d, 0x0ebe, 0x0ebe, 0x0ec0,
   0x0104, 0x1a2b, 0x0176, 0x1c4c, 0x1c50, 0x0104, 0x1c40, 0x1c4a,
   0x0176, 0x1c4c, 0x1c4f, 0x1100, 0x0976, 0x1c53, 0x1c58, 0x1a2c },
{ /* STM_1920X1080I50, */
   0x0576, 0x0c2a, 0x013d, 0x0c56, 0x0104, 0x0f6e, 0x013d, 0x0d06,
   0x0104, 0x0c29, 0x1409, 0x0576, 0x0c2a, 0x013d, 0x0e66, 0x0e67,
   0x0104, 0x0c29, 0x1401, 0x0976, 0x0c2a, 0x003d, 0x0ebe, 0x0f9b,
   0x0f9b, 0x0104, 0x1a2b, 0x0176, 0x1c4c, 0x1c51, 0x0104, 0x1c40,
   0x1c4a, 0x0176, 0x1c4c, 0x1c51, 0x0976, 0x1c54, 0x1c59, 0x1a2c }
};

stm_reg_t hdf_cfg[STM_TIMING_MODES_NBR][23] =  {
{ /* STM_PAL, */
  {0x00, 0x01170000}, {0x04, 0x000003e0}, {0x08, 0x000003e9}, {0x0c, 0x000003e9},
  {0x10, 0x00000000}, {0x14, 0x01ed0005}, {0x18, 0x00fc827f}, {0x1c, 0x008fe20b},
  {0x20, 0x00f684fc}, {0x24, 0x050f7c24}, {0x28, 0x00f4857c}, {0x2c, 0x0a1f402e},
  {0x30, 0x00fa027f}, {0x34, 0x0e076e1d}, {0x40, 0x01ed0004}, {0x44, 0x00fc827f},
  {0x48, 0x008fe20b}, {0x4c, 0x00f684fc}, {0x50, 0x050f7c24}, {0x54, 0x00f4857c},
  {0x58, 0x0a1f402e}, {0x5c, 0x00fa027f}, {0x60, 0x0e076e1d}},
{ /* STM_NTSC, */
  {0x00, 0x01170000}, {0x04, 0x00000401}, {0x08, 0x000003e0}, {0x0c, 0x000003f8},
  {0x10, 0x00000000}, {0x14, 0x01ed0005}, {0x18, 0x00fc827f}, {0x1c, 0x008fe20b},
  {0x20, 0x00f684fc}, {0x24, 0x050f7c24}, {0x28, 0x00f4857c}, {0x2c, 0x0a1f402e},
  {0x30, 0x00fa027f}, {0x34, 0x0e076e1d}, {0x40, 0x01ed0004}, {0x44, 0x00fc827f},
  {0x48, 0x008fe20b}, {0x4c, 0x00f684fc}, {0x50, 0x050f7c24}, {0x54, 0x00f4857c},
  {0x58, 0x0a1f402e}, {0x5c, 0x00fa027f}, {0x60, 0x0e076e1d}},
{ /* STM_720X576P50, */
  {0x00, 0x01170055}, {0x04, 0x00c2026b}, {0x08, 0x07e20266}, {0x0c, 0x07e20266},
  {0x10, 0x00000000}, {0x14, 0x01ed0005}, {0x18, 0x00fc827f}, {0x1c, 0x008fe20b},
  {0x20, 0x00f684fc}, {0x24, 0x050f7c24}, {0x28, 0x00f4857c}, {0x2c, 0x0a1f402e},
  {0x30, 0x00fa027f}, {0x34, 0x0e076e1d}, {0x40, 0x01ed0004}, {0x44, 0x00fc827f},
  {0x48, 0x008fe20b}, {0x4c, 0x00f684fc}, {0x50, 0x050f7c24}, {0x54, 0x00f4857c},
  {0x58, 0x0a1f402e}, {0x5c, 0x00fa027f}, {0x60, 0x0e076e1d}},
{ /* STM_720X480P60, */
  {0x00, 0x01170015}, {0x04, 0x00c10268}, {0x08, 0x07e20266}, {0x0c, 0x07e20266},
  {0x10, 0x00000000}, {0x14, 0x01ed0005}, {0x18, 0x00fc827f}, {0x1c, 0x008fe20b},
  {0x20, 0x00f684fc}, {0x24, 0x050f7c24}, {0x28, 0x00f4857c}, {0x2c, 0x0a1f402e},
  {0x30, 0x00fa027f}, {0x34, 0x0e076e1d}, {0x40, 0x01ed0004}, {0x44, 0x00fc827f},
  {0x48, 0x008fe20b}, {0x4c, 0x00f684fc}, {0x50, 0x050f7c24}, {0x54, 0x00f4857c},
  {0x58, 0x0a1f402e}, {0x5c, 0x00fa027f}, {0x60, 0x0e076e1d}},
{ /* STM_1280X720P60, */
  {0x00, 0x01170025}, {0x04, 0x00cd026e}, {0x08, 0x07ea0260}, {0x0c, 0x07ea0260},
  {0x10, 0x00000000}, {0x14, 0x00000017}, {0x18, 0x00fe83fb}, {0x1c, 0x1f900401},
  {0x20, 0x00000000}, {0x24, 0x00000000}, {0x28, 0x00f408f9}, {0x2c, 0x055f7c25},
  {0x30, 0x00000000}, {0x34, 0x00000000}, {0x40, 0x00000004}, {0x44, 0x00fe83fb},
  {0x48, 0x1f900401}, {0x4c, 0x00000000}, {0x50, 0x00000000}, {0x54, 0x00f408f9},
  {0x58, 0x55f7c25 }, {0x5c, 0x00000000}, {0x60, 0x00000000}},
{ /* STM_1280X720P50, */
  {0x00, 0x01170025}, {0x04, 0x00cd026e}, {0x08, 0x07ea0260}, {0x0c, 0x07ea0260},
  {0x10, 0x00000000}, {0x14, 0x00000017}, {0x18, 0x00fe83fb}, {0x1c, 0x1f900401},
  {0x20, 0x00000000}, {0x24, 0x00000000}, {0x28, 0x00f408f9}, {0x2c, 0x055f7c25},
  {0x30, 0x00000000}, {0x34, 0x00000000}, {0x40, 0x00000004}, {0x44, 0x00fe83fb},
  {0x48, 0x1f900401}, {0x4c, 0x00000000}, {0x50, 0x00000000}, {0x54, 0x00f408f9},
  {0x58, 0x55f7c25 }, {0x5c, 0x00000000}, {0x60, 0x00000000}},
{ /* STM_1920X1080P60, */
  {0x00, 0x01170025}, {0x04, 0x00c50256}, {0x08, 0x0db0249}, {0x0c, 0x00db0249},
  {0x10, 0x00000000}, {0x14, 0x00000017}, {0x18, 0x0fc827f}, {0x1c, 0x008fe20b},
  {0x20, 0x00f684fc}, {0x24, 0x050f7c24}, {0x28, 0x0f4857c}, {0x2c, 0x0a1f402e},
  {0x30, 0x00fa027f}, {0x34, 0x0e076e1d}, {0x40, 0x0000004}, {0x44, 0x00fc827f},
  {0x48, 0x008fe20b}, {0x4c, 0x00f684fc}, {0x50, 0x50f7c24}, {0x54, 0x00f4857c},
  {0x58, 0x0a1f402e}, {0x5c, 0x00fa027f}, {0x60, 0xe076e1d}},
{ /* STM_1920X1080P50, */
  {0x00, 0x01170025}, {0x04, 0x00c50256}, {0x08, 0x0db0249}, {0x0c, 0x00db0249},
  {0x10, 0x00000000}, {0x14, 0x00000017}, {0x18, 0x0fc827f}, {0x1c, 0x008fe20b},
  {0x20, 0x00f684fc}, {0x24, 0x050f7c24}, {0x28, 0x0f4857c}, {0x2c, 0x0a1f402e},
  {0x30, 0x00fa027f}, {0x34, 0x0e076e1d}, {0x40, 0x0000004}, {0x44, 0x00fc827f},
  {0x48, 0x008fe20b}, {0x4c, 0x00f684fc}, {0x50, 0x50f7c24}, {0x54, 0x00f4857c},
  {0x58, 0x0a1f402e}, {0x5c, 0x00fa027f}, {0x60, 0xe076e1d}},
{ /* STM_1920X1080I60, */
  {0x00, 0x01170021}, {0x04, 0x00cd026e}, {0x08, 0x07ea0260}, {0x0c, 0x07ea0260},
  {0x10, 0x00000000}, {0x14, 0x01130000}, {0x18, 0x00fe83fb}, {0x1c, 0x1f900401},
  {0x20, 0x00000000}, {0x24, 0x00000000}, {0x28, 0x00f408f9}, {0x2c, 0x055f7c25},
  {0x30, 0x00000000}, {0x34, 0x00000000}, {0x40, 0x01130000}, {0x44, 0x00fe83fb},
  {0x48, 0x1f900401}, {0x4c, 0x00000000}, {0x50, 0x00000000}, {0x54, 0x00f408f9},
  {0x58, 0x055f7c25}, {0x5c, 0x00000000}, {0x60, 0x00000000}},
{ /* STM_1920X1080I50, */
  {0x00, 0x01170021}, {0x04, 0x00cd026e}, {0x08, 0x07ea0260}, {0x0c, 0x07ea0260},
  {0x10, 0x00000000}, {0x14, 0x01130000}, {0x18, 0x00fe83fb}, {0x1c, 0x1f900401},
  {0x20, 0x00000000}, {0x24, 0x00000000}, {0x28, 0x00f408f9}, {0x2c, 0x055f7c25},
  {0x30, 0x00000000}, {0x34, 0x00000000}, {0x40, 0x01130000}, {0x44, 0x00fe83fb},
  {0x48, 0x1f900401}, {0x4c, 0x00000000}, {0x50, 0x00000000}, {0x54, 0x00f408f9},
  {0x58, 0x055f7c25}, {0x5c, 0x00000000}, {0x60, 0x00000000}}
};

stm_display_t display[] = {
  {STM_PAL,          {720,  576 }},
  {STM_NTSC,         {720,  480 }},
  {STM_720X576P50,   {720,  576 }},
  {STM_720X480P60,   {720,  480 }},
  {STM_1280X720P60,  {1280, 720 }},
  {STM_1280X720P50,  {1280, 720 }},
  {STM_1920X1080P60, {1920, 1080}},
  {STM_1920X1080P50, {1920, 1080}},
  {STM_1920X1080I60, {1920, 1080}},
  {STM_1920X1080I50, {1920, 1080}}
};

stm_vtg_t vtg_conf[]= {
  /* Format :
   * SD_MODE_ID, { CLKCLN , HLFLN ,
   *               DENC_Top_Bot_Vrt_Hrz_Delays,
   *               0x00000000,
   *               MIX_Top_Bot_Vrt_Hrz_Delays} //Mixer Values only applicable for STiH390 family
   *
   * HD_MODE_ID, { CLKCLN , HLFLN ,
   *               HDMI_Top_Bot_Vrt_Hrz_Delays,
   *               HDF_Top_Bot_Vrt_Hrz_Delays,
   *               MIX_Top_Bot_Vrt_Hrz_Delays} //Mixer Values only applicable for STiH390 family
   */
  {STM_PAL,         {0x00000360, 0x00000271,
                     0x003b035c, 0x01391fff, 0x1fff0138, 0x01ac7fff, 0x7fff035c,
                     0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
                     0x003f0000, 0x00040001, 0x00030000, 0x00000000, 0x01b001b0}},
  {STM_NTSC,        {0x0000035a, 0x0000020d,
                     0x003d0359, 0x01071fff, 0x1fff0106, 0x01ac7fff, 0x7fff0359,
                     0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
                     0x003f0000, 0x00040001, 0x00030000, 0x00000000, 0x01b001b0}},
  {STM_720X576P50,  {0x00000360, 0x000004e2,
                     0x00450005, 0x00060001, 0x00060001, 0x00050005, 0x00050005,
#ifndef stx390
                     0x003c035c, 0x00050271, 0x00050271, 0x035d035d, 0x035d035d,
#else
                     0x003e035e, 0x00050271, 0x00050271, 0x035e035e, 0x035e035e,
#endif
                     0x00280000, 0x00060001, 0x00060001, 0x00000000, 0x00000000}},
  {STM_720X480P60,  {0x0000035a, 0x0000041a,
                     0x00440006, 0x00070001, 0x00070001, 0x00060006, 0x00060006,
#ifndef stx390
                     0x003b0357, 0x0006020d, 0x0006020d, 0x00030003, 0x00030003,
#else
                     0x003d0359, 0x0006020d, 0x0006020d, 0x00010001, 0x00010001,
#endif
                     0x00280000, 0x00060001, 0x00060001, 0x00000000, 0x00000000}},
  {STM_1280X720P60, {0x00000672, 0x000005dc,
                     0x002e0006, 0x00060001, 0x00060001, 0x00060006, 0x00060006,
#ifndef stx390
                     0x0024066e, 0x00060001, 0x00060001, 0x066e066e, 0x066e066e,
#else
                     0x00260670, 0x00060001, 0x00060001, 0x06700670, 0x06700670,
#endif
                     0x00280000, 0x00060001, 0x00060001, 0x00000000, 0x00000000}},
  {STM_1280X720P50, {0x000007bc, 0x000005dc,
#ifndef stx390
                     0x002e0006, 0x00060001, 0x00060001, 0x00060006, 0x00060006,
                     0x002607ba, 0x00060001, 0x00060001, 0x07ba07ba, 0x07ba07ba,
#else
                     0x002e0006, 0x00010006, 0x00010006, 0x00060006, 0x00060006, //Polarity inverted for h390
                     0x002707bb, 0x00060001, 0x00060001, 0x07bb07bb, 0x07bb07bb,
#endif
                     0x00280000, 0x00060001, 0x00060001, 0x00000000, 0x00000000}},
  {STM_1920X1080P60,{0x00000898, 0x000008ca,
                     0x00310005, 0x00060001, 0x00060001, 0x00050005, 0x00050005,
#ifndef stx390
                     0x00280894, 0x00060001, 0x00060001, 0x08940894, 0x08940894,
#else
                     0x002a0896, 0x00060001, 0x00060001, 0x08960896, 0x08960896,
#endif
                     0x00280000, 0x00060001, 0x00060001, 0x00000000, 0x00000000}},
  {STM_1920X1080P50,{0x00000a50, 0x000008ca,
                     0x00310005, 0x00060001, 0x00060001, 0x00050005, 0x00050005,
#ifndef stx390
                     0x00260a4c, 0x00060001, 0x00060001, 0x0a4c0a4c, 0x0a4c0a4c,
#else
                     0x002a0a4e, 0x00060001, 0x00060001, 0x0a4e0a4e, 0x0a4e0a4e,
#endif
                     0x00280000, 0x00060001, 0x00060001, 0x00000000, 0x00000000}},
  {STM_1920X1080I60,{0x00000898, 0x00000465,
                     0x00310005, 0x00060001, 0x00050000, 0x00050005, 0x04510451,
#ifndef stx390
                     0x00280894, 0x00060001, 0x00050000, 0x04480448, 0x08940894,
#else
                     0x002a0896, 0x00060001, 0x00050000, 0x044a0004, 0x08960896,
#endif
                     0x002c0000, 0x00060001, 0x00050000, 0x00000000, 0x044c044c}},
  {STM_1920X1080I50,{0x00000a50, 0x00000465,
                     0x00310005, 0x00060001, 0x00050000, 0x00050005, 0x052d052d,
#ifndef stx390
                     0x00280a4c, 0x00060001, 0x00050000, 0x05240524, 0x0a4c0a4c,
#else
                     0x002a0a4e, 0x00060001, 0x00050000, 0x05260006, 0x0a4e0a4e,
#endif
                     0x002c0000, 0x00060001, 0x00050000, 0x00000000, 0x05280528}}
};

stm_vmix_t vmix_conf[] = {
  {STM_PAL,         {0x002e0084, 0x026d0353, 0x00080084, 0x026d0353}},
  {STM_NTSC,        {0x00260077, 0x02050346, 0x00080077, 0x02050346}},
  {STM_720X576P50,  {0x002d0084, 0x026c0353, 0x00060084, 0x026c0353}},
  {STM_720X480P60,  {0x0025007a, 0x02040349, 0x0007007a, 0x02040349}},
  {STM_1280X720P60, {0x001a0104, 0x02e90603, 0x00060104, 0x02e90603}},
  {STM_1280X720P50, {0x001a0104, 0x02e90603, 0x00060104, 0x02e90603}},
  {STM_1920X1080P60,{0x002a00c0, 0x0461083f, 0x000600c0, 0x0461083f}},
  {STM_1920X1080P50,{0x002a00c0, 0x0461083f, 0x000600c0, 0x0461083f}},
  {STM_1920X1080I60,{0x002a00c0, 0x0461083f, 0x000c00c0, 0x0461083f}},
  {STM_1920X1080I50,{0x002a00c0, 0x0461083f, 0x000c00c0, 0x0461083f}}
};
//cfg = STM_HDMI_CFG_EN | STM_HDMI_CFG_HDMI_NOT_DVI | STM_HDMI_CFG_ESS_NOT_OESS | STM_HDMI_V29_422_DECIMATION_BYPASS | CUSTOM
//Main difference in HDMI_CFG : SInk supports|not deep colour + fifo_underrun_clr|not
#ifdef stx410
stm_hdmi_t hdmi_config[] = {
  {STM_PAL,{0}},
  {STM_NTSC,{0}},
  {STM_720X576P50,  {0x0085, 0x0354, 0x002d, 0x026c, 0x001f1011, 0x03022801, 0x4000305b, 0x2200, 0x000d0282, 0x80680000 + (0xff & (0x35 + RGB_CHECKSUM_OFFSET)), 0x0012}},
  {STM_720X480P60,  {0x007b, 0x034a, 0x0025, 0x0204, 0x000f0001, 0x03022801, 0x0000325b, 0x2200, 0x000d0282, 0x00580000 + (0xff & (0xd5 + RGB_CHECKSUM_OFFSET)), 0x0002}},
  {STM_1280X720P60, {0x0105, 0x0604, 0x001a, 0x02e9, 0x000f0001, 0x02042801, 0x4000320b, 0x2200, 0x000d0282, 0x00a80000 + (0xff & (0x83 + RGB_CHECKSUM_OFFSET)), 0x0004}},
  {STM_1280X720P50, {0x0105, 0x0604, 0x001a, 0x02e9, 0x000f0001, 0x02042801, 0x0000320b, 0x2200, 0x000d0282, 0x00a80000 + (0xff & (0x74 + RGB_CHECKSUM_OFFSET)), 0x0013}},
  {STM_1920X1080P60,{0x00c1, 0x0840, 0x002a, 0x0461, 0x000f0001, 0x01082801, 0x4000320b, 0x2200, 0x000d0282, 0x00a80000 + (0xff & (0x77 + RGB_CHECKSUM_OFFSET)), 0x0010}},
  {STM_1920X1080P50,{0x00c1, 0x0840, 0x002a, 0x0461, 0x000f0001, 0x01082801, 0x4000120b, 0x2200, 0x000d0282, 0x00a80000 + (0xff & (0x68 + RGB_CHECKSUM_OFFSET)), 0x001f}},
  {STM_1920X1080I60,{0x00c1, 0x0840, 0x0015, 0x0230, 0x000f0001, 0x02042801, 0x4000320b, 0x2200, 0x000d0282, 0x00a80000 + (0xff & (0x82 + RGB_CHECKSUM_OFFSET)), 0x0005}},
  {STM_1920X1080I50,{0x00c1, 0x0840, 0x0015, 0x0230, 0x000f0001, 0x02042801, 0x4000320b, 0x2200, 0x000d0282, 0x00a80000 + (0xff & (0x73 + RGB_CHECKSUM_OFFSET)), 0x0014}}
};
#elif stx418
stm_hdmi_t hdmi_config[] = {
  {STM_PAL,{0}},
  {STM_NTSC,{0}},
  {STM_720X576P50,  {0x0085, 0x0354, 0x002d, 0x026c,  0x00070001, 0x04011401, 0x0000125b, 0x2200, 0x000d0282, 0x00580000 + (0xff & (0xc6 + RGB_CHECKSUM_OFFSET)), 0x0011}},
  {STM_720X480P60,  {0x007b, 0x034a, 0x0025, 0x0204,  0x00070001, 0x04011401, 0x0000125b, 0x2200, 0x000d0282, 0x00580000 + (0xff & (0xd5 + RGB_CHECKSUM_OFFSET)), 0x0002}},
  {STM_1280X720P60, {0x0105, 0x0604, 0x001a, 0x02e9,  0x00070001, 0x03021401, 0x4000320b, 0x2200, 0x000d0282, 0x00a80000 + (0xff & (0x83 + RGB_CHECKSUM_OFFSET)), 0x0004}},
  {STM_1280X720P50, {0x0105, 0x0604, 0x001a, 0x02e9,  0x00070001, 0x03021401, 0x0000120b, 0x2200, 0x000d0282, 0x00a80000 + (0xff & (0x74 + RGB_CHECKSUM_OFFSET)), 0x0013}},
  {STM_1920X1080P60,{0x00c1, 0x0840, 0x002a, 0x0461,  0x00071111, 0x02041401, 0x4000120b, 0x2200, 0x000d0282, 0x00a80000 + (0xff & (0x77 + RGB_CHECKSUM_OFFSET)), 0x0010}},
  {STM_1920X1080P50,{0x00c1, 0x0840, 0x002a, 0x0461,  0x00071111, 0x02041401, 0x4000120b, 0x2200, 0x000d0282, 0x00a80000 + (0xff & (0x68 + RGB_CHECKSUM_OFFSET)), 0x001f}},
  {STM_1920X1080I60,{0x00c1, 0x0840, 0x0015, 0x0230,  0x00070001, 0x03021401, 0x4000320b, 0x2200, 0x000d0282, 0x00a80000 + (0xff & (0x82 + RGB_CHECKSUM_OFFSET)), 0x0005}},
  {STM_1920X1080I50,{0x00c1, 0x0840, 0x0015, 0x0230,  0x00070001, 0x03021401, 0x4000320b, 0x2200, 0x000d0282, 0x00a80000 + (0xff & (0x73 + RGB_CHECKSUM_OFFSET)), 0x0014}}
};
#elif stx390
stm_hdmi_t hdmi_config[] = {
  {STM_PAL,{0}},
  {STM_NTSC,{0}},
  {STM_720X576P50,  {0x0085, 0x0354, 0x002d, 0x026c,  0x00070001, 0x03022801, 0x0000125b, 0x2200, 0x000d0282, 0x00580000 + (0xff & (0xc6 + RGB_CHECKSUM_OFFSET)), 0x0011}},
  {STM_720X480P60,  {0x007b, 0x034a, 0x0025, 0x0204,  0x00070001, 0x03022801, 0x0000125b, 0x2200, 0x000d0282, 0x00580000 + (0xff & (0xd5 + RGB_CHECKSUM_OFFSET)), 0x0002}},
  {STM_1280X720P60, {0x0105, 0x0604, 0x001a, 0x02e9,  0x00070001, 0x02042801, 0x4000320b, 0x2200, 0x000d0282, 0x00a80000 + (0xff & (0x83 + RGB_CHECKSUM_OFFSET)), 0x0004}},
  {STM_1280X720P50, {0x0105, 0x0604, 0x001a, 0x02e9,  0x00070001, 0x02042801, 0x0000120b, 0x2200, 0x000d0282, 0x00a80000 + (0xff & (0x74 + RGB_CHECKSUM_OFFSET)), 0x0013}},
  {STM_1920X1080P60,{0x00c1, 0x0840, 0x002a, 0x0461,  0x00071111, 0x01082801, 0x4000120b, 0x2200, 0x000d0282, 0x00a80000 + (0xff & (0x77 + RGB_CHECKSUM_OFFSET)), 0x0010}},
  {STM_1920X1080P50,{0x00c1, 0x0840, 0x002a, 0x0461,  0x00071111, 0x01082801, 0x4000120b, 0x2200, 0x000d0282, 0x00a80000 + (0xff & (0x68 + RGB_CHECKSUM_OFFSET)), 0x001f}},
  {STM_1920X1080I60,{0x00c1, 0x0840, 0x0015, 0x0230,  0x00070001, 0x02042801, 0x4000320b, 0x2200, 0x000d0282, 0x00a80000 + (0xff & (0x82 + RGB_CHECKSUM_OFFSET)), 0x0005}},
  {STM_1920X1080I50,{0x00c1, 0x0840, 0x0015, 0x0230,  0x00070001, 0x02042801, 0x4000320b, 0x2200, 0x000d0282, 0x00a80000 + (0xff & (0x73 + RGB_CHECKSUM_OFFSET)), 0x0014}}
};
#endif

/*********************************************************/
/* Private function                                      */
/*********************************************************/
static int Clock(splash_modes_t main_mode, splash_modes_t aux_mode, int enable)
{
  stm_clock_t clock_cfg;

  if(!enable)
    return clk_disable_clocks();

  if(clk_get_clock_configuration(main_mode, &clock_cfg) < 0)
    return -1;

  if(clk_set_clocks(&clock_cfg) < 0)
    return -2;

  splash_debug("Clock init done.\n");
  return 0;
}

#ifdef stx337
#define HDMI_SYNC_SEL   3
#define HDF_SYNC_SEL    6
#define DENC_SYNC_SEL   7
#define MIX_SYNC_SEL    1
#elif  stx390
#define HDMI_SYNC_SEL   4
#define HDF_SYNC_SEL   10
#define DENC_SYNC_SEL  12
#define MIX_SYNC_SEL   1
#else
#define HDMI_SYNC_SEL   5
#define HDF_SYNC_SEL    3
#define DENC_SYNC_SEL   1
#endif

#define CHAN_BASE(y, x)    ((y) + 0xC0 + (x - 1) * 0x20)

#define HDMI_CHAN          CHAN_BASE( VTG_MAIN_BASE, HDMI_SYNC_SEL)
#define HDF_CHAN           CHAN_BASE( VTG_MAIN_BASE, HDF_SYNC_SEL)
#define MAIN_MIX_CHAN      CHAN_BASE( VTG_MAIN_BASE, MIX_SYNC_SEL)

#define DENC_CHAN          CHAN_BASE( VTG_AUX_BASE, DENC_SYNC_SEL)
#define AUX_MIX_CHAN       CHAN_BASE( VTG_AUX_BASE, MIX_SYNC_SEL)

#define VTG_HOST_ITS(x)            ((x) + 0x078)
#define VTG_HOST_ITS_BCLR(x)       ((x) + 0x07c)
#define VTG_HOST_ITM_BCLR(x)       ((x) + 0x088)
#define VTG_MODE(x)                ((x) + 0x0)
#define VTG_INV_BNOTTOP(x)         ((x) + 0x4)
#define VTG_CLKLN(x)               ((x) + 0x8)
#define VTG_HLFLN(x)               ((x) + 0xc)
#define VTGn_DRST(x)               ((x) + 0x10)

#define VTG_HOST_ITM_BSET(x)       ((x) + 0x8c)

#define VTGn_H_HD(x)            (x)
#define VTGn_TOP_V_VD(x)        ((x)+ 0x4)
#define VTGn_BOT_V_VD(x)        ((x)+ 0x8)
#define VTGn_TOP_V_HD(x)        ((x)+ 0xc)
#define VTGn_BOT_V_HD(x)        ((x)+ 0x10)

static void VTG_Main(splash_modes_t mode, int enable)
{
  main_mode = mode;

  STSYS_WriteRegDev32LE( VTG_MODE(VTG_MAIN_BASE), 0x200);
  STSYS_WriteRegDev32LE( VTG_HOST_ITM_BCLR(VTG_MAIN_BASE), 0xFF);
  STSYS_WriteRegDev32LE( VTG_HOST_ITS_BCLR(VTG_MAIN_BASE), 0xFF);

  if(enable)
  {
    STSYS_WriteRegDev32LE( VTG_HOST_ITM_BSET(VTG_MAIN_BASE), 0x03);

    if (mode == STM_1920X1080I60 || mode == STM_1920X1080I50)
    {
      STSYS_WriteRegDev32LE( VTG_INV_BNOTTOP(VTG_MAIN_BASE), 0x24);
    }
    STSYS_WriteRegDev32LE( VTG_CLKLN(VTG_MAIN_BASE), vtg_conf[mode].vtg.vtg0);
    STSYS_WriteRegDev32LE( VTG_HLFLN(VTG_MAIN_BASE), vtg_conf[mode].vtg.vtg1);

    STSYS_WriteRegDev32LE( VTGn_H_HD(HDMI_CHAN),     vtg_conf[mode].vtg.vtg2);
    STSYS_WriteRegDev32LE( VTGn_TOP_V_VD(HDMI_CHAN), vtg_conf[mode].vtg.vtg3);
    STSYS_WriteRegDev32LE( VTGn_BOT_V_VD(HDMI_CHAN), vtg_conf[mode].vtg.vtg4);
    STSYS_WriteRegDev32LE( VTGn_TOP_V_HD(HDMI_CHAN), vtg_conf[mode].vtg.vtg5);
    STSYS_WriteRegDev32LE( VTGn_BOT_V_HD(HDMI_CHAN), vtg_conf[mode].vtg.vtg6);

    STSYS_WriteRegDev32LE( VTGn_H_HD(HDF_CHAN),     vtg_conf[mode].vtg.vtg7);
    STSYS_WriteRegDev32LE( VTGn_TOP_V_VD(HDF_CHAN), vtg_conf[mode].vtg.vtg8);
    STSYS_WriteRegDev32LE( VTGn_BOT_V_VD(HDF_CHAN), vtg_conf[mode].vtg.vtg9);
    STSYS_WriteRegDev32LE( VTGn_TOP_V_HD(HDF_CHAN), vtg_conf[mode].vtg.vtg10);
    STSYS_WriteRegDev32LE( VTGn_BOT_V_HD(HDF_CHAN), vtg_conf[mode].vtg.vtg11);
#ifdef stx390
    STSYS_WriteRegDev32LE( VTGn_H_HD(MAIN_MIX_CHAN),     vtg_conf[mode].vtg.vtg12);
    STSYS_WriteRegDev32LE( VTGn_TOP_V_VD(MAIN_MIX_CHAN), vtg_conf[mode].vtg.vtg13);
    STSYS_WriteRegDev32LE( VTGn_BOT_V_VD(MAIN_MIX_CHAN), vtg_conf[mode].vtg.vtg14);
    STSYS_WriteRegDev32LE( VTGn_TOP_V_HD(MAIN_MIX_CHAN), vtg_conf[mode].vtg.vtg15);
    STSYS_WriteRegDev32LE( VTGn_BOT_V_HD(MAIN_MIX_CHAN), vtg_conf[mode].vtg.vtg16);
#endif
    STSYS_WriteRegDev32LE( VTG_MODE(VTG_MAIN_BASE), 0x0);
    STSYS_WriteRegDev32LE( VTGn_DRST(VTG_MAIN_BASE), 0x1);
  }
}

static void VTG_Aux(splash_modes_t mode, int enable)
{
  aux_mode = mode;

  STSYS_WriteRegDev32LE( VTG_MODE(VTG_AUX_BASE), 0x200);
  STSYS_WriteRegDev32LE( VTG_HOST_ITM_BCLR(VTG_AUX_BASE), 0xFF);
  STSYS_WriteRegDev32LE( VTG_HOST_ITS_BCLR(VTG_AUX_BASE), 0xFF);

  if(enable)
  {
    STSYS_WriteRegDev32LE( VTG_HOST_ITM_BSET(VTG_AUX_BASE), 0x03);

    STSYS_WriteRegDev32LE( VTG_INV_BNOTTOP(VTG_AUX_BASE), 0x0);
    STSYS_WriteRegDev32LE( VTG_CLKLN(VTG_AUX_BASE), vtg_conf[mode].vtg.vtg0);
    STSYS_WriteRegDev32LE( VTG_HLFLN(VTG_AUX_BASE), vtg_conf[mode].vtg.vtg1);

    STSYS_WriteRegDev32LE( VTGn_H_HD(DENC_CHAN),     vtg_conf[mode].vtg.vtg2);
    STSYS_WriteRegDev32LE( VTGn_TOP_V_VD(DENC_CHAN), vtg_conf[mode].vtg.vtg3);
    STSYS_WriteRegDev32LE( VTGn_BOT_V_VD(DENC_CHAN), vtg_conf[mode].vtg.vtg4);
    STSYS_WriteRegDev32LE( VTGn_TOP_V_HD(DENC_CHAN), vtg_conf[mode].vtg.vtg5);
    STSYS_WriteRegDev32LE( VTGn_BOT_V_HD(DENC_CHAN), vtg_conf[mode].vtg.vtg6);
#ifdef stx390
    STSYS_WriteRegDev32LE( VTGn_H_HD(AUX_MIX_CHAN),     vtg_conf[mode].vtg.vtg12);
    STSYS_WriteRegDev32LE( VTGn_TOP_V_VD(AUX_MIX_CHAN), vtg_conf[mode].vtg.vtg13);
    STSYS_WriteRegDev32LE( VTGn_BOT_V_VD(AUX_MIX_CHAN), vtg_conf[mode].vtg.vtg14);
    STSYS_WriteRegDev32LE( VTGn_TOP_V_HD(AUX_MIX_CHAN), vtg_conf[mode].vtg.vtg15);
    STSYS_WriteRegDev32LE( VTGn_BOT_V_HD(AUX_MIX_CHAN), vtg_conf[mode].vtg.vtg16);
#endif
    STSYS_WriteRegDev32LE( VTG_MODE(VTG_AUX_BASE), 0x0);
    STSYS_WriteRegDev32LE( VTGn_DRST(VTG_AUX_BASE), 0x1);
  }
}

static void VTG_WaitForVsync(const int vtg_base, const int event_mask, const int vsync_count)
{
  int i = 0;

  /* Wait for Vsync events */
  for(i = 0; i < vsync_count; i++)
  {
    /* Clear current interrupr status */
    STSYS_WriteRegDev32LE( VTG_HOST_ITS_BCLR(vtg_base), 0xFF);

    while (!(STSYS_ReadRegDev32LE(VTG_HOST_ITS(vtg_base)) & event_mask))
    {
      splash_debug("Waiting for %d Wsync on %s VTG\n", (vsync_count-i), (vtg_base==VTG_MAIN_BASE ? "Main" : "Aux"));
    }
  }
}

static void DENC_576i(void) //PAL
{
  /* DENC CFG */
  STSYS_WriteRegDev8(DENC_BASE + 0x0,   0x16);  //DENC_CFG0 //PAL
  STSYS_WriteRegDev8(DENC_BASE + 0x4,   0x60);  //DENC_CFG1
  STSYS_WriteRegDev8(DENC_BASE + 0x8,   0x28);  //DENC_CFG2
  STSYS_WriteRegDev8(DENC_BASE + 0xc,   0x8);   //DENC_CFG3 Enable of chroma to luma delay programming
  STSYS_WriteRegDev8(DENC_BASE + 0x10,  0x8);   //DENC_CFG4
  STSYS_WriteRegDev8(DENC_BASE + 0x14,  0x0);   //DENC_CFG5
  STSYS_WriteRegDev8(DENC_BASE + 0x18,  0x10);  //DENC_CFG6
  STSYS_WriteRegDev8(DENC_BASE + 0x1c,  0x0);   //DENC_CFG7
  STSYS_WriteRegDev8(DENC_BASE + 0x20,  0x24);  //DENC_CFG8
  STSYS_WriteRegDev8(DENC_BASE + 0x144, 0x35);  //DENC_CFG9 -1pix delay , Luma Filter threshold 1024
  STSYS_WriteRegDev8(DENC_BASE + 0x170, 0x8);   //DENC_CFG10
  STSYS_WriteRegDev8(DENC_BASE + 0x174, 0x38);  //DENC_CFG11
  STSYS_WriteRegDev8(DENC_BASE + 0x178, 0x0);   //DENC_CFG12
  STSYS_WriteRegDev8(DENC_BASE + 0x17c, 0x3f);  //DENC_CFG13 DENC<->DAC output configuration, -,-,-,C,Y,CVBS

  /* Digital frequency synthesizer */
  /* Follwing settings need to be refined when needed
  STSYS_WriteRegDev8(DENC_BASE + 0x28,  0x0);   //DENC_DFS_INC0
  STSYS_WriteRegDev8(DENC_BASE + 0x2c,  0xb8);  //DENC_DFS_INC1
  STSYS_WriteRegDev8(DENC_BASE + 0x30,  0x2);   //DENC_DFS_INC2
  STSYS_WriteRegDev8(DENC_BASE + 0x34,  0x95);  //DENC_DFS_PHASE0
  STSYS_WriteRegDev8(DENC_BASE + 0x38,  0x68);  //DENC_DFS_PHASE1
  */

  /* DAC inputs */ //TODO : Check if DAC updates has an effect here
  STSYS_WriteRegDev8(DENC_BASE + 0x44,  0x82);  //DENC_DAC1_3_MULT
  STSYS_WriteRegDev8(DENC_BASE + 0x48,  0x5);   //DENC_DAC3_4_MULT
  STSYS_WriteRegDev8(DENC_BASE + 0x4c,  0x55);  //DENC_DAC4_5_MULT
  STSYS_WriteRegDev8(DENC_BASE + 0x1a8, 0x20);  //DENC_DAC2_MULT
  STSYS_WriteRegDev8(DENC_BASE + 0x1ac, 0x15);  //DENC_DAC6_MULT

  /* Input demultiplexor */
  STSYS_WriteRegDev8(DENC_BASE + 0x114, 0x80);  //DENC_BRIGHT
  STSYS_WriteRegDev8(DENC_BASE + 0x118, 0x0);   //DENC_CONTRAST
  STSYS_WriteRegDev8(DENC_BASE + 0x11c, 0x80);  //DENC_SATURATION

  /* Chroma coefficient */
  STSYS_WriteRegDev8(DENC_BASE + 0x120, 0x20);  //DENC_CF_COEF0
  STSYS_WriteRegDev8(DENC_BASE + 0x124, 0x27);  //DENC_CF_COEF1
  STSYS_WriteRegDev8(DENC_BASE + 0x128, 0x54);  //DENC_CF_COEF2
  STSYS_WriteRegDev8(DENC_BASE + 0x12c, 0x47);  //DENC_CF_COEF3
  STSYS_WriteRegDev8(DENC_BASE + 0x130, 0x5f);  //DENC_CF_COEF4
  STSYS_WriteRegDev8(DENC_BASE + 0x134, 0x77);  //DENC_CF_COEF5
  STSYS_WriteRegDev8(DENC_BASE + 0x138, 0x6c);  //DENC_CF_COEF6
  STSYS_WriteRegDev8(DENC_BASE + 0x13c, 0x7b);  //DENC_CF_COEF7
  STSYS_WriteRegDev8(DENC_BASE + 0x140, 0x80);  //DENC_CF_COEF8

  /* Luma coefficient */
  STSYS_WriteRegDev8(DENC_BASE + 0x148, 0x21);  //DENC_LU_COEF0
  STSYS_WriteRegDev8(DENC_BASE + 0x14c, 0x7f);  //DENC_LU_COEF1
  STSYS_WriteRegDev8(DENC_BASE + 0x150, 0xf7);  //DENC_LU_COEF2
  STSYS_WriteRegDev8(DENC_BASE + 0x154, 0x3);   //DENC_LU_COEF3
  STSYS_WriteRegDev8(DENC_BASE + 0x158, 0x1f);  //DENC_LU_COEF4
  STSYS_WriteRegDev8(DENC_BASE + 0x15c, 0xfb);  //DENC_LU_COEF5
  STSYS_WriteRegDev8(DENC_BASE + 0x160, 0xac);  //DENC_LU_COEF6
  STSYS_WriteRegDev8(DENC_BASE + 0x164, 0x7);   //DENC_LU_COEF7
  STSYS_WriteRegDev8(DENC_BASE + 0x168, 0x3d);  //DENC_LU_COEF8
  STSYS_WriteRegDev8(DENC_BASE + 0x16c, 0xf8);  //DENC_LU_COEF9

  /* Software Reset */
  STSYS_WriteRegDev8(DENC_BASE + 0x18,  0x80);   //DENC_CFG6

}

static void DENC_480i(void) //NTSC
{
  /* DENC CFG */
  STSYS_WriteRegDev8(DENC_BASE + 0x0,   0x96);  //DENC_CFG0 //NTSC
  STSYS_WriteRegDev8(DENC_BASE + 0x4,   0x40);  //DENC_CFG1
  STSYS_WriteRegDev8(DENC_BASE + 0x8,   0x28);  //DENC_CFG2
  STSYS_WriteRegDev8(DENC_BASE + 0xc,   0x8);   //DENC_CFG3
  STSYS_WriteRegDev8(DENC_BASE + 0x10,  0x8);   //DENC_CFG4
  STSYS_WriteRegDev8(DENC_BASE + 0x14,  0x0);   //DENC_CFG5
  STSYS_WriteRegDev8(DENC_BASE + 0x18,  0x10);  //DENC_CFG6
  STSYS_WriteRegDev8(DENC_BASE + 0x1c,  0x0);   //DENC_CFG7
  STSYS_WriteRegDev8(DENC_BASE + 0x20,  0x24);  //DENC_CFG8
  STSYS_WriteRegDev8(DENC_BASE + 0x144, 0x35);  //DENC_CFG9
  STSYS_WriteRegDev8(DENC_BASE + 0x170, 0x8);   //DENC_CFG10
  STSYS_WriteRegDev8(DENC_BASE + 0x174, 0x38);  //DENC_CFG11
  STSYS_WriteRegDev8(DENC_BASE + 0x178, 0x0);   //DENC_CFG12
  STSYS_WriteRegDev8(DENC_BASE + 0x17c, 0x3f);  //DENC_CFG13

  /* Digital frequency synthesizer */
  /* Follwing settings need to be refined when needed
  STSYS_WriteRegDev8(DENC_BASE + 0x28,  0x0);   //DENC_DFS_INC0
  STSYS_WriteRegDev8(DENC_BASE + 0x2c,  0xb8);  //DENC_DFS_INC1
  STSYS_WriteRegDev8(DENC_BASE + 0x30,  0x2);   //DENC_DFS_INC2
  STSYS_WriteRegDev8(DENC_BASE + 0x34,  0x13);  //DENC_DFS_PHASE0
  STSYS_WriteRegDev8(DENC_BASE + 0x38,  0x82);  //DENC_DFS_PHASE1
  */

  /* DAC inputs */
  STSYS_WriteRegDev8(DENC_BASE + 0x44,  0x82);  //DENC_DAC1_3_MULT
  STSYS_WriteRegDev8(DENC_BASE + 0x48,  0x8);   //DENC_DAC3_4MULT
  STSYS_WriteRegDev8(DENC_BASE + 0x4c,  0xa2);  //DENC_DAC4_5_MULT
  STSYS_WriteRegDev8(DENC_BASE + 0x1a8, 0x20);  //DENC_DAC2_MULT
  STSYS_WriteRegDev8(DENC_BASE + 0x1ac, 0x22);  //DENC_DAC6_MULT

  /* Input demultiplexor */
  STSYS_WriteRegDev8(DENC_BASE + 0x114, 0x80);  //DENC_BRIGHT
  STSYS_WriteRegDev8(DENC_BASE + 0x118, 0x0);   //DENC_CONTRAST
  STSYS_WriteRegDev8(DENC_BASE + 0x11c, 0x80);  //DENC_SATURATION

  /* Chroma coefficient */
  STSYS_WriteRegDev8(DENC_BASE + 0x120, 0x9a);  //DENC_CF_COEF0
  STSYS_WriteRegDev8(DENC_BASE + 0x124, 0x26);
  STSYS_WriteRegDev8(DENC_BASE + 0x128, 0x37);
  STSYS_WriteRegDev8(DENC_BASE + 0x12c, 0x7);
  STSYS_WriteRegDev8(DENC_BASE + 0x130, 0x9);
  STSYS_WriteRegDev8(DENC_BASE + 0x134, 0x2d);
  STSYS_WriteRegDev8(DENC_BASE + 0x138, 0x49);
  STSYS_WriteRegDev8(DENC_BASE + 0x13c, 0x86);
  STSYS_WriteRegDev8(DENC_BASE + 0x140, 0x9a);

  /* Luma coefficient */
  STSYS_WriteRegDev8(DENC_BASE + 0x148, 0x3d);  //DENC_LU_COEF0
  STSYS_WriteRegDev8(DENC_BASE + 0x14c, 0xbc);
  STSYS_WriteRegDev8(DENC_BASE + 0x150, 0xf9);
  STSYS_WriteRegDev8(DENC_BASE + 0x154, 0xff);
  STSYS_WriteRegDev8(DENC_BASE + 0x158, 0x23);
  STSYS_WriteRegDev8(DENC_BASE + 0x15c, 0x5);
  STSYS_WriteRegDev8(DENC_BASE + 0x160, 0xa0);
  STSYS_WriteRegDev8(DENC_BASE + 0x164, 0xf1);
  STSYS_WriteRegDev8(DENC_BASE + 0x168, 0x47);
  STSYS_WriteRegDev8(DENC_BASE + 0x16c, 0x1e);

  /* Software Reset */
  STSYS_WriteRegDev8(DENC_BASE + 0x18,  0x80);   //DENC_CFG6
}

static void DENC(splash_modes_t mode, int enable)
{
  int val = 0;
  val = STSYS_ReadRegDev32LE(SYSCFG_CORE + SYS_CFG_DAC_CTRL);
  if(enable)
  {
    if (mode == STM_NTSC)
    {
      DENC_480i();
    }
    else
    {
      if(mode == STM_PAL)
      DENC_576i();
    }
    STSYS_WriteRegDev32LE(SYSCFG_CORE + SYS_CFG_DAC_CTRL, val & (~1));
#ifdef stx390 //Recommendation from VALID is to set both Registers to ON to enable DACs
  val = STSYS_ReadRegDev32LE(STiH390_TVOUT1_BASE + TVO_QUAD_DAC_CONFIG);
  STSYS_WriteRegDev32LE( STiH390_TVOUT1_BASE + TVO_QUAD_DAC_CONFIG, val & (~(TVO_POFF_DACS | TVO_QUAD_DAC_HZX)));
#endif
  }
  else
  {
    STSYS_WriteRegDev32LE(SYSCFG_CORE + SYS_CFG_DAC_CTRL, val | 1); //According to Valid team this should be sufficient for CannesWifi
                                                                    //In case of a problem, we can check if we also need to use TVO_QUAD_DAC_CONFIG
  }
}

#ifdef STM_HD_COMPONENT
static void AWG_SYNC(splash_modes_t mode)
{
  int i = 0;
  for (i = 0; i < 64; i++)
  {
    STSYS_WriteRegDev32LE( AWG_SYNC_BASE + 4*i, awg_ram[mode][i]);
  }
}
#endif

static void HD_Formatter(int enable, splash_modes_t mode)
{
  int i = 0;
  int val = STSYS_ReadRegDev32LE(SYSCFG_CORE + SYS_CFG_DAC_CTRL);

  if(enable)
  {
#ifdef STM_HD_COMPONENT
    AWG_SYNC(mode);
#endif
    for (i=0; i < 23; i++)
    {
      STSYS_WriteRegDev32LE( HD_FORMATTER_BASE + hdf_cfg[mode][i].offset, hdf_cfg[mode][i].value);
    }

    STSYS_WriteRegDev32LE(SYSCFG_CORE + SYS_CFG_DAC_CTRL, val & (~2));  //
#ifdef stx390 //Recommendation from VALID is to set both Registers to ON to enable DACs
  val = STSYS_ReadRegDev32LE(STiH390_TVOUT1_BASE + TVO_QUAD_DAC_CONFIG);
  STSYS_WriteRegDev32LE( STiH390_TVOUT1_BASE + TVO_QUAD_DAC_CONFIG, val & (~(TVO_POFF_DACS | TVO_QUAD_DAC_HZUVW)));
#endif

  }
  else
  {
    STSYS_WriteRegDev32LE( HD_FORMATTER_BASE + 0x0, 0x0);
    STSYS_WriteRegDev32LE(SYSCFG_CORE + SYS_CFG_DAC_CTRL, val | 2);//According to Valid team this should be sufficient for CannesWifi
                                                                    //In case of a problem, we can check if we also need to use TVO_QUAD_DAC_CONFIG
  }
}

static inline void TVOUT_PF_MAIN(void)
{
  /* TVO_MAIN_PF_BASE */ /* Conversion Matrix */
  STSYS_WriteRegDev32LE(TVO_MAIN_PF_BASE + 0x0,  0xf927082e);  //TVO_CSC_PF_MAIN_M0
  STSYS_WriteRegDev32LE(TVO_MAIN_PF_BASE + 0x4,  0x4c9feab);   //TVO_CSC_PF_MAIN_M1
  STSYS_WriteRegDev32LE(TVO_MAIN_PF_BASE + 0x8,  0x1d30964);   //TVO_CSC_PF_MAIN_M2
  STSYS_WriteRegDev32LE(TVO_MAIN_PF_BASE + 0xc,  0xfa95fd3d);  //TVO_CSC_PF_MAIN_M3
  STSYS_WriteRegDev32LE(TVO_MAIN_PF_BASE + 0x10, 0x82e);       //TVO_CSC_PF_MAIN_M4
  STSYS_WriteRegDev32LE(TVO_MAIN_PF_BASE + 0x14, 0x2000);      //TVO_CSC_PF_MAIN_M5
  STSYS_WriteRegDev32LE(TVO_MAIN_PF_BASE + 0x18, 0x2000);      //TVO_CSC_PF_MAIN_M6
  STSYS_WriteRegDev32LE(TVO_MAIN_PF_BASE + 0x1c, 0x0);         //TVO_CSC_PF_MAIN_M7

  STSYS_WriteRegDev32LE(TVO_MAIN_PF_BASE + 0x20, 0x0);         //TVO_PF_MAIN_AD_DCMTN_FLTR_CFG
  STSYS_WriteRegDev32LE(TVO_MAIN_PF_BASE + 0x30, 0x1);         //TVO_PF_MAIN_IN_VID_FORMAT
  // TODO : Don't we need to use the TVO_COMPO_MAIN_MIXER_SYNC_SEL for CannesWifi??
}

static inline void TVOUT_VIP_HDMI(void)
{
  /* TVO_VIP_HDMI_BASE */
  STSYS_WriteRegDev32LE(TVO_VIP_HDMI_BASE + 0x0,  0x2010200 | HDMI_YCBCR_LIMITED_RANGE | HDMI_YCBCR_CONV_ENABLE);  //TVO_VIP_HDMI
#ifdef stx390
  STSYS_WriteRegDev32LE(TVO_VIP_HDMI_BASE + 0x18, 0x0);        //TVO_HDMI_SYNC_SEL  : HDMI -- Main
#else
  STSYS_WriteRegDev32LE(TVO_VIP_HDMI_BASE + 0x18, 0x5);        //TVO_HDMI_SYNC_SEL  : HDMI -- Main_5
#endif
}

static inline void TVOUT_VIP_HDF(void)
{
  /* TVO_VIP_HDF_BASE */
#ifdef STM_HD_COMPONENT
  STSYS_WriteRegDev32LE(TVO_VIP_HDF_BASE + 0x0,  0x2010311);   //TVO_VIP_HDF   : main_pf_converted --> hdf
#ifdef stx390
  STSYS_WriteRegDev32LE(TVO_VIP_HDF_BASE + 0x18, 0x0);       //TVO_HD_SYNC_SEL (HDF -- MAIN, HDDCS -- MAIN, VGA -- MAIN, no vga delay)
#else
  STSYS_WriteRegDev32LE(TVO_VIP_HDF_BASE + 0x18, 0x403);       //TVO_HD_SYNC_SEL (HDF -- MAIN_3, HDDCS -- MAIN_4)
#endif
  STSYS_WriteRegDev32LE(TVO_VIP_HDF_BASE + 0x20, 0x0);         //TVO_HD_DAC_CONFIG
#else
  STSYS_WriteRegDev32LE(TVO_VIP_HDF_BASE + 0x0,  0x102001d);   //TVO_VIP_HDF   : dac123--> hdf
#ifdef stx390
  STSYS_WriteRegDev32LE(TVO_VIP_HDF_BASE + 0x18, 0x10101);      //TVO_HD_SYNC_SEL (HDF --AUX, HDDCS -- AUX, VGA -- AUX, no vga delay)
#else
  STSYS_WriteRegDev32LE(TVO_VIP_HDF_BASE + 0x18, 0x1413);      //TVO_HD_SYNC_SEL (HDF --AUX_3, HDDCS -- AUX_4)
#endif
#endif
}

static inline void TVOUT_MAIN(void)
{
  TVOUT_PF_MAIN();
  TVOUT_VIP_HDF();
  TVOUT_VIP_HDMI();
}

static inline void TVOUT_PF_AUX(void)
{
  /*TVO_AUX_PF_BASE */ /* Conversion Matrix */
  STSYS_WriteRegDev32LE(TVO_AUX_PF_BASE + 0x0, 0xf927082e);   //TVO_CSC_PF_AUX_M0
  STSYS_WriteRegDev32LE(TVO_AUX_PF_BASE + 0x4, 0x4c9feab);    //TVO_CSC_PF_AUX_M1
  STSYS_WriteRegDev32LE(TVO_AUX_PF_BASE + 0x8, 0x1d30964);    //TVO_CSC_PF_AUX_M2
  STSYS_WriteRegDev32LE(TVO_AUX_PF_BASE + 0xc, 0xfa95fd3d);   //TVO_CSC_PF_AUX_M3
  STSYS_WriteRegDev32LE(TVO_AUX_PF_BASE + 0x10, 0x82e);       //TVO_CSC_PF_AUX_M4
  STSYS_WriteRegDev32LE(TVO_AUX_PF_BASE + 0x14, 0x2000);      //TVO_CSC_PF_AUX_M5
  STSYS_WriteRegDev32LE(TVO_AUX_PF_BASE + 0x18, 0x2000);      //TVO_CSC_PF_AUX_M6
  STSYS_WriteRegDev32LE(TVO_AUX_PF_BASE + 0x1c, 0x0);         //TVO_CSC_PF_AUX_M7

  STSYS_WriteRegDev32LE(TVO_AUX_PF_BASE + 0x30, 0x1);         //TVO_PF_AUX_IN_VID_FORMAT
  // TODO : Don't we need to use the TVO_COMPO_AUX_MIXER_SYNC_SEL for CannesWifi??
}

static inline void TVOUT_VIP_DENC(void)
{
  /* TVO_VIP_DENC_BASE */
  STSYS_WriteRegDev32LE(TVO_VIP_DENC_BASE + 0x0,  0x2010009);  //TVO_VIP_DENC
#ifdef stx390
  STSYS_WriteRegDev32LE(TVO_VIP_DENC_BASE + 0x18, 0x01000101); //TVO_SD_SYNC_SEL  (TTX -- AUX , DENC -- AUX, DCS -- AUX )
#else
  STSYS_WriteRegDev32LE(TVO_VIP_DENC_BASE + 0x18, 0x10101211); //TVO_SD_SYNC_SEL  (TTX -- AUX_REF, DENC -- AUX_1, DCS -- AUX_2 )
#endif
  STSYS_WriteRegDev32LE(TVO_VIP_DENC_BASE + 0x20, 0x10);       //TVO_SD_DAC_CONFIG : PowerOn DAC, dac456 --> SD DACS , no delay
}

static inline void TVOUT_SD_COEFF(void)
{
  /* TVO_TTXT_BASE */ /* HDF Upsampling configuration */
  STSYS_WriteRegDev32LE(TVO_TTXT_BASE + 0x40,  0x1ed0005);    //TVO_SD_SRC_Y_CFG
  STSYS_WriteRegDev32LE(TVO_TTXT_BASE + 0x44,  0xfc827f);     //TVO_SD_COEFF_Y_PHASE1_TAP123
  STSYS_WriteRegDev32LE(TVO_TTXT_BASE + 0x48,  0x8fe20b);     //TVO_SD_COEFF_Y_PHASE1_TAP456
  STSYS_WriteRegDev32LE(TVO_TTXT_BASE + 0x4c,  0xf684fc);     //TVO_SD_COEFF_Y_PHASE2_TAP123
  STSYS_WriteRegDev32LE(TVO_TTXT_BASE + 0x50,  0x50f7c24);    //TVO_SD_COEFF_Y_PHASE2_TAP456
  STSYS_WriteRegDev32LE(TVO_TTXT_BASE + 0x54,  0xf4857c);     //TVO_SD_COEFF_Y_PHASE3_TAP123
  STSYS_WriteRegDev32LE(TVO_TTXT_BASE + 0x58,  0xa1f402e);    //TVO_SD_COEFF_Y_PHASE3_TAP456
  STSYS_WriteRegDev32LE(TVO_TTXT_BASE + 0x5c,  0xfa027f);     //TVO_SD_COEFF_Y_PHASE4_TAP123
  STSYS_WriteRegDev32LE(TVO_TTXT_BASE + 0x60,  0xe076e1d);    //TVO_SD_COEFF_Y_PHASE4_TAP456

  STSYS_WriteRegDev32LE(TVO_TTXT_BASE + 0x64,  0x1ed0004);    //TVO_SD_ANA_SRC_C_CFG
  STSYS_WriteRegDev32LE(TVO_TTXT_BASE + 0x68,  0xfc827f);     //TVO_SD_COEFF_C_PHASE1_TAP123
  STSYS_WriteRegDev32LE(TVO_TTXT_BASE + 0x6c,  0x8fe20b);     //TVO_SD_COEFF_C_PHASE1_TAP456
  STSYS_WriteRegDev32LE(TVO_TTXT_BASE + 0x70,  0xf684fc);     //TVO_SD_COEFF_C_PHASE2_TAP123
  STSYS_WriteRegDev32LE(TVO_TTXT_BASE + 0x74,  0x50f7c24);    //TVO_SD_COEFF_C_PHASE3_TAP456
  STSYS_WriteRegDev32LE(TVO_TTXT_BASE + 0x78,  0xf4857c);     //TVO_SD_COEFF_C_PHASE3_TAP123
  STSYS_WriteRegDev32LE(TVO_TTXT_BASE + 0x7c,  0xa1f402e);    //TVO_SD_COEFF_C_PHASE3_TAP456
  STSYS_WriteRegDev32LE(TVO_TTXT_BASE + 0x80,  0xfa027f);     //TVO_SD_COEFF_C_PHASE4_TAP123
  STSYS_WriteRegDev32LE(TVO_TTXT_BASE + 0x84,  0xe076e1d);    //TVO_SD_COEFF_C_PHASE4_TAP456
}

static inline void TVOUT_AUX(void)
{
  TVOUT_PF_AUX();
  TVOUT_SD_COEFF();
  TVOUT_VIP_DENC();
}

static void TVOUT(void)
{
  TVOUT_MAIN();
  TVOUT_AUX();
}

static void PowerOn(int enable)
{
  U32 tmp = 0;
  if(enable)
  {
#ifdef stx390
    tmp = STSYS_ReadRegDev32LE(SYSCFG_CORE + SYS_CFG_COMPO);
    tmp |= (SYS_CFG6002_RST_N_COMPO_0 | SYS_CFG6002_CLK_EN_COMPO_0);
    STSYS_WriteRegDev32LE(SYSCFG_CORE + SYS_CFG_COMPO, tmp);
#endif
    tmp = STSYS_ReadRegDev32LE(SYSCFG_CORE + SYS_CFG_HDTVOUT);
    tmp |= (SYS_CFG_CLK_EN_HDTVOUT|SYS_CFG_RST_N_HDTVOUT);
    STSYS_WriteRegDev32LE(SYSCFG_CORE + SYS_CFG_HDTVOUT, tmp);
  }
  else
  {
    tmp = STSYS_ReadRegDev32LE(SYSCFG_CORE + SYS_CFG_HDTVOUT);
    tmp &= ~(SYS_CFG_CLK_EN_HDTVOUT);
    STSYS_WriteRegDev32LE(SYSCFG_CORE + SYS_CFG_HDTVOUT, tmp);
#ifdef stx390
    tmp = STSYS_ReadRegDev32LE(SYSCFG_CORE + SYS_CFG_COMPO);
    tmp &= ~SYS_CFG6002_CLK_EN_COMPO_0;
    STSYS_WriteRegDev32LE(SYSCFG_CORE + SYS_CFG_COMPO, tmp);
#endif
  }
}

#define PHY_STA_PLL_LCK                       (1L<<4)

static void HDMI(splash_modes_t mode, int enable)
{
#ifdef stx390
  unsigned short timeout = (unsigned short)-1;
#endif

  if(enable)
  {
#ifdef stx390
  //Reset
  STSYS_WriteRegDev32LE(0x150E0000, 0x00010001); //Reset HDMI IP

  STSYS_WriteRegDev32LE(HDMI_BASE + 0x004, 0x9df); //Configure interrupts
  STSYS_WriteRegDev32LE(HDMI_BASE + 0x200, 0x117d0002); //Disable audio transmission
#endif
#ifndef stx390
  STSYS_WriteRegDev32LE(HDMI_BASE + 0x504, hdmi_config[mode].hdmi.hdmi4); //STM_HDMI_SRZ_CFG
  STSYS_WriteRegDev32LE(HDMI_BASE + 0x510, hdmi_config[mode].hdmi.hdmi5); //STM_HDMI_SRZ_PLL_CFG
#else
  STSYS_WriteRegDev32LE(HDMI_PHY_BASE + 0x050, 1);          //STM_PHYCNTRLR_RX_TERMINATION
  STSYS_WriteRegDev32LE(HDMI_PHY_BASE + 0x06c, 1);          //STM_PHYCNTRLR_BIAS_RESISTANCE
  STSYS_WriteRegDev32LE(HDMI_PHY_BASE + 0x000, 0x211);      //STM_PHYCNTRLR_CFG
  STSYS_WriteRegDev32LE(HDMI_PHY_BASE + 0x010, 0);          //STM_PHYCNTRLR_PRE_EMPHASIS_CFG
  STSYS_WriteRegDev32LE(HDMI_PHY_BASE + 0x014, 0);          //STM_PHYCNTRLR_PRE_EMPHASIS_CH1_COEFF
  STSYS_WriteRegDev32LE(HDMI_PHY_BASE + 0x018, 0);          //STM_PHYCNTRLR_PRE_EMPHASIS_CH2_COEFF
  STSYS_WriteRegDev32LE(HDMI_PHY_BASE + 0x080, 0x2042801);  //STM_PHYCNTRLR_PLL_CONTROL

  while(timeout > 0) {
    if((STSYS_ReadRegDev32LE(HDMI_PHY_BASE + 0x100) & PHY_STA_PLL_LCK) != 0)
      break;
    timeout--; //Wait for PHY to lock PLL
  }
  if(timeout == 0)
    splash_print("!!! ERROR: Unable to lock HDMI PLL\n");
#endif

  //Configure VID window
  STSYS_WriteRegDev32LE(HDMI_BASE + 0x100, hdmi_config[mode].hdmi.hdmi0); //STM_HDMI_ACTIVE_VID_XMIN
  STSYS_WriteRegDev32LE(HDMI_BASE + 0x104, hdmi_config[mode].hdmi.hdmi1); //STM_HDMI_ACTIVE_VID_XMAX
  STSYS_WriteRegDev32LE(HDMI_BASE + 0x108, hdmi_config[mode].hdmi.hdmi2); //STM_HDMI_ACTIVE_VID_YMIN
  STSYS_WriteRegDev32LE(HDMI_BASE + 0x10c, hdmi_config[mode].hdmi.hdmi3); //STM_HDMI_ACTIVE_VID_YMAX

  STSYS_WriteRegDev32LE(HDMI_BASE + 0x0,   hdmi_config[mode].hdmi.hdmi6); //STM_HDMI_CFG

  /* Info Frame */
  STSYS_WriteRegDev32LE(HDMI_BASE + 0x230, hdmi_config[mode].hdmi.hdmi7); //STM_HDMI_IFRAME_CFG
  STSYS_WriteRegDev32LE(HDMI_BASE + 0x620, hdmi_config[mode].hdmi.hdmi8); //STM_HDMI_DI3_HEAD_WD
  STSYS_WriteRegDev32LE(HDMI_BASE + 0x624, hdmi_config[mode].hdmi.hdmi9 | HDMI_YCBCR_ENABLE); //STM_HDMI_DI3_HEAD_WD + 0x4
  STSYS_WriteRegDev32LE(HDMI_BASE + 0x628, hdmi_config[mode].hdmi.hdmi10);//STM_HDMI_DI3_HEAD_WD + 0x8
  }
  else
  {
    STSYS_WriteRegDev32LE(HDMI_BASE + 0x0,   0x0);                        //STM_HDMI_CFG
#ifdef stx390
    STSYS_WriteRegDev32LE(0x150E0000, 0x0); //Reset HDMI IP
#endif
  }
}

void static VMIX_Main(splash_modes_t mode, int enable)
{
  /* Wait for TOP event in case of Progressive display */
  U32 event = 0x1;

  if ((mode == STM_NTSC) || (mode == STM_PAL)
  ||  (mode == STM_1920X1080I60) || (mode == STM_1920X1080I50))
  {
    /* Wait for BOT event in case of Interlaced display */
    event = 0x2;
  }
  VTG_WaitForVsync(VTG_MAIN_BASE, event, 1);

  if(enable)
  {
    STSYS_WriteRegDev32LE(MIXER1_BASE + 0x00, GAM_MIXER_MAIN_CTL);          //MXn_CTL_REG_OFFSET
    STSYS_WriteRegDev32LE(MIXER1_BASE + 0x04, 0x101060);                    //MXn_BKC_REG_OFFSET
#ifdef stx410
    STSYS_WriteRegDev32LE(MIXER1_BASE + 0x08, 0x0);                         //MXn_MISC_REG_OFFSET
#else
    STSYS_WriteRegDev32LE(MIXER1_BASE + 0x08, 0x2);                         //MXn_MISC_REG_OFFSET , Enable VTG counter into compo
#endif
    STSYS_WriteRegDev32LE(MIXER1_BASE + 0x0c, vmix_conf[mode].vmix.vmix0);  //MXn_BCO_REG_OFFSET
    STSYS_WriteRegDev32LE(MIXER1_BASE + 0x10, vmix_conf[mode].vmix.vmix1);  //MXn_BCS_REG_OFFSET
    STSYS_WriteRegDev32LE(MIXER1_BASE + 0x28, vmix_conf[mode].vmix.vmix2);  //MXn_AVO_REG_OFFSET
    STSYS_WriteRegDev32LE(MIXER1_BASE + 0x2c, vmix_conf[mode].vmix.vmix3);  //MXn_AVS_REG_OFFSET
    STSYS_WriteRegDev32LE(MIXER1_BASE + 0x34, GAM_MIXER_MAIN_CRB);          //MXn_CRB_REG_OFFSET
  }
  else
  {
    STSYS_WriteRegDev32LE(MIXER1_BASE + 0x00, 0);                           //MXn_CTL_REG_OFFSET : Disable everything
  }

  /* Wait for n Vsync to get HW programmed as well as requested */
  VTG_WaitForVsync(VTG_MAIN_BASE, 0x1, MIXER_FETCH_VSYNC_COUNTER);

}

void static VMIX_Aux(splash_modes_t mode, int enable)
{
  /* Wait for TOP event in case of Progressive display */
  U32 event = 0x1;

  if ((mode == STM_NTSC) || (mode == STM_PAL)
  ||  (mode == STM_1920X1080I60) || (mode == STM_1920X1080I50))
  {
    /* Wait for BOT event in case of Interlaced display */
    event = 0x2;
  }
  VTG_WaitForVsync(VTG_AUX_BASE, event, 1);

  if(enable)
  {
    STSYS_WriteRegDev32LE(MIXER2_BASE + 0x00, GAM_MIXER_AUX_CTL);           //MXn_CTL_REG_OFFSET
    STSYS_WriteRegDev32LE(MIXER2_BASE + 0x04, 0x101060);                    //MXn_BKC_REG_OFFSET
#ifdef stx410
    STSYS_WriteRegDev32LE(MIXER2_BASE + 0x08, 0x0);                         //MXn_MISC_REG_OFFSET
#else
    STSYS_WriteRegDev32LE(MIXER2_BASE + 0x08, 0x2);                         //MXn_MISC_REG_OFFSET , Enable VTG counter into compo
#endif
    STSYS_WriteRegDev32LE(MIXER2_BASE + 0x0c, vmix_conf[mode].vmix.vmix0);  //MXn_BCO_REG_OFFSET
    STSYS_WriteRegDev32LE(MIXER2_BASE + 0x10, vmix_conf[mode].vmix.vmix1);  //MXn_BCS_REG_OFFSET
    STSYS_WriteRegDev32LE(MIXER2_BASE + 0x28, vmix_conf[mode].vmix.vmix2);  //MXn_AVO_REG_OFFSET
    STSYS_WriteRegDev32LE(MIXER2_BASE + 0x2c, vmix_conf[mode].vmix.vmix3);  //MXn_AVS_REG_OFFSET
    STSYS_WriteRegDev32LE(MIXER2_BASE + 0x34, GAM_MIXER_AUX_CRB);           //MXn_CRB_REG_OFFSET
  }
  else
  {
    STSYS_WriteRegDev32LE(MIXER2_BASE + 0x00, 0);                           //MXn_CTL_REG_OFFSET : Disable everything
  }

  /* Wait for n Vsync to get HW programmed as well as requested */
  VTG_WaitForVsync(VTG_AUX_BASE, 0x1, MIXER_FETCH_VSYNC_COUNTER);
}

static void GDP_START(void)
{
  /* Wait until TOP field interrupt is fired on Aux VTG */
  VTG_WaitForVsync(VTG_AUX_BASE, 0x1, 1);

  /* Write GDP Node address*/
  STSYS_WriteRegDev32LE(GDP_AUX_BASE + GDP_NVN_OFFSET,((U32)gdp4_node2)); //GAM_GDP_NVN : Set the node to display for next vsync

  /* Wait until TOP field interrupt is fired on Main VTG */
  VTG_WaitForVsync(VTG_MAIN_BASE, 0x1, 1);

#ifndef stx418
  /* Submit GDP Node to HW*/
  STSYS_WriteRegDev32LE(GDP_MAIN_BASE + GDP_NVN_OFFSET,((U32)gdp3_node2)); //GAM_GDP_NVN : Set the node to display for next vsync
#else
  /* Submit GDPPlus Node to HW */
  STSYS_WriteRegDev32LE(GDP_MAIN_BASE + GDP_GQR_NVN_OFFSET,((U32)gdp3_node2)); //GAM_GDP_GQR_NVN : Set the node to display for next vsync
#endif

}

static void GDP_STOP(void)
{
  /* FIXME : For Cannes2 SoCs we need the GDP crash WA !! */

#ifndef stx418
  STSYS_WriteRegDev32LE(GDP_MAIN_BASE + GDP_NVN_OFFSET, 0); //GAM_GDP_NVN : Set the node to display for next vsync
#else
  /* Write GDP Node address*/
  STSYS_WriteRegDev32LE(GDP_MAIN_BASE + GDP_GQR_NVN_OFFSET, 0); //GAM_GDP_NVN : Set the node to display for next vsync
#endif

  /* Wait until TOP field interrupt is fired on Main VTG */
  VTG_WaitForVsync(VTG_MAIN_BASE, 0x1, 1);

  /* Write GDP Node address*/
  STSYS_WriteRegDev32LE(GDP_AUX_BASE + GDP_NVN_OFFSET, 0); //GAM_GDP_NVN : Set the node to display for next vsync

  /* Wait until TOP field interrupt is fired on Aux VTG */
  VTG_WaitForVsync(VTG_AUX_BASE, 0x1, 1);
}

#define FIXED_POINT_ONE               (1L << 13)
#define GDP_VSRC_INITIAL_PHASE_SHIFT  (16)
#define GDP_VSRC_VSRC_INC_XTN_SHIFT   (27)
static void splash_display_fill_gdp_nodes( stm_img_t *imgInfo, const splash_modes_t mode
                                            , GDP_Node_t *gdp_top_node
                                            , GDP_Node_t *gdp_bot_node)
{
  U32               is_interlaced     = 0;
  U32               filter_index      = 0;
  U32               gdp_ctl           = 0;
  U32               colorFormat       = 0;

  U32               srcHeight         = imgInfo->height;
  U32               pixelLocationTop  = ((U32)imgInfo->fbuffer);
  U32               pixelLocationBot  = ((U32)imgInfo->fbuffer);
  U32               srcPitch          = imgInfo->width * (imgInfo->bits_per_pixel/8);
  U32               display_height    = display[mode].height;
  U32               display_width     = display[mode].width;

  switch(imgInfo->pixel_format)
  {
    case PIXEL_FORMAT_RGB_AUTO:
      if(imgInfo->bits_per_pixel == 32)
        colorFormat = ARGB8888;
      else if(imgInfo->bits_per_pixel == 24)
        colorFormat = RGB888;
      else if(imgInfo->bits_per_pixel == 16)
        colorFormat = ARGB1555;
      break;

    case PIXEL_FORMAT_CLUT8:
      colorFormat = CLUT8;
      break;

    case PIXEL_FORMAT_RGB_BITFIELDS:
			if(imgInfo->bits_per_pixel == 16)
				colorFormat = RGB565;
      break;

    default:
      break;
  }

  if ((mode == STM_NTSC) || (mode == STM_PAL)
  ||  (mode == STM_1920X1080I60) || (mode == STM_1920X1080I50))
  {
    is_interlaced = 1;
    display_height &= ~0x1;
    display_height /= 2;
  }

  if(is_interlaced)
  {
    /*
     * Progressive on interlaced display, the start position of the
     * bottom field depends on the scaling factor involved. Note that
     * we round to the next line if the sample position is in the last 1/16th.
     * The filter phase (which has 8 positions) will have been rounded up
     * and then wrapped to 0 in the filter setup.
     *
     * Note: this could potentially cause us to read beyond the end of the
     * image buffer.
     */

    U32 linestobottomfield = (((FIXED_POINT_ONE * (imgInfo->height/display[mode].height))
                                    + (FIXED_POINT_ONE / 16))
                                    / FIXED_POINT_ONE);
    pixelLocationBot = pixelLocationTop + (srcPitch*linestobottomfield);
    vclk_debug(2, "linestobottomfield = %d\n", linestobottomfield);
  }
  else
  {
    /*
     * Progressive content on a progressive display has the same start pointer
     * for both nodes, i.e. the content is repeated exactly.
     */
    pixelLocationBot = pixelLocationTop;
  }

  splash_debug("vtg width         = %d\n"     , display[mode].width);
  splash_debug("vtg height        = %d\n"     , display[mode].height);
  splash_debug("bitmap width      = %d\n"     , imgInfo->width);
  splash_debug("bitmap height     = %d\n"     , imgInfo->height);
  splash_debug("display width     = %d\n"     , display_width);
  splash_debug("display height    = %d\n"     , display_height);
  splash_debug("pixelLocationTop  = 0x%08x\n" , pixelLocationTop);
  splash_debug("pixelLocationBot  = 0x%08x\n" , pixelLocationBot);

  gdp_ctl  = GDP_CTL_WAIT_NEXT_VSYNC;

  if(imgInfo->width != display_width)
  {
    U32 hw_srcinc_ext = 0;
    U32 hw_srcinc = (imgInfo->width*FIXED_POINT_ONE*2)/ display_width;

    hw_srcinc += 1;
    hw_srcinc >>= 1;

    hw_srcinc_ext = (hw_srcinc & 0x1f) << GDP_VSRC_VSRC_INC_XTN_SHIFT;
    hw_srcinc >>= 5;

    for(filter_index=0;filter_index<N_ELEMENTS(HSRC_index);filter_index++)
    {
      if(hw_srcinc <= HSRC_index[filter_index])
      {
        gdp_top_node->GDPn_HFP = hfilters + (filter_index*HFILTERS_ENTRY_SIZE);
        gdp_bot_node->GDPn_HFP = hfilters + (filter_index*HFILTERS_ENTRY_SIZE);
        break;
      }
    }
    gdp_ctl  |= (GDP_CTL_EN_H_RESIZE | GDP_CTL_EN_HFILTER_UPD);
    gdp_top_node->GDPn_HSRC = (hw_srcinc | hw_srcinc_ext | GDP_HSRC_FILTER_EN);
    gdp_bot_node->GDPn_HSRC = (hw_srcinc | hw_srcinc_ext | GDP_HSRC_FILTER_EN);
  }

  if(is_interlaced || (imgInfo->height != display_height))
  {
    U32 skip_line     = 0;
    U32 hw_srcinc     = 0;
    U32 hw_srcinc_ext = 0;
restart:
    hw_srcinc = (srcHeight*FIXED_POINT_ONE*2)/ display_height;
    hw_srcinc += 1;
    hw_srcinc >>= 1;

    if(hw_srcinc > (2*FIXED_POINT_ONE))
    {
      skip_line++;
      srcHeight = imgInfo->height / skip_line;
      srcPitch  = (imgInfo->width * (imgInfo->bits_per_pixel/8)) * skip_line;

      goto restart;
    }

    hw_srcinc_ext = (hw_srcinc & 0x1f) << GDP_VSRC_VSRC_INC_XTN_SHIFT;
    hw_srcinc >>= 5;

    for(filter_index=0;filter_index<N_ELEMENTS(VSRC_index);filter_index++)
    {
      if(hw_srcinc <= VSRC_index[filter_index])
      {
        gdp_top_node->GDPn_VFP = vfilters + (filter_index*VFILTERS_ENTRY_SIZE);
        gdp_bot_node->GDPn_VFP = vfilters + (filter_index*VFILTERS_ENTRY_SIZE);
        break;
      }
    }

    gdp_ctl  |= (GDP_CTL_EN_V_RESIZE | GDP_CTL_EN_VFILTER_UPD);
    gdp_top_node->GDPn_VSRC   = (hw_srcinc | hw_srcinc_ext | GDP_VSRC_FILTER_EN);
    gdp_bot_node->GDPn_VSRC   = (hw_srcinc | hw_srcinc_ext | GDP_VSRC_FILTER_EN);

    if(is_interlaced)
    {
      /*
       * When putting progressive content on an interlaced display
       * we adjust the filter phase of the bottom field to start
       * an appropriate distance lower in the source bitmap, based on the
       * scaling factor. If the scale means that the bottom field
       * starts >1 source bitmap line lower then this will get dealt
       * with in the memory setup by adjusting the source bitmap address.
       */
      U32 bottomfieldinc   = ((FIXED_POINT_ONE * imgInfo->height)/display[mode].height);
      U32 integerinc       = bottomfieldinc / FIXED_POINT_ONE;
      U32 fractionalinc    = bottomfieldinc - (integerinc * FIXED_POINT_ONE);
      U32 bottomfieldphase = ((((fractionalinc * 8) + (FIXED_POINT_ONE / 2)) / FIXED_POINT_ONE) % 8);

      splash_debug("\nbottomfieldinc   = %u\n", bottomfieldinc);
      splash_debug("integerinc       = %u\n", integerinc);
      splash_debug("fractionalinc    = %u\n", fractionalinc);
      splash_debug("bottomfieldphase = %u\n", bottomfieldphase);
      gdp_bot_node->GDPn_VSRC |= (bottomfieldphase<<GDP_VSRC_INITIAL_PHASE_SHIFT);
    }
  }

  /* gdp_top_node */
  gdp_top_node->GDPn_CTL  = colorFormat | gdp_ctl;
  gdp_top_node->GDPn_AGC  = 0x800080;
  gdp_top_node->GDPn_VPO  = vmix_conf[mode].vmix.vmix0;
  gdp_top_node->GDPn_VPS  = vmix_conf[mode].vmix.vmix1;
  gdp_top_node->GDPn_PML  = pixelLocationTop;
  gdp_top_node->GDPn_PMP  = srcPitch;
  gdp_top_node->GDPn_SIZE = ((srcHeight << 16)|(imgInfo->width));
  gdp_top_node->GDPn_NVN  = ((U32)gdp_bot_node);
  gdp_top_node->GDPn_KEY1 = 0;
  gdp_top_node->GDPn_KEY2 = 0;
  gdp_top_node->GDPn_PPT  = 0;
  if(colorFormat == CLUT8)
    gdp_top_node->GDPn_CML  = ((U32)(imgInfo->Palette));
  else
    gdp_top_node->GDPn_CML  = 0;

  splash_debug("\nTop Node : \n");
  splash_debug("GDPn_CTL  = 0x%08x\n", gdp_top_node->GDPn_CTL);
  splash_debug("GDPn_AGC  = 0x%08x\n", gdp_top_node->GDPn_AGC);
  splash_debug("GDPn_VPO  = 0x%08x\n", gdp_top_node->GDPn_VPO);
  splash_debug("GDPn_VPS  = 0x%08x\n", gdp_top_node->GDPn_VPS);
  splash_debug("GDPn_PML  = 0x%08x\n", gdp_top_node->GDPn_PML);
  splash_debug("GDPn_PMP  = 0x%08x\n", gdp_top_node->GDPn_PMP);
  splash_debug("GDPn_SIZE = 0x%08x\n", gdp_top_node->GDPn_SIZE);
  splash_debug("GDPn_NVN  = 0x%08x\n", gdp_top_node->GDPn_NVN);
  splash_debug("GDPn_HSRC = 0x%08x\n", gdp_top_node->GDPn_HSRC);
  splash_debug("GDPn_VSRC = 0x%08x\n", gdp_top_node->GDPn_VSRC);

  /* gdp_bot_node */
  gdp_bot_node->GDPn_CTL  = colorFormat | gdp_ctl;
  gdp_bot_node->GDPn_AGC  = 0x800080;
  gdp_bot_node->GDPn_VPO  = vmix_conf[mode].vmix.vmix0;
  gdp_bot_node->GDPn_VPS  = vmix_conf[mode].vmix.vmix1;
  gdp_bot_node->GDPn_PML  = pixelLocationBot;
  gdp_bot_node->GDPn_PMP  = srcPitch;
  gdp_bot_node->GDPn_SIZE = ((srcHeight << 16)|(imgInfo->width));
  gdp_bot_node->GDPn_NVN  = ((U32)gdp_top_node);
  gdp_bot_node->GDPn_KEY1 = 0;
  gdp_bot_node->GDPn_KEY2 = 0;
  gdp_bot_node->GDPn_PPT  = 0;
  if(colorFormat == CLUT8)
    gdp_bot_node->GDPn_CML  = ((U32)(imgInfo->Palette));
  else
    gdp_bot_node->GDPn_CML  = 0;

  splash_debug("\nBottom Node : \n");
  splash_debug("GDPn_CTL  = 0x%08x\n", gdp_bot_node->GDPn_CTL);
  splash_debug("GDPn_AGC  = 0x%08x\n", gdp_bot_node->GDPn_AGC);
  splash_debug("GDPn_VPO  = 0x%08x\n", gdp_bot_node->GDPn_VPO);
  splash_debug("GDPn_VPS  = 0x%08x\n", gdp_bot_node->GDPn_VPS);
  splash_debug("GDPn_PML  = 0x%08x\n", gdp_bot_node->GDPn_PML);
  splash_debug("GDPn_PMP  = 0x%08x\n", gdp_bot_node->GDPn_PMP);
  splash_debug("GDPn_SIZE = 0x%08x\n", gdp_bot_node->GDPn_SIZE);
  splash_debug("GDPn_NVN  = 0x%08x\n", gdp_bot_node->GDPn_NVN);
  splash_debug("GDPn_HSRC = 0x%08x\n", gdp_bot_node->GDPn_HSRC);
  splash_debug("GDPn_VSRC = 0x%08x\n", gdp_bot_node->GDPn_VSRC);
}

#ifdef stx418
int ValToFixedPoint(int val, U32 multiple)
{
    int tmp = 1;
    if(val < 0)
    {
      tmp = -1;
      val = -val;
    }
    tmp *= (int)(((uint64_t)val * FIXED_POINT_ONE) / multiple);
    return tmp;
}

int FixedPointToInteger(int fp_val, int *frac)
{
    int integer = fp_val / FIXED_POINT_ONE;
    if(frac)
        *frac = fp_val - (integer * FIXED_POINT_ONE);

    return integer;
}

/***********************************************************/
/*                                                         */
/*                 VERTICAL COEFFICIENTS                   */
/*         ----------------------------------------        */
/*         1      < Zoom < 10     -> use coeffs 'A'        */
/*                  Zoom = 1      -> use coeffs 'B'        */
/*         0.80   < Zoom < 1      -> use coeffs 'C'        */
/*         0.60   < Zoom < 0.80   -> use coeffs 'D'        */
/*         0.50   < Zoom < 0.60   -> use coeffs 'E'        */
/*         0.25   < Zoom < 0.50   -> use coeffs 'F'        */
/*                                                         */
/***********************************************************/

const U32 *zoomToVertFilter(U32 scaleFactor)
{
    if ( scaleFactor > 10 ) {
        splash_debug("Using Vertical Filter A\n");
        return VertFilterA;
    } else if ( scaleFactor > 8) {
        splash_debug("Using Vertical Filter C\n");
        return VertFilterC;
    } else if ( scaleFactor > 6) {
        splash_debug("Using Vertical Filter E\n");
        return VertFilterD;
    } else if ( scaleFactor > 5) {
        splash_debug("Using Vertical Filter E\n");
        return VertFilterE;
    } else {
        splash_debug("Using Vertical Filter F\n");
        return VertFilterF;
    }
}

const U8 zoomToVertLumaNormalization(U32 scaleFactor)
{
    if ( scaleFactor > 5) {
        splash_debug("Using Vertical Normalization 8\n");
        return 8;
    } else {
        splash_debug("Using Vertical Normalization 9\n");
        return 9;
    }
}

const U8 zoomToVertChromaNormalization(U32 scaleFactor)
{
    if (scaleFactor > 10) {
        splash_debug("Using Horizontal Normalization 9\n");
        return 9;
    }
    if ( scaleFactor > 5) {
        splash_debug("Using Vertical Normalization 8\n");
        return 8;
    } else {
        splash_debug("Using Vertical Normalization 9\n");
        return 9;
    }
}

/***********************************************************/
/*                                                         */
/*                 HORIZONTAL COEFFICIENTS                 */
/*         ----------------------------------------        */
/*         1      < Zoom < 10     -> use coeffs 'A'        */
/*                  Zoom = 1      -> use coeffs 'B'        */
/*         0.80   < Zoom < 1      -> use coeffs 'C'        */
/*         0.60   < Zoom < 0.80   -> use coeffs 'D'        */
/*         0.50   < Zoom < 0.60   -> use coeffs 'E'        */
/*         0.25   < Zoom < 0.50   -> use coeffs 'F'        */
/*                                                         */
/***********************************************************/

const U32 *zoomToHorzFilter(U32 scaleFactor)
{
    if ( scaleFactor > 10 ) {
        splash_debug("Using Horizontal Filter A\n");
        return HorzFilterA;
    } else if ( scaleFactor > 8) {
        splash_debug("Using Horizontal Filter C\n");
        return HorzFilterC;
    } else if ( scaleFactor > 6) {
        splash_debug("Using Horizontal Filter E\n");
        return HorzFilterD;
    } else if ( scaleFactor > 5) {
        splash_debug("Using Horizontal Filter E\n");
        return HorzFilterE;
    } else {
        splash_debug("Using Horizontal Filter F\n");
        return HorzFilterF;
    }
}

const U8 zoomToHorzLumaNormalization(U32 scaleFactor)
{
    if ( scaleFactor > 5) {
        splash_debug("Using Horizontal Normalization 8\n");
        return 8;
    } else {
        splash_debug("Using Horizontal Normalization 9\n");
        return 9;
    }
}

const U8 zoomToHorzChromaNormalization(U32 scaleFactor)
{
    if ( scaleFactor > 5) {
        splash_debug("Using Horizontal Normalization 8\n");
        return 8;
    } else {
        splash_debug("Using Horizontal Normalization 9\n");
        return 9;
    }
}

static void splash_display_fill_gdp_gqr_nodes( stm_img_t *imgInfo, const splash_modes_t mode
                                            , GDP_GQR_Node_t *gdp_top_node
                                            , GDP_GQR_Node_t *gdp_bot_node)
{
    U32               is_interlaced     = 0;
    U32               gdp_ctl           = 0;
    U32               colorFormat       = 0;

    U32               srcHeight         = imgInfo->height;
    U32               pixelLocationTop  = ((U32)imgInfo->fbuffer);
    U32               pixelLocationBot  = ((U32)imgInfo->fbuffer);
    U32               srcPitch          = imgInfo->width * (imgInfo->bits_per_pixel/8);
    U32               display_height    = display[mode].height;
    U32               display_width     = display[mode].width;

    int               subpixelpos       = 0;
    U32               horz_phase        = 0;

	switch(imgInfo->pixel_format)
	{
		case PIXEL_FORMAT_RGB_AUTO:
			if(imgInfo->bits_per_pixel == 32)
				colorFormat = ARGB8888;
			else if(imgInfo->bits_per_pixel == 24)
				colorFormat = RGB888;
			else if(imgInfo->bits_per_pixel == 16)
				colorFormat = ARGB1555;
			break;
		case PIXEL_FORMAT_CLUT8:
			colorFormat = CLUT8;
			break;
		case PIXEL_FORMAT_RGB_BITFIELDS:
			if(imgInfo->bits_per_pixel == 16)
				colorFormat = RGB565;

		default:
			break;
	}

    if ((mode == STM_NTSC) || (mode == STM_PAL)
    ||  (mode == STM_1920X1080I60) || (mode == STM_1920X1080I50))
    {
        is_interlaced = 1;
        display_height >>=1;
    }

    if(is_interlaced)
    {
        /*
        * Precondition : we always use a progressive source in splash
        * On interlaced display, the start position of bottom field depends on
        * the scaling factor involved.
        * Note that we round to the next line if the sample position is in the last 1/16th.
        * The filter phase (which has 8 positions) will have been rounded up
        * and then wrapped to 0 in the filter setup.
        *
        * NOTE: this could potentially cause us to read beyond the end of the
        * image buffer.
        */

        U32 linestobottomfield = (((FIXED_POINT_ONE * (imgInfo->height/display[mode].height))
                                    + (FIXED_POINT_ONE / 32))
                                    / FIXED_POINT_ONE);
        pixelLocationBot = pixelLocationTop + (srcPitch*linestobottomfield);
        splash_debug("linestobottomfield = %d\n", linestobottomfield);
    } else {
        pixelLocationBot = pixelLocationTop;
    }

    splash_debug("vtg width         = %d\n"     , display[mode].width);
    splash_debug("vtg height        = %d\n"     , display[mode].height);
    splash_debug("bitmap width      = %d\n"     , imgInfo->width);
    splash_debug("bitmap height     = %d\n"     , imgInfo->height);
    splash_debug("display width     = %d\n"     , display_width);
    splash_debug("display height    = %d\n"     , display_height);
    splash_debug("pixelLocationTop  = 0x%08x\n" , pixelLocationTop);
    splash_debug("pixelLocationBot  = 0x%08x\n" , pixelLocationBot);

    gdp_ctl  = GDP_CTL_WAIT_NEXT_VSYNC;

    gdp_top_node->GDPn_GQR_HP1 = 0;
    gdp_bot_node->GDPn_GQR_HP1 = 0;

    /* Fitler Setting Starts */
    //Horizontal Resize
    if(imgInfo->width != display_width)
    {
        U32 hw_srcinc = (imgInfo->width*FIXED_POINT_ONE*2)/ display_width;

        hw_srcinc += 1;
        hw_srcinc >>= 1;

        //ToDo : Do We need to check for increment Max/Min limits here ?

        if (imgInfo->width > display_width) //Downscale
        {
            /* Disable AntiRinging & Disable Horiz Overshoot */
            gdp_top_node->GDPn_GQR_HP1 |= (0x0 << 4); // HP1.KRINGY = 0x0;
            gdp_top_node->GDPn_GQR_HP1 |= (0x8 << 16);// HP1.KVOSY  = 0x8;
            gdp_top_node->GDPn_GQR_HP1 |= (0x0 << 0); // HP1.KRINGUV = 0x0;
            gdp_top_node->GDPn_GQR_HP1 |= (0x8 << 12);// HP1.KVOSUV  = 0x8;
        } else {                            //Upscale
            /* Enable Horizontal Antiringing and Horizontal Overshooting in width upscaling */
            gdp_top_node->GDPn_GQR_HP1 |= (0x1 << 4); // HP1.KRINGY = 0x1;
            gdp_top_node->GDPn_GQR_HP1 |= (0x0 << 16);// HP1.KVOSY  = 0x0;
            gdp_top_node->GDPn_GQR_HP1 |= (0x1 << 0); // HP1.KRINGUV = 0x1;
            gdp_top_node->GDPn_GQR_HP1 |= (0x0 << 12);// HP1.KVOSUV  = 0x0;
        }

        if (hw_srcinc != FIXED_POINT_ONE)
        {
            gdp_top_node->GDPn_GQR_HSRC = hw_srcinc;
            gdp_bot_node->GDPn_GQR_HSRC = hw_srcinc;

            gdp_ctl  |= GDP_CTL_EN_H_RESIZE | GDP_CTL_EN_HFILTER_UPD;

            FixedPointToInteger(ValToFixedPoint(imgInfo->width, 16), &subpixelpos);

            horz_phase = (subpixelpos * FIXED_POINT_ONE);

            gdp_top_node->GDPn_GQR_HIP = (horz_phase & 0x1FFF);
            gdp_bot_node->GDPn_GQR_HIP = (horz_phase & 0x1FFF);

            gdp_top_node->GDPn_GQR_HFP = (U32)zoomToHorzFilter(10*display_width/imgInfo->width);
            gdp_bot_node->GDPn_GQR_HFP = gdp_top_node->GDPn_GQR_HFP;

            gdp_top_node->GDPn_GQR_HP2 = (0x0 << GDP_PLUS_HVP2_ATHR_SHIFT)                   |
                                         (0x1 << GDP_PLUS_HVP2_DIS_ALPHA_SHIFT)              |
                                         (zoomToHorzLumaNormalization(10*display_width/imgInfo->width) << GDP_PLUS_HVP2_MAXAY_SHIFT) |
                                         (zoomToHorzChromaNormalization(10*display_width/imgInfo->width) << GDP_PLUS_HVP2_MAXUV_SHIFT);
            gdp_bot_node->GDPn_GQR_HP2 = gdp_top_node->GDPn_GQR_HP2;
        }
    }

    //Vertical Resize
    if( is_interlaced ||
       (imgInfo->height != display_height))
    {
        U32 hw_srcinc     = 0;

        //Calculate increment
        hw_srcinc = (srcHeight*FIXED_POINT_ONE*2)/ display_height;
        hw_srcinc += 1;
        hw_srcinc >>= 1;
        //ToDo : Do We need to check for increment Max/Min limits here ?

        //Set-up Vertical Antiringing/Overshooting
        if (imgInfo->height > display_height) //Downscale
        {
            /* Disable Vert. AntiRinging & Disable Vert. Overshoot */
            gdp_top_node->GDPn_GQR_VP1 |= (0x0 << 4);  //VP1.KRINGY  = 0x0;
            gdp_top_node->GDPn_GQR_VP1 |= (0x8 << 16); //VP1.KVOS    = 0x8;
            gdp_top_node->GDPn_GQR_VP1 |= (0x0 << 0);  //VP1.KRINGUV = 0x0;
            gdp_top_node->GDPn_GQR_VP1 |= (0x8 << 12); //VP1.KVOSUV  = 0x8;
        } else {                              //Upscale
            /* Enable Vert. Antiringing and Vert. Overshooting in height upscaling */
            gdp_top_node->GDPn_GQR_VP1 |= (0x1 << 4);  //VP1.KRINGY  = 0x1;
            gdp_top_node->GDPn_GQR_VP1 |= (0x0 << 16); //VP1.KVOS    = 0x0;
            gdp_top_node->GDPn_GQR_VP1 |= (0x1 << 0);  //VP1.KRINGUV = 0x1;
            gdp_top_node->GDPn_GQR_VP1 |= (0x0 << 12); //VP1.KVOSUV  = 0x0;
        }

        gdp_top_node->GDPn_GQR_VSRC = hw_srcinc;
        gdp_bot_node->GDPn_GQR_VSRC = hw_srcinc;
        gdp_ctl  |= (GDP_CTL_EN_V_RESIZE | GDP_CTL_EN_VFILTER_UPD);

        gdp_top_node->GDPn_GQR_VIP = 0;

        if(is_interlaced)
        {
            /*
             * precondition : We are using a progessive source in Splash
             * on interlaced display we need adjust the filter phase of
             * the bottom field to start an appropriate distance lower
             * in the source bitmap, based on the scaling factor.
             */
            U32 bottomfieldinc   = ((FIXED_POINT_ONE * imgInfo->height)/display[mode].height);
            U32 integerinc       = bottomfieldinc / FIXED_POINT_ONE;
            U32 fractionalinc    = bottomfieldinc - (integerinc * FIXED_POINT_ONE);
            U32 bottomfieldphase = 256 * ((((fractionalinc * 32) + (FIXED_POINT_ONE / 2)) / FIXED_POINT_ONE) % 32);

            gdp_bot_node->GDPn_GQR_VIP |= (bottomfieldphase & 0x1FFF);
        }

        //U32 vert_phase = (gdp_bot_node->GDPn_GQR_VIP & 0x1FFF); ToDo: Revisit if some errors occur in filte association with zoom factors

        //Set-up Vert. Filter
        gdp_top_node->GDPn_GQR_VFP = (U32)zoomToVertFilter(10*display_height/imgInfo->height);
        gdp_bot_node->GDPn_GQR_VFP = gdp_top_node->GDPn_GQR_VFP;

        gdp_top_node->GDPn_GQR_VP2 = (0x0 << GDP_PLUS_HVP2_ATHR_SHIFT)                   |
                                     (0x1 << GDP_PLUS_HVP2_DIS_ALPHA_SHIFT)              |
                                     (zoomToVertLumaNormalization(10*display_height/imgInfo->height)   << GDP_PLUS_HVP2_MAXAY_SHIFT) |
                                     (zoomToVertChromaNormalization(10*display_height/imgInfo->height)   << GDP_PLUS_HVP2_MAXUV_SHIFT);
    }
    gdp_bot_node->GDPn_GQR_HP1 = gdp_top_node->GDPn_GQR_HP1;
    gdp_bot_node->GDPn_GQR_HP2 = gdp_top_node->GDPn_GQR_HP2;
    gdp_bot_node->GDPn_GQR_VP1 = gdp_top_node->GDPn_GQR_VP1;
    gdp_bot_node->GDPn_GQR_VP2 = gdp_top_node->GDPn_GQR_VP2;

    /* Filter setting ends */

    /* gdp_top_node */
    gdp_top_node->GDPn_GQR_CTL  = colorFormat | gdp_ctl;
    gdp_top_node->GDPn_GQR_AGC  = 0x800080;
    gdp_top_node->GDPn_GQR_VPO  = vmix_conf[mode].vmix.vmix0;
    gdp_top_node->GDPn_GQR_VPS  = vmix_conf[mode].vmix.vmix1;
    gdp_top_node->GDPn_GQR_PML  = pixelLocationTop;
    gdp_top_node->GDPn_GQR_PMP  = srcPitch;
    gdp_top_node->GDPn_GQR_SIZE = ((srcHeight << 16)|(imgInfo->width));
    gdp_top_node->GDPn_GQR_NVN  = ((U32)gdp_bot_node);
    gdp_top_node->GDPn_GQR_KEY1 = 0;
    gdp_top_node->GDPn_GQR_KEY2 = 0;
    gdp_top_node->GDPn_GQR_PPT  = 0;
    if(colorFormat == CLUT8)
        gdp_top_node->GDPn_GQR_CML  = ((U32)(imgInfo->Palette));
    else
        gdp_top_node->GDPn_GQR_CML  = 0;

    splash_debug("\nTop Node : \n");
    splash_debug("GDPn_CTL  = 0x%08x\n", gdp_top_node->GDPn_GQR_CTL);
    splash_debug("GDPn_AGC  = 0x%08x\n", gdp_top_node->GDPn_GQR_AGC);
    splash_debug("GDPn_VPO  = 0x%08x\n", gdp_top_node->GDPn_GQR_VPO);
    splash_debug("GDPn_VPS  = 0x%08x\n", gdp_top_node->GDPn_GQR_VPS);
    splash_debug("GDPn_PML  = 0x%08x\n", gdp_top_node->GDPn_GQR_PML);
    splash_debug("GDPn_PMP  = 0x%08x\n", gdp_top_node->GDPn_GQR_PMP);
    splash_debug("GDPn_SIZE = 0x%08x\n", gdp_top_node->GDPn_GQR_SIZE);
    splash_debug("GDPn_NVN  = 0x%08x\n", gdp_top_node->GDPn_GQR_NVN);
    splash_debug("GDPn_HSRC = 0x%08x\n", gdp_top_node->GDPn_GQR_HSRC);
    splash_debug("GDPn_VSRC = 0x%08x\n", gdp_top_node->GDPn_GQR_VSRC);
    splash_debug("GDPn_HIP  = 0x%08x\n", gdp_top_node->GDPn_GQR_HIP);
    splash_debug("GDPn_HP1  = 0x%08x\n", gdp_top_node->GDPn_GQR_HP1);
    splash_debug("GDPn_HP2  = 0x%08x\n", gdp_top_node->GDPn_GQR_HP2);
    splash_debug("GDPn_VIP  = 0x%08x\n", gdp_top_node->GDPn_GQR_VIP);
    splash_debug("GDPn_VP1  = 0x%08x\n", gdp_top_node->GDPn_GQR_VP1);
    splash_debug("GDPn_VP2  = 0x%08x\n", gdp_top_node->GDPn_GQR_VP2);

    /* gdp_bot_node */
    gdp_bot_node->GDPn_GQR_CTL  = colorFormat | gdp_ctl;
    gdp_bot_node->GDPn_GQR_AGC  = 0x800080;
    gdp_bot_node->GDPn_GQR_VPO  = vmix_conf[mode].vmix.vmix0;
    gdp_bot_node->GDPn_GQR_VPS  = vmix_conf[mode].vmix.vmix1;
    gdp_bot_node->GDPn_GQR_PML  = pixelLocationBot;
    gdp_bot_node->GDPn_GQR_PMP  = srcPitch;
    gdp_bot_node->GDPn_GQR_SIZE = ((srcHeight << 16)|(imgInfo->width));
    gdp_bot_node->GDPn_GQR_NVN  = ((U32)gdp_top_node);
    gdp_bot_node->GDPn_GQR_KEY1 = 0;
    gdp_bot_node->GDPn_GQR_KEY2 = 0;
    gdp_bot_node->GDPn_GQR_PPT  = 0;
    if(colorFormat == CLUT8)
        gdp_bot_node->GDPn_GQR_CML  = ((U32)(imgInfo->Palette));
    else
        gdp_bot_node->GDPn_GQR_CML  = 0;

    splash_debug("\nBottom Node : \n");
    splash_debug("GDPn_CTL  = 0x%08x\n", gdp_bot_node->GDPn_GQR_CTL);
    splash_debug("GDPn_AGC  = 0x%08x\n", gdp_bot_node->GDPn_GQR_AGC);
    splash_debug("GDPn_VPO  = 0x%08x\n", gdp_bot_node->GDPn_GQR_VPO);
    splash_debug("GDPn_VPS  = 0x%08x\n", gdp_bot_node->GDPn_GQR_VPS);
    splash_debug("GDPn_PML  = 0x%08x\n", gdp_bot_node->GDPn_GQR_PML);
    splash_debug("GDPn_PMP  = 0x%08x\n", gdp_bot_node->GDPn_GQR_PMP);
    splash_debug("GDPn_SIZE = 0x%08x\n", gdp_bot_node->GDPn_GQR_SIZE);
    splash_debug("GDPn_NVN  = 0x%08x\n", gdp_bot_node->GDPn_GQR_NVN);
    splash_debug("GDPn_HSRC = 0x%08x\n", gdp_bot_node->GDPn_GQR_HSRC);
    splash_debug("GDPn_VSRC = 0x%08x\n", gdp_bot_node->GDPn_GQR_VSRC);
    splash_debug("GDPn_HIP  = 0x%08x\n", gdp_bot_node->GDPn_GQR_HIP);
    splash_debug("GDPn_HP1  = 0x%08x\n", gdp_bot_node->GDPn_GQR_HP1);
    splash_debug("GDPn_HP2  = 0x%08x\n", gdp_bot_node->GDPn_GQR_HP2);
    splash_debug("GDPn_VIP  = 0x%08x\n", gdp_bot_node->GDPn_GQR_VIP);
    splash_debug("GDPn_VP1  = 0x%08x\n", gdp_bot_node->GDPn_GQR_VP1);
    splash_debug("GDPn_VP2  = 0x%08x\n", gdp_bot_node->GDPn_GQR_VP2);
}
#endif

static void splash_display_prepare_gdp_setup(stm_img_t *imgInfo, const splash_modes_t main_mode, const splash_modes_t aux_mode)
{
#ifndef stx418
  splash_display_fill_gdp_nodes(imgInfo, main_mode, gdp3_node1, gdp3_node2);
#else
  splash_display_fill_gdp_gqr_nodes(imgInfo, main_mode, gdp3_node1, gdp3_node2);
#endif
  splash_display_fill_gdp_nodes(imgInfo, aux_mode , gdp4_node1, gdp4_node2);
}

void splash_display_img(stm_img_t *imgInfo)
{
  splash_display_prepare_gdp_setup(imgInfo, main_mode, aux_mode);
}

void do_splash_ext(int idx)
{
#if defined(SPLASH_IMAGE_BMP)
  switch(idx)
  {
      case 0 : bmp_data = bmp_splash[0];
          break;
      case 1 : bmp_data = bmp_splash[1];
          break;
      default: bmp_data = bmp_splash[0];
  }
#endif
  do_splash();
}


void do_splash(void)
{

  U32               nodes_buffer   = 0;
  stm_img_t         imgInfo;

  /* GDP Aux nodes*/
#ifndef stx418
  nodes_buffer = (U32)splash_malloc(0x400, GDP_NODE_ALIGNMENT);
#else
  nodes_buffer = (U32)splash_malloc(0x600, GDP_NODE_ALIGNMENT);
#endif
  gdp4_node1 = (GDP_Node_t *) (nodes_buffer);
  splash_memset (gdp4_node1,0, sizeof(GDP_Node_t));
  nodes_buffer +=0x100;
  gdp4_node2 = (GDP_Node_t *) (nodes_buffer);
  splash_memset (gdp4_node2,0, sizeof(GDP_Node_t));
  /* GDP Main nodes*/
  nodes_buffer +=0x100;

#ifndef stx418
  gdp3_node1 = (GDP_Node_t *) (nodes_buffer);
  splash_memset (gdp3_node1,0, sizeof(GDP_Node_t));
  nodes_buffer +=0x100;
  gdp3_node2 = (GDP_Node_t *) (nodes_buffer);
  splash_memset (gdp3_node2,0, sizeof(GDP_Node_t));
#else
  gdp3_node1 = (GDP_GQR_Node_t *) (nodes_buffer);
  splash_memset (gdp3_node1,0, sizeof(GDP_GQR_Node_t));
  nodes_buffer +=0x200;
  gdp3_node2 = (GDP_GQR_Node_t *) (nodes_buffer);
  splash_memset (gdp3_node2,0, sizeof(GDP_GQR_Node_t));
#endif

  hfilters = (U32) splash_malloc(sizeof(HSRC_Coeffs), GDP_FILTERS_ALIGNMENT);
  splash_memcpy((void*)hfilters, HSRC_Coeffs,sizeof(HSRC_Coeffs));

  vfilters  = (U32) splash_malloc(sizeof(VSRC_Coeffs), GDP_FILTERS_ALIGNMENT);
  splash_memcpy((void*)vfilters, VSRC_Coeffs,sizeof(VSRC_Coeffs));

#if defined(SPLASH_IMAGE_BMP)
  splash_get_bmp_info(bmp_data, &imgInfo);
#elif defined(SPLASH_IMAGE_GIMP_HEADER)
  splash_read_gimp_header(&imgInfo);
#else
  /* do nothing if no image built in */
  return;
#endif

  splash_display_prepare_gdp_setup(&imgInfo, MAIN_MODE, AUX_MODE);
  PowerOn(1);
  if(Clock(MAIN_MODE, AUX_MODE, 1) < 0)
    return;
  VTG_Main(MAIN_MODE, 1);
  VTG_Aux(AUX_MODE, 1);
  GDP_START();
  TVOUT();
  DENC(AUX_MODE, 1);
#ifdef STM_HD_COMPONENT
  HD_Formatter(1, MAIN_MODE);
#else
  HD_Formatter(1, AUX_MODE);
#endif
  HDMI(MAIN_MODE, 1);
  VMIX_Main(MAIN_MODE, 1);
  VMIX_Aux(AUX_MODE, 1);
}

void splash_enable(splash_modes_t main_mode, splash_modes_t aux_mode, int enable, stm_img_t *imgInfo)
{
  if(enable)
  {
    splash_display_prepare_gdp_setup(imgInfo, main_mode, aux_mode);
    PowerOn(1);
    if(Clock(main_mode, aux_mode, 1) < 0)
      return;
    VTG_Main(main_mode, 1);
    VTG_Aux(aux_mode, 1);
    GDP_START();
    TVOUT();
    DENC(aux_mode, 1);
#ifdef STM_HD_COMPONENT
    HD_Formatter(1, main_mode);
#else
    HD_Formatter(1, aux_mode);
#endif
    HDMI(main_mode, 1);
    VMIX_Main(main_mode, 1);
    VMIX_Aux(aux_mode, 1);
  }
  else
  {
    GDP_STOP();
    VMIX_Main(0, 0);
    VMIX_Aux(0, 0);
    DENC(0, 0);
    HD_Formatter(0, 0);
    HDMI(0, 0);
    VTG_Main(0, 0);
    VTG_Aux(0, 0);
    PowerOn(0);
    Clock(0, 0, 0);
  }

}

int splash_get_hdmi_status(stm_hdmi_sta_t status)
{
  U32 value = 0;
  value = STSYS_ReadRegDev32LE(HDMI_BASE + 0x10);
  switch(status)
  {
     case HOT_PLUG_STA:
       return (value & HOTPLUG_STATUS_MASK);
     case RX_SENSE_STA:
       return (value & RXSENSE_STATUS_MASK);
     default:
       break;
  }
  return 0;
}

#ifdef TEST_ALL_MODES
static const struct
  {
    const char  *name;
    splash_modes_t main_mode;
    splash_modes_t aux_mode;
  } splash_mode_data[] =
  {
    { "(480p60  , NTSC)"   , STM_720X480P60  , STM_NTSC }
  , { "(576p50  , PAL)"    , STM_720X576P50  , STM_PAL  }
  , { "(1080i60 , NTSC)"   , STM_1920X1080I60, STM_NTSC }
  , { "(1080i50 , PAL)"    , STM_1920X1080I50, STM_PAL  }
  , { "(1080p60 , NTSC)"   , STM_1920X1080P60, STM_NTSC }
  , { "(1080p50 , PAL)"    , STM_1920X1080P50, STM_PAL  }
  , { "(720p60  , NTSC)"   , STM_1280X720P60 , STM_NTSC }
  , { "(720p50  , PAL)"    , STM_1280X720P50 , STM_PAL  }
  };

void splash_test_all_modes(void)
{
  int i = 0;
  stm_img_t     imgInfo;

  splash_get_bmp_info(bmp_data, &imgInfo);

  splash_print("\nPresss any key to start the Switch Modes test.\n");
  getchar();

  for(i = 0; i < N_ELEMENTS(splash_mode_data); i++)
  {
    splash_print("Testing \e[93m%s\e[39m modes\n", splash_mode_data[i].name);
    splash_enable(0, 0, 0, 0);
    //splash_get_bmp_info(bmp_data, &imgInfo);
    splash_enable( splash_mode_data[i].main_mode
                 , splash_mode_data[i].aux_mode
                 , 1, &imgInfo);
    splash_print("Presss any key to continue.\n");
    getchar();
  }
}
#endif

#ifdef TEST_ALL_FORMATS
static const struct
  {
    const char  *name;
    U8          *bmp_data;
  } splash_format_data[] =
  {
    { "CLUT8"     , private_bmp_data          }
  , { "RGB656"    , private_bmp_data_rgb565   }
  , { "RGB888"    , private_bmp_data_rgb888   }
  , { "ARGB8888"  , private_bmp_data_argb8888 }
  , { "ARGB1555"  , private_bmp_data_argb1555 }
  };

void splash_test_all_formats(void)
{
  int i = 0;
  stm_img_t     imgInfo;

  splash_print("\nPresss any key to start the switch format test.\n");
  getchar();

  for(i = 0; i < N_ELEMENTS(splash_format_data); i++)
  {
    splash_print("Testing \e[93m%s\e[39m format\n", splash_format_data[i].name);
    splash_enable(0, 0, 0, 0);
    splash_get_bmp_info(splash_format_data[i].bmp_data, &imgInfo);
    splash_enable(MAIN_MODE, AUX_MODE, 1, &imgInfo);
    splash_print("Presss any key to continue.\n");
    getchar();
  }
}
#endif

void splash_set_wss(stm_wss_config_t wss_config)
{
  U8 value1 = 0;
  U8 value2 = 0;

  value1 = STSYS_ReadRegDev8(DENC_BASE + 0x3c) & ASPECT_RATIO_MASK;
  value2 = STSYS_ReadRegDev8(DENC_BASE + 0x0c) & ~WSS_ENABLE;
  switch(wss_config.aspect_ratio)
  {
    case STM_WSS_ASPECT_RATIO_DISABLED:
      STSYS_WriteRegDev8(DENC_BASE + 0x0c,  value2);
      break;
  case STM_WSS_ASPECT_RATIO_4_3:
      STSYS_WriteRegDev8(DENC_BASE + 0x3c, value1 | 0x8);
      STSYS_WriteRegDev8(DENC_BASE + 0x0c, value2 | WSS_ENABLE);
      break;
  case STM_WSS_ASPECT_RATIO_16_9:
      STSYS_WriteRegDev8(DENC_BASE + 0x3c, value1 | 0x7);
      STSYS_WriteRegDev8(DENC_BASE + 0x0c, value2 | WSS_ENABLE);
      break;
  case STM_WSS_ASPECT_RATIO_14_9:
      STSYS_WriteRegDev8(DENC_BASE + 0x3c, value1 | 0x1);
      STSYS_WriteRegDev8(DENC_BASE + 0x0c, value2 | WSS_ENABLE);
      break;
  case STM_WSS_ASPECT_RATIO_GT_16_9:
      STSYS_WriteRegDev8(DENC_BASE + 0x3c, value1 | 0xD);
      STSYS_WriteRegDev8(DENC_BASE + 0x0c, value2 | WSS_ENABLE);
      break;
  default:
      break;
  }

  value1 = STSYS_ReadRegDev8(DENC_BASE + 0x40) & CGMS_A_MASK;
  value2 = STSYS_ReadRegDev8(DENC_BASE + 0x0c) & ~CGMS_A_ENABLE;
  switch(wss_config.cgms_a)
  {
    case STM_WSS_CGMS_A_DISABLED:
      STSYS_WriteRegDev8(DENC_BASE + 0x0c, value2 );
      break;
    case STM_WSS_CGMS_A_COPY_RESTRICTED:
      STSYS_WriteRegDev8(DENC_BASE + 0x40, value1 | (1<<7) );
      STSYS_WriteRegDev8(DENC_BASE + 0x0c, value2 | CGMS_A_ENABLE);
      break;
    case STM_WSS_CGMS_A_COPY_NOT_RESTRICTED:
      STSYS_WriteRegDev8(DENC_BASE + 0x40, value1);
      STSYS_WriteRegDev8(DENC_BASE + 0x0c, value2 | CGMS_A_ENABLE);
      break;
  }
}
