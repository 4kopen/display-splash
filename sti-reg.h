/*
* This file is part of Splash Screen
*
* Copyright 2013, STMicroelectronics - All Rights Reserved
* Author(s): mohamed-ali.fodha@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* Splash Screen may alternatively be licensed under GPL V2 license from ST.
*/

#ifndef _STI_REG_H
#define _STI_REG_H

#ifdef stx410

#include "stiH407-reg.h"
/* Reg Base Definiton */
#define  VTG_MAIN_BASE         STiH407_VTG_MAIN_BASE
#define  VTG_AUX_BASE          STiH407_VTG_AUX_BASE
#define  DENC_BASE             STiH407_DENC_BASE
#define  TVO_MAIN_PF_BASE      STiH407_TVO_MAIN_PF_BASE
#define  TVO_AUX_PF_BASE       STiH407_TVO_AUX_PF_BASE
#define  TVO_TTXT_BASE         STiH407_TVO_TTXT_BASE
#define  TVO_VIP_DENC_BASE     STiH407_TVO_VIP_DENC_BASE
#define  TVO_VIP_HDMI_BASE     STiH407_TVO_VIP_HDMI_BASE
#define  TVO_VIP_HDF_BASE      STiH407_TVO_VIP_HDF_BASE
#define  HD_FORMATTER_BASE     STiH407_HD_FORMATTER_BASE
#define  AWG_SYNC_BASE         STiH407_HD_FORMATTER_AWG
#define  SYSCFG_CORE           STiH407_SYSCFG_CORE
#define  SYS_CFG_HDTVOUT       SYS_CFG5131
#define  SYS_CFG_CLK_EN_HDTVOUT SYS_CFG5131_CLK_EN_HDTVOUT
#define  SYS_CFG_RST_N_HDTVOUT SYS_CFG5131_RST_N_HDTVOUT
#define  SYS_CFG_DAC_CTRL      SYS_CFG5072
#define  HDMI_BASE             STiH407_HDMI_BASE
#define  MIXER1_BASE           STiH407_MIXER1_BASE
#define  MIXER2_BASE           STiH407_MIXER2_BASE
#define  GDP_MAIN_BASE         STiH407_GDP3_BASE
#define  GDP_AUX_BASE          STiH407_GDP4_BASE

/* Constant Definition */
#define  GAM_MIXER_MAIN_CTL    0x21     /* GDP3 + BCKG color */
#define  GAM_MIXER_MAIN_CRB    0x2e899
#define  GAM_MIXER_AUX_CTL     0x41     /* GDP4 + BCKG color */
#define  GAM_MIXER_AUX_CRB     0xd1a

#elif stx418
#include "stiH418-reg.h"
/* Reg Base Definiton */
#define  VTG_MAIN_BASE         STiH418_VTG_MAIN_BASE
#define  VTG_AUX_BASE          STiH418_VTG_AUX_BASE
#define  DENC_BASE             STiH418_DENC_BASE
#define  TVO_MAIN_PF_BASE      STiH418_TVO_MAIN_PF_BASE
#define  TVO_AUX_PF_BASE       STiH418_TVO_AUX_PF_BASE
#define  TVO_TTXT_BASE         STiH418_TVO_TTXT_BASE
#define  TVO_VIP_DENC_BASE     STiH418_TVO_VIP_DENC_BASE
#define  TVO_VIP_HDMI_BASE     STiH418_TVO_VIP_HDMI_BASE
#define  TVO_VIP_HDF_BASE      STiH418_TVO_VIP_HDF_BASE
#define  HD_FORMATTER_BASE     STiH418_HD_FORMATTER_BASE
#define  AWG_SYNC_BASE         STiH418_HD_FORMATTER_AWG
#define  SYSCFG_CORE           STiH418_SYSCFG_CORE
#define  SYS_CFG_HDTVOUT       SYS_CFG5131
#define  SYS_CFG_CLK_EN_HDTVOUT SYS_CFG5131_CLK_EN_HDTVOUT
#define  SYS_CFG_RST_N_HDTVOUT SYS_CFG5131_RST_N_HDTVOUT
#define  SYS_CFG_DAC_CTRL      SYS_CFG5072
#define  HDMI_BASE             STiH418_HDMI_BASE
#define  MIXER1_BASE           STiH418_MIXER1_BASE
#define  MIXER2_BASE           STiH418_MIXER2_BASE
#define  GDP_MAIN_BASE         STiH418_GDP1_BASE
#define  GDP_AUX_BASE          STiH418_GDP6_BASE

/* Constant Definition */
#define  GAM_MIXER_MAIN_CTL    0x9  /* GDP1 + BCKG color */
#define  GAM_MIXER_MAIN_CRB    0x3
#define  GAM_MIXER_AUX_CTL     0x101 /* GDP6 + BCKG color */
#define  GAM_MIXER_AUX_CRB     0x8

#elif stx390
#include "stiH390-reg.h"
/* Reg Base Definiton */
#define  VTG_MAIN_BASE         STiH390_VTG_MAIN_BASE
#define  VTG_AUX_BASE          STiH390_VTG_AUX_BASE
#define  DENC_BASE             STiH390_DENC_BASE
#define  TVO_MAIN_PF_BASE      STiH390_TVO_MAIN_PF_BASE
#define  TVO_AUX_PF_BASE       STiH390_TVO_AUX_PF_BASE
#define  TVO_TTXT_BASE         STiH390_TVO_TTXT_BASE
#define  TVO_VIP_DENC_BASE     STiH390_TVO_VIP_DENC_BASE
#define  TVO_VIP_HDMI_BASE     STiH390_TVO_VIP_HDMI_BASE
#define  TVO_VIP_HDF_BASE      STiH390_TVO_VIP_HDF_BASE
#define  HD_FORMATTER_BASE     STiH390_HD_FORMATTER_BASE
#define  AWG_SYNC_BASE         STiH390_HD_FORMATTER_AWG
#define  SYSCFG_CORE           STiH390_SYSCFG_CORE
#define  SYS_CFG_HDTVOUT       SYS_CFG6003
# define SYS_CFG_CLK_EN_HDTVOUT SYS_CFG6003_CLK_EN_HDTVOUT
# define SYS_CFG_RST_N_HDTVOUT SYS_CFG6003_RST_N_HDTVOUT
#define  SYS_CFG_COMPO         SYS_CFG6002
# define SYS_CFG_CLK_EN_COMPO  SYS_CFG6002_CLK_EN_COMPO_0
# define SYS_CFG_RST_N_COMPO   SYS_CFG6002_RST_N_COMPO_0
#define  SYS_CFG_VDP           SYS_CFG6001
# define SYS_CFG_CLK_EN_VDP    SYS_CFG6001_CLK_EN_VDP_AUX_0
# define SYS_CFG_RST_N_VDP     SYS_CFG6001_RST_N_VDP_AUX_
#define  SYS_CFG_HQVDP         SYS_CFG6000
# define SYS_CFG_CLK_EN_HQVDP  SYS_CFG6000_CLK_EN_HQVDP_0
# define SYS_CFG_RST_N_HQVDP   SYS_CFG6000_RST_N_HQVDP_0
#define  SYS_CFG_DAC_CTRL      SYS_CFG6404
#define  HDMI_BASE             STiH390_HDMI_BASE
#define  HDMI_PHY_BASE         STiH390_HDMI_TX_PHY_BASE
#define  MIXER1_BASE           STiH390_MIXER0_BASE
#define  MIXER2_BASE           STiH390_MIXER1_BASE
#define  GDP_MAIN_BASE         STiH390_COMPOSITOR_BASE+STiH390_GDP5_OFFSET
#define  GDP_AUX_BASE          STiH390_COMPOSITOR_BASE+STiH390_GDP6_OFFSET

/* Constant Definition */
#define  GAM_MIXER_MAIN_CTL    0x81  /* GDP5 + BCKG color */
#define  GAM_MIXER_MAIN_CRB    0x7
#define  GAM_MIXER_AUX_CTL     0x0000101 /* GDP6 + BCKG color  + P2I disabled*/
#define  GAM_MIXER_AUX_CRB     0x8

#endif

#endif /* _STI_REG_H */
