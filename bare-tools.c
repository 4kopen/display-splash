/*
* This file is part of Splash Screen
*
* Copyright 2013, STMicroelectronics - All Rights Reserved
* Author(s): mohamed-ali.fodha@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* Splash Screen may alternatively be licensed under GPL V2 license from ST.
*/

#include "sys.h"

#define PAGE_SIZE                 0x100
#define _ALIGN_UP(addr, size)     (((addr)+((size)-1))&(~((size)-1)))

void *splash_memcpy(void *dest, const void * const src, const U32 len)
{
  U32 l = len;
  char* dst8 = (char*)dest;
  char* src8 = (char*)src;
  while (l--)
  {
    *dst8++ = *src8++;
  }
  return dest;
}

void* splash_memset(void *ptr, const int ch, const U32 len)
{
  char *p =(char *) ptr;
  U32 i;
  for (i=0; i<len; i++)
  {
    p[i] = ch;
  }
  return ptr;
}

void* splash_malloc(const U32 size, const U16 align)
{
  static U32 mem_alloc_addr = (U32) ((DISPLAY_SPLASH_MEM_BASE+DISPLAY_SPLASH_MEM_SIZE)&(~((0x1000)-1)));

  U32 ptr, mem_alloc_size;

  if(align > PAGE_SIZE)
    mem_alloc_size = _ALIGN_UP(size, align);
  else
    mem_alloc_size = _ALIGN_UP(size, PAGE_SIZE);

  ptr = (mem_alloc_addr - mem_alloc_size);
  if(ptr < DISPLAY_SPLASH_MEM_BASE)
    return NULL;

  mem_alloc_addr = ptr;

  return (void*)ptr;
}
