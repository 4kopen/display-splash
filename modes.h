/*
* This file is part of Splash Screen
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
* Author(s): mohamed-ali.fodha@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* Splash Screen may alternatively be licensed under GPL V2 license from ST.
*/

#ifndef __MODES_H
#define __MODES_H

#include "splash.h"

typedef struct stm_display_s
{
  splash_modes_t mode;
  struct {
  unsigned int width;
  unsigned int height;
  };
} stm_display_t;

typedef struct sp_clock_def_s
{
  char          *clk_name;
  char          *parent_name;
  unsigned int  clk_rate;
} sp_clock_def_t;

typedef struct stm_clock_s
{
  /* bit field of main modes, could be replaced by a new enum values: ED/HD/FHD.. */
  unsigned int main_modes;
  unsigned int nb_clocks;
  const sp_clock_def_t *clocks;
} stm_clock_t;

typedef struct vtg_s
{
  unsigned int vtg0;
  unsigned int vtg1;
  unsigned int vtg2;
  unsigned int vtg3;
  unsigned int vtg4;
  unsigned int vtg5;
  unsigned int vtg6;
  unsigned int vtg7;
  unsigned int vtg8;
  unsigned int vtg9;
  unsigned int vtg10;
  unsigned int vtg11;
  unsigned int vtg12;
  unsigned int vtg13;
  unsigned int vtg14;
  unsigned int vtg15;
  unsigned int vtg16;
} vtg_t;

typedef struct stm_vtg_s
{
  splash_modes_t mode;
  vtg_t vtg;
} stm_vtg_t;

typedef struct vmix_s
{
  unsigned int vmix0;
  unsigned int vmix1;
  unsigned int vmix2;
  unsigned int vmix3;
} vmix_t;

typedef struct stm_vmix_s
{
  splash_modes_t mode;
  vmix_t vmix;
} stm_vmix_t;

typedef struct hdmi_s
{
  unsigned int hdmi0;
  unsigned int hdmi1;
  unsigned int hdmi2;
  unsigned int hdmi3;
  unsigned int hdmi4;
  unsigned int hdmi5;
  unsigned int hdmi6;
  unsigned int hdmi7;
  unsigned int hdmi8;
  unsigned int hdmi9;
  unsigned int hdmi10;
} hdmi_t;

typedef struct stm_hdmi_s
{
  splash_modes_t mode;
  hdmi_t hdmi;
} stm_hdmi_t;

typedef struct stm_reg_s
{
  unsigned int offset;
  unsigned int value;
} stm_reg_t;


#endif /* __MODES_H */
