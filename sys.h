/*
* This file is part of Splash Screen
*
* Copyright 2013, STMicroelectronics - All Rights Reserved
* Author(s): mohamed-ali.fodha@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* Splash Screen may alternatively be licensed under GPL V2 license from ST.
*/

#ifndef __SYS_H
#define __SYS_H

#include <stdio.h>

typedef unsigned char  U8;
typedef unsigned short U16;
typedef unsigned int   U32;
typedef volatile U32   STSYS_DU32;
typedef volatile U8    STSYS_DU8;

#define N_ELEMENTS(array) (sizeof (array) / sizeof ((array)[0]))

#define splash_print(fmt, ...) printf(fmt, ##__VA_ARGS__)
#ifdef DEBUG
#define splash_debug(fmt,...) printf(fmt, ##__VA_ARGS__)
#else
#define splash_debug(fmt,...)
#endif

/* ------------------------------------------------------- */
/* U8 STSYS_ReadRegDev8(void *Address_p); */
/* ------------------------------------------------------- */
#define STSYS_ReadRegDev8(Address_p) (*((STSYS_DU8 *) (Address_p)))

/* ------------------------------------------------------- */
/* void STSYS_WriteRegDev8(void *Address_p, U8 Value); */
/* ------------------------------------------------------- */
#define STSYS_WriteRegDev8(Address_p, Value)                       \
    {                                                              \
        *((STSYS_DU8 *) (Address_p)) = (U8) (Value);               \
    }

/* ------------------------------------------------------- */
/* U32 STSYS_ReadRegDev32LE(void *Address_p); */
/* ------------------------------------------------------- */
#define STSYS_ReadRegDev32LE(Address_p) (*((STSYS_DU32 *) (Address_p)))

/* ------------------------------------------------------- */
/* void STSYS_WriteRegDev32LE(void *Address_p, U32 Value); */
/* ------------------------------------------------------- */

#define STSYS_WriteRegDev32LE(Address_p, Value)                   \
    {                                                             \
        *((STSYS_DU32 *) (Address_p)) = (U32) (Value);            \
    }

#endif /* #ifndef __SYS_H */

/* End of sys.h */
