/*
* This file is part of Validation CLK Driver (VCLKD).
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
* Author(s): fabrice.charpentier@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* VCLKD may alternatively be licensed under GPL V2 license from ST.
*/

#include "vclk.h"
#include <stddef.h>
#include "vclk_private.h"
#include <string.h>
#include "vclk_flexgen.h"

/*
 * Private variables
 */

static int vclk_init_done = 0;
/* VCLKD functional modes
   mode=0 => exclusive clocks control (ex: single driver)
   mode=1 => shared control. Must refresh internal
             infos prior returning them.
 */
static int vclk_mode=0;

/*
 * Private functions
 */

/* Look for child clocks of 'parent'.
   If 'parent' is NULL, looking for top level clocks
 */
static struct vclk *find_childs(struct vclk *parent)
{
    int i;
    struct vclk *clk, **prevnext = NULL, *firsttop = NULL;

    /* Now updating infos from HW and creating tree */
    for (i = 0; i < vclk_nb; i++) {
        clk = &vclk_clocks[i];
        if (!clk->name)
            continue; // Undefined
        if (clk == parent)
            continue; // I'm the parent

        /* Updating infos from HW is not already done */
        if (!(clk->flags & VCLK_UPD))
            vclk_update(clk, 0);

        if (clk->parent != parent)
            continue;

        if (parent) {
// printf("%s is a child of %s\n", clk->name, parent->name);
            /* Append at end of current childs list */
            struct vclk **clk2;

            for (clk2 = &parent->child; *clk2; clk2 = &(*clk2)->next);
            *clk2 = clk;
        } else {
// printf("%s is a TOP level clock\n", clk->name);
            if (prevnext)
                *prevnext = clk;
            prevnext = &clk->next;
            if (!firsttop)
                firsttop = clk;
        }

        find_childs(clk);
    }

    return firsttop;
}

/* Recompute clock rate from HW status */
static int recalc(struct vclk *clk)
{
    struct vclk_group *group;

    if (clk->flags & VCLK_FIXED) {
        vclk_debug(1, "recalc(%s)\n", clk->name);
        vclk_debug(1, "  rate=%lu (FIXED)\n", clk->rate);
        return 0; // FIXED clock
    }

    group = vclk_get_group(clk);
    if (group && group->recalc_rate)
        group->recalc_rate(clk);
    else
        /* If no group or no recalc, assuming ON */
        clk->flags &= ~VCLK_OFF;

    return 0;
}

/* 'src' info has changed. Propagating to childs.
   'src' = changed source */
static void propagate(struct vclk *src)
{
    int i;
    struct vclk_group *group;

    for (i = 0; i < vclk_nb; i++) {
        if (!vclk_clocks[i].name)
            continue;   // Undefined
        if (&vclk_clocks[i] == src)
            continue;   // I'm the source
        if (!vclk_clocks[i].parent)
            continue;   // No parent
        if (vclk_clocks[i].parent != src)
            continue;   // Not the source of this clock

        /* 'vclk_clocks[i]' is sourced from 'src' which has changed.
           Updating xable infos
           then recomputing its rate */
        if ((src->flags & VCLK_OFF) || ((src->flags & VCLK_POFF)))
            vclk_clocks[i].flags |= VCLK_POFF;
        else
            vclk_clocks[i].flags &= ~VCLK_POFF;
        group = vclk_get_group(&vclk_clocks[i]);
        if (group && group->recalc_rate) {
            group->recalc_rate(&vclk_clocks[i]);
            propagate(&vclk_clocks[i]);
        }
    }
}

/*
 * Exported functions
 */

/* Clock driver init.
   To be done prior to any vclk API use
   mode: 0=VCLK has exclusive control on clocks setup
         1=VCLK shares clocks control and must refresh infos from HW
   Returns: 0=ok, -1=ERROR
 */
int vclk_init(int mode)
{
    vclk_mode = mode;
    vclk_debug(2, "VCLKD mode = %d\n", vclk_mode);
    vclk_soc_init();

    /* If exclusive clocks control, let's read HW status once */
    if (vclk_mode == 0) {
        int i;

        for (i = 0; i < vclk_nb; i++)
            vclk_clocks[i].flags &= ~VCLK_UPD;
        for (i = 0; i < vclk_nb; i++) {
            if (!vclk_clocks[i].name)
                continue; // Undefined
            vclk_update(&vclk_clocks[i], 0);
        }
    }
    vclk_init_done = 1;

    return 0;
}

/* Look for clock name and return 'struct vclk'
   Returns: pointer to 'struct vclk' or NULL if unknown
 */
struct vclk *vclk_get(char *name)
{
    int i;

    if (!vclk_init_done) {
        vclk_error("ERROR: vclk_get(%s), init not done yet\n", name);
        return NULL;
    }

    for (i = 0; i < vclk_nb; i++) {
        if (!vclk_clocks[i].name)
            continue; // Undefined
        if (!strcmp(vclk_clocks[i].name, name))
            return &vclk_clocks[i];
    }

    vclk_error("ERROR: '%s' is UNKNOWN\n", name);
    return NULL;
}

/* Look for clock identifier and return 'struct vclk'
   Returns: pointer to 'struct vclk' or NULL if unknown
 */
struct vclk *vclk_get_id(int id)
{
    if (!vclk_init_done) {
        vclk_error("ERROR: vclk_get_id(%d), init not done yet\n", id);
        return NULL;
    }

    if (id >= vclk_nb)
        return NULL; // Out of range
    if (!vclk_clocks[id].name)
        return NULL; // Undefined

    return &vclk_clocks[id];
}

/* A clock can belong to a flexiclockgen, that itself belongs
   to a clock group.
   Return: the top level group for this clock or NULL if none */
struct vclk_group *vclk_get_group(struct vclk *clk)
{
    struct vclk_group *group = NULL;

    if (clk) {
        if (clk->flags & VCLK_FG) {
            struct flexgen *fg = (struct flexgen *)clk->group;
            if (fg)
                group = fg->group;
        } else
            group = (struct vclk_group *)clk->group;
    }

    return group;
}

/* Update clock infos & rate from HW
   - identify source
   - then read programmed rate & xable infos
   mode: 0=take 'VCLK_UPD' flag into account
         1=always update parent
   Returns: 0=OK, -1=ERROR
 */
int vclk_update(struct vclk *clk, int mode)
{
    int err = 0;
    struct vclk_group *group = vclk_get_group(clk);

    if (!clk)
        return -1;

    vclk_debug(1, "vclk_update(%s, mode=%d)\n", clk->name, mode);

    /* Identify parent clock except if
       - clock is fixed (ex: OSC input)
       - clock has a single source
     */
    if (!(clk->flags & VCLK_SINGLESRC) && !(clk->flags & VCLK_FIXED))
        if (group && group->identify_parent)
            err = group->identify_parent(clk);

    /* Updating source infos if required */
    if (!err && clk->parent) {
        if ((mode == 1) || !(clk->parent->flags & VCLK_UPD))
            err = vclk_update(clk->parent, mode);
    }

    /* Finally computing rate & infos */
    if (!err) {
        recalc(clk);
        if (clk->parent && ((clk->parent->flags & VCLK_OFF) || (clk->parent->flags & VCLK_POFF)))
            clk->flags |= VCLK_POFF;
        else
            clk->flags &= ~VCLK_POFF;
    }

    clk->flags |= VCLK_UPD;

    if (err)
        vclk_error("ERROR: vclk_update(%s)\n", clk->name);
    return err;
}

/* Get 'clk' programmed rate even if disabled.
   Note that vclk_update() must be called first if
     vclkd is not the exclusive clock driver.
   Returns: rate or 0 if ERROR */
unsigned long vclk_get_rate(struct vclk *clk)
{
    if (!vclk_init_done)
        return 0;
    if (!clk)
        return 0;

    vclk_debug(1, "vclk_get_rate(%s)\n", clk->name);

    if (vclk_mode == 1) {
        vclk_update(clk, 1); // Update status from HW first
    }
    return clk->rate;
}

/* Check if 'clk' clock is running.
   Note that vclk_update() must be called first if
     vclkd is not the exclusive clock driver.
   Return: 1=TRUE, 0=FALSE/ERROR */
int vclk_is_running(struct vclk *clk)
{
    if (!clk)
        return 0;
    if (!vclk_init_done) {
        vclk_error("ERROR: vclk init not done\n");
        return 0;
    }

    vclk_debug(1, "vclk_is_running(%s)\n", clk->name);

    if (vclk_mode == 1) {
        vclk_update(clk, 1); // Update status from HW first
    }
    if ((clk->flags & VCLK_OFF) || (clk->flags & VCLK_POFF))
        return 0;
    return 1;
}

/* Check if 'clkname' clock is running.
   Note that vclk_update() must be called first if
     vclkd is not the exclusive clock driver.
   Return: 1=TRUE, 0=FALSE/ERROR */
int vclk_is_running_name(char *clkname)
{
    struct vclk *clk;

    if (!clkname)
        return 0;
    if (!vclk_init_done) {
        vclk_error("ERROR: vclk init not done\n");
        return 0;
    }

    clk = vclk_get(clkname);
    if (!clk)
        return 0;
    return vclk_is_running(clk);
}

/* Enable/disable clock at HW level, update infos,
   then propagate to childs.
   Returns: 0=OK, -1=ERROR
 */
int vclk_xable(struct vclk *clk, int enable)
{
    int err = 0;
    struct vclk_group *group = vclk_get_group(clk);

    if (!vclk_init_done) {
        vclk_error("ERROR: Lib init must be done first\n");
        return -1;
    }
    if (!clk) {
        vclk_error("ERROR: lost in vclk_xable(), clk=NULL\n");
        return -1;
    };

    if (!group || !group->xable)
        return 0; // No enable/disable feature

    err = group->xable(clk, enable);
    /* Need to read back HW infos here
       to remove from SOC code */
    /* Propagate only if exclusive mode.
       If not not needed since it will be
       forced on childs clocks read */
    if (!err & !vclk_mode) {
        recalc(clk);
        propagate(clk);
    }

    return err;
}

/* Set 'clk' rate with given parent rate.
   Useful to set a clock prior to its parent.
   Returns: 0=OK, -1=ERROR */
int vclk_set_ratep(struct vclk *clk, unsigned long rate, unsigned long prate)
{
    struct vclk_group *group = vclk_get_group(clk);
    int err = 0;

    if (!vclk_init_done) {
        vclk_error("ERROR: Lib init must be done first\n");
        return -1;
    }
    if (!clk) {
        vclk_error("ERROR: vclk_set_ratep() bad params; clk=NULL\n");
        return -1;
    };
    if (!prate) {
        vclk_error("ERROR: vclk_set_ratep() bad params; prate=0\n");
        return -1;
    };

    if (!group || !group->set_ratep)
        return -1;

    err = group->set_ratep(clk, rate, prate);
    /* Need to read back HW infos here
       to remove from SOC code */
    /* Propagate only if exclusive mode.
       If not not needed since it will be
       forced on childs clocks read */
    if (!err & !vclk_mode) {
        recalc(clk);
        propagate(clk);
    }

    return err;
}

/* Set 'clk' rate with CURRENT parent rate.
   Returns: 0=OK, -1=ERROR */
int vclk_set_rate(struct vclk *clk, unsigned long rate)
{
    unsigned long prate = 0;

    if (!vclk_init_done) {
        vclk_error("ERROR: Lib init must be done first\n");
        return -1;
    }
    if (!clk) {
        vclk_error("ERROR: lost in vclk_set_rate(), clk=NULL\n");
        return -1;
    };

    /* If shared clock control we must read HW to know
       parent rate */
    if (vclk_mode == 1) // Shared clock control
        vclk_update(clk, 1);
    if (clk->parent)
        prate = clk->parent->rate;
    return vclk_set_ratep(clk, rate, prate);
}

/* Route clock for observation if possible
   Returns: 0=ok, -1=error
 */
int vclk_observe(struct vclk *clk, unsigned long *div)
{
    struct vclk_group *group = vclk_get_group(clk);

    if (!vclk_init_done) {
        vclk_error("ERROR: Lib init must be done first\n");
        return -1;
    }

    if (!group || !group->observe)
        return -1;

    return group->observe(clk, div);
}

/* Change clock source (when possible)
   Returns: 0=ok, -1=error
 */
int vclk_set_parent(struct vclk *clk, struct vclk *src)
{
    struct vclk_group *group = vclk_get_group(clk);

    if (!vclk_init_done) {
        vclk_error("ERROR: Lib init must be done first\n");
        return -1;
    }

    if (!group || !group->set_parent)
        return -1;

    return group->set_parent(clk, src);
}

/* Parse clocks list and call 'func' for each clock
 */
int vclk_for_each(void (*func)(struct vclk *clk, void *data), void *data)
{
    int i;

    if (!vclk_init_done) {
        vclk_error("ERROR: Lib init must be done first\n");
        return -1;
    }

    for (i = 0; i < vclk_nb; i++) {
        if (!vclk_clocks[i].name)
            continue; // Undefined
        func(&vclk_clocks[i], data);
    }

    return 0;
}

/* Returns length of longest clock name
   or -1 if ERROR */
int vclk_max_len(void)
{
    int max = 0, i, l;

    if (!vclk_init_done) {
        vclk_error("ERROR: Lib init must be done first\n");
        return -1;
    }

    for (i = 0; i < vclk_nb; i++) {
        if (!vclk_clocks[i].name)
            continue; // Undefined
        l = strlen(vclk_clocks[i].name);
        if (l > max)
            max = l;
    }

    return max;
}

/* Parse clocks list and create a clock tree
   updating 'next' & 'child' fields.
   Note that full update from HW is done.
   Returns: -1=ERROR, 0=OK
 */
int vclk_tree(struct vclk **tree, char *clkname)
{
    int i;

    if (!vclk_init_done) {
        vclk_error("ERROR: Lib init must be done first\n");
        return -1;
    }

    /* Resetting VCLK_UPD flag as well as 'child' & 'next' fields */
    for (i = 0; i < vclk_nb; i++) {
        vclk_clocks[i].flags &= ~(VCLK_UPD);
        vclk_clocks[i].next = NULL;
        vclk_clocks[i].child = NULL;
    }

    /* Now updating infos from HW and creating tree */
    *tree = find_childs(NULL);

    return 0;
}
