/*
* This file is part of Validation CLK Driver (VCLKD).
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
* Author(s): fabrice.charpentier@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* VCLKD may alternatively be licensed under GPL V2 license from ST.
*/

/* LLA version: YYYYMMDD */
#define VCLK_SOC_VERSION "20150910"

enum {
    /* SOC input clocks */
    CLK_SYSIN,

    /* CA9 PLL4600 */
    CLK_A9_REF,
    CLK_A9_FVCOBY2,         /* PLL4600 FVCOBY2 */
    CLK_A9_PHI0,            /* PLL4600 PHI0 (FVCOBY2/ODF0) */
    CLK_A9,                 /* CA9 clock */
    CLK_A9_PERIPHS,         /* CA9.gt CA9.twd clock */

    /* Clockgen A0 (LMI)*/
    CLK_A0_REF,             /* refclkin[0] */
    CLK_A0_REFOUT0,         /* refclkout[0] = gfg0 in */
    CLK_A0_PLL0_VCO,        /* GFG0: PLL3200 FVCOBY2 */
    CLK_A0_PLL0_PHI0,       /* GFG0: PLL3200 PHI0 (FVCOBY2/ODF0) */

    CLK_IC_LMI0,            /* Div0 */
    CLK_IC_LMI1,            /* Div1 */
    CLK_A0_SPARE_2,
    CLK_A0_SPARE_3,
    CLK_A0_SPARE_4,
    CLK_A0_SPARE_5,         /* Div5 */
    CLK_A0_SPARE_6,
    CLK_A0_SPARE_7,
    CLK_A0_SPARE_8,
    CLK_A0_SPARE_9,
    CLK_A0_SPARE_10,        /* Div10 */
    CLK_A0_SPARE_11,
    CLK_A0_SPARE_12,
    CLK_A0_SPARE_13,
    CLK_A0_SPARE_14,        /* Div14 */

    /* Clockgen C0 (main system clocks) */
    CLK_C0_REF,             /* refclkin[0] */
    CLK_C0_REFOUT0,         /* refclkout[0] = gfg0 in */
    CLK_C0_REFOUT1,         /* refclkout[1] = gfg1 in */
    CLK_C0_REFOUT2,         /* refclkout[2] = gfg2 in */
    CLK_C0_PLL0_VCO,        /* GFG0: PLL3200 FVCOBY2 */
    CLK_C0_PLL0_PHI0,       /* GFG0: PLL3200 PHI0 (FVCOBY2/ODF0) */
    CLK_C0_PLL1_VCO,        /* GFG1: PLL3200 FVCOBY2 */
    CLK_C0_PLL1_PHI0,       /* GFG1: PLL3200 PHI0 (FVCOBY2/ODF0) */
    CLK_C0_FS0_VCO,         /* GFG2: FS embedded VCOCLK */
    CLK_C0_FS0_CH0,         /* FS channel 0 */
    CLK_C0_FS0_CH1,         /* FS channel 1 */
    CLK_C0_FS0_CH2,         /* FS channel 2 */
    CLK_C0_FS0_CH3,         /* FS channel 3 */

    CLK_C0_JEIN_0,          /* je_in[0] */
    CLK_HDMIRXFSYN_2,       /* je_in[1] */
    CLK_HDMIRXFSYN_3,       /* je_in[2] */
    CLK_HDMITX_PLLINFOUT,   /* je_in[3] */
    CLK_HDMITX_PLLREFOUT,   /* je_in[4] */
    CLK_HDMITX_TMDSCLK,     /* je_in[5] */

    CLK_ICN_GPU,            /* Div0 */
    CLK_FDMA,               /* Div1 */
    CLK_NAND,               /* Div2 */
    CLK_HVA,
    CLK_PROC_STFE,
    CLK_TP,
    CLK_RX_ICN_DMU,         /* Div6 */
    CLK_RX_ICN_HVA,         /* Div7 */
    CLK_C0_SPARE8,          /* Div8 */
    CLK_TX_ICN_DMU,         /* Div9 */
    CLK_MMC_0,              /* Div10 */
    CLK_MMC_1,              /* Div11 */
    CLK_JPEGDEC,
    CLK_ICN_REG,            /* Div13 */
    CLK_IC_BDISP_0,
    CLK_IC_BDISP_1,
    CLK_PP_DMU,
    CLK_VID_DMU,
    CLK_DSS_LPC,
    CLK_ST231_AUD_0,        /* Div19 */
    CLK_ST231_GP_1,
    CLK_ST231_DMU,
    CLK_ICN_LMI,
    CLK_TX_ICN_1,           /* Div23 */
    CLK_ICN_SBC,
    CLK_STFE_FRC2,
    CLK_ETH_PHY,            /* Div26 */
    CLK_ETH_PHYREF,         /* Div27 */
    CLK_FLASH_PROMIP,       /* Div28 */
    CLK_MAIN_DISP,
    CLK_AUX_DISP,
    CLK_COMPO_DVP,          /* Div31 */
    CLK_TX_ICN_HADES,       /* Div32 */
    CLK_RX_ICN_HADES,       /* Div33 */
    CLK_ICN_REG_16,         /* Div34 */
    CLK_PP_HEVC,            /* Div35 */
    CLK_CLUST_HEVC,         /* Div36 */
    CLK_HWPE_HEVC,          /* Div37 */
    CLK_FC_HEVC,            /* Div38 */
    CLK_PROC_MIXER,         /* Div39 */
    CLK_PROC_SC,            /* Div40 */
    CLK_AVSP_HEVC,          /* Div41 */

    CLK_ICN_REG_DIV2,       /* CLK_ICN_REG / 2 */

    /* C0 gated clocks */
    CLK_DMU_BUS,            /* Src=C0-9 */
    CLK_DMU_TARG,           /* Src=C0-13 */
    CLK_DMU_PP,             /* Src=C0-16 */
    CLK_DMU_CPU,            /* Src=C0-21 */
    CLK_STBE_TP,            /* Src=C0-5 */
    CLK_STBE_SYSTEM,        /* Src=C0-9 */
    CLK_STBE_CONFIG,        /* Src=C0-13 */
    CLK_HEVC_BUS,           /* Src=C0-32 */
    CLK_HEVC_TARG,          /* Src=C0-34 */
    CLK_HEVC_PP,            /* Src=C0-35 */
    CLK_HEVC_CLUST,         /* Src=C0-36 */
    CLK_HEVC_HWPE,          /* Src=C0-37 */
    CLK_HEVC_FC,            /* Src=C0-38 */
    CLK_HEVC_ASVP,          /* Src=C0-41 */
    CLK_HQVDP0_SYS,         /* Src=C0-9 */
    CLK_HQVDP0_REG,         /* Src=C0-13 */
    CLK_HQVDP0_IQI,         /* Src=C0-29 */
    CLK_HQVDP0_PROC,        /* Src=C0-29 */
    CLK_HQVDP0_PIX,         /* Src=C0-39 */
    CLK_HQVDP1_SYS,         /* Src=C0-9 */
    CLK_HQVDP1_REG,         /* Src=C0-13 */
    CLK_HQVDP1_IQI,         /* Src=C0-29 */
    CLK_HQVDP1_PROC,        /* Src=C0-29 */
    CLK_HQVDP1_PIX,         /* Src=C0-39 */
    CLK_HQVDP2_SYS,         /* Src=C0-9 */
    CLK_HQVDP2_REG,         /* Src=C0-13 */
    CLK_HQVDP2_IQI,         /* Src=C0-29 */
    CLK_HQVDP2_PROC,        /* Src=C0-30 */
    CLK_HQVDP2_PIX,         /* Src=C0-39 */
    CLK_HVA_GATED,          /* Src=C0-3 */
    CLK_HVA_ICNDDR,         /* Src=C0-9 */
    CLK_HVA_ICNESRAM,       /* Src=C0-9 */
    CLK_HVA_ICNPERIPH,      /* Src=C0-13 */

    /* Clockgen D0 */
    CLK_D0_REF,             /* refclkin[0] */
    CLK_D0_REFOUT0,         /* refclkout[0] = gfg0 in */
    CLK_D0_FS0_VCO,         /* FS embedded VCO clock: not connected */
    CLK_D0_FS0_CH0,         /* FS channel 0 */
    CLK_D0_FS0_CH1,         /* FS channel 1 */
    CLK_D0_FS0_CH2,         /* FS channel 2 */
    CLK_D0_FS0_CH3,         /* FS channel 3 */

    CLK_D0_JEIN_0,          /* je_in[0] */
    CLK_MIPHYPS_0_RX,       /* je_in[1] = miphy_pciesata_0 rx */
    CLK_MIPHYPS_0_TX,       /* je_in[2] = miphy_pciesata_0 tx */
    CLK_MIPHYPS_1_RX,       /* je_in[3] = miphy_pciesata_1 rx */
    CLK_MIPHYPS_1_TX,       /* je_in[4] = miphy_pciesata_1 tx */
    CLK_MIPHYUSB3_RX,       /* je_in[5] = miphy_usb3 rx */
    CLK_MIPHYUSB3_TX,       /* je_in[6] = miphy_usb3 tx */

    CLK_USB2_PHY,           /* 'ext_refclkout0' to picophy */

    CLK_PCM_0,              /* Div0 */
    CLK_PCM_1,              /* Div1 */
    CLK_PCM_2,              /* Div2 */
    CLK_SPDIFF,             /* Div3 */
    CLK_PCMR0_MASTER,       /* Div4: PCM READER */
    CLK_D0_SPARE_5,
    CLK_D0_SPARE_6,
    CLK_D0_SPARE_7,
    CLK_D0_SPARE_8,
    CLK_D0_SPARE_9,
    CLK_D0_SPARE_10,
    CLK_D0_SPARE_11,
    CLK_D0_SPARE_12,
    CLK_D0_SPARE_13,        /* Div13 */

    /* Clockgen D2 (Video display and output stage) */
    CLK_D2_REF,             /* refclkin[0] */
    CLK_D2_REF1,            /* refclkin[1] but no connection */
    CLK_D2_REFOUT0,         /* refclkout[0] = gfg0 in */
    CLK_D2_FS0_VCO,         /* FS embedded VCO clock: not connected */
    CLK_D2_FS0_CH0,         /* FS channel 0 */
    CLK_D2_FS0_CH1,         /* FS channel 1 */
    CLK_D2_FS0_CH2,         /* FS channel 2 */
    CLK_D2_FS0_CH3,         /* FS channel 3 */

    CLK_TMDSOUT_HDMI,       /* srcclkin0: From HDMI TX PHY 'TMDSCK_OUT' */ /* WHAT is this clock */
    CLK_PIX_HD_P_IN,        /* srcclkin1: From PIX_HD_P pad */  /* WHAT is this clock */

    CLK_D2_JEIN0,           /* je_in[0]: from D0-13 */
    CLK_USB2_480,           /* je_in[2]: USB2tripPhy_CLK480M */
    CLK_USB2_PHY0,          /* je_in[3]: USB2tripPhy_PHYCLOCK0 */
    CLK_USB2_PHY1,          /* je_in[4]: USB2tripPhy_PHYCLOCK1 */
    CLK_USB2_PHY2,          /* je_in[5]: USB2tripPhy_PHYCLOCK2 */

    CLK_PIX_MAIN_DISP,      /* Div0 */
    CLK_D2_SPARE1,
    CLK_D2_SPARE2,
    CLK_D2_SPARE3,
    CLK_D2_SPARE4,
    CLK_TMDS_HDMI_DIV2,     /* Div5 */
    CLK_PIX_AUX_DISP,       /* Div6 */
    CLK_DENC,               /* Div7: DENC & FRC0 */
    CLK_PIX_HDDAC,          /* Div8 */
    CLK_HDDAC,
    CLK_SDDAC,              /* Div10 */
    CLK_PIX_DVO,
    CLK_DVO,                /* Div12 */
    CLK_PIX_HDMI,
    CLK_TMDS_HDMI,          /* Div14: to HDMI TX PHY 'TMDSCK' in */
    CLK_REF_HDMIPHY,        /* Div15: to HDMI TX PHY 'CkPxPLL' */
    CLK_D2_SPARE16,
    CLK_D2_SPARE17,
    CLK_D2_SPARE18,
    CLK_D2_SPARE19,
    CLK_D2_SPARE20,
    CLK_D2_SPARE21,
    CLK_D2_SPARE22,
    CLK_D2_SPARE23,
    CLK_D2_SPARE24,
    CLK_D2_SPARE25,
    CLK_D2_SPARE26,
    CLK_D2_SPARE27,
    CLK_D2_SPARE28,
    CLK_D2_SPARE29,
    CLK_D2_SPARE30,
    CLK_D2_SPARE31,
    CLK_D2_SPARE32,
    CLK_D2_SPARE33,
    CLK_D2_SPARE34,
    CLK_D2_SPARE35,
    CLK_D2_SPARE36,
    CLK_D2_SPARE37,
    CLK_D2_SPARE38,
    CLK_D2_SPARE39,
    CLK_D2_SPARE40,
    CLK_D2_SPARE41,
    CLK_D2_SPARE42,
    CLK_D2_SPARE43,
    CLK_D2_SPARE44,
    CLK_D2_SPARE45,
    CLK_D2_SPARE46,
    CLK_VP9,

    /* Clockgen D3 (TransportSS and MCHI) */
    CLK_D3_REF,             /* refclkin[0] */
    CLK_D3_REFOUT0,         /* refclkout[0] = gfg0 in */
    CLK_D3_FS0_VCO,         /* FS embedded VCO clock: not connected */
    CLK_D3_FS0_CH0,         /* FS channel 0 */
    CLK_D3_FS0_CH1,         /* FS channel 1 */
    CLK_D3_FS0_CH2,         /* FS channel 2 */
    CLK_D3_FS0_CH3,         /* FS channel 3 */

    CLK_ACG,                /* je_in[0] = acg_glue_sbc_10-clk_out */
    CLK_OSCI_32K,           /* je_in[1] = osci_32k_10-clk */
    CLK_HDMIRX_FS2,         /* je_in[2] = hdmi_rx_fsyn-clk_out_2 */
    CLK_HDMIRX_FS3,         /* je_in[3] = hdmi_rx_fsyn-clk_out_3 */

    CLK_STFE_FRC1,          /* Div0 */
    CLK_TSOUT_0,
    CLK_TSOUT_1,            /* Div2 */
    CLK_MCHI,
    CLK_VSENS_COMPO,        /* Div4 */
    CLK_FRC1_REMOTE,
    CLK_LPC_0,              /* Div6 */
    CLK_LPC_1
};
