/*
* This file is part of Validation CLK Driver (VCLKD).
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
* Author(s): fabrice.charpentier@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* VCLKD may alternatively be licensed under GPL V2 license from ST.
*/

#include <stddef.h>
#include "vclk.h"
#include "vclk_flexgen.h"
#include "vclk-h390.h"
#include "vclk_algos.h"
#include "vclk_private.h"
#include "vclk_sysconf.h"

/*
 * Privates
 */

/* Base addresses */
#define CKG_A0_BASE_ADDRESS     0x8060000
#define CKG_C0_BASE_ADDRESS     0x8000000
#define CKG_D0_BASE_ADDRESS     0x8030000
#define CKG_D1_BASE_ADDRESS     0x8040000
#define SYSCFG_000_999          0x09000000
#define SYSCFG_PIO_FRONT_0_BASE 0x8300000
#define SYSCFG_PIO_FRONT_1_BASE 0x8310000
#define SYSCFG_PIO_REAR_BASE    0x8320000
#define SYSCFG_2000_2999        0x0D000000
#define SYSCFG_6000_6999        0x15000000
#define A9_SYSCFG_BASE          0x261F0000

#define SYSTEM_CONFIG24002      (SYSCFG_PIO_FRONT_1_BASE + 0x8)
#define SYSTEM_CONFIG24040      (SYSCFG_PIO_FRONT_1_BASE + 0xc)
#define SYSTEM_CONFIG24050      (SYSCFG_PIO_FRONT_1_BASE + 0x10)
#define SYSTEM_CONFIG24060      (SYSCFG_PIO_FRONT_1_BASE + 0x18)

/* Prototypes */
struct vclk vclk_clocks[];

/* Sysconf base addresses declarations */
static struct sysconf_grp sysconf_groups[] = {
    { 2000, 2000, (void*)SYSCFG_2000_2999 },
    { 2010, 2100, (void*)SYSCFG_2000_2999 + 0x10000, 0x10000 },
    { 2400, 2999, (void*)SYSCFG_2000_2999 + 0x130000, 4 },
    { 6000, 6100, (void*)SYSCFG_6000_6999, 0x10000 },
    { 6400, 6999, (void*)SYSCFG_6000_6999 + 0x130000, 4 },
    { 22000, 22999, (void*)SYSCFG_PIO_REAR_BASE },
    { -1 }
};

#if defined(VCLK_VIRTUAL_PLATFORM1)
/* COEMUL: some clocks are injected and can't be read by SW.
   setifunknown=1 => if clock unknown, set 12121212 freq & returns 0
   setifunknown=0 => if clock unknown, returns -1
 */
static int vp_get_rate(struct vclk *clk, int setifunknown)
{
    static struct vpclk {
        int id;
        unsigned long freq; /* In MHz */
    } vp_clocks[] = {
        { CLK_A9_PHI, 1200},

        { CLK_A0_PLL0_VCO, 0 },
        { CLK_A0_PLL0_PHI0, 1066 },

        { CLK_C0_PLL0_VCO, 0 },
        { CLK_C0_PLL0_PHI0, 1000 },
        { CLK_C0_PLL0_PHI1, 1000 },
        { CLK_C0_PLL0_PHI2, 1000 },
        { CLK_C0_PLL0_PHI3, 1000 },
        { CLK_C0_PLL1_VCO, 0 },
        { CLK_C0_PLL1_PHI0, 1200 },
        { CLK_C0_PLL1_PHI1, 1200 },
        { CLK_C0_PLL1_PHI2, 1200 },
        { CLK_C0_PLL1_PHI3, 1200 },
        { CLK_C0_FS0_VCO, 0 },
        { CLK_C0_FS0_CH0, 100 },
        { CLK_C0_FS0_CH1, 100 },
        { CLK_C0_FS0_CH2, 36 },
        { CLK_C0_FS0_CH3, 27 },

        { CLK_D0_FS0_VCO, 0 },
        { CLK_D0_FS0_CH0, 100 },
        { CLK_D0_FS0_CH1, 100 },
        { CLK_D0_FS0_CH2, 100 },
        { CLK_D0_FS0_CH3, 200 },

        { CLK_D1_FS0_VCO, 0 },
        { CLK_D1_FS0_CH0, 148 },
        { CLK_D1_FS0_CH1, 240 },
        { CLK_D1_FS0_CH2, 27 },
        { CLK_D1_FS0_CH3, 200 },
    };
    int i, size;

    size = sizeof(vp_clocks) / sizeof(struct vpclk);
    for (i = 0; (i < size) && (clk->id != vp_clocks[i].id); i++);
    if (i == size) {
        /* Unknown clock */
        if (!setifunknown)
            return -1;
        clk->rate = 12121212;
    } else {
        clk->rate = vp_clocks[i].freq * 1000000;
        clk->flags &= ~VCLK_OFF; // Assuming enabled
    }

    return 0;
}
#endif

/* ========================================================================
   Name:        recalc
   Description: Get CKG GFG programmed clocks frequencies
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int recalc(struct vclk *clk)
{
    unsigned long prate;

    if (!clk)
        return -1;

    vclk_debug(1, "recalc(%s)\n", clk->name);

#if defined(VCLK_VIRTUAL_PLATFORM1)
    /* Clock injected ? */
    if (!vp_get_rate(clk, 0))
        return 0;
#endif

    /* Flexiclockgen lib handled clocks */
    if (clk->flags & VCLK_FG)
        return flexgen_recalc(clk);

    prate = vclk_get_prate(clk);

    switch (clk->id) {
    /* Special cases */
    case CLK_REF:
        clk->rate = prate;
        break;
    case CLK_EXT2F_A9_BY2:
        clk->rate = prate / 2;
        break;

    /* Unexpected clocks */
    default:
        return -1;
    }

    vclk_debug(1, "  rate=%lu\n", clk->rate);
    return 0;
}

/* ========================================================================
   Name:        identify_parent
   Description: Identify source clock
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int identify_parent(struct vclk *clk)
{
    if (!clk)
        return -1;

    vclk_debug(1, "identify_parent(%s)\n", clk->name);

    /* Flexiclockgen handled clocks */
    if (clk->flags & VCLK_FG)
        return flexgen_identify_parent(clk);

    /* Special cases */
    if (clk->id == CLK_REF) {
        uint32_t val;

        val = vclk_peek_p((void*)SYSCFG_000_999 + 0x130048); // SYSTEM_STATU418
        if (val & (1 << 7)) // WIFI ext ref clock ?
            clk->parent = &vclk_clocks[CLK_WIFI_40];
        else
            clk->parent = &vclk_clocks[CLK_MIPHY_30];
        return 0;
    }

    vclk_error("ERROR: identify_parent(%s). Unsupported clock\n", clk->name);
    return -1;
}

/* ========================================================================
   Name:        set_parent
   Description: Set clock parent
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int set_parent(struct vclk *clk, struct vclk *src)
{
    if (!clk)
        return -1;

    vclk_debug(1, "set_parent(%s, src=%s)\n", clk->name, src->name);

    /* Flexiclockgen handled clocks */
    if (clk->flags & VCLK_FG)
        return flexgen_set_parent(clk, src);

    return -1;
}

/* ========================================================================
   Name:        set_ratep
   Description: Set clock frequency
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int set_ratep(struct vclk *clk, unsigned long freq, unsigned long prate)
{
    if (!clk)
        return -1;

    vclk_debug(1, "set_ratep(%s, rate=%lu, prate=%lu)\n", clk->name, freq, prate);

#if defined(VCLK_VIRTUAL_PLATFORM1)
    /* If clock is injected, no way to set it */
    if (!vp_get_rate(clk, 0))
        return 0;
#endif

    /* Flexiclockgen handled clocks */
    if (clk->flags & VCLK_FG)
        return flexgen_set_ratep(clk, freq, prate);

    /* Unexpected clocks */
    return -1;
}

/* ========================================================================
   Name:        xable
   Description: Enable/disable clock
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int xable(struct vclk *clk, int enable)
{
    if (!clk)
        return -1;

    vclk_debug(1, "xable(%s, en=%d)\n", clk->name, enable);

#if defined(VCLK_VIRTUAL_PLATFORM1)
    /* If clock injected, can't enable it */
    if (!vp_get_rate(clk, 0))
        return 0;
#endif

    /* Flexiclockgen handled clocks */
    if (clk->flags & VCLK_FG)
        return flexgen_xable(clk, enable);

    /* Unexpected clocks */
    return -1;
}

/******************************************************************************
CA9 PLL
******************************************************************************/

/* ========================================================================
   Name:        a9_recalc
   Description: Get clocks frequencies (in Hz)
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int a9_recalc(struct vclk *clk)
{
    unsigned long odf, prate;

    if (!clk)
        return -1;

    vclk_debug(1, "a9_recalc(%s)\n", clk->name);

    clk->flags &= ~VCLK_OFF; // Assuming clock enabled
    prate = vclk_get_prate(clk);

    switch (clk->id) {
    case CLK_A9:
        clk->rate = prate;
        break;
    case CLK_A9_PERIPHS:
        clk->rate = prate / 2;
        break;
    case CLK_A9_PHI:
        {
            unsigned long a9_config8, a9_config10;
            struct vclk_pll pll = {
                .type = VCLK_PLL4600C28,
            };
            int err;

            a9_config8 = vclk_peek_p((void*)A9_SYSCFG_BASE + 0x20);
            a9_config10 = vclk_peek_p((void*)A9_SYSCFG_BASE + 0x28);

            if (a9_config8 & 1)
                /* A9_PLL_PD=1 => PLL disabled */
                clk->flags |= VCLK_OFF;

            /* Computing FVCOby2 */
            pll.idf = (a9_config8 >> 25) & 0x7;
            pll.ndiv = a9_config10 & 0xff;
            err = vclk_pll_get_rate(prate, &pll, &clk->rate);
            if (err != 0)
                return -1;
            vclk_debug(1, "  idf=%lu ndiv=%lu prate=%lu => rate=%lu\n", pll.idf, pll.ndiv, prate, clk->rate);

            /* Computing PHI */
            odf = (a9_config10 >> 8) & 0x3f;
            if (odf == 0)
                odf = 1;
            clk->rate /= odf;
        }
        break;
    default:
        return -1; /* Unknown clock */
    }

    return 0;
}

/* ========================================================================
   Name:        a9_identify_parent
   Description: Identify parent clock for clockgen A9 clocks.
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int a9_identify_parent(struct vclk *clk)
{
    unsigned long a9_config7;

    vclk_debug(1, "a9_identify_parent(%s)\n", clk->name);

    if (clk->id != CLK_A9) /* Other clocks have static parent */
        return 0;

    a9_config7 = vclk_peek_p((void*)A9_SYSCFG_BASE + 0x1C);

    /* Is CA9 clock sourced from PLL or C0-13 ? */
    if ((a9_config7 >> 1) & 1) {
        /* Yes. Div by 1 or div by 2 ? */
        if ((a9_config7 >> 0) & 1)
            clk->parent = &vclk_clocks[CLK_EXT2F_A9];
        else
            clk->parent = &vclk_clocks[CLK_EXT2F_A9_BY2];
    } else
        clk->parent = &vclk_clocks[CLK_A9_PHI];

    return 0;
}

/* ========================================================================
   Name:        a9_set_ratep
   Description: Set clock frequency
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int a9_set_ratep(struct vclk *clk, unsigned long freq, unsigned long prate)
{
    unsigned long odf, vcoby2_rate;
    struct vclk_pll pll = {
        .type = VCLK_PLL4600C28,
    };
    int err = 0;
    unsigned long a9_config7, a9_config8, a9_config10, a9_status23;

    if (!clk)
        return -1;
    if (!clk->parent)
        return -1;

    vclk_debug(1, "a9_set_ratep(%s, %lu, prate=%lu)\n", clk->name, freq, prate);

    /* Computing ODF: min FVCOBY2=1200Mhz. Using ODF to go below */
    if (freq < 1200000000) {
        odf = 1200000000 / freq;
        if (1200000000 % freq)
            odf = odf + 1;
    } else
        odf = 1;
    vcoby2_rate = freq * odf;

    err = vclk_pll_get_params(prate, vcoby2_rate, &pll);
    if (err != 0) {
        vclk_error("ERROR: Can't compute PLL4600 params for %lu Hz\n", freq);
        return err;
    }

    a9_config7 = vclk_peek_p((void*)A9_SYSCFG_BASE + 0x1C);
    a9_config8 = vclk_peek_p((void*)A9_SYSCFG_BASE + 0x20);
    a9_config10 = vclk_peek_p((void*)A9_SYSCFG_BASE + 0x28);

    a9_config7 |= (1 << 1);
    vclk_poke_p((void*)A9_SYSCFG_BASE + 0x1C, a9_config7); // CLK_CA9=C0-5 clock

    a9_config8 |= (1 << 0);
    vclk_poke_p((void*)A9_SYSCFG_BASE + 0x20, a9_config8); // PLL power down
    a9_config8 &= ~(0x7 << 25);
    a9_config8 |= (pll.idf << 25);
    vclk_poke_p((void*)A9_SYSCFG_BASE + 0x20, a9_config8); // IDF
    a9_config10 &= ~((0x3f << 8) | 0xff);
    a9_config10 |= (odf << 8) | pll.ndiv;
    vclk_poke_p((void*)A9_SYSCFG_BASE + 0x28, a9_config10); // NDIV + ODF
    a9_config8 &= ~(0x1);
    vclk_poke_p((void*)A9_SYSCFG_BASE + 0x20, a9_config8); // PLL power up

    a9_status23 = vclk_peek_p((void*)A9_SYSCFG_BASE + 0x5C);
    while (!(a9_status23 & 1)) // Wait for lock
        a9_status23 = vclk_peek_p((void*)A9_SYSCFG_BASE + 0x5C);
        ;
    /* Can't put any delay because may rely on a clock that is currently
    changing (running from CA9 case). */

    a9_config7 &= ~(1 << 1);
    vclk_poke_p((void*)A9_SYSCFG_BASE + 0x1C, a9_config7); // CLK_CA9=PLL

    /* Recalc is now done by upper layer */
    return 0;
}

/* ========================================================================
   Name:        a9_set_parent
   Description: Identify parent clock for clockgen A9 clocks.
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int a9_set_parent(struct vclk *clk, struct vclk *src)
{
    unsigned long a9_config7;

    vclk_debug(1, "a9_set_parent(%s, %s)\n", clk->name, src->name);

    if (clk->id != CLK_A9) /* Other clocks have static parent */
        return 0;

    a9_config7 = vclk_peek_p((void*)A9_SYSCFG_BASE + 0x1C);
    if (src->id == CLK_A9_PHI) {
        a9_config7 &= ~(1 << 1); // CLK_CA9=PLL
        clk->parent = &vclk_clocks[CLK_A9_PHI];
    } else {
        a9_config7 |= (1 << 1); // CLK_CA9=C0-5 clock
        if (src->id == CLK_EXT2F_A9) {
            a9_config7 |= (1 << 0);
            clk->parent = &vclk_clocks[CLK_EXT2F_A9];
        } else {
            a9_config7 &= ~(1 << 0);
            clk->parent = &vclk_clocks[CLK_EXT2F_A9_BY2];
        }
    }
    vclk_poke_p((void*)A9_SYSCFG_BASE + 0x1C, a9_config7);

    /* Recalc done by upper layer */
    return 0;
}

/* ========================================================================
   Name:        a9_xable
   Description: Enable/disable A9 clock
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int a9_xable(struct vclk *clk, int enable)
{
    unsigned long a9_config8;

    // "xable(id=%d, en=%d)\n"
    // bf_print_dbg(config->output, 1, MSG_CLK_XABLE);
    // bf_print_arg32(config->output, clk->id);
    // bf_print_arg32(config->output, enable);

    switch (clk->id) {
    case CLK_A9:
    case CLK_A9_PERIPHS:
        return 0;
    case CLK_A9_PHI:
        break;
    default:
        return -1;
    }

    a9_config8 = vclk_peek_p((void*)A9_SYSCFG_BASE + 0x20);
    if (enable)
        a9_config8 &= ~(0x1); // PLL power up
    else
        a9_config8 |= (0x1); // PLL power down
    vclk_poke_p((void*)A9_SYSCFG_BASE + 0x20, a9_config8);

    return 0;
}

/*
 * Gated clocks
 */

/* ========================================================================
   Name:        gated_recalc
   Description: Get clocks frequencies (in Hz)
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int gated_recalc(struct vclk *clk)
{
    unsigned long val, reg;

    switch (clk->id) {
    case CLK_HQVDP0_REG ... CLK_HQVDP0_PROC:
        reg = 6000;
        break;
    case CLK_VDPAUX_PIX ... CLK_VDPAUX_SYS:
        reg = 6001;
        break;
    case CLK_COMPO_CPU ... CLK_COMPO_MIX2:
        reg = 6002;
        break;
    case CLK_HDTVOUT_IC ... CLK_HDTVOUT_TMDS_HDMI:
        reg = 6003;
        break;
    case CLK_FDMA0_T1 ... CLK_FDMA0_SLIM:
        reg = 6004;
        break;
    case CLK_FDMA1_T1 ... CLK_FDMA1_SLIM:
        reg = 6005;
        break;
    case CLK_BDISP_CPU_CK ... CLK_BDISP_CK:
        reg = 6010;
        break;
    case CLK_HDMIRX_IC_100:
        reg = 6011;
        break;
    case CLK_DVPIN_CPU ... CLK_DVPIN_ST:
        reg = 6013;
        break;
    case CLK_GMAC_AXI ... CLK_GMAC_TX:
        reg = 2000;
        break;
    case CLK_WIFI_AXI ... CLK_WIFI_500:
        reg = 2010;
        break;
    default:
        return -1;
    };

    if (clk->parent)
        clk->rate = clk->parent->rate;
    else
        clk->flags |= VCLK_NORATE;
    val = vclk_syscfg_read(reg, 16, 16);
    if (val)
        clk->flags &= ~VCLK_OFF;
    else
        clk->flags |= VCLK_OFF;
    return 0;
}

/* ========================================================================
   Name:        gated_xable
   Description: Enabled/disable clock
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int gated_xable(struct vclk *clk, int enable)
{
    unsigned long reg;

    /* Bit is always 0 */
    switch (clk->id) {
    case CLK_HQVDP0_REG ... CLK_HQVDP0_PROC:
        reg = 6000;
        break;
    case CLK_VDPAUX_PIX ... CLK_VDPAUX_SYS:
        reg = 6001;
        break;
    case CLK_COMPO_CPU ... CLK_COMPO_MIX2:
        reg = 6002;
        break;
    case CLK_HDTVOUT_IC ... CLK_HDTVOUT_TMDS_HDMI:
        reg = 6003;
        break;
    case CLK_FDMA0_T1 ... CLK_FDMA0_SLIM:
        reg = 6004;
        break;
    case CLK_FDMA1_T1 ... CLK_FDMA1_SLIM:
        reg = 6005;
        break;
    case CLK_BDISP_CPU_CK ... CLK_BDISP_CK:
        reg = 6010;
        break;
    case CLK_HDMIRX_IC_100:
        reg = 6011;
        break;
    case CLK_DVPIN_CPU ... CLK_DVPIN_ST:
        reg = 6013;
        break;
    case CLK_GMAC_AXI ... CLK_GMAC_TX:
        reg = 2000;
        break;
    case CLK_WIFI_AXI ... CLK_WIFI_500:
        reg = 2010;
        break;
    default:
        return -1;
    }

    if (enable)
        vclk_syscfg_write(reg, 16, 16, 1);
    else
        vclk_syscfg_write(reg, 16, 16, 0);
    return 0;
}

/*
 * Observation & measure features
 */

/* ========================================================================
   Name:        observe
   Description: Route clock to IO for observation purposes
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int observe(struct vclk *clk, unsigned long *div)
{
    if (!clk || !*div)
        return -1;

    /* Observation points:
       A0 => AltFunc4 of PIO31[4]
       C0 => AltFunc4 of PIO12[2]
       D0 => AltFunc3 of PIO31[0]
       D1 => AltFunc3 of PIO31[2]
    */

    /* Configuring appropriate PIO */
    switch (clk->id) {
    case CLK_A0_REF ... CLK_IC_LMI:
        vclk_syscfg_write(22001, 16, 18, 7);   /* Selecting alternate 7 */
        vclk_syscfg_write(22040, 12, 12, 1); /* Output Enable */
        vclk_syscfg_write(22060, 12, 12, 0); /* Open drain */
        vclk_syscfg_write(22050, 12, 12, 0); /* pull up */
        break;
    case CLK_C0_REF ... CLK_PERIPH_CPU:
        /* NON LINEAR SYS CONF REG's*/
        vclk_poke_p((void*)SYSTEM_CONFIG24002, ((vclk_peek_p((void*)SYSTEM_CONFIG24002)&(~(0xf<<8))) | (8<<8))); /* Selecting alternate 8 */
        vclk_poke_p((void*)SYSTEM_CONFIG24040, ((vclk_peek_p((void*)SYSTEM_CONFIG24040)&(~(1<<18))) | (1<<18))); /* Output Enable */
        vclk_poke_p((void*)SYSTEM_CONFIG24060, (vclk_peek_p((void*)SYSTEM_CONFIG24060)&(~(1<<18)))); /* Open drain */
        vclk_poke_p((void*)SYSTEM_CONFIG24050, (vclk_peek_p((void*)SYSTEM_CONFIG24050)&(~(1<<18)))); /* pull up */
        //vclk_syscfg_write(24002, 8, 11, 8); /* Selecting alternate 8 */
        //vclk_syscfg_write(24003, 18, 18, 1); /* Output Enable */
        //vclk_syscfg_write(24006, 18, 18, 0); /* Open drain */
        //vclk_syscfg_write(24004, 18, 18, 0); /* pull up */
        break;
    case CLK_D0_REF ... CLK_MCLK_OUT:
        vclk_syscfg_write(22001, 0, 2, 7); /* Selecting alternate 3 */
        vclk_syscfg_write(22040, 8, 8, 1); /* Output Enable */
        vclk_syscfg_write(22060, 8, 8, 0); /* Open drain */
        vclk_syscfg_write(22050, 8, 8, 0); /* pull up */
        break;
    case CLK_D1_REF ... CLK_HDMI_PHASE_REG:
        vclk_syscfg_write(22001, 8, 10, 7); /* Selecting alternate 3 */
        vclk_syscfg_write(22040, 10, 10, 1); /* Output Enable */
        vclk_syscfg_write(22060, 10, 10, 0); /* Open drain */
        vclk_syscfg_write(22050, 10, 10, 0); /* pull up */
        break;
    /* Unexpected clocks */
    default:
        return -1;
    }

    if (*div < 1 || *div > 128)
        return -1;

    return flexgen_observe(clk, div);
}

/* ========================================================================
   Name:        get_measure
   Description: Perform HW measure
   Returns:     0=OK, -1=ERROR/UNSUPPORTED
   ======================================================================== */

int get_measure(struct vclk *clk, unsigned long *rate)
{
    switch (clk->id) {
    case CLK_A0_REF ... CLK_IC_LMI:
        return flexgen_get_measure(clk, &vclk_clocks[CLK_A0_REFOUT0], rate);
    case CLK_C0_REF ... CLK_PERIPH_CPU:
        return flexgen_get_measure(clk, &vclk_clocks[CLK_C0_REFOUT0], rate);
    case CLK_D0_REF ... CLK_MCLK_OUT:
        return flexgen_get_measure(clk, &vclk_clocks[CLK_D0_REFOUT0], rate);
    case CLK_D1_REF ... CLK_HDMI_PHASE_REG:
        return flexgen_get_measure(clk, &vclk_clocks[CLK_D1_REFOUT0], rate);
    };

    return -1;
}

/*
 * Clocks declarations
 * Groups, flexgens, then clocks
 */

_CLK_GROUP(grouptop,
    "TOP",
    identify_parent,
    recalc,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL // get_measure
);
_CLK_GROUP(groupa9,
    "A9",
    a9_identify_parent,
    a9_recalc,
    a9_xable,
    a9_set_ratep,
    a9_set_parent,
    NULL, // observe: TO BE IMPLEMENTED
    NULL,
    NULL // get_measure
);
_CLK_GROUP(groupa0,
    "A0",
    identify_parent,
    recalc,
    xable,
    set_ratep,
    set_parent,
    observe,
    "PIO31[4]", /* or PIO31[5] */
    get_measure
);
_CLK_GROUP(groupc0,
    "C0",
    identify_parent,
    recalc,
    xable,
    set_ratep,
    set_parent,
    observe,
    "PIO12[2]", /* or PIO12[3] */
    get_measure
);
_CLK_GROUP(groupd0,
    "D0",
    identify_parent,
    recalc,
    xable,
    set_ratep,
    set_parent,
    observe,
    "PIO31[0]", /* or PIO31[1] */
    get_measure
);
_CLK_GROUP(groupd1,
    "D1",
    identify_parent,
    recalc,
    xable,
    set_ratep,
    set_parent,
    observe,
    "PIO31[2]", /* or PIO31[3] */
    get_measure
);
_CLK_GROUP(gated,
    "Gated",
    NULL,
    gated_recalc,
    gated_xable,
    NULL,
    NULL,
    NULL, // observe: TO BE IMPLEMENTED
    NULL,
    NULL // get_measure
);

/* Cross bar sources in connected order */
static int xbar_src_A0[] = {
    CLK_A0_PLL0_PHI0,   /* PLL3200 PHI0 */
    CLK_A0_REF,         /* refclkin[0] */
    CLK_A0_REF1,        /* refclkin[1] */
    CLK_A0_SRCIN0,      /* srcclkin[0] */
    CLK_A0_SRCIN1,      /* srcclkin[1] */
    CLK_A0_SRCIN2,      /* srcclkin[2] */
    CLK_A0_REF,         /* !! Mode pin selected !! */
    -1 /* Keep this last */
};
static int xbar_src_C0[] = {
    CLK_C0_PLL0_PHI0,   /* PLL3200 PHI0 */
    CLK_C0_PLL0_PHI1,   /* PLL3200 PHI1 */
    CLK_C0_PLL0_PHI2,   /* PLL3200 PHI2 */
    CLK_C0_PLL0_PHI3,   /* PLL3200 PHI3 */
    CLK_C0_PLL1_PHI0,   /* PLL3200 PHI0 */
    CLK_C0_PLL1_PHI1,   /* PLL3200 PHI1 */
    CLK_C0_PLL1_PHI2,   /* PLL3200 PHI2 */
    CLK_C0_PLL1_PHI3,   /* PLL3200 PHI3 */
    CLK_C0_FS0_VCO,     /* FS660 VCO */
    CLK_C0_FS0_CH0,     /* FS660 chan 0 */
    CLK_C0_FS0_CH1,     /* FS660 chan 1 */
    CLK_C0_FS0_CH2,     /* FS660 chan 2 */
    CLK_C0_FS0_CH3,     /* FS660 chan 3 */
    CLK_C0_REF,         /* refclkin[0] */
    CLK_C0_REF1,        /* refclkin[1] */
    CLK_C0_SRCIN0,      /* srcclkin[0] */
    CLK_C0_SRCIN1,      /* srcclkin[1] */
    CLK_C0_SRCIN2,      /* srcclkin[2] */
    CLK_C0_REF,         /* !! Mode pin selected !! */
    -1 /* Keep this last */
};
static int xbar_src_D0[] = {
    CLK_D0_FS0_VCO,     /* FS660 VCO */
    CLK_D0_FS0_CH0,     /* FS660 chan 0 */
    CLK_D0_FS0_CH1,     /* FS660 chan 1 */
    CLK_D0_FS0_CH2,     /* FS660 chan 2 */
    CLK_D0_FS0_CH3,     /* FS660 chan 3 */
    CLK_D0_REF,         /* refclkin[0] */
    CLK_D0_REF1,        /* refclkin[1] */
    CLK_D0_SRCIN0,      /* srcclkin[0] */
    CLK_D0_SRCIN1,      /* srcclkin[1] */
    CLK_D0_SRCIN2,      /* srcclkin[3] */
    CLK_D0_SRCIN2,      /* srcclkin[4] */
    CLK_D0_REF,         /* !! Mode pin selected !! */
    -1 /* Keep this last */
};
static int xbar_src_D1[] = {
    CLK_D1_FS0_VCO,     /* FS660 VCO */
    CLK_D1_FS0_CH0,     /* FS660 chan 0 */
    CLK_D1_FS0_CH1,     /* FS660 chan 1 */
    CLK_D1_FS0_CH2,     /* FS660 chan 2 */
    CLK_D1_FS0_CH3,     /* FS660 chan 3 */
    CLK_D1_REF,         /* refclkin[0] */
    CLK_D1_REF1,        /* refclkin[1] */
    CLK_D1_SRCIN0,      /* srcclkin[0] */
    CLK_D1_SRCIN1,      /* srcclkin[1] */
    CLK_D1_SRCIN2,      /* srcclkin[3] */
    CLK_D1_SRCIN3,      /* srcclkin[4] */
    CLK_D0_REF,         /* !! Mode pin selected !! */
    -1 /* Keep this last */
};

/* Flexiclockgen hierarchical description
   - Clockgen name (ex: "A0")
   - Physical base address
   - Group the flexgen belongs to
   - Number of 'refclkin' (even if not connected)
   - Refclkout config (muxmap, divmap)
   - Xbar inputs list (in cross bar input order)
*/
static struct flexgen flexgens[] = {
_FLEXGEN("A0", (void*)CKG_A0_BASE_ADDRESS, &groupa0, 2, 0x7, 0x6,  xbar_src_A0),
_FLEXGEN("C0", (void*)CKG_C0_BASE_ADDRESS, &groupc0, 2, 0x1f, 0x18, xbar_src_C0),
_FLEXGEN("D0", (void*)CKG_D0_BASE_ADDRESS, &groupd0, 2, 0x7, 0x6, xbar_src_D0),
_FLEXGEN("D1", (void*)CKG_D1_BASE_ADDRESS, &groupd1, 2, 0x07, 0x06, xbar_src_D1),
_FLEXGEN_END() /* Keep this last */
};

/* Clocks declaration */
struct vclk vclk_clocks[CLK_LAST_ID] = {
/* Top level clocks */
_CLK_FIXED(CLK_MIPHY_30,        &grouptop, 30 * 1000000), // MIPHY internal 30Mhz
_CLK_FIXED(CLK_WIFI_40,         &grouptop, 40 * 1000000), // WIFI external 40Mhz
_CLK(CLK_REF, &grouptop), // Mux output: CLK_MIPHY_30 | CLK_WIFI_40

/* CA9 PLL */
_CLK_P(CLK_A9_PHI,              &groupa9,  &vclk_clocks[CLK_REF]),
_CLK(CLK_A9,                    &groupa9),
_CLK_P(CLK_A9_PERIPHS,          &groupa9,  &vclk_clocks[CLK_A9]),

/* Clockgen A0 */
_CLK_FG_P(CLK_A0_REF,           &flexgens[0], FG_REFIN, 0, 0, &vclk_clocks[CLK_REF]),
_CLK_FG2(CLK_A0_REF1,           &flexgens[0], VCLK_NOSOURCE, FG_REFIN, 1, 0),

_CLK_FG(CLK_A0_REFOUT0,         &flexgens[0], FG_REFOUT, 0, 0),
_CLK_FG(CLK_A0_REFOUT1,         &flexgens[0], FG_REFOUT, 1, 0),
_CLK_FG_P(CLK_A0_PLL0_VCO,      &flexgens[0], FG_GFGOUT | FG_PLL3200C28 | FG_FVCOBY2 | FG_FRAC, 0, 0, &vclk_clocks[CLK_A0_REFOUT0]),
_CLK_FG_P(CLK_A0_PLL0_PHI0,     &flexgens[0], FG_GFGOUT | FG_PLL3200C28 | FG_PHI, 0, 0, &vclk_clocks[CLK_A0_PLL0_VCO]),

_CLK_FG_DIV(CLK_IC_LMI,         &flexgens[0], 0),

/* Clockgen C0 */
_CLK_FG_P(CLK_C0_REF,           &flexgens[1], FG_REFIN, 0, 0, &vclk_clocks[CLK_REF]),
_CLK_FG2(CLK_C0_REF1,           &flexgens[1], VCLK_NOSOURCE, FG_REFIN, 1, 0),
_CLK_FG(CLK_C0_REFOUT0,         &flexgens[1], FG_REFOUT, 0, 0),
_CLK_FG(CLK_C0_REFOUT1,         &flexgens[1], FG_REFOUT, 1, 0),
_CLK_FG(CLK_C0_REFOUT2,         &flexgens[1], FG_REFOUT, 2, 0),

_CLK_FG_P(CLK_C0_PLL0_VCO,      &flexgens[1], FG_GFGOUT | FG_PLL3200C28 | FG_FVCOBY2, 0, 0, &vclk_clocks[CLK_C0_REFOUT0]),
_CLK_FG_P(CLK_C0_PLL0_PHI0,     &flexgens[1], FG_GFGOUT | FG_PLL3200C28 | FG_PHI, 0, 0, &vclk_clocks[CLK_C0_PLL0_VCO]),
_CLK_FG_P(CLK_C0_PLL0_PHI1,     &flexgens[1], FG_GFGOUT | FG_PLL3200C28 | FG_PHI, 0, 1, &vclk_clocks[CLK_C0_PLL0_VCO]),
_CLK_FG_P(CLK_C0_PLL0_PHI2,     &flexgens[1], FG_GFGOUT | FG_PLL3200C28 | FG_PHI, 0, 2, &vclk_clocks[CLK_C0_PLL0_VCO]),
_CLK_FG_P(CLK_C0_PLL0_PHI3,     &flexgens[1], FG_GFGOUT | FG_PLL3200C28 | FG_PHI, 0, 3, &vclk_clocks[CLK_C0_PLL0_VCO]),
_CLK_FG_P(CLK_C0_PLL1_VCO,      &flexgens[1], FG_GFGOUT | FG_PLL3200C28 | FG_FVCOBY2, 1, 0, &vclk_clocks[CLK_C0_REFOUT1]),
_CLK_FG_P(CLK_C0_PLL1_PHI0,     &flexgens[1], FG_GFGOUT | FG_PLL3200C28 | FG_PHI, 1, 0, &vclk_clocks[CLK_C0_PLL1_VCO]),
_CLK_FG_P(CLK_C0_PLL1_PHI1,     &flexgens[1], FG_GFGOUT | FG_PLL3200C28 | FG_PHI, 1, 1, &vclk_clocks[CLK_C0_PLL1_VCO]),
_CLK_FG_P(CLK_C0_PLL1_PHI2,     &flexgens[1], FG_GFGOUT | FG_PLL3200C28 | FG_PHI, 1, 2, &vclk_clocks[CLK_C0_PLL1_VCO]),
_CLK_FG_P(CLK_C0_PLL1_PHI3,     &flexgens[1], FG_GFGOUT | FG_PLL3200C28 | FG_PHI, 1, 3, &vclk_clocks[CLK_C0_PLL1_VCO]),
_CLK_FG_P(CLK_C0_FS0_VCO,       &flexgens[1], FG_GFGOUT | FG_FS660C28 | FG_FVCO, 2, 0, &vclk_clocks[CLK_C0_REFOUT2]),
_CLK_FG_P(CLK_C0_FS0_CH0,       &flexgens[1], FG_GFGOUT | FG_FS660C28 | FG_CHAN, 2, 0, &vclk_clocks[CLK_C0_FS0_VCO]),
_CLK_FG_P(CLK_C0_FS0_CH1,       &flexgens[1], FG_GFGOUT | FG_FS660C28 | FG_CHAN, 2, 1, &vclk_clocks[CLK_C0_FS0_VCO]),
_CLK_FG_P(CLK_C0_FS0_CH2,       &flexgens[1], FG_GFGOUT | FG_FS660C28 | FG_CHAN, 2, 2, &vclk_clocks[CLK_C0_FS0_VCO]),
_CLK_FG_P(CLK_C0_FS0_CH3,       &flexgens[1], FG_GFGOUT | FG_FS660C28 | FG_CHAN, 2, 3, &vclk_clocks[CLK_C0_FS0_VCO]),

_CLK_FG_DIV(CLK_SLIM_CC,        &flexgens[1], 0),
_CLK_FG_DIV(CLK_SPI,            &flexgens[1], 1),
_CLK_FG_DIV(CLK_FLASH,          &flexgens[1], 2),
_CLK_FG_DIV(CLK_MMC_0,          &flexgens[1], 3),
_CLK_FG_DIV(CLK_PERIPH_TS,      &flexgens[1], 4),
_CLK_FG_DIV(CLK_EXT2F_A9,       &flexgens[1], 5),
_CLK_FG_DIV(CLK_ICN_TS,         &flexgens[1], 6),
_CLK_FG_DIV(CLK_NAND,           &flexgens[1], 7),
_CLK_FG_DIV(CLK_MMC_1,          &flexgens[1], 8),
_CLK_FG_DIV(CLK_ST231_GP_2,     &flexgens[1], 9),
_CLK_FG_DIV(CLK_ST231_VDEC,     &flexgens[1], 10),
_CLK_FG_DIV(CLK_ST231_GP_0,     &flexgens[1], 11),
_CLK_FG_DIV(CLK_ST231_GP_1_AUDIO, &flexgens[1], 12),
_CLK_FG_DIV(CLK_ETH_GMAC_S,     &flexgens[1], 13),
_CLK_FG_DIV(CLK_GMAC_S_PHYCLK,  &flexgens[1], 14),
_CLK_FG_DIV(CLK_ETH_GMAC_W,     &flexgens[1], 15),
_CLK_FG_DIV(CLK_GMAC_W_PHYCLK,  &flexgens[1], 16),
_CLK_FG_DIV(CLK_STBE,           &flexgens[1], 17),
_CLK_FG_DIV(CLK_HEVC_H264,      &flexgens[1], 18),
_CLK_FG_DIV(CLK_AVSP,           &flexgens[1], 19),
_CLK_FG_DIV(CLK_TIMER_A9,       &flexgens[1], 20),
_CLK_FG_DIV(CLK_SLIM_VDEC,      &flexgens[1], 21),
_CLK_FG_DIV(CLK_DEC_JPEG,       &flexgens[1], 22),
_CLK_FG_DIV(CLK_WIFI,           &flexgens[1], 23),
_CLK_FG_DIV(CLK_RX_ICN_L0L1,    &flexgens[1], 24),
_CLK_FG_DIV(CLK_FRC_1,          &flexgens[1], 25),
_CLK_FG_DIV(CLK_FRC_1_PAD,      &flexgens[1], 26),
_CLK_FG_DIV(CLK_DVP_PROC,       &flexgens[1], 27),
_CLK_FG_DIV(CLK_TX_ICN_L0L1,    &flexgens[1], 28),
_CLK_FG_DIV(CLK_SYS_CLK_OUT,    &flexgens[1], 29),
_CLK_FG_DIV(CLK_SYSTEM_STFE,    &flexgens[1], 30),
_CLK_FG_DIV(CLK_C0_SPARE_31,    &flexgens[1], 31),
_CLK_FG_DIV(CLK_SLIM_FDMA_1,    &flexgens[1], 32),
_CLK_FG_DIV(CLK_ATB,            &flexgens[1], 33),
_CLK_FG_DIV(CLK_TRACE,          &flexgens[1], 34),
_CLK_FG_DIV(CLK_BDISP,          &flexgens[1], 35),
_CLK_FG_DIV(CLK_PROC_VDP,       &flexgens[1], 36),
_CLK_FG_DIV(CLK_DSS,            &flexgens[1], 37),
_CLK_FG_DIV(CLK_C0_SPARE_38,    &flexgens[1], 38),
_CLK_FG_DIV(CLK_LPC,            &flexgens[1], 39),
_CLK_FG_DIV(CLK_GPU,            &flexgens[1], 40),
_CLK_FG_DIV(CLK_ICN_WIFI,       &flexgens[1], 41),
_CLK_FG_DIV(CLK_ICN_DISP,       &flexgens[1], 42),
_CLK_FG_DIV(CLK_ICN_CPU,        &flexgens[1], 43),
_CLK_FG_DIV(CLK_PERIPH_WIFI,    &flexgens[1], 44),
_CLK_FG_DIV(CLK_SLIM_FDMA_0,    &flexgens[1], 45),
_CLK_FG_DIV(CLK_PERIPH_DISP,    &flexgens[1], 46),
_CLK_FG_DIV(CLK_PERIPH_CPU,     &flexgens[1], 47),

_CLK_P(CLK_EXT2F_A9_BY2,        &groupc0, &vclk_clocks[CLK_EXT2F_A9]),

/* Clockgen D0 */
_CLK_FG_P(CLK_D0_REF,           &flexgens[2], FG_REFIN, 0, 0, &vclk_clocks[CLK_REF]),
_CLK_FG2(CLK_D0_REF1,           &flexgens[2], VCLK_NOSOURCE, FG_REFIN, 1, 0),
_CLK_FG(CLK_D0_SRCIN0,          &flexgens[2], FG_SRCIN | FG_BEAT, 0, 0),
_CLK_FG(CLK_D0_SRCIN1,          &flexgens[2], FG_SRCIN | FG_BEAT, 1, 0),
_CLK_FG(CLK_D0_SRCIN2,          &flexgens[2], FG_SRCIN | FG_BEAT, 2, 0),
_CLK_FG(CLK_D0_SRCIN3,          &flexgens[2], FG_SRCIN | FG_BEAT, 3, 0),

_CLK_FG(CLK_D0_REFOUT0,         &flexgens[2], FG_REFOUT, 0, 0),
_CLK_FG_P(CLK_D0_FS0_VCO,       &flexgens[2], FG_GFGOUT | FG_FS660C28 | FG_FVCO, 0, 0, &vclk_clocks[CLK_D0_REFOUT0]),
_CLK_FG_P(CLK_D0_FS0_CH0,       &flexgens[2], FG_GFGOUT | FG_FS660C28 | FG_CHAN | FG_BEAT, 0, 0, &vclk_clocks[CLK_D0_FS0_VCO]),
_CLK_FG_P(CLK_D0_FS0_CH1,       &flexgens[2], FG_GFGOUT | FG_FS660C28 | FG_CHAN | FG_BEAT, 0, 1, &vclk_clocks[CLK_D0_FS0_VCO]),
_CLK_FG_P(CLK_D0_FS0_CH2,       &flexgens[2], FG_GFGOUT | FG_FS660C28 | FG_CHAN, 0, 2, &vclk_clocks[CLK_D0_FS0_VCO]),
_CLK_FG_P(CLK_D0_FS0_CH3,       &flexgens[2], FG_GFGOUT | FG_FS660C28 | FG_CHAN, 0, 3, &vclk_clocks[CLK_D0_FS0_VCO]),

_CLK_FG_DIV(CLK_PCM_0,          &flexgens[2], 0),
_CLK_FG_DIV(CLK_PCM_1,          &flexgens[2], 1),
_CLK_FG_DIV(CLK_PCM_2,          &flexgens[2], 2),
_CLK_FG_DIV(CLK_SPDIF,          &flexgens[2], 3),
_CLK_FG_DIV(CLK_PCMR_0_MASTER,  &flexgens[2], 4),
_CLK_FG_DIV(CLK_MCLK_OUT,       &flexgens[2], 5),

/* Clockgen D1 */
_CLK_FG_P(CLK_D1_REF,               &flexgens[3], FG_REFIN, 0, 0, &vclk_clocks[CLK_REF]),
_CLK_FG2(CLK_D1_REF1,               &flexgens[3], VCLK_NOSOURCE, FG_REFIN, 1, 0),
_CLK_FG(CLK_D1_SRCIN0,              &flexgens[3], FG_SRCIN | FG_BEAT, 0, 0),
_CLK_FG2(CLK_D1_SRCIN1,             &flexgens[3], VCLK_NOSOURCE, FG_SRCIN | FG_BEAT, 1, 0),
_CLK_FG2(CLK_D1_SRCIN2,             &flexgens[3], VCLK_NOSOURCE, FG_SRCIN | FG_BEAT, 2, 0),
_CLK_FG2(CLK_D1_SRCIN3,             &flexgens[3], VCLK_NOSOURCE, FG_SRCIN | FG_BEAT, 3, 0),

_CLK_FG(CLK_D1_REFOUT0,             &flexgens[3], FG_REFOUT, 0, 0),
_CLK_FG_P(CLK_D1_FS0_VCO,           &flexgens[3], FG_GFGOUT | FG_FS660C28 | FG_FVCO, 0, 0, &vclk_clocks[CLK_D1_REFOUT0]),
_CLK_FG_P(CLK_D1_FS0_CH0,           &flexgens[3], FG_GFGOUT | FG_FS660C28 | FG_CHAN | FG_BEAT | FG_NSDIV1, 0, 0, &vclk_clocks[CLK_D1_FS0_VCO]),
_CLK_FG_P(CLK_D1_FS0_CH1,           &flexgens[3], FG_GFGOUT | FG_FS660C28 | FG_CHAN | FG_BEAT | FG_NSDIV1, 0, 1, &vclk_clocks[CLK_D1_FS0_VCO]),
_CLK_FG_P(CLK_D1_FS0_CH2,           &flexgens[3], FG_GFGOUT | FG_FS660C28 | FG_CHAN | FG_BEAT | FG_NSDIV1, 0, 2, &vclk_clocks[CLK_D1_FS0_VCO]),
_CLK_FG_P(CLK_D1_FS0_CH3,           &flexgens[3], FG_GFGOUT | FG_FS660C28 | FG_CHAN | FG_BEAT | FG_NSDIV1, 0, 3, &vclk_clocks[CLK_D1_FS0_VCO]),

_CLK_FG_DIV2(CLK_PIX_MAIN_DISP,     &flexgens[3], FG_SYNC, 0),
_CLK_FG_DIV2(CLK_PIX_AUX_DISP,      &flexgens[3], FG_SYNC, 1),
_CLK_FG_DIV2(CLK_PIX_HDDAC,         &flexgens[3], FG_SYNC, 2),
_CLK_FG_DIV2(CLK_HDDAC,             &flexgens[3], FG_SYNC, 3),
_CLK_FG_DIV2(CLK_TVOUT_DEBUG,       &flexgens[3], FG_SYNC, 4),
_CLK_FG_DIV2(CLK_TVOUT_PIX_DEBUG,   &flexgens[3], FG_SYNC, 5),
_CLK_FG_DIV2(CLK_SDDAC,             &flexgens[3], FG_SYNC, 6),
_CLK_FG_DIV2(CLK_PIX_HDMI,          &flexgens[3], FG_SYNC, 7),
_CLK_FG_DIV2(CLK_DENC,              &flexgens[3], FG_SYNC, 8),
_CLK_FG_DIV2(CLK_REF_HDMIPHY,       &flexgens[3], FG_SYNC, 9),
_CLK_FG_DIV(CLK_TMDS_HDMI,          &flexgens[3], 10),
_CLK_FG_DIV(CLK_PROC_HQVDP,         &flexgens[3], 11),
_CLK_FG_DIV(CLK_IQI_HQVDP,          &flexgens[3], 12),
_CLK_FG_DIV(CLK_PROC_MIXER,         &flexgens[3], 13),
_CLK_FG_DIV(CLK_PROC_GDP_COMPO,     &flexgens[3], 14),
_CLK_FG_DIV(CLK_PWM,                &flexgens[3], 15),
_CLK_FG_DIV(CLK_FRC_0,              &flexgens[3], 16),
_CLK_FG_DIV(CLK_FRC_0_PAD,          &flexgens[3], 17),
_CLK_FG_DIV(CLK_FRC_2,              &flexgens[3], 18),
_CLK_FG_DIV(CLK_HDMI_PHASE_REG,     &flexgens[3], 40),

/* Functional clusters gated clocks */
_CLK_P(CLK_HQVDP0_REG,          &gated, &vclk_clocks[CLK_PERIPH_DISP]), // Src=clk_periph_display, C0-46
_CLK_P(CLK_HQVDP0_SYS,          &gated, &vclk_clocks[CLK_ICN_DISP]), // Src=clk_icn_display, C0-42
_CLK_P(CLK_HQVDP0_PIX,          &gated, &vclk_clocks[CLK_PROC_MIXER]), // Src=clk_mixer_proc_compo, D1-13
_CLK_P(CLK_HQVDP0_IQI,          &gated, &vclk_clocks[CLK_IQI_HQVDP]), // Src=D1-12 clk_iqi_hqvdp_0
_CLK_P(CLK_HQVDP0_PROC,         &gated, &vclk_clocks[CLK_PROC_HQVDP]), // Src=D1-11 clk_proc_hqvdp_0
_CLK_P(CLK_VDPAUX_PIX,          &gated, &vclk_clocks[CLK_PROC_HQVDP]), // Src=D1-11 clk_proc_hqvdp_0
_CLK_P(CLK_VDPAUX_PROC,         &gated, &vclk_clocks[CLK_PROC_VDP]), // Src=C0-36 clk_proc_vdp_0
_CLK_P(CLK_VDPAUX_REG,          &gated, &vclk_clocks[CLK_PERIPH_DISP]), // Src=C0-46 clk_periph_display
_CLK_P(CLK_VDPAUX_SYS,          &gated, &vclk_clocks[CLK_ICN_DISP]), // Src=C0-42 clk_icn_display
_CLK_P(CLK_COMPO_CPU,           &gated, &vclk_clocks[CLK_PERIPH_DISP]), // Src=clk_periph_display, C0-46
_CLK_P(CLK_COMPO_ST,            &gated, &vclk_clocks[CLK_ICN_DISP]), // Src=clk_icn_display, C0-42
_CLK_P(CLK_COMPO_GDP_PROC,      &gated, &vclk_clocks[CLK_PROC_GDP_COMPO]), // Src=clk_proc_gdp_compo_0, D1-14
_CLK_P(CLK_COMPO_MIX_PROC,      &gated, &vclk_clocks[CLK_PROC_HQVDP]), // Src=clk_proc_hqvdp_, D1-11
_CLK_P(CLK_COMPO_MIX1,          &gated, &vclk_clocks[CLK_PIX_MAIN_DISP]),// Src=clk_mix1_compo_0, D1-0
_CLK_P(CLK_COMPO_MIX2,          &gated, &vclk_clocks[CLK_PIX_AUX_DISP]), // Src=clk_mix2_compo_0, D1-1
_CLK_P(CLK_HDTVOUT_IC,          &gated, &vclk_clocks[CLK_PERIPH_DISP]), // Src=clk_periph_display, C0-46
_CLK_P(CLK_HDTVOUT_DENC,        &gated, &vclk_clocks[CLK_DENC]), // Src=clk_tvout_denc, D1-8
_CLK_P(CLK_HDTVOUT_DVO_1,       &gated, &vclk_clocks[CLK_TVOUT_DEBUG]), // Src=clk_tvout_dvo_1, D1-4
_CLK_P(CLK_HDTVOUT_OUT_HDDAC,   &gated, &vclk_clocks[CLK_HDDAC]), // Src=clk_tvout_hddac, D1-3
_CLK_P(CLK_HDTVOUT_PIX_HDDAC,   &gated, &vclk_clocks[CLK_PIX_HDDAC]), // Src=clk_tvout_pix_hddac, D1-2
_CLK_P(CLK_HDTVOUT_OUT_SDDAC,   &gated, &vclk_clocks[CLK_SDDAC]), // Src=clk_tvout_sddac, D1-6
_CLK_P(CLK_HDTVOUT_PIX_AUX,     &gated, &vclk_clocks[CLK_PIX_AUX_DISP]), // Src=clk_mix2_compo_0, D1-1
_CLK_P(CLK_HDTVOUT_PIX_DVO_1,   &gated, &vclk_clocks[CLK_TVOUT_PIX_DEBUG]), // Src=clk_tvout_pix_dvo_1, D1-5
_CLK_P(CLK_HDTVOUT_PIX_HDMI,    &gated, &vclk_clocks[CLK_PIX_HDMI]), // Src=clk_tvout_pix_hdmi, D1-7
_CLK_P(CLK_HDTVOUT_PIX_MAIN,    &gated, &vclk_clocks[CLK_PIX_MAIN_DISP]), // Src=clk_mix1_compo_0, D1-0
_CLK_P(CLK_HDTVOUT_TMDS_HDMI,   &gated, &vclk_clocks[CLK_TMDS_HDMI]), // Src=clk_tmds_hdmi_hdtvout_0, D1-10
_CLK_P(CLK_FDMA0_T1,            &gated, &vclk_clocks[CLK_PERIPH_DISP]), // Src=clk_periph_display, C0-46
_CLK_P(CLK_FDMA0_T3_0,          &gated, &vclk_clocks[CLK_PERIPH_DISP]), // Src=clk_periph_display, C0-46
_CLK_P(CLK_FDMA0_T3_1,          &gated, &vclk_clocks[CLK_ICN_DISP]), // Src=clk_icn_display, C0-42
_CLK_P(CLK_FDMA0_SLIM,          &gated, &vclk_clocks[CLK_SLIM_FDMA_0]), // Src=clk_slim_fdma_0, C0-45
_CLK_P(CLK_FDMA1_T1,            &gated, &vclk_clocks[CLK_PERIPH_DISP]), // Src=clk_periph_display, C0-46
_CLK_P(CLK_FDMA1_T3_0,          &gated, &vclk_clocks[CLK_PERIPH_DISP]), // Src=clk_periph_display, C0-46
_CLK_P(CLK_FDMA1_T3_1,          &gated, &vclk_clocks[CLK_ICN_DISP]), // Src=clk_icn_display, C0-42
_CLK_P(CLK_FDMA1_SLIM,          &gated, &vclk_clocks[CLK_SLIM_FDMA_1]), // Src=clk_slim_fdma_1, C0-32
_CLK_P(CLK_BDISP_CPU_CK,        &gated, &vclk_clocks[CLK_PERIPH_DISP]), // Src=clk_periph_display, C0-46
_CLK_P(CLK_BDISP_IC_CK,         &gated, &vclk_clocks[CLK_ICN_DISP]), // Src=clk_icn_display, C0-42
_CLK_P(CLK_BDISP_CK,            &gated, &vclk_clocks[CLK_BDISP]), // Src=clk_bdisp_ck, C0-35
_CLK_P(CLK_HDMIRX_IC_100,       &gated, &vclk_clocks[CLK_PERIPH_DISP]), // Src=clk_periph_display, C0-46
_CLK_P(CLK_DVPIN_CPU,           &gated, &vclk_clocks[CLK_PERIPH_DISP]), // Src=clk_periph_display, C0-46
_CLK_P(CLK_DVPIN_PROC,          &gated, &vclk_clocks[CLK_DVP_PROC]), // Src=clk_dvp_proc, C0-27
_CLK_P(CLK_DVPIN_ST,            &gated, &vclk_clocks[CLK_ICN_DISP]), // Src=clk_icn_display, C0-42
_CLK_P(CLK_GMAC_AXI,            &gated, &vclk_clocks[CLK_ICN_WIFI]), // Src=clk_icn_wifi, C0-41
_CLK_P(CLK_GMAC_T1,             &gated, &vclk_clocks[CLK_PERIPH_WIFI]), // Src=clk_periph_wifi, C0-44
_CLK_P(CLK_GMAC_PTP_REF,        &gated, &vclk_clocks[CLK_PERIPH_WIFI]), // Src=clk_periph_wifi, C0-44
_CLK_P(CLK_GMAC_TX,             &gated, &vclk_clocks[CLK_ETH_GMAC_W]), // Src=clk_250_125_wifi, C0-15
_CLK_P(CLK_WIFI_AXI,            &gated, &vclk_clocks[CLK_ICN_WIFI]), // Src=clk_icn_wifi, C0-41
_CLK_P(CLK_WIFI_T1,             &gated, &vclk_clocks[CLK_PERIPH_WIFI]), // Src=clk_periph_wifi, C0-44
_CLK_P(CLK_WIFI_500,            &gated, &vclk_clocks[CLK_WIFI]), // Src=clk_500_wifi, C0-23
};

int vclk_nb = (sizeof(vclk_clocks) / sizeof(struct vclk));

int vclk_soc_init(void)
{
    vclk_debug(1, "H390 vclkd %s\n", VCLK_SOC_VERSION);

    /* Flexiclockgen lib init */
    if (flexgen_init(vclk_clocks, vclk_nb) != 0)
        return -1;
    /* System config lib init */
    if (vclk_syscfg_init(sysconf_groups) != 0)
        return -1;

    return 0;
}

