/*
* This file is part of Validation CLK Driver (VCLKD).
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
* Author(s): fabrice.charpentier@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* VCLKD may alternatively be licensed under GPL V2 license from ST.
*/

#include <stddef.h>
#include "vclk.h"
#include "vclk_flexgen.h"
#include "vclk-h337.h"
#include "vclk_algos.h"
#include "vclk_private.h"
#include "vclk_sysconf.h"

/*
 * Privates
 */

/* Base addresses */
#define CKG_A0_BASE_ADDRESS     0x8060000
#define CKG_A1_BASE_ADDRESS     0x8070000
#define CKG_C0_BASE_ADDRESS     0x8000000
#define CKG_D0_BASE_ADDRESS     0x8030000
#define CKG_D1_BASE_ADDRESS     0x8040000
#define A9_SYSCFG_BASE          0x261F0000
#define SYSCFG_2000_2999        0x0D000000
#define SYSCFG_3000_3999        0x0F000000
#define SYSCFG_6000_6999        0x15000000

/* Sysconf base addresses declarations */
struct sysconf_grp sysconf_groups[] = {
    {00, 29, (void*)A9_SYSCFG_BASE, 4},
    {2000, 2999, (void*)SYSCFG_2000_2999, 4},
    {3000, 3004, (void*)SYSCFG_3000_3999, 0x10000},
    {6000, 6015, (void*)SYSCFG_6000_6999, 0x10000},
    {-1, 0, 0, 0}
};

/* Prototypes */
struct vclk vclk_clocks[];

#if defined(VCLK_VIRTUAL_PLATFORM1)
/* COEMUL: some clocks are injected and can't be read by SW.
   setifunknown=1 => if clock unknown, set 12121212 freq & returns 0
   setifunknown=0 => if clock unknown, returns -1
 */
static int vp_get_rate(struct vclk *clk, int setifunknown)
{
    static struct vpclk {
        int id;
        unsigned long freq; /* In MHz */
    } vp_clocks[] = {
        // { CLK_A9_PHI, 1200},

        { CLK_A0_PLL0_VCO, 1400 },
        { CLK_A0_PLL0_PHI0, 1400 },
        // { CLK_A0_PLL0_PHI1, 1400 },
        // { CLK_A0_PLL0_PHI2, 1400 },
        // { CLK_A0_PLL0_PHI3, 1400 },
        { CLK_ICN_LMI, 700 },

        { CLK_A1_PLL0_PHI0, 1400 },
        // { CLK_A1_PLL0_PHI1, 1400 },
        // { CLK_A1_PLL0_PHI2, 1400 },
        // { CLK_A1_PLL0_PHI3, 1400 },

        { CLK_C0_PLL0_PHI0, 700 },
        { CLK_C0_PLL0_PHI1, 700 },
        { CLK_C0_PLL0_PHI2, 700 },
        { CLK_C0_PLL0_PHI3, 700 },
        { CLK_C0_PLL1_PHI0, 1400 },
        { CLK_C0_PLL1_PHI1, 1400 },
        { CLK_C0_PLL1_PHI2, 1400 },
        { CLK_C0_PLL1_PHI3, 1400 },
        { CLK_C0_FS0_CH0, 87 },
        { CLK_C0_FS0_CH1, 87 },
        { CLK_C0_FS0_CH2, 35 },
        { CLK_C0_FS0_CH3, 26 },

        { CLK_D0_FS0_CH0, 87 },
        { CLK_D0_FS0_CH1, 87 },
        { CLK_D0_FS0_CH2, 87 },
        { CLK_D0_FS0_CH3, 175 },

        { CLK_D1_FS0_CH0, 175 },
        { CLK_D1_FS0_CH1, 175 },
        { CLK_D1_FS0_CH2, 26 },
        { CLK_D1_FS0_CH3, 175 },
    };
    int i, size;

    size = sizeof(vp_clocks) / sizeof(struct vpclk);
    for (i = 0; (i < size) && (clk->id != vp_clocks[i].id); i++);
    if (i == size) {
        /* Unknown clock */
        if (!setifunknown)
            return -1;
        clk->rate = 12121212;
    } else {
        clk->rate = vp_clocks[i].freq * 1000000;
        clk->flags &= ~VCLK_OFF; // Assuming enabled
    }

    return 0;
}
#endif

/* ========================================================================
   Name:        recalc
   Description: Recompute clocks frequencies from HW status
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int recalc(struct vclk *clk)
{
    unsigned long prate;

    if (!clk)
        return -1;

    vclk_debug(1, "recalc(%s)\n", clk->name);

#if defined(VCLK_VIRTUAL_PLATFORM1)
    /* Clock injected ? */
    if (!vp_get_rate(clk, 0))
        return 0;
#endif

    prate = vclk_get_prate(clk);

    /* Flexiclockgen lib handled clocks */
    if (clk->flags & VCLK_FG)
        return flexgen_recalc(clk);

    /* Special cases */
    switch (clk->id) {
    case CLK_EXT2F_A9_BY2:
        clk->rate = prate / 2;
        break;
    /* Unexpected clocks */
    default:
        return -1;
    }

    vclk_debug(1, "  rate=%lu\n", clk->rate);
    return 0;
}

/* ========================================================================
   Name:        identify_parent
   Description: Identify source clock
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int identify_parent(struct vclk *clk)
{
    if (!clk)
        return -1;

    vclk_debug(1, "identify_parent(%s)\n", clk->name);

    /* Flexiclockgen handled clocks */
    if (clk->flags & VCLK_FG)
        return flexgen_identify_parent(clk);

    /* Special cases */
    // if (clk->id == CLK_REF) {
        // uint32_t val;

        // val = vclk_peek_p(SYSCFG_000_999 + 0x130048); // SYSTEM_STATU418
        // if (val & (1 << 7)) // WIFI ext ref clock ?
            // clk->parent = &vclk_clocks[CLK_WIFI_40];
        // else
            // clk->parent = &vclk_clocks[CLK_MIPHY_30];
        // return 0;
    // }

    vclk_error("ERROR: identify_parent(%s). Unsupported clock\n", clk->name);
    return -1;
}

/* ========================================================================
   Name:        set_parent
   Description: Set clock parent
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int set_parent(struct vclk *clk, struct vclk *src)
{
    if (!clk)
        return -1;

    vclk_debug(1, "set_parent(%s, src=%s)\n", clk->name, src->name);

    /* Flexiclocken lib handled clocks */
    if (clk->flags & VCLK_FG)
        return flexgen_set_parent(clk, src);

    return -1;
}

/* ========================================================================
   Name:        set_ratep
   Description: Set clock frequency
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int set_ratep(struct vclk *clk, unsigned long freq, unsigned long prate)
{

    if (!clk)
        return -1;

    vclk_debug(1, "set_ratep(%s, rate=%lu, prate=%lu)\n", clk->name, freq, prate);

#if defined(VCLK_VIRTUAL_PLATFORM1)
    /* If clock is injected, no way to set it */
    if (!vp_get_rate(clk, 0))
        return 0;
#endif

    /* Flexiclockgen lib handled clocks */
    if (clk->flags & VCLK_FG)
        return flexgen_set_ratep(clk, freq, prate);

    switch (clk->id) {
    case CLK_EXT2F_A9_BY2:
        break;
    /* Unexpected clocks */
    default:
        return -1;
    }

    return 0;
}

/* ========================================================================
   Name:        xable
   Description: Enable/disable clock
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int xable(struct vclk *clk, int enable)
{
    int err;

    if (!clk)
        return -1;

    vclk_debug(1, "xable(%s, en=%d)\n", clk->name, enable);

#if defined(VCLK_VIRTUAL_PLATFORM1)
    /* If clock injected, can't enable it */
    if (!vp_get_rate(clk, 0))
        return 0;
#endif

    if (clk->flags & VCLK_FG)
        err = flexgen_xable(clk, enable);
    else
        /* Unexpected clocks */
        return -1;

    return err;
}

/******************************************************************************
CA9 PLL
******************************************************************************/

/* ========================================================================
   Name:        a9_recalc
   Description: Get clocks frequencies (in Hz)
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int a9_recalc(struct vclk *clk)
{
    unsigned long prate;

    if (!clk)
        return -1;

    vclk_debug(1, "a9_recalc(%s)\n", clk->name);

    clk->flags &= ~VCLK_OFF; // Assuming clock enabled
    prate = vclk_get_prate(clk);

    switch (clk->id) {
    case CLK_A9:
        clk->rate = prate;
        break;
    case CLK_A9_PERIPHS:
        clk->rate = prate / 2;
        break;
    case CLK_A9_PHI:
        {
            unsigned long vcoby2_rate, odf;
            vclk_pll_t pll = {
                .type = VCLK_PLL4600C28,
            };
            int err;

            if (vclk_syscfg_read(8, 0, 0)) {
                /* A9_PLL_PD=1 => PLL disabled */
                vclk_debug(1, "  PLL is POWERED DOWN\n");
                clk->flags |= VCLK_OFF;
            }

            /* Computing FVCOby2 */
            pll.idf = vclk_syscfg_read(8, 25, 27);
            pll.ndiv = vclk_syscfg_read(10, 0, 7);
            err = vclk_pll_get_rate(prate, &pll, &vcoby2_rate);
            if (err)
                return -1;
            vclk_debug(1, "  idf=%lu ndiv=%lu prate=%lu => rate=%lu\n", pll.idf, pll.ndiv, prate, clk->rate);

            /* Computing PHI */
            odf = vclk_syscfg_read(10, 8, 13);
            if (odf == 0)
                odf = 1;
            clk->rate = vcoby2_rate / odf;
        }
        break;
    default:
        return -1; /* Unknown clock */
    }

    return 0;
}

/* ========================================================================
   Name:        a9_identify_parent
   Description: Identify parent clock for clockgen A9 clocks.
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int a9_identify_parent(struct vclk *clk)
{
    vclk_debug(1, "a9_identify_parent(%s)\n", clk->name);

    if (clk->id != CLK_A9)
        return -1;

    /* Is CA9 clock sourced from PLL or C0-13 ? */
    if (vclk_syscfg_read(7, 1, 1)) {
        /* Yes. Div by 1 or div by 2 ? */
        if (vclk_syscfg_read(7, 0, 0))
            clk->parent = &vclk_clocks[CLK_EXT2F_A9];
        else
            clk->parent = &vclk_clocks[CLK_EXT2F_A9_BY2];
    } else
        clk->parent = &vclk_clocks[CLK_A9_PHI];

    return 0;
}

/* ========================================================================
   Name:        a9_set_ratep
   Description: Set clock frequency
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int a9_set_ratep(struct vclk *clk, unsigned long freq, unsigned long prate)
{
    unsigned long odf, vcoby2_rate;
    struct vclk_pll pll = {
        .type = VCLK_PLL4600C28,
    };
    int err = 0;

    if (!clk)
        return -1;
    if (!clk->parent)
        return -1;

    vclk_debug(1, "a9_set_ratep(%s, %lu, prate=%lu)\n", clk->name, freq, prate);

    /* Computing ODF: min FVCOBY2=1200Mhz. Using ODF to go below */
    if (freq < 1200000000) {
        odf = 1200000000 / freq;
        if (1200000000 % freq)
            odf = odf + 1;
    } else
        odf = 1;
    vcoby2_rate = freq * odf;

    /* Computing FVCOby2 params */
    err = vclk_pll_get_params(prate, vcoby2_rate, &pll);
    if (err != 0) {
        vclk_error("ERROR: Can't compute PLL4600 params for %lu Hz\n", freq);
        return err;
    }

    vclk_syscfg_write(7, 1, 1, 1);         /* CLK_CA9=C0-13 clock */
    vclk_syscfg_write(8, 0, 0, 1);         /* PLL power down */
    vclk_syscfg_write(8, 25, 27, pll.idf); /* IDF */
    vclk_syscfg_write(10, 0, 7, pll.ndiv); /* NDIV */
    vclk_syscfg_write(10, 8, 13, odf);     /* ODF */
    vclk_syscfg_write(8, 0, 0, 0);         /* PLL power up */
    while (!vclk_syscfg_read(23, 0, 0))    /* Wait for lock */
        ;
    /* Can't put any delay because may rely on a clock that is currently
       changing (running from CA9 case). */
    vclk_syscfg_write(7, 1, 1, 0);      /* CLK_CA9=PLL */

    /* Recalc is now done by upper layer */
    return 0;
}

/* ========================================================================
   Name:        a9_set_parent
   Description: Identify parent clock for clockgen A9 clocks.
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int a9_set_parent(struct vclk *clk, struct vclk *src)
{


    vclk_debug(1, "a9_set_parent(%s, %s)\n", clk->name, src->name);

    if (clk->id != CLK_A9) /* Other clocks have static parent */
        return 0;

    if (src->id == CLK_A9_PHI) {
        vclk_syscfg_write(7, 1, 1, 0);
        clk->parent = &vclk_clocks[CLK_A9_PHI];
    } else {
        vclk_syscfg_write(7, 1, 1, 1);
        if (src->id == CLK_EXT2F_A9) {
            vclk_syscfg_write(7, 0, 0, 1);
            clk->parent = &vclk_clocks[CLK_EXT2F_A9];
        } else {
            vclk_syscfg_write(7, 0, 0, 0);
            clk->parent = &vclk_clocks[CLK_EXT2F_A9_BY2];
        }
    }

    /* Recalc done by upper layer */
    return 0;
}

/* ========================================================================
   Name:        a9_xable
   Description: Enable/disable A9 clock
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int a9_xable(struct vclk *clk, int enable)
{
    vclk_debug(1, "a9_xable(%s, en=%d)\n", clk->name, enable);

    switch (clk->id) {
    case CLK_A9:
    case CLK_A9_PERIPHS:
        return 0;
    case CLK_A9_PHI:
        break;
    default:
        return -1;
    }

    if (enable)
        vclk_syscfg_write(8, 0, 0, 1);          /* PLL power down */
    else
        vclk_syscfg_write(8, 0, 0, 0);          /* PLL power up */

    return 0;
}

/*
 * Gated clocks
 */

/* ========================================================================
   Name:        gated_recalc
   Description: Get clocks frequencies (in Hz)
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

const struct gated_clocks {
    unsigned short id;      // Clock ID
    unsigned long syscfg;   // Sysconf number
    unsigned short bit;     // Sysconf bit to enable
} gated_clocks[] = {
    {CLK_2X_DEMOD_GATED, 2000, 16},
    {CLK_ADC_IQ_GATED, 2000, 16},
    {CLK_FSK_GATED, 2000, 17},
    {CLK_ADC_DISEQC_GATED, 2000, 18},

    {CLK_PP_AVSP_GATED, 3001, 16},
    {CLK_PP_BUS_GATED, 3001, 16},
    {CLK_PP_HEVC_GATED, 3001, 16},
    {CLK_PP_H264_GATED, 3001, 16},
    {CLK_PP_TARG_GATED, 3001, 16},
    {CLK_VDEC_BUS_GATED, 3000, 16},
    {CLK_VDEC_CPU_GATED, 3000, 16},
    {CLK_VDEC_SLIM_GATED, 3000, 16},
    {CLK_VDEC_TARG_GATED, 3000, 16},
    {CLK_DMU_IC_GATED, 3004, 16},
    {CLK_DMU_BUS_GATED, 3003, 16},
    {CLK_DMU_CPU_GATED, 3003, 16},
    {CLK_AUDIO_PCM_0, 6015, 16},
    {CLK_AUDIO_PCM_1, 6006, 16},
    {CLK_AUDIO_PCM_2, 6007, 16},
    {CLK_AUDIO_SPDIF, 6008, 16},
    {CLK_BDISP_CK, 6010, 16},
    {CLK_BDISP_CPU, 6010, 16},
    {CLK_BDISP_IC, 6010, 16},
    {CLK_COMPO_CPU, 6002, 16},
    {CLK_COMPO_PROC, 6002, 16},
    {CLK_COMPO_MIX, 6002, 16},
    {CLK_COMPO_MIX1, 6002, 16},
    {CLK_COMPO_MIX2, 6002, 16},
    {CLK_COMPO_ST, 6002, 16},
    {CLK_FDMA0_SLIM, 6004, 16},
    {CLK_FDMA0_STBUS_T1, 6004, 16},
    {CLK_FDMA0_STBUS_T3_0, 6004, 16},
    {CLK_FDMA0_STBUS_T3_1, 6004, 16},
    {CLK_FDMA1_SLIM, 6005, 16},
    {CLK_FDMA1_STBUS_T1, 6005, 16},
    {CLK_FDMA1_STBUS_T3_0, 6005, 16},
    {CLK_FDMA1_STBUS_T3_1, 6005, 16},
    {CLK_HDTVOUT_DENC, 6003, 16},
    {CLK_HDTVOUT_IC, 6003, 16},
    {CLK_HDTVOUT_HDDAC, 6003, 16},
    {CLK_HDTVOUT_SDDAC, 6003, 16},
    {CLK_HDTVOUT_PIX_AUX, 6003, 16},
    {CLK_HDTVOUT_PIX_HDDAC, 6003, 16},
    {CLK_HDTVOUT_PIX_HDMI, 6003, 16},
    {CLK_HDTVOUT_PIX_MAIN, 6003, 16},
    {CLK_HDTVOUT_TMDS, 6003, 16},
    {CLK_HQVDP_COMPO, 6000, 16},
    {CLK_HQVDP_PROC, 6000, 16},
    {CLK_HQVDP_REG, 6000, 16},
    {CLK_HQVDP_SYS, 6000, 16},
    {CLK_VDP_PIX, 6001, 16},
    {CLK_VDP_PROC, 6001, 16},
    {CLK_VDP_REG, 6001, 16},
    {CLK_VDP_SYS, 6001, 16},
    {CLK_VDAC_UVW, 6003, 16},
    {CLK_VDAC_X, 6003, 16},
};

static int gated_recalc(struct vclk *clk)
{
    int i, size;
    unsigned long val;

    size = sizeof(gated_clocks) / sizeof(struct gated_clocks);
    for (i = 0; (i < size) && (clk->id != gated_clocks[i].id); i++);
    if (i == size)
        return -1; // Unknown gated clock

    if (clk->parent)
        clk->rate = clk->parent->rate;
    else
        clk->flags |= VCLK_NORATE;
    val = vclk_syscfg_read(gated_clocks[i].syscfg, gated_clocks[i].bit, gated_clocks[i].bit);
    if (val)
        clk->flags &= ~VCLK_OFF;
    else
        clk->flags |= VCLK_OFF;

    return 0;
}

/* ========================================================================
   Name:        gated_xable
   Description: Enabled/disable clock
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int gated_xable(struct vclk *clk, int enable)
{
    int i, size;

    size = sizeof(gated_clocks) / sizeof(struct gated_clocks);
    for (i = 0; (i < size) && (clk->id != gated_clocks[i].id); i++);
    if (i == size)
        return -1; // Unknown gated clock

    if (enable)
        vclk_syscfg_write(gated_clocks[i].syscfg, gated_clocks[i].bit, gated_clocks[i].bit, 1);
    else
        vclk_syscfg_write(gated_clocks[i].syscfg, gated_clocks[i].bit, gated_clocks[i].bit, 0);
    return 0;
}

/*
 * Observation & measure features
 */

/* ========================================================================
   Name:        observe
   Description: Route clock to IO for observation purposes
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int observe(struct vclk *clk, unsigned long *div)
{
    if (!clk || !*div)
        return -1;

    /* Observation points:
       A0 => AltFunc4 of PIO31[4]
       C0 => AltFunc4 of PIO12[2]
       D0 => AltFunc3 of PIO31[0]
       D1 => AltFunc3 of PIO31[2]
    */

    /* Configuring appropriate PIO */
    switch (clk->id) {
    // case CLK_A0_REF ... CLK_IC_LMI:
        // vclk_syscfg_write(22001, 16, 18, 7);   /* Selecting alternate 7 */
        // vclk_syscfg_write(22040, 12, 12, 1); /* Output Enable */
        // vclk_syscfg_write(22060, 12, 12, 0); /* Open drain */
        // vclk_syscfg_write(22050, 12, 12, 0); /* pull up */
        // break;
    // case CLK_C0_REF ... CLK_PERIPH_CPU:
        /* NON LINEAR SYS CONF REG's*/
        // vclk_poke_p(SYSTEM_CONFIG24002, ((vclk_peek_p(SYSTEM_CONFIG24002)&(~(0xf<<8))) | (8<<8))); /* Selecting alternate 8 */
        // vclk_poke_p(SYSTEM_CONFIG24040, ((vclk_peek_p(SYSTEM_CONFIG24040)&(~(1<<18))) | (1<<18))); /* Output Enable */
        // vclk_poke_p(SYSTEM_CONFIG24060, (vclk_peek_p(SYSTEM_CONFIG24060)&(~(1<<18)))); /* Open drain */
        // vclk_poke_p(SYSTEM_CONFIG24050, (vclk_peek_p(SYSTEM_CONFIG24050)&(~(1<<18)))); /* pull up */
        // break;
    // case CLK_D0_REF ... CLK_MCLK_OUT:
        // vclk_syscfg_write(22001, 0, 2, 7); /* Selecting alternate 3 */
        // vclk_syscfg_write(22040, 8, 8, 1); /* Output Enable */
        // vclk_syscfg_write(22060, 8, 8, 0); /* Open drain */
        // vclk_syscfg_write(22050, 8, 8, 0); /* pull up */
        // break;
    // case CLK_D1_REF ... CLK_HDMI_PHASE_REG:
        // vclk_syscfg_write(22001, 8, 10, 7); /* Selecting alternate 3 */
        // vclk_syscfg_write(22040, 10, 10, 1); /* Output Enable */
        // vclk_syscfg_write(22060, 10, 10, 0); /* Open drain */
        // vclk_syscfg_write(22050, 10, 10, 0); /* pull up */
        // break;
    /* Unexpected clocks */
    default:
        return -1;
    }

    if (*div < 1 || *div > 128)
        return -1;

    return flexgen_observe(clk, div);
}

/* ========================================================================
   Name:        get_measure
   Description: Perform HW measure
   Returns:     0=OK, -1=ERROR/UNSUPPORTED
   ======================================================================== */

int get_measure(struct vclk *clk, unsigned long *rate)
{
    switch (clk->id) {
    case CLK_A0_REF ... CLK_ICN_LMI:
        return flexgen_get_measure(clk, &vclk_clocks[CLK_A0_REFOUT0], rate);
    case CLK_A1_REF ... CLK_ADC_DISEQC:
        return flexgen_get_measure(clk, &vclk_clocks[CLK_A1_REFOUT0], rate);
    case CLK_C0_REF ... CLK_PERIPH_CPU:
        return flexgen_get_measure(clk, &vclk_clocks[CLK_C0_REFOUT0], rate);
    case CLK_D0_REF ... CLK_TO_CLKGENC_SRC0:
        return flexgen_get_measure(clk, &vclk_clocks[CLK_D0_REFOUT0], rate);
    case CLK_D1_REF ... CLK_TO_CLKGENC_SRC1:
        return flexgen_get_measure(clk, &vclk_clocks[CLK_D1_REFOUT0], rate);
    };

    return -1;
}

/*
 * Clocks declarations
 * Groups, flexgens, then clocks
 */

_CLK_GROUP(grouptop,
    "TOP",
    identify_parent,
    recalc,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL // get_measure
);
_CLK_GROUP(groupa0,
    "A0",
    identify_parent,
    recalc,
    xable,
    set_ratep,
    set_parent,
    observe,
    "PIO?",
    get_measure
);
_CLK_GROUP(groupa1,
    "A1",
    identify_parent,
    recalc,
    xable,
    set_ratep,
    set_parent,
    observe,
    "PIO?",
    get_measure
);
_CLK_GROUP(groupc0,
    "C0",
    identify_parent,
    recalc,
    xable,
    set_ratep,
    set_parent,
    observe,
    "PIO?",
    get_measure
);
_CLK_GROUP(groupd0,
    "D0",
    identify_parent,
    recalc,
    xable,
    set_ratep,
    set_parent,
    observe,
    "PIO?",
    get_measure
);
_CLK_GROUP(groupd1,
    "D1",
    identify_parent,
    recalc,
    xable,
    set_ratep,
    set_parent,
    observe,
    "PIO?",
    get_measure
);
_CLK_GROUP(groupa9,
    "A9",
    a9_identify_parent,
    a9_recalc,
    a9_xable,
    a9_set_ratep,
    a9_set_parent,
    NULL, // observe: TO BE IMPLEMENTED
    NULL,
    NULL // get_measure
);
_CLK_GROUP(gated,
    "Gated",
    NULL,
    gated_recalc,
    gated_xable,
    NULL,
    NULL,
    NULL, // observe: TO BE IMPLEMENTED
    NULL,
    NULL // get_measure
);

/* Cross bar sources in connected order */
static int xbar_src_A0[] = {
    CLK_A0_PLL0_PHI0,   /* PLL3200 PHI0 */
    CLK_A0_REF,         /* refclkin[0] */
    CLK_A0_REF1,        /* refclkin[1] */
    CLK_A0_SRCIN0,      /* srcclkin[0] */
    CLK_A0_SRCIN1,      /* srcclkin[1] */
    CLK_A0_SRCIN2,      /* srcclkin[2] */
    CLK_A0_REF,         /* !! Mode pin selected !! */
    -1 /* Keep this last */
};
static int xbar_src_A1[] = {
    CLK_A1_PLL0_PHI0,   /* PLL3200 PHI0 */
    CLK_A1_REF,         /* refclkin[0] */
    CLK_A1_REF1,        /* refclkin[1] */
    CLK_A1_SRCIN0,      /* srcclkin[0] */
    CLK_A1_SRCIN1,      /* srcclkin[1] */
    CLK_A1_SRCIN2,      /* srcclkin[2] */
    CLK_A1_REF,         /* !! Mode pin selected !! */
    -1 /* Keep this last */
};
static int xbar_src_C0[] = {
    CLK_C0_PLL0_PHI0,   /* PLL3200 PHI0 */
    CLK_C0_PLL0_PHI1,   /* PLL3200 PHI1 */
    CLK_C0_PLL0_PHI2,   /* PLL3200 PHI2 */
    CLK_C0_PLL0_PHI3,   /* PLL3200 PHI3 */
    CLK_C0_PLL1_PHI0,   /* PLL3200 PHI0 */
    CLK_C0_PLL1_PHI1,   /* PLL3200 PHI1 */
    CLK_C0_PLL1_PHI2,   /* PLL3200 PHI2 */
    CLK_C0_PLL1_PHI3,   /* PLL3200 PHI3 */
    CLK_C0_FS0_VCO,     /* FS660 VCO */
    CLK_C0_FS0_CH0,     /* FS660 chan 0 */
    CLK_C0_FS0_CH1,     /* FS660 chan 1 */
    CLK_C0_FS0_CH2,     /* FS660 chan 2 */
    CLK_C0_FS0_CH3,     /* FS660 chan 3 */
    CLK_C0_REF,         /* refclkin[0] */
    CLK_C0_REF1,        /* refclkin[1] */
    CLK_C0_SRCIN0,      /* srcclkin[0] */
    CLK_C0_SRCIN1,      /* srcclkin[1] */
    CLK_C0_SRCIN2,      /* srcclkin[2] */
    CLK_C0_REF,         /* !! Mode pin selected !! */
    -1 /* Keep this last */
};
static int xbar_src_D0[] = {
    CLK_D0_FS0_VCO,     /* FS660 VCO */
    CLK_D0_FS0_CH0,     /* FS660 chan 0 */
    CLK_D0_FS0_CH1,     /* FS660 chan 1 */
    CLK_D0_FS0_CH2,     /* FS660 chan 2 */
    CLK_D0_FS0_CH3,     /* FS660 chan 3 */
    CLK_D0_REF,         /* refclkin[0] */
    CLK_D0_REF1,        /* refclkin[1] */
    CLK_D0_SRCIN0,      /* srcclkin[0] */
    CLK_D0_SRCIN1,      /* srcclkin[1] */
    CLK_D0_SRCIN2,      /* srcclkin[3] */
    CLK_D0_SRCIN2,      /* srcclkin[4] */
    CLK_D0_REF,         /* !! Mode pin selected !! */
    -1 /* Keep this last */
};
static int xbar_src_D1[] = {
    CLK_D1_FS0_VCO,     /* FS660 VCO */
    CLK_D1_FS0_CH0,     /* FS660 chan 0 */
    CLK_D1_FS0_CH1,     /* FS660 chan 1 */
    CLK_D1_FS0_CH2,     /* FS660 chan 2 */
    CLK_D1_FS0_CH3,     /* FS660 chan 3 */
    CLK_D1_REF,         /* refclkin[0] */
    CLK_D1_REF1,        /* refclkin[1] */
    CLK_D1_SRCIN0,      /* srcclkin[0] */
    CLK_D1_SRCIN1,      /* srcclkin[1] */
    CLK_D1_SRCIN2,      /* srcclkin[3] */
    CLK_D1_SRCIN3,      /* srcclkin[4] */
    CLK_D1_REF,         /* !! Mode pin selected !! */
    -1 /* Keep this last */
};

/* Flexiclockgen hierarchical description
   - Clockgen name (ex: "A0")
   - Physical base address
   - Group the flexgen belongs to
   - Number of 'refclkin' (even if not connected)
   - Refclkout config (muxmap, divmap)
   - Xbar inputs list (in cross bar input order)
*/
static struct flexgen flexgens[] = {
      {
        "A0",
        (void*)CKG_A0_BASE_ADDRESS,
        &groupa0,
        2,        /* Nb of 'refclkin' */
        0x7, 0x6, /* refclkout config */
        xbar_src_A0,
      },
      {
        "A1",
        (void*)CKG_A1_BASE_ADDRESS,
         &groupa1,
         2,        /* Nb of 'refclkin' */
         0x7, 0x6, /* refclkout config */
         xbar_src_A1,
      },
      {
        "C0",
        (void*)CKG_C0_BASE_ADDRESS,
        &groupc0,
        2,          /* Nb of 'refclkin' */
        0x1f, 0x18, /* refclkout config */
        xbar_src_C0,
      },
      {
        "D0",
        (void*)CKG_D0_BASE_ADDRESS,
        &groupd0,
        2,        /* Nb of 'refclkin' */
        0x7, 0x6, /* refclkout config */
        xbar_src_D0,
      },
      {
        "D1",
        (void*)CKG_D1_BASE_ADDRESS,
        &groupd1,
        2,          /* Nb of 'refclkin' */
        0x07, 0x06, /* refclkout config */
        xbar_src_D1,
      },
};

/* Clocks declaration */
struct vclk vclk_clocks[CLK_LAST_ID] = {
/* Top level clocks */
_CLK_FIXED(CLK_REF,                 &grouptop, 25 * 1000000), // 30Mhz possible too

/* CA9 PLL */
_CLK_P(CLK_A9_PHI,                  &groupa9, &vclk_clocks[CLK_REF]),
_CLK(CLK_A9,                        &groupa9),
_CLK_P(CLK_A9_PERIPHS,              &groupa9, &vclk_clocks[CLK_A9]),

/* Clockgen A0 */
_CLK_FG_P(CLK_A0_REF,               &flexgens[0], FG_REFIN, 0, 0, &vclk_clocks[CLK_REF]),
_CLK_FG2(CLK_A0_REF1,               &flexgens[0], VCLK_NOSOURCE, FG_REFIN, 1, 0),

_CLK_FG(CLK_A0_REFOUT0,             &flexgens[0], FG_REFOUT, 0, 0),
_CLK_FG(CLK_A0_REFOUT1,             &flexgens[0], FG_REFOUT, 1, 0),
_CLK_FG_P(CLK_A0_PLL0_VCO,          &flexgens[0], FG_GFGOUT | FG_PLL3200C28 | FG_FVCOBY2, 0, 0, &vclk_clocks[CLK_A0_REFOUT0]),
_CLK_FG_P(CLK_A0_PLL0_PHI0,         &flexgens[0], FG_GFGOUT | FG_PLL3200C28 | FG_PHI, 0, 0, &vclk_clocks[CLK_A0_PLL0_VCO]),

_CLK_FG_DIV(CLK_ICN_LMI,            &flexgens[0], 0),

/* Clockgen A1 */
_CLK_FG_P(CLK_A1_REF,               &flexgens[1], FG_REFIN, 0, 0, &vclk_clocks[CLK_REF]),
_CLK_FG2(CLK_A1_REF1,               &flexgens[1], VCLK_NOSOURCE, FG_REFIN, 1, 0),

_CLK_FG(CLK_A1_REFOUT0,             &flexgens[1], FG_REFOUT, 0, 0),
_CLK_FG(CLK_A1_REFOUT1,             &flexgens[1], FG_REFOUT, 1, 0),
_CLK_FG_P(CLK_A1_PLL0_VCO,          &flexgens[1], FG_GFGOUT | FG_PLL3200C28 | FG_FVCOBY2, 0, 0, &vclk_clocks[CLK_A1_REFOUT0]),
_CLK_FG_P(CLK_A1_PLL0_PHI0,         &flexgens[1], FG_GFGOUT | FG_PLL3200C28 | FG_PHI, 0, 0, &vclk_clocks[CLK_A1_PLL0_VCO]),

_CLK_FG_DIV(CLK_2X_DEMOD,           &flexgens[1], 0),
_CLK_FG_DIV(CLK_ADC_IQ,             &flexgens[1], 1),
_CLK_FG_DIV(CLK_FSK,                &flexgens[1], 2),
_CLK_FG_DIV(CLK_ADC_DISEQC,         &flexgens[1], 3),

/* Clockgen C0 */
_CLK_FG_P(CLK_C0_REF,               &flexgens[2], FG_REFIN, 0, 0, &vclk_clocks[CLK_REF]),
_CLK_FG2(CLK_C0_REF1,               &flexgens[2], VCLK_NOSOURCE, FG_REFIN, 1, 0),

_CLK_FG(CLK_C0_REFOUT0,             &flexgens[2], FG_REFOUT, 0, 0),
_CLK_FG(CLK_C0_REFOUT1,             &flexgens[2], FG_REFOUT, 1, 0),
_CLK_FG(CLK_C0_REFOUT2,             &flexgens[2], FG_REFOUT, 2, 0),
_CLK_FG_P(CLK_C0_PLL0_VCO,          &flexgens[2], FG_GFGOUT | FG_PLL3200C28 | FG_FVCOBY2, 0, 0, &vclk_clocks[CLK_C0_REFOUT0]),
_CLK_FG_P(CLK_C0_PLL0_PHI0,         &flexgens[2], FG_GFGOUT | FG_PLL3200C28 | FG_PHI, 0, 0, &vclk_clocks[CLK_C0_PLL0_VCO]),
_CLK_FG_P(CLK_C0_PLL0_PHI1,         &flexgens[2], FG_GFGOUT | FG_PLL3200C28 | FG_PHI, 0, 1, &vclk_clocks[CLK_C0_PLL0_VCO]),
_CLK_FG_P(CLK_C0_PLL0_PHI2,         &flexgens[2], FG_GFGOUT | FG_PLL3200C28 | FG_PHI, 0, 2, &vclk_clocks[CLK_C0_PLL0_VCO]),
_CLK_FG_P(CLK_C0_PLL0_PHI3,         &flexgens[2], FG_GFGOUT | FG_PLL3200C28 | FG_PHI, 0, 3, &vclk_clocks[CLK_C0_PLL0_VCO]),
_CLK_FG_P(CLK_C0_PLL1_VCO,          &flexgens[2], FG_GFGOUT | FG_PLL3200C28 | FG_FVCOBY2, 1, 0, &vclk_clocks[CLK_C0_REFOUT1]),
_CLK_FG_P(CLK_C0_PLL1_PHI0,         &flexgens[2], FG_GFGOUT | FG_PLL3200C28 | FG_PHI, 1, 0, &vclk_clocks[CLK_C0_PLL1_VCO]),
_CLK_FG_P(CLK_C0_PLL1_PHI1,         &flexgens[2], FG_GFGOUT | FG_PLL3200C28 | FG_PHI, 1, 1, &vclk_clocks[CLK_C0_PLL1_VCO]),
_CLK_FG_P(CLK_C0_PLL1_PHI2,         &flexgens[2], FG_GFGOUT | FG_PLL3200C28 | FG_PHI, 1, 2, &vclk_clocks[CLK_C0_PLL1_VCO]),
_CLK_FG_P(CLK_C0_PLL1_PHI3,         &flexgens[2], FG_GFGOUT | FG_PLL3200C28 | FG_PHI, 1, 3, &vclk_clocks[CLK_C0_PLL1_VCO]),
_CLK_FG_P(CLK_C0_FS0_VCO,           &flexgens[2], FG_GFGOUT | FG_FS660C28 | FG_FVCO, 2, 0, &vclk_clocks[CLK_C0_REFOUT2]),
_CLK_FG_P(CLK_C0_FS0_CH0,           &flexgens[2], FG_GFGOUT | FG_FS660C28 | FG_CHAN, 2, 0, &vclk_clocks[CLK_C0_FS0_VCO]),
_CLK_FG_P(CLK_C0_FS0_CH1,           &flexgens[2], FG_GFGOUT | FG_FS660C28 | FG_CHAN, 2, 1, &vclk_clocks[CLK_C0_FS0_VCO]),
_CLK_FG_P(CLK_C0_FS0_CH2,           &flexgens[2], FG_GFGOUT | FG_FS660C28 | FG_CHAN, 2, 2, &vclk_clocks[CLK_C0_FS0_VCO]),
_CLK_FG_P(CLK_C0_FS0_CH3,           &flexgens[2], FG_GFGOUT | FG_FS660C28 | FG_CHAN, 2, 3, &vclk_clocks[CLK_C0_FS0_VCO]),

_CLK_FG_DIV(CLK_SLIM_CC,            &flexgens[2], 0),
_CLK_FG_DIV(CLK_SPI,                &flexgens[2], 1),
_CLK_FG_DIV(CLK_FLASH,              &flexgens[2], 2),
_CLK_FG_DIV(CLK_MMC_0,              &flexgens[2], 3),
_CLK_FG_DIV(CLK_PERIPH_TS,          &flexgens[2], 4),
_CLK_FG_DIV(CLK_EXT2F_A9,           &flexgens[2], 5),
_CLK_FG_DIV(CLK_ICN_TS,             &flexgens[2], 6),
_CLK_FG_DIV(CLK_NAND,               &flexgens[2], 7),
_CLK_FG_DIV(CLK_MMC_1,              &flexgens[2], 8),
_CLK_FG_DIV(CLK_ST231_GP_2,         &flexgens[2], 9),
_CLK_FG_DIV(CLK_ST231_VDEC,         &flexgens[2], 10),
_CLK_FG_DIV(CLK_ST231_GP_0,         &flexgens[2], 11),
_CLK_FG_DIV(CLK_ST231_GP_1_AUDIO,   &flexgens[2], 12),
_CLK_FG_DIV(CLK_ETH_GMAC,           &flexgens[2], 13),
_CLK_FG_DIV(CLK_GMAC_PHYCLK,        &flexgens[2], 14),
_CLK_FG_DIV(CLK_RCS_RECOVERED_0,    &flexgens[2], 15),
_CLK_FG_DIV(CLK_RCS_RECOVERED_1,    &flexgens[2], 16),
_CLK_FG_DIV(CLK_STBE,               &flexgens[2], 17),
_CLK_FG_DIV(CLK_HEVC_H264,          &flexgens[2], 18),
_CLK_FG_DIV(CLK_AVSP,               &flexgens[2], 19),
_CLK_FG_DIV(CLK_TIMER_A9,           &flexgens[2], 20),
_CLK_FG_DIV(CLK_SLIM_VDEC,          &flexgens[2], 21),
_CLK_FG_DIV(CLK_TSOUT_0,            &flexgens[2], 22),
_CLK_FG_DIV(CLK_TSOUT_1,            &flexgens[2], 23),
_CLK_FG_DIV(CLK_RX_ICN_L0L1,        &flexgens[2], 24),
_CLK_FG_DIV(CLK_FRC_1,              &flexgens[2], 25),
_CLK_FG_DIV(CLK_FRC_1_PAD,          &flexgens[2], 26),
_CLK_FG_DIV(CLK_PWM_REAR,           &flexgens[2], 27),
_CLK_FG_DIV(CLK_TX_ICN_L0L1,        &flexgens[2], 28),
_CLK_FG_DIV(CLK_SPARE_CLK_OUT,      &flexgens[2], 29),
_CLK_FG_DIV(CLK_SYSTEM_STFE,        &flexgens[2], 30),
_CLK_FG_DIV(CLK_C0_SPARE_31,        &flexgens[2], 31),
_CLK_FG_DIV(CLK_SLIM_FDMA_1,        &flexgens[2], 32),
_CLK_FG_DIV(CLK_ATB,                &flexgens[2], 33),
_CLK_FG_DIV(CLK_TRACE,              &flexgens[2], 34),
_CLK_FG_DIV(CLK_BDISP,              &flexgens[2], 35),
_CLK_FG_DIV(CLK_PROC_VDP,           &flexgens[2], 36),
_CLK_FG_DIV(CLK_DSS,                &flexgens[2], 37),
_CLK_FG_DIV(CLK_C0_SPARE_38,        &flexgens[2], 38),
_CLK_FG_DIV(CLK_LPC,                &flexgens[2], 39),
_CLK_FG_DIV(CLK_GPU,                &flexgens[2], 40),
_CLK_FG_DIV(CLK_ICN_DISP,           &flexgens[2], 42),
_CLK_FG_DIV(CLK_ICN_CPU,            &flexgens[2], 43),
_CLK_FG_DIV(CLK_PWM_FRONT,          &flexgens[2], 44),
_CLK_FG_DIV(CLK_SLIM_FDMA_0,        &flexgens[2], 45),
_CLK_FG_DIV(CLK_PERIPH_DISP,        &flexgens[2], 46),
_CLK_FG_DIV(CLK_PERIPH_CPU,         &flexgens[2], 47),

_CLK_P(CLK_EXT2F_A9_BY2,            &groupc0, &vclk_clocks[CLK_EXT2F_A9]),

/* Clockgen D0 */
_CLK_FG_P(CLK_D0_REF,               &flexgens[3], FG_REFIN, 0, 0, &vclk_clocks[CLK_REF]),
_CLK_FG2(CLK_D0_REF1,               &flexgens[3], VCLK_NOSOURCE, FG_REFIN, 1, 0),
_CLK_FG2(CLK_D0_SRCIN0,             &flexgens[3], VCLK_NOSOURCE, FG_SRCIN | FG_BEAT, 0, 0),
_CLK_FG2(CLK_D0_SRCIN1,             &flexgens[3], VCLK_NOSOURCE, FG_SRCIN | FG_BEAT, 1, 0),
_CLK_FG2(CLK_D0_SRCIN2,             &flexgens[3], VCLK_NOSOURCE, FG_SRCIN | FG_BEAT, 2, 0),
_CLK_FG2(CLK_D0_SRCIN3,             &flexgens[3], VCLK_NOSOURCE, FG_SRCIN | FG_BEAT, 3, 0),

_CLK_FG(CLK_D0_REFOUT0,             &flexgens[3], FG_REFOUT, 0, 0),
_CLK_FG_P(CLK_D0_FS0_VCO,           &flexgens[3], FG_GFGOUT | FG_FS660C28 | FG_FVCO, 0, 0, &vclk_clocks[CLK_D0_REFOUT0]),
_CLK_FG_P(CLK_D0_FS0_CH0,           &flexgens[3], FG_GFGOUT | FG_FS660C28 | FG_CHAN | FG_BEAT, 0, 0, &vclk_clocks[CLK_D0_FS0_VCO]),
_CLK_FG_P(CLK_D0_FS0_CH1,           &flexgens[3], FG_GFGOUT | FG_FS660C28 | FG_CHAN | FG_BEAT, 0, 1, &vclk_clocks[CLK_D0_FS0_VCO]),
_CLK_FG_P(CLK_D0_FS0_CH2,           &flexgens[3], FG_GFGOUT | FG_FS660C28 | FG_CHAN, 0, 2, &vclk_clocks[CLK_D0_FS0_VCO]),
_CLK_FG_P(CLK_D0_FS0_CH3,           &flexgens[3], FG_GFGOUT | FG_FS660C28 | FG_CHAN, 0, 3, &vclk_clocks[CLK_D0_FS0_VCO]),

_CLK_FG_DIV(CLK_PCM_0,              &flexgens[3],   0),
_CLK_FG_DIV(CLK_PCM_1,              &flexgens[3],   1),
_CLK_FG_DIV(CLK_PCM_2,              &flexgens[3],   2),
_CLK_FG_DIV(CLK_SPDIF,              &flexgens[3],   3),
_CLK_FG_DIV(CLK_PCMR_0_MASTER,      &flexgens[3],   4),
_CLK_FG_DIV(CLK_PCMIN0_MCLK_OUT,    &flexgens[3],   5),
_CLK_FG_DIV(CLK_TO_CLKGENC_SRC0,    &flexgens[3],  47),

/* Clockgen D1 */
_CLK_FG_P(CLK_D1_REF,               &flexgens[4], FG_REFIN, 0, 0, &vclk_clocks[CLK_REF]),
_CLK_FG2(CLK_D1_REF1,               &flexgens[4], VCLK_NOSOURCE, FG_REFIN, 1, 0),
_CLK_FG(CLK_D1_SRCIN0,              &flexgens[4], FG_SRCIN | FG_BEAT, 0, 0),
_CLK_FG2(CLK_D1_SRCIN1,             &flexgens[4], VCLK_NOSOURCE, FG_SRCIN | FG_BEAT, 1, 0),
_CLK_FG2(CLK_D1_SRCIN2,             &flexgens[4], VCLK_NOSOURCE, FG_SRCIN | FG_BEAT, 2, 0),
_CLK_FG2(CLK_D1_SRCIN3,             &flexgens[4], VCLK_NOSOURCE, FG_SRCIN | FG_BEAT, 3, 0),

_CLK_FG(CLK_D1_REFOUT0,             &flexgens[4], FG_REFOUT, 0, 0),
_CLK_FG_P(CLK_D1_FS0_VCO,           &flexgens[4], FG_GFGOUT | FG_FS660C28 | FG_FVCO, 0, 0, &vclk_clocks[CLK_D1_REFOUT0]),
_CLK_FG_P(CLK_D1_FS0_CH0,           &flexgens[4], FG_GFGOUT | FG_FS660C28 | FG_CHAN | FG_BEAT, 0, 0, &vclk_clocks[CLK_D1_FS0_VCO]),
_CLK_FG_P(CLK_D1_FS0_CH1,           &flexgens[4], FG_GFGOUT | FG_FS660C28 | FG_CHAN | FG_BEAT, 0, 1, &vclk_clocks[CLK_D1_FS0_VCO]),
_CLK_FG_P(CLK_D1_FS0_CH2,           &flexgens[4], FG_GFGOUT | FG_FS660C28 | FG_CHAN, 0, 2, &vclk_clocks[CLK_D1_FS0_VCO]),
_CLK_FG_P(CLK_D1_FS0_CH3,           &flexgens[4], FG_GFGOUT | FG_FS660C28 | FG_CHAN, 0, 3, &vclk_clocks[CLK_D1_FS0_VCO]),

_CLK_FG_DIV2(CLK_PIX_MAIN_DISP,     &flexgens[4], FG_SYNC, 0),
_CLK_FG_DIV2(CLK_PIX_AUX_DISP,      &flexgens[4], FG_SYNC, 1),
_CLK_FG_DIV2(CLK_PIX_HDDAC,         &flexgens[4], FG_SYNC, 2),
_CLK_FG_DIV2(CLK_HDDAC,             &flexgens[4], FG_SYNC, 3),
_CLK_FG_DIV2(CLK_D1_SPARE_4,        &flexgens[4], FG_SYNC, 4),
_CLK_FG_DIV2(CLK_D1_SPARE_5,        &flexgens[4], FG_SYNC, 5),
_CLK_FG_DIV2(CLK_SDDAC,             &flexgens[4], FG_SYNC, 6),
_CLK_FG_DIV2(CLK_PIX_HDMI,          &flexgens[4], FG_SYNC, 7),
_CLK_FG_DIV2(CLK_DENC,              &flexgens[4], FG_SYNC, 8),
_CLK_FG_DIV2(CLK_REF_HDMIPHY,       &flexgens[4], FG_SYNC, 9),
_CLK_FG_DIV(CLK_TMDS_HDMI,          &flexgens[4], 10),
_CLK_FG_DIV(CLK_PROC_HQVDP,         &flexgens[4], 11),
_CLK_FG_DIV(CLK_PROC_MIXER,         &flexgens[4], 13),
_CLK_FG_DIV(CLK_PROC_GDP_COMPO,     &flexgens[4], 14),
_CLK_FG_DIV(CLK_FRC_0,              &flexgens[4], 16),
_CLK_FG_DIV(CLK_FRC_0_PAD,          &flexgens[4], 17),
_CLK_FG_DIV(CLK_FRC_2,              &flexgens[4], 18),
_CLK_FG_DIV(CLK_NCR_SAT_DEMOD,      &flexgens[4], 20),
_CLK_FG_DIV(CLK_HDMI_PHASE_REG,     &flexgens[4], 40),
_CLK_FG_DIV(CLK_TO_CLKGENC_SRC1,    &flexgens[4], 47),

/* Functional clusters gated clocks */
_CLK_P(CLK_2X_DEMOD_GATED,          &gated, &vclk_clocks[CLK_2X_DEMOD]),
_CLK_P(CLK_ADC_IQ_GATED,            &gated, &vclk_clocks[CLK_ADC_IQ]),
_CLK_P(CLK_FSK_GATED,               &gated, &vclk_clocks[CLK_FSK]),
_CLK_P(CLK_ADC_DISEQC_GATED,        &gated, &vclk_clocks[CLK_ADC_DISEQC]),
_CLK_P(CLK_PP_AVSP_GATED,           &gated, &vclk_clocks[CLK_AVSP]),
_CLK_P(CLK_PP_BUS_GATED,            &gated, &vclk_clocks[CLK_ST231_VDEC]),
_CLK_P(CLK_PP_HEVC_GATED,           &gated, &vclk_clocks[CLK_AVSP]),
_CLK_P(CLK_PP_H264_GATED,           &gated, &vclk_clocks[CLK_AVSP]),
_CLK_P(CLK_PP_TARG_GATED,           &gated, &vclk_clocks[CLK_PERIPH_TS]),
_CLK_P(CLK_VDEC_BUS_GATED,          &gated, &vclk_clocks[CLK_ICN_TS]),
_CLK_P(CLK_VDEC_CPU_GATED,          &gated, &vclk_clocks[CLK_ST231_VDEC]),
_CLK_P(CLK_VDEC_SLIM_GATED,         &gated, &vclk_clocks[CLK_SLIM_VDEC]),
_CLK_P(CLK_VDEC_TARG_GATED,         &gated, &vclk_clocks[CLK_PERIPH_TS]),
_CLK_P(CLK_DMU_IC_GATED,            &gated, &vclk_clocks[CLK_PERIPH_TS]),
_CLK_P(CLK_DMU_BUS_GATED,           &gated, &vclk_clocks[CLK_ST231_VDEC]),
_CLK_P(CLK_DMU_CPU_GATED,           &gated, &vclk_clocks[CLK_ST231_VDEC]),

_CLK_P(CLK_AUDIO_PCM_0,             &gated, &vclk_clocks[CLK_PCM_0]),
_CLK_P(CLK_AUDIO_PCM_1,             &gated, &vclk_clocks[CLK_PCM_1]),
_CLK_P(CLK_AUDIO_PCM_2,             &gated, &vclk_clocks[CLK_PCM_2]),
_CLK_P(CLK_AUDIO_SPDIF,             &gated, &vclk_clocks[CLK_SPDIF]),
_CLK_P(CLK_BDISP_CK,                &gated, &vclk_clocks[CLK_BDISP]),
_CLK_P(CLK_BDISP_CPU,               &gated, &vclk_clocks[CLK_PERIPH_DISP]),
_CLK_P(CLK_BDISP_IC,                &gated, &vclk_clocks[CLK_ICN_DISP]),
_CLK_P(CLK_COMPO_CPU,               &gated, &vclk_clocks[CLK_PERIPH_DISP]),
_CLK_P(CLK_COMPO_PROC,              &gated, &vclk_clocks[CLK_PROC_GDP_COMPO]),
_CLK_P(CLK_COMPO_MIX,               &gated, &vclk_clocks[CLK_PROC_MIXER]),
_CLK_P(CLK_COMPO_MIX1,              &gated, &vclk_clocks[CLK_PIX_MAIN_DISP]),
_CLK_P(CLK_COMPO_MIX2,              &gated, &vclk_clocks[CLK_PIX_AUX_DISP]),
_CLK_P(CLK_COMPO_ST,                &gated, &vclk_clocks[CLK_ICN_DISP]),
_CLK_P(CLK_FDMA0_SLIM,              &gated, &vclk_clocks[CLK_SLIM_FDMA_0]),
_CLK_P(CLK_FDMA0_STBUS_T1,          &gated, &vclk_clocks[CLK_PERIPH_DISP]),
_CLK_P(CLK_FDMA0_STBUS_T3_0,        &gated, &vclk_clocks[CLK_PERIPH_DISP]),
_CLK_P(CLK_FDMA0_STBUS_T3_1,        &gated, &vclk_clocks[CLK_ICN_DISP]),
_CLK_P(CLK_FDMA1_SLIM,              &gated, &vclk_clocks[CLK_SLIM_FDMA_1]),
_CLK_P(CLK_FDMA1_STBUS_T1,          &gated, &vclk_clocks[CLK_PERIPH_DISP]),
_CLK_P(CLK_FDMA1_STBUS_T3_0,        &gated, &vclk_clocks[CLK_PERIPH_DISP]),
_CLK_P(CLK_FDMA1_STBUS_T3_1,        &gated, &vclk_clocks[CLK_ICN_DISP]),
_CLK_P(CLK_HDTVOUT_DENC,            &gated, &vclk_clocks[CLK_DENC]),
_CLK_P(CLK_HDTVOUT_IC,              &gated, &vclk_clocks[CLK_PERIPH_DISP]),
_CLK_P(CLK_HDTVOUT_HDDAC,           &gated, &vclk_clocks[CLK_HDDAC]),
_CLK_P(CLK_HDTVOUT_SDDAC,           &gated, &vclk_clocks[CLK_SDDAC]),
_CLK_P(CLK_HDTVOUT_PIX_AUX,         &gated, &vclk_clocks[CLK_PIX_AUX_DISP]),
_CLK_P(CLK_HDTVOUT_PIX_HDDAC,       &gated, &vclk_clocks[CLK_PIX_HDDAC]),
_CLK_P(CLK_HDTVOUT_PIX_HDMI,        &gated, &vclk_clocks[CLK_PIX_HDMI]),
_CLK_P(CLK_HDTVOUT_PIX_MAIN,        &gated, &vclk_clocks[CLK_PIX_MAIN_DISP]),
_CLK_P(CLK_HDTVOUT_TMDS,            &gated, &vclk_clocks[CLK_TMDS_HDMI]),
_CLK_P(CLK_HQVDP_COMPO,             &gated, &vclk_clocks[CLK_PROC_MIXER]),
_CLK_P(CLK_HQVDP_PROC,              &gated, &vclk_clocks[CLK_PROC_HQVDP]),
_CLK_P(CLK_HQVDP_REG,               &gated, &vclk_clocks[CLK_PERIPH_DISP]),
_CLK_P(CLK_HQVDP_SYS,               &gated, &vclk_clocks[CLK_ICN_DISP]),
_CLK_P(CLK_VDP_PIX,                 &gated, &vclk_clocks[CLK_PROC_MIXER]),
_CLK_P(CLK_VDP_PROC,                &gated, &vclk_clocks[CLK_PROC_VDP]),
_CLK_P(CLK_VDP_REG,                 &gated, &vclk_clocks[CLK_PERIPH_DISP]),
_CLK_P(CLK_VDP_SYS,                 &gated, &vclk_clocks[CLK_ICN_DISP]),
_CLK_P(CLK_VDAC_UVW,                &gated, &vclk_clocks[CLK_HDDAC]),
_CLK_P(CLK_VDAC_X,                  &gated, &vclk_clocks[CLK_SDDAC]),
};

int vclk_nb = (sizeof(vclk_clocks) / sizeof(struct vclk));

int vclk_soc_init(void)
{
    vclk_debug(1, "H337/L2A vclkd %s\n", VCLK_SOC_VERSION);

    /* Flexiclockgen lib init */
    if (flexgen_init(vclk_clocks, vclk_nb) != 0)
        return -1;
    /* System config lib init */
    if (vclk_syscfg_init(sysconf_groups) != 0)
        return -1;

    return 0;
}

