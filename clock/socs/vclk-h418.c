/*
* This file is part of Validation CLK Driver (VCLKD).
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
* Author(s): fabrice.charpentier@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* VCLKD may alternatively be licensed under GPL V2 license from ST.
*/

#include <stddef.h>
#include "vclk.h"
#include "vclk_flexgen.h"
#include "vclk-h418.h"
#include "vclk_algos.h"
#include "vclk_private.h"
#include "vclk_sysconf.h"

/* Base addresses */
#define CKG_A0_BASE_ADDRESS     0x90ff000
#define CKG_C0_BASE_ADDRESS     0x9103000
#define CKG_D0_BASE_ADDRESS     0x9104000
#define CKG_D2_BASE_ADDRESS     0x9106000
#define CKG_D3_BASE_ADDRESS     0x9107000

#define SYSCFG_PIO_FRONT_BASE   0x9280000
#define SYSCFG_PIO_REAR_BASE    0x9290000
#define SYSCFG_5000_5999_BASE   0x92B0000

/* SOC top input clocks. */
#define SYS_CLKIN   30

/* Sysconf base addresses declarations */
static struct sysconf_grp sysconf_groups[] = {
    { 1000, 1999, (void*)SYSCFG_PIO_FRONT_BASE },
    { 2000, 2999, (void*)SYSCFG_PIO_REAR_BASE },
    { 5000, 5999, (void*)SYSCFG_5000_5999_BASE },
    { -1 }
};

/* ========================================================================
   Name:        recalc
   Description: Get CKG GFG programmed clocks frequencies
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int recalc(struct vclk *clk)
{
    if (!clk)
        return -1;

    vclk_debug(1, "recalc(%s)\n", clk->name);

    switch (clk->id) {
    /* Flexiclockgen lib handled clocks */
    case CLK_A0_REF ... CLK_A0_SPARE_14:
    case CLK_C0_REF ... CLK_C0_FS0_CH3:
    case CLK_ICN_GPU ... CLK_AVSP_HEVC:
    case CLK_D0_REF ... CLK_D0_FS0_CH3:
    case CLK_USB2_PHY... CLK_D0_SPARE_13:
    case CLK_D2_REF ... CLK_D2_FS0_CH3:
    case CLK_PIX_MAIN_DISP ... CLK_VP9:
    case CLK_D3_REF ... CLK_D3_FS0_CH3:
    case CLK_STFE_FRC1 ... CLK_LPC_1:
        return flexgen_recalc(clk);

    /* Special cases */
    case CLK_SYSIN:
    case CLK_C0_JEIN_0 ... CLK_HDMITX_TMDSCLK:
    case CLK_D0_JEIN_0 ... CLK_MIPHYUSB3_TX:
    case CLK_TMDSOUT_HDMI ... CLK_USB2_PHY2:
    case CLK_ACG ... CLK_HDMIRX_FS3:
        clk->flags &= ~VCLK_OFF;
        if (!clk->parent)
            clk->flags |= VCLK_NORATE;
        else
            clk->rate = clk->parent->rate;
        break;

    /* Unexpected clocks */
    default:
        return -1;
    }

    return 0;
}

/* ========================================================================
   Name:        identify_parent
   Description: Identify GFG source clock
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int identify_parent(struct vclk *clk)
{
    if (!clk)
        return -1;

    vclk_debug(1, "identify_parent(%s)\n", clk->name);

    switch (clk->id) {
    /* Single source clocks */
    case CLK_A0_REF:
    case CLK_A0_PLL0_VCO ... CLK_A0_PLL0_PHI0:
    case CLK_C0_REF:
    case CLK_C0_PLL0_VCO ... CLK_C0_FS0_CH3:
    case CLK_D0_REF:
    case CLK_D0_FS0_VCO... CLK_D0_FS0_CH3:
    case CLK_D2_REF:
    case CLK_D2_FS0_VCO ... CLK_D2_FS0_CH3:
    case CLK_TMDSOUT_HDMI:  /* From HDMI TX PHY */
    case CLK_PIX_HD_P_IN:   /* From pad */
    case CLK_D2_JEIN0 ... CLK_USB2_PHY2: /* clk_je_inputs */
    case CLK_D3_REF:
    case CLK_D3_FS0_VCO ... CLK_D3_FS0_CH3:
        break;

    /* Flexiclocken lib handled clocks */
    default:
        return flexgen_identify_parent(clk);
    }

    return 0;
}

/* ========================================================================
   Name:        set_parent
   Description: Set clock parent
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int set_parent(struct vclk *clk, struct vclk *src)
{
    if (!clk)
        return -1;

    vclk_debug(1, "set_parent(%s, src=%s)\n", clk->name, src->name);

    switch (clk->id) {
    /* Single source clocks not handled by flexiclockgen */
    case CLK_SYSIN:
    case CLK_A0_REF:
    case CLK_C0_REF:
    case CLK_D0_REF:
    case CLK_USB2_PHY:
    case CLK_D2_REF ... CLK_D2_REF1:
    case CLK_TMDSOUT_HDMI ... CLK_USB2_PHY2: /* srcclkinX + je_inX */
    case CLK_D3_REF:
        break;

    /* Flexiclocken lib handled clocks */
    default:
        return flexgen_set_parent(clk, src);
    }

    return 0;
}

/* ========================================================================
   Name:        set_ratep
   Description: Set clock frequency
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int set_ratep(struct vclk *clk, unsigned long freq, unsigned long prate)
{
    int err;

    if (!clk)
        return -1;

    vclk_debug(1, "set_ratep(%s, rate=%lu, prate=%lu)\n", clk->name, freq, prate);

    switch (clk->id) {
    /* Flexiclockgen lib handled clocks */
    case CLK_A0_REF ... CLK_IC_LMI1:
    case CLK_C0_REF ... CLK_AVSP_HEVC:
    case CLK_D0_REF ... CLK_USB2_PHY:
    case CLK_D0_SPARE_5:
    case CLK_D2_REF ... CLK_VP9:
    case CLK_D3_REF ... CLK_LPC_1:
        return flexgen_set_ratep(clk, freq, prate);

    /* Special treatment for audio clockgen (D0).
       Changing clock at FS output, keeping div in bypass */
    case CLK_PCM_0 ... CLK_PCMR0_MASTER:
        err = flexgen_set_ratep(clk->parent, freq, prate);
        return err;

    /* Unexpected clocks */
    default:
        return -1;
    }

    return 0;
}

/* ========================================================================
   Name:        xable
   Description: Enable/disable clock
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int xable(struct vclk *clk, int enable)
{
    int err;

    if (!clk)
        return -1;

    vclk_debug(1, "xable(%s, en=%d)\n", clk->name, enable);

    switch (clk->id) {
    /* Flexiclockgen lib handled clocks */
    case CLK_A0_REF ... CLK_IC_LMI1:
    case CLK_C0_REF ... CLK_AVSP_HEVC:
    case CLK_D0_REF ... CLK_USB2_PHY:
    case CLK_D0_SPARE_5:
    case CLK_D2_REF ... CLK_VP9:
    case CLK_D3_REF ... CLK_LPC_1:
        err = flexgen_xable(clk, enable);
        break;

    /* Special treatment for audio clockgen (D0).
       Enabling source (FS) then audio clock */
    case CLK_PCM_0 ... CLK_PCMR0_MASTER:
        err = flexgen_xable(clk->parent, enable);
        if (!err)
            err = flexgen_xable(clk, enable);
        break;

    /* Unexpected clocks */
    default:
        return -1;
    }

    /* Recalc is now done by upper layer */
    return err;
}

/* ========================================================================
   Name:        observe
   Description: Route clock to IO for observation purposes
   Returns:     'vclk_err_t' error code
   ======================================================================== */

static int observe(struct vclk *clk, unsigned long *div)
{
    if (!clk || !*div)
        return -1;

    /* Observation points:
       A0 => AltFunc4 of PIO16[0]
       C0 => AltFunc4 of PIO16[6]
       D0 => AltFunc3 of PIO33[4]
       D2 => AltFunc3 of PIO35[3]
       D3 => AltFunc4 of PIO17[0]
    */

/*
# warning "warning: clkgen_freq_synth_observe needs extra work "
# warning "warning: because all the PIO"
# warning "warning: already acquired by PAD manager"
*/

    /* Configuring appropriate PIO */
    switch (clk->id) {
    case CLK_A0_REF ... CLK_IC_LMI0:
        vclk_syscfg_write(1006, 0, 2, 4);   /* Selecting alternate 4 */
        vclk_syscfg_write(1041, 16, 16, 1); /* Output Enable */
        vclk_syscfg_write(1061, 16, 16, 0); /* Open drain */
        vclk_syscfg_write(1051, 16, 16, 0); /* pull up */
        break;
    case CLK_C0_REF ... CLK_ICN_REG_DIV2:
        vclk_syscfg_write(1006, 24, 26, 4); /* Selecting alternate 4 */
        vclk_syscfg_write(1041, 22, 22, 1); /* Output Enable */
        vclk_syscfg_write(1061, 22, 22, 0); /* Open drain */
        vclk_syscfg_write(1051, 22, 22, 0); /* pull up */
        break;
    case CLK_D0_REF ... CLK_USB2_PHY:
        vclk_syscfg_write(2003, 16, 18, 3); /* Selecting alternate 3 */
        vclk_syscfg_write(2040, 28, 28, 1); /* Output Enable */
        vclk_syscfg_write(2060, 28, 28, 0); /* Open drain */
        vclk_syscfg_write(2050, 28, 28, 0); /* pull up */
        break;
    case CLK_D2_REF ... CLK_VP9:
        vclk_syscfg_write(2005, 12, 14, 3); /* Selecting alternate 3 */
        vclk_syscfg_write(2041, 11, 11, 1); /* Output Enable */
        vclk_syscfg_write(2061, 11, 11, 0); /* Open drain */
        vclk_syscfg_write(2051, 11, 11, 0); /* pull up */
        break;
    case CLK_D3_REF ... CLK_LPC_1:
        vclk_syscfg_write(1007, 0, 2, 4);   /* Selecting alternate 4 */
        vclk_syscfg_write(1041, 24, 24, 1); /* Output Enable */
        vclk_syscfg_write(1061, 24, 24, 0); /* Open drain */
        vclk_syscfg_write(1051, 24, 24, 0); /* pull up */
        break;
    /* Unexpected clocks */
    default:
        return -1;
    }

    if (*div < 1 || *div > 128)
        return -1;

    return flexgen_observe(clk, div);
}

/******************************************************************************
CA9 PLL
******************************************************************************/

/* ========================================================================
   Name:        a9_recalc
   Description: Get clocks frequencies (in Hz)
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int a9_recalc(struct vclk *clk)
{
    unsigned long odf, prate;

    if (!clk)
        return -1;

    vclk_debug(1, "a9_recalc(%s)\n", clk->name);

    clk->flags &= ~VCLK_OFF; // Assuming clock enabled
    prate = vclk_get_prate(clk);
    switch (clk->id) {
    case CLK_A9_REF:
    case CLK_A9:
        clk->rate = prate;
        break;
    case CLK_A9_PERIPHS:
        clk->rate = prate / 2;
        break;
    case CLK_A9_PHI0:
        odf = vclk_syscfg_read(5108, 8, 13);
        if (odf == 0)
            odf = 1;
        clk->rate = prate / odf;
        break;
    case CLK_A9_FVCOBY2:
        {
            struct vclk_pll pll = {
                .type = VCLK_PLL4600C28,
            };
            int err;

            if (vclk_syscfg_read(5106, 0, 0))
                /* A9_PLL_PD=1 => PLL disabled */
                clk->flags |= VCLK_OFF;

            pll.idf = vclk_syscfg_read(5106, 25, 27);
            pll.ndiv = vclk_syscfg_read(5108, 0, 7);
            err = vclk_pll_get_rate(prate, &pll, &clk->rate);
            vclk_debug(1, "  idf=%u ndiv=%u prate=%lu => rate=%lu\n", pll.idf, pll.ndiv, prate, clk->rate);
            if (err)
                return -1;
        }
        break;
    default:
        return -1;   /* Unknown clock */
    }

    return 0;
}

/* ========================================================================
   Name:        a9_identify_parent
   Description: Identify parent clock for clockgen A9 clocks.
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int a9_identify_parent(struct vclk *clk)
{
    vclk_debug(1, "a9_identify_parent(%s)\n", clk->name);

    if (clk->id != CLK_A9) /* Other clocks have static parent */
        return 0;

    /* Is CA9 clock sourced from PLL or C0-13 ? */
    if (vclk_syscfg_read(5105, 1, 1)) {
        /* Yes. Div by 1 or div by 2 ? */
        if (vclk_syscfg_read(5105, 0, 0))
            clk->parent = &vclk_clocks[CLK_ICN_REG];
        else
            clk->parent = &vclk_clocks[CLK_ICN_REG_DIV2];
    } else
        clk->parent = &vclk_clocks[CLK_A9_PHI0];

    return 0;
}

/* ========================================================================
   Name:        a9_set_ratep
   Description: Set clock frequency
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int a9_set_ratep(struct vclk *clk, unsigned long freq, unsigned long prate)
{
    unsigned long odf;
    struct vclk_pll pll = {
        .type = VCLK_PLL4600C28,
    };
    int err = 0;

    if (!clk)
        return -1;
    if (!clk->parent)
        return -1;

    vclk_debug(1, "a9_set_ratep(%s, %lu, prate=%lu)\n", clk->name, freq, prate);

    switch (clk->id) {
    case CLK_A9: // A9 = mux output
        return a9_set_ratep(clk->parent, freq, prate);
    case CLK_A9_PHI0:
        odf = vclk_best_div(prate, freq);
        vclk_syscfg_write(5108, 8, 13, odf);       /* ODF */
        break;
    case CLK_A9_FVCOBY2:
        err = vclk_pll_get_params(prate, freq, &pll);
        if (err != 0) {
            vclk_error("ERROR: Can't compute PLL4600 params for %lu Hz\n", freq);
            return err;
        }
        vclk_debug(1, "  idf=%u ndiv=%u\n", pll.idf, pll.ndiv, pll.cp);

        vclk_syscfg_write(5105, 1, 1, 1);          /* CLK_CA9=C0-13 clock */

        vclk_syscfg_write(5106, 0, 0, 1);          /* Disabling PLL */
        vclk_syscfg_write(5106, 25, 27, pll.idf);  /* IDF */
        vclk_syscfg_write(5108, 0, 7, pll.ndiv);   /* NDIV */
        vclk_syscfg_write(5106, 0, 0, 0);          /* Reenabling PLL */

        while (!vclk_syscfg_read(5543, 0, 0))      /* Wait for lock */
            ;
        /* Can't put any delay because may rely on a clock that is currently
           changing (running from CA9 case). */

        vclk_syscfg_write(5105, 1, 1, 0);          /* CLK_CA9=PLL */
        break;
    default:
        return -1;
    }

    /* Recalc is now done by upper layer */
    return 0;
}

/* ========================================================================
   Name:        a9_set_parent
   Description: Identify parent clock for clockgen A9 clocks.
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int a9_set_parent(struct vclk *clk, struct vclk *src)
{
    vclk_debug(1, "a9_set_parent(%s, %s)\n", clk->name, src->name);

    if (clk->id != CLK_A9) /* Other clocks have static parent */
        return 0;

    if (src->id == CLK_A9_PHI0) {
        vclk_syscfg_write(5105, 1, 1, 0);
        clk->parent = &vclk_clocks[CLK_A9_PHI0];
    } else {
        vclk_syscfg_write(5105, 1, 1, 1);
        if (src->id == CLK_ICN_REG) {
            vclk_syscfg_write(5105, 0, 0, 1);
            clk->parent = &vclk_clocks[CLK_ICN_REG];
        } else {
            vclk_syscfg_write(5105, 0, 0, 0);
            clk->parent = &vclk_clocks[CLK_ICN_REG_DIV2];
        }
    }

    /* Recalc done by upper layer */
    return 0;
}

/*
 * Gated clocks
 */

/* ========================================================================
   Name:        gated_recalc
   Description: Get clocks frequencies (in Hz)
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int gated_recalc(struct vclk *clk)
{
    unsigned long val, reg;

    switch (clk->id) {
    case CLK_DMU_BUS ... CLK_DMU_CPU:
        reg = 5208;
        break;
    case CLK_STBE_TP ... CLK_STBE_CONFIG:
        reg = 5210;
        break;
    case CLK_HEVC_BUS ... CLK_HEVC_ASVP:
        reg = 5211;
        break;
    case CLK_HQVDP0_SYS ... CLK_HQVDP0_PIX:
        reg = 5212;
        break;
    case CLK_HQVDP1_SYS ... CLK_HQVDP1_PIX:
        reg = 5213;
        break;
    case CLK_HQVDP2_SYS ... CLK_HQVDP2_PIX:
        reg = 5214;
        break;
    case CLK_HVA_GATED ... CLK_HVA_ICNPERIPH:
        reg = 5209;
        break;
    default:
        return -1;
    };

    if (clk->parent)
        clk->rate = clk->parent->rate;
    else
        clk->flags |= VCLK_NORATE;
    val = vclk_syscfg_read(reg, 0, 0);
    if (val)
        clk->flags &= ~VCLK_OFF;
    else
        clk->flags |= VCLK_OFF;
    return 0;
}

/* ========================================================================
   Name:        gated_xable
   Description: Enabled/disable clock
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int gated_xable(struct vclk *clk, int enable)
{
    unsigned long reg;

    /* Bit is always 0 */
    switch (clk->id) {
    case CLK_DMU_BUS ... CLK_DMU_CPU:
        reg = 5208;
        break;
    case CLK_STBE_TP ... CLK_STBE_CONFIG:
        reg = 5210;
        break;
    case CLK_HEVC_BUS ... CLK_HEVC_ASVP:
        reg = 5211;
        break;
    case CLK_HQVDP0_SYS ... CLK_HQVDP0_PIX:
        reg = 5212;
        break;
    case CLK_HQVDP1_SYS ... CLK_HQVDP1_PIX:
        reg = 5213;
        break;
    case CLK_HQVDP2_SYS ... CLK_HQVDP2_PIX:
        reg = 5214;
        break;
    case CLK_HVA_GATED ... CLK_HVA_ICNPERIPH:
        reg = 5209;
        break;
    default:
        return -1;
    }

    if (enable)
        vclk_syscfg_write(reg, 0, 0, 1);
    else
        vclk_syscfg_write(reg, 0, 0, 0);

    /* Recalc is now done by upper layer */
    return 0;
}

/*
 * Clocks declarations
 * Groups, flexgens, then clocks
 */

_CLK_GROUP(grouptop,
    "TOP",
    identify_parent,
    recalc,
    NULL, // xable,
    NULL, // set_ratep,
    NULL, // set_parent,
    NULL, // observe
    NULL, // observation point
    NULL  // get_measure
);
_CLK_GROUP(groupa0,
    "A0",
    identify_parent,
    recalc,
    xable,
    set_ratep,
    set_parent,
    observe,
    "PIO16[0]",
    NULL  // get_measure
);
_CLK_GROUP(groupc0,
    "C0",
    identify_parent,
    recalc,
    xable,
    set_ratep,
    set_parent,
    observe,
    "PIO16[6]",
    NULL  // get_measure
);
_CLK_GROUP(groupd0,
    "D0",
    identify_parent,
    recalc,
    xable,
    set_ratep,
    set_parent,
    observe,
    "PIO33[4]",
    NULL  // get_measure
);
_CLK_GROUP(groupd2,
    "D2",
    identify_parent,
    recalc,
    xable,
    set_ratep,
    set_parent,
    observe,
    "PIO35[3]",
    NULL  // get_measure
);
_CLK_GROUP(groupd3,
    "D3",
    identify_parent,
    recalc,
    xable,
    set_ratep,
    set_parent,
    observe,
    "PIO17[0]",
    NULL  // get_measure
);
_CLK_GROUP(groupa9,
    "A9",
    a9_identify_parent,
    a9_recalc,
    NULL, // xable: TO BE IMPLEMENTED
    a9_set_ratep,
    a9_set_parent,
    NULL, // observe: TO BE IMPLEMENTED
    NULL,
    NULL  // get_measure
);
_CLK_GROUP(gated,
    "Gated",
    NULL,
    gated_recalc,
    gated_xable,
    NULL,
    NULL,
    NULL, // observe: TO BE IMPLEMENTED
    NULL,
    NULL  // get_measure
);

/* Cross bar sources in connected order */
static int xbar_src_A0[] = {
    CLK_A0_PLL0_PHI0,   /* PLL3200 PHI0 */
    CLK_A0_REF,         /* refclkin[0] */
    -1 /* Keep this last */
};
static int xbar_src_C0[] = {
    CLK_C0_PLL0_PHI0,   /* PLL3200 PHI0 */
    CLK_C0_PLL1_PHI0,   /* PLL3200 PHI0 */
    CLK_C0_FS0_CH0,     /* Chan 0 */
    CLK_C0_FS0_CH1,
    CLK_C0_FS0_CH2,
    CLK_C0_FS0_CH3,
    CLK_C0_REF,         /* refclkin[0] */
    -1 /* Keep this last */
};
static int xbar_src_D0[] = {
    CLK_D0_FS0_CH0,
    CLK_D0_FS0_CH1,
    CLK_D0_FS0_CH2,
    CLK_D0_FS0_CH3,
    CLK_D0_REF,
    -1 /* Keep this last */
};
static int xbar_src_D2[] = {
    CLK_D2_FS0_CH0,
    CLK_D2_FS0_CH1,
    CLK_D2_FS0_CH2,
    CLK_D2_FS0_CH3,
    CLK_D2_REF,         /* refclkin[0] */
    CLK_D2_REF1,        /* refclkin[1], no source */
    CLK_TMDSOUT_HDMI,   /* srcclkin[0] */
    CLK_PIX_HD_P_IN,    /* srcclkin[1] */
    -1 /* Keep this last */
};
static int xbar_src_D3[] = {
    CLK_D3_FS0_CH0,
    CLK_D3_FS0_CH1,
    CLK_D3_FS0_CH2,
    CLK_D3_FS0_CH3,
    CLK_D3_REF,         /* refclkin[0] */
    -1 /* Keep this last */
};

/* Flexiclockgen hierarchical description
   - Clockgen name (ex: "A0")
   - Physical base address
   - Group the flexgen belongs to
   - Number of 'refclkin' (even if not connected)
   - Refclkout config (muxmap, divmap)
   - Xbar inputs list (in cross bar input order)
*/
static struct flexgen flexgens[] = {
    {
        "A0",
        (void*)CKG_A0_BASE_ADDRESS,
        &groupa0,
        2,          /* Nb of 'refclkin' */
        0x7, 0x6,   /* refclkout config */
        xbar_src_A0,
    },
    {
        "C0",
        (void*)CKG_C0_BASE_ADDRESS,
        &groupc0,
        2,          /* Nb of 'refclkin' */
        0x1f, 0x18, /* refclkout config */
        xbar_src_C0,
    },
    {
        "D0",
        (void*)CKG_D0_BASE_ADDRESS,
        &groupd0,
        2,          /* Nb of 'refclkin' */
        0x7, 0x6,   /* refclkout config */
        xbar_src_D0,
    },
    {
        "D2",
        (void*)CKG_D2_BASE_ADDRESS,
        &groupd2,
        2,          /* Nb of 'refclkin' */
        0x7, 0x6,   /* refclkout config */
        xbar_src_D2,/* Xbar inputs */
    },
    {
        "D3",
        (void*)CKG_D3_BASE_ADDRESS,
        &groupd3,
        2,          /* Nb of 'refclkin' */
        0x7, 0x6,   /* refclkout config */
        xbar_src_D3,
    },
    {
        NULL /* Keep this last */
    }
};

struct vclk vclk_clocks[] = {
/* Top level clocks */
_CLK_FIXED(CLK_SYSIN, &grouptop, SYS_CLKIN * 1000000),

/* CA9 PLL */
_CLK_P(CLK_A9_REF,      &groupa9,  &vclk_clocks[CLK_SYSIN]),
_CLK_P(CLK_A9_FVCOBY2,  &groupa9,  &vclk_clocks[CLK_A9_REF]),
_CLK_P(CLK_A9_PHI0,     &groupa9,  &vclk_clocks[CLK_A9_FVCOBY2]),
_CLK(CLK_A9,            &groupa9),
_CLK_P(CLK_A9_PERIPHS,  &groupa9,  &vclk_clocks[CLK_A9]),

/* Clockgen A0 */
_CLK_FG_P(CLK_A0_REF,       &flexgens[0], FG_REFIN, 0, 0, &vclk_clocks[CLK_SYSIN]),
_CLK_FG(CLK_A0_REFOUT0,     &flexgens[0], FG_REFOUT, 0, 0),
_CLK_FG_P(CLK_A0_PLL0_VCO,  &flexgens[0], FG_GFGOUT | FG_PLL3200C32 | FG_FVCOBY2, 0, 0, &vclk_clocks[CLK_A0_REFOUT0]),
_CLK_FG_P(CLK_A0_PLL0_PHI0, &flexgens[0], FG_GFGOUT | FG_PLL3200C32 | FG_PHI, 0, 0, &vclk_clocks[CLK_A0_PLL0_VCO]),

_CLK_FG_DIV(CLK_IC_LMI0,    &flexgens[0], 0),
_CLK_FG_DIV(CLK_IC_LMI1,    &flexgens[0], 1),

_CLK_FG_DIV(CLK_A0_SPARE_14,&flexgens[0], 14),

/* Clockgen C0 */
_CLK_FG_P(CLK_C0_REF,       &flexgens[1], FG_REFIN, 0, 0, &vclk_clocks[CLK_SYSIN]),
_CLK_FG(CLK_C0_REFOUT0,     &flexgens[1], FG_REFOUT, 0, 0),
_CLK_FG(CLK_C0_REFOUT1,     &flexgens[1], FG_REFOUT, 1, 0),
_CLK_FG(CLK_C0_REFOUT2,     &flexgens[1], FG_REFOUT, 2, 0),
_CLK_FG_P(CLK_C0_PLL0_VCO,  &flexgens[1], FG_GFGOUT | FG_PLL3200C32 | FG_FVCOBY2, 0, 0, &vclk_clocks[CLK_C0_REFOUT0]),
_CLK_FG_P(CLK_C0_PLL0_PHI0, &flexgens[1], FG_GFGOUT | FG_PLL3200C32 | FG_PHI, 0, 0, &vclk_clocks[CLK_C0_PLL0_VCO]),
_CLK_FG_P(CLK_C0_PLL1_VCO,  &flexgens[1], FG_GFGOUT | FG_PLL3200C32 | FG_FVCOBY2, 1, 0, &vclk_clocks[CLK_C0_REFOUT1]),
_CLK_FG_P(CLK_C0_PLL1_PHI0, &flexgens[1], FG_GFGOUT | FG_PLL3200C32 | FG_PHI, 1, 0, &vclk_clocks[CLK_C0_PLL1_VCO]),
_CLK_FG_P(CLK_C0_FS0_VCO,   &flexgens[1], FG_GFGOUT | FG_FS660C32 | FG_FVCO, 2, 0, &vclk_clocks[CLK_C0_REFOUT2]),
_CLK_FG_P(CLK_C0_FS0_CH0,   &flexgens[1], FG_GFGOUT | FG_FS660C32 | FG_CHAN, 2, 0, &vclk_clocks[CLK_C0_FS0_VCO]),
_CLK_FG_P(CLK_C0_FS0_CH1,   &flexgens[1], FG_GFGOUT | FG_FS660C32 | FG_CHAN, 2, 1, &vclk_clocks[CLK_C0_FS0_VCO]),
_CLK_FG_P(CLK_C0_FS0_CH2,   &flexgens[1], FG_GFGOUT | FG_FS660C32 | FG_CHAN, 2, 2, &vclk_clocks[CLK_C0_FS0_VCO]),
_CLK_FG_P(CLK_C0_FS0_CH3,   &flexgens[1], FG_GFGOUT | FG_FS660C32 | FG_CHAN, 2, 3, &vclk_clocks[CLK_C0_FS0_VCO]),

_CLK_FG_P(CLK_C0_JEIN_0,        &flexgens[1],   FG_JEIN, 0, 0, &vclk_clocks[CLK_A0_SPARE_14]), /* je_in[0] */
_CLK_FG(CLK_HDMIRXFSYN_2,       &flexgens[1],   FG_JEIN, 1, 0), /* je_in[1] */
_CLK_FG(CLK_HDMIRXFSYN_3,       &flexgens[1],   FG_JEIN, 2, 0), /* je_in[2] */
_CLK_FG(CLK_HDMITX_PLLINFOUT,   &flexgens[1],   FG_JEIN, 3, 0), /* je_in[3] */
_CLK_FG(CLK_HDMITX_PLLREFOUT,   &flexgens[1],   FG_JEIN, 4, 0), /* je_in[4] */
_CLK_FG(CLK_HDMITX_TMDSCLK,     &flexgens[1],   FG_JEIN, 5, 0), /* je_in[5] */

_CLK_FG_DIV(CLK_ICN_GPU,        &flexgens[1],   0),
_CLK_FG_DIV(CLK_FDMA,           &flexgens[1],   1),
_CLK_FG_DIV(CLK_NAND,           &flexgens[1],   2),
_CLK_FG_DIV(CLK_HVA,            &flexgens[1],   3),
_CLK_FG_DIV(CLK_PROC_STFE,      &flexgens[1],   4),
_CLK_FG_DIV(CLK_TP,             &flexgens[1],   5),
_CLK_FG_DIV(CLK_RX_ICN_DMU,     &flexgens[1],   6),
_CLK_FG_DIV(CLK_RX_ICN_HVA,     &flexgens[1],   7),
_CLK_FG_DIV(CLK_TX_ICN_DMU,     &flexgens[1],   9),
_CLK_FG_DIV(CLK_MMC_0,          &flexgens[1],   10),
_CLK_FG_DIV(CLK_MMC_1,          &flexgens[1],   11),
_CLK_FG_DIV(CLK_JPEGDEC,        &flexgens[1],   12),
_CLK_FG_DIV(CLK_ICN_REG,        &flexgens[1],   13),
_CLK_FG_DIV(CLK_IC_BDISP_0,     &flexgens[1],   14),
_CLK_FG_DIV(CLK_IC_BDISP_1,     &flexgens[1],   15),
_CLK_FG_DIV(CLK_PP_DMU,         &flexgens[1],   16),
_CLK_FG_DIV(CLK_VID_DMU,        &flexgens[1],   17),
_CLK_FG_DIV(CLK_DSS_LPC,        &flexgens[1],   18),
_CLK_FG_DIV(CLK_ST231_AUD_0,    &flexgens[1],   19),
_CLK_FG_DIV(CLK_ST231_GP_1,     &flexgens[1],   20),
_CLK_FG_DIV(CLK_ST231_DMU,      &flexgens[1],   21),
_CLK_FG_DIV(CLK_ICN_LMI,        &flexgens[1],   22),
_CLK_FG_DIV(CLK_TX_ICN_1,       &flexgens[1],   23),
_CLK_FG_DIV(CLK_ICN_SBC,        &flexgens[1],   24),
_CLK_FG_DIV(CLK_STFE_FRC2,      &flexgens[1],   25),
_CLK_FG_DIV(CLK_ETH_PHY,        &flexgens[1],   26),
_CLK_FG_DIV(CLK_ETH_PHYREF,     &flexgens[1],   27),
_CLK_FG_DIV(CLK_FLASH_PROMIP,   &flexgens[1],   28),
_CLK_FG_DIV(CLK_MAIN_DISP,      &flexgens[1],   29),
_CLK_FG_DIV(CLK_AUX_DISP,       &flexgens[1],   30),
_CLK_FG_DIV(CLK_COMPO_DVP,      &flexgens[1],   31),
_CLK_FG_DIV(CLK_TX_ICN_HADES,   &flexgens[1],   32),
_CLK_FG_DIV(CLK_RX_ICN_HADES,   &flexgens[1],   33),
_CLK_FG_DIV(CLK_ICN_REG_16,     &flexgens[1],   34),
_CLK_FG_DIV(CLK_PP_HEVC,        &flexgens[1],   35),
_CLK_FG_DIV(CLK_CLUST_HEVC,     &flexgens[1],   36),
_CLK_FG_DIV(CLK_HWPE_HEVC,      &flexgens[1],   37),
_CLK_FG_DIV(CLK_FC_HEVC,        &flexgens[1],   38),
_CLK_FG_DIV(CLK_PROC_MIXER,     &flexgens[1],   39),
_CLK_FG_DIV(CLK_PROC_SC,        &flexgens[1],   40),
_CLK_FG_DIV(CLK_AVSP_HEVC,      &flexgens[1],   41),

/* Clockgen D0 */
_CLK_FG_P(CLK_D0_REF,           &flexgens[2],   FG_REFIN, 0, 0, &vclk_clocks[CLK_SYSIN]),
_CLK_FG(CLK_D0_REFOUT0,         &flexgens[2],   FG_REFOUT, 0, 0),
_CLK_FG_P(CLK_D0_FS0_VCO,       &flexgens[2],   FG_GFGOUT | FG_FS660C32 | FG_FVCO, 0, 0, &vclk_clocks[CLK_D0_REFOUT0]),
_CLK_FG_P(CLK_D0_FS0_CH0,       &flexgens[2],   FG_GFGOUT | FG_FS660C32 | FG_CHAN, 0, 0, &vclk_clocks[CLK_D0_FS0_VCO]),
_CLK_FG_P(CLK_D0_FS0_CH1,       &flexgens[2],   FG_GFGOUT | FG_FS660C32 | FG_CHAN, 0, 1, &vclk_clocks[CLK_D0_FS0_VCO]),
_CLK_FG_P(CLK_D0_FS0_CH2,       &flexgens[2],   FG_GFGOUT | FG_FS660C32 | FG_CHAN, 0, 2, &vclk_clocks[CLK_D0_FS0_VCO]),
_CLK_FG_P(CLK_D0_FS0_CH3,       &flexgens[2],   FG_GFGOUT | FG_FS660C32 | FG_CHAN, 0, 3, &vclk_clocks[CLK_D0_FS0_VCO]),

_CLK_FG_P(CLK_D0_JEIN_0,        &flexgens[2],   FG_JEIN, 0, 0, &vclk_clocks[CLK_TMDS_HDMI]), /* je_in[0] */
_CLK_FG(CLK_MIPHYPS_0_RX,       &flexgens[2],   FG_JEIN, 1, 0), /* je_in[1] */
_CLK_FG(CLK_MIPHYPS_0_TX,       &flexgens[2],   FG_JEIN, 2, 0), /* je_in[2] */
_CLK_FG(CLK_MIPHYPS_1_RX,       &flexgens[2],   FG_JEIN, 3, 0), /* je_in[3] */
_CLK_FG(CLK_MIPHYPS_1_TX,       &flexgens[2],   FG_JEIN, 4, 0), /* je_in[4] */
_CLK_FG(CLK_MIPHYUSB3_RX,       &flexgens[2],   FG_JEIN, 5, 0), /* je_in[5] */
_CLK_FG(CLK_MIPHYUSB3_TX,       &flexgens[2],   FG_JEIN, 6, 0), /* je_in[6] */

_CLK_FG(CLK_USB2_PHY,           &flexgens[2],   FG_EREFOUT, 0, 0), /* ext_refclkout[0] */

_CLK_FG_DIV(CLK_PCM_0,          &flexgens[2],   0),
_CLK_FG_DIV(CLK_PCM_1,          &flexgens[2],   1),
_CLK_FG_DIV(CLK_PCM_2,          &flexgens[2],   2),
_CLK_FG_DIV(CLK_SPDIFF,         &flexgens[2],   3),
_CLK_FG_DIV(CLK_PCMR0_MASTER,   &flexgens[2],   4),
_CLK_FG_DIV(CLK_D0_SPARE_5,     &flexgens[2],   5),
_CLK_FG_DIV(CLK_D0_SPARE_6,     &flexgens[2],   6),
_CLK_FG_DIV(CLK_D0_SPARE_7,     &flexgens[2],   7),
_CLK_FG_DIV(CLK_D0_SPARE_8,     &flexgens[2],   8),
_CLK_FG_DIV(CLK_D0_SPARE_9,     &flexgens[2],   9),
_CLK_FG_DIV(CLK_D0_SPARE_10,    &flexgens[2],   10),
_CLK_FG_DIV(CLK_D0_SPARE_11,    &flexgens[2],   11),
_CLK_FG_DIV(CLK_D0_SPARE_12,    &flexgens[2],   12),
_CLK_FG_DIV(CLK_D0_SPARE_13,    &flexgens[2],   13),

/* Clockgen D2 */
_CLK_FG_P(CLK_D2_REF,           &flexgens[3],   FG_REFIN, 0, 0, &vclk_clocks[CLK_SYSIN]),
_CLK_FG(CLK_D2_REFOUT0,         &flexgens[3],   FG_REFOUT, 0, 0),
_CLK_FG_P(CLK_D2_FS0_VCO,       &flexgens[3],   FG_GFGOUT | FG_FS660C32 | FG_FVCO, 0, 0, &vclk_clocks[CLK_D2_REFOUT0]),
_CLK_FG_P(CLK_D2_FS0_CH0,       &flexgens[3],   FG_GFGOUT | FG_FS660C32 | FG_CHAN | FG_BEAT | FG_NSDIV1, 0, 0, &vclk_clocks[CLK_D2_FS0_VCO]),
_CLK_FG_P(CLK_D2_FS0_CH1,       &flexgens[3],   FG_GFGOUT | FG_FS660C32 | FG_CHAN | FG_BEAT | FG_NSDIV1, 0, 1, &vclk_clocks[CLK_D2_FS0_VCO]),
_CLK_FG_P(CLK_D2_FS0_CH2,       &flexgens[3],   FG_GFGOUT | FG_FS660C32 | FG_CHAN | FG_BEAT | FG_NSDIV1, 0, 2, &vclk_clocks[CLK_D2_FS0_VCO]),
_CLK_FG_P(CLK_D2_FS0_CH3,       &flexgens[3],   FG_GFGOUT | FG_FS660C32 | FG_CHAN | FG_BEAT | FG_NSDIV1, 0, 3, &vclk_clocks[CLK_D2_FS0_VCO]),

_CLK_FG(CLK_TMDSOUT_HDMI,       &flexgens[3],   FG_SRCIN | FG_BEAT, 0, 0), /* srcclkin0 */
_CLK_FG(CLK_PIX_HD_P_IN,        &flexgens[3],   FG_SRCIN | FG_BEAT, 1, 0), /* srcclkin1 */

_CLK_FG_P(CLK_D2_JEIN0,         &flexgens[3],   FG_JEIN, 0, 0, &vclk_clocks[CLK_D0_SPARE_13]), /* je_in[0] */
_CLK_FG(CLK_USB2_480,           &flexgens[3],   FG_JEIN, 2, 0), /* je_in[2] */
_CLK_FG(CLK_USB2_PHY0,          &flexgens[3],   FG_JEIN, 3, 0), /* je_in[3] */
_CLK_FG(CLK_USB2_PHY1,          &flexgens[3],   FG_JEIN, 4, 0), /* je_in[4] */
_CLK_FG(CLK_USB2_PHY2,          &flexgens[3],   FG_JEIN, 5, 0), /* je_in[5] */

_CLK_FG_DIV2(CLK_PIX_MAIN_DISP, &flexgens[3],   FG_SYNC, 0),
_CLK_FG_DIV(CLK_TMDS_HDMI_DIV2, &flexgens[3],   5),
_CLK_FG_DIV2(CLK_PIX_AUX_DISP,  &flexgens[3],   FG_SYNC, 6),
_CLK_FG_DIV2(CLK_DENC,          &flexgens[3],   FG_SYNC, 7),
_CLK_FG_DIV2(CLK_PIX_HDDAC,     &flexgens[3],   FG_SYNC, 8),
_CLK_FG_DIV2(CLK_HDDAC,         &flexgens[3],   FG_SYNC, 9),
_CLK_FG_DIV2(CLK_SDDAC,         &flexgens[3],   FG_SYNC, 10),
_CLK_FG_DIV2(CLK_PIX_DVO,       &flexgens[3],   FG_SYNC, 11),
_CLK_FG_DIV2(CLK_DVO,           &flexgens[3],   FG_SYNC, 12),
_CLK_FG_DIV2(CLK_PIX_HDMI,      &flexgens[3],   FG_SYNC, 13),
_CLK_FG_DIV(CLK_TMDS_HDMI,      &flexgens[3],   14),
_CLK_FG_DIV(CLK_REF_HDMIPHY,    &flexgens[3],   15),
_CLK_FG_DIV(CLK_VP9,            &flexgens[3],   47),

/* Clockgen D3 */
_CLK_FG_P(CLK_D3_REF,       &flexgens[4],   FG_REFIN, 0, 0, &vclk_clocks[CLK_SYSIN]),
_CLK_FG(CLK_D3_REFOUT0,     &flexgens[4],   FG_REFOUT, 0, 0),
_CLK_FG_P(CLK_D3_FS0_VCO,   &flexgens[4],   FG_GFGOUT | FG_FS660C32 | FG_FVCO, 0, 0, &vclk_clocks[CLK_D3_REFOUT0]),
_CLK_FG_P(CLK_D3_FS0_CH0,   &flexgens[4],   FG_GFGOUT | FG_FS660C32 | FG_CHAN, 0, 0, &vclk_clocks[CLK_D3_FS0_VCO]),
_CLK_FG_P(CLK_D3_FS0_CH1,   &flexgens[4],   FG_GFGOUT | FG_FS660C32 | FG_CHAN, 0, 1, &vclk_clocks[CLK_D3_FS0_VCO]),
_CLK_FG_P(CLK_D3_FS0_CH2,   &flexgens[4],   FG_GFGOUT | FG_FS660C32 | FG_CHAN, 0, 2, &vclk_clocks[CLK_D3_FS0_VCO]),
_CLK_FG_P(CLK_D3_FS0_CH3,   &flexgens[4],   FG_GFGOUT | FG_FS660C32 | FG_CHAN, 0, 3, &vclk_clocks[CLK_D3_FS0_VCO]),

_CLK_FG(CLK_ACG,            &flexgens[4],   FG_JEIN, 0, 0), /* je_in[0] = acg_glue_sbc_10-clk_out */
_CLK_FG(CLK_OSCI_32K,       &flexgens[4],   FG_JEIN, 1, 0), /* je_in[1] = osci_32k_10-clk */
_CLK_FG(CLK_HDMIRX_FS2,     &flexgens[4],   FG_JEIN, 2, 0), /* je_in[2] = hdmi_rx_fsyn-clk_out_2 */
_CLK_FG(CLK_HDMIRX_FS3,     &flexgens[4],   FG_JEIN, 3, 0), /* je_in[3] = hdmi_rx_fsyn-clk_out_3 */

_CLK_FG_DIV(CLK_STFE_FRC1,  &flexgens[4],   0),
_CLK_FG_DIV(CLK_TSOUT_0,    &flexgens[4],   1),
_CLK_FG_DIV(CLK_TSOUT_1,    &flexgens[4],   2),
_CLK_FG_DIV(CLK_MCHI,       &flexgens[4],   3),
_CLK_FG_DIV(CLK_VSENS_COMPO,&flexgens[4],   4),
_CLK_FG_DIV(CLK_FRC1_REMOTE,&flexgens[4],   5),
_CLK_FG_DIV(CLK_LPC_0,      &flexgens[4],   6),
_CLK_FG_DIV(CLK_LPC_1,      &flexgens[4],   7),

/* Gated clocks */
_CLK_P(CLK_DMU_BUS,             &gated,         &vclk_clocks[CLK_TX_ICN_DMU]),
_CLK_P(CLK_DMU_TARG,            &gated,         &vclk_clocks[CLK_ICN_REG]),
_CLK_P(CLK_DMU_PP,              &gated,         &vclk_clocks[CLK_PP_DMU]),
_CLK_P(CLK_DMU_CPU,             &gated,         &vclk_clocks[CLK_ST231_DMU]),
_CLK_P(CLK_STBE_TP,             &gated,         &vclk_clocks[CLK_TP]), /* Src=C0-5 */
_CLK_P(CLK_STBE_SYSTEM,         &gated,         &vclk_clocks[CLK_TX_ICN_DMU]), /* Src=C0-9 */
_CLK_P(CLK_STBE_CONFIG,         &gated,         &vclk_clocks[CLK_ICN_REG]), /* Src=C0-13 */
_CLK_P(CLK_HEVC_BUS,            &gated,         &vclk_clocks[CLK_TX_ICN_HADES]), /* Src=C0-32 */
_CLK_P(CLK_HEVC_TARG,           &gated,         &vclk_clocks[CLK_ICN_REG_16]), /* Src=C0-34 */
_CLK_P(CLK_HEVC_PP,             &gated,         &vclk_clocks[CLK_PP_HEVC]), /* Src=C0-35 */
_CLK_P(CLK_HEVC_CLUST,          &gated,         &vclk_clocks[CLK_CLUST_HEVC]), /* Src=C0-36 */
_CLK_P(CLK_HEVC_HWPE,           &gated,         &vclk_clocks[CLK_HWPE_HEVC]), /* Src=C0-37 */
_CLK_P(CLK_HEVC_FC,             &gated,         &vclk_clocks[CLK_FC_HEVC]), /* Src=C0-38 */
_CLK_P(CLK_HEVC_ASVP,           &gated,         &vclk_clocks[CLK_AVSP_HEVC]), /* Src=C0-41 */
_CLK_P(CLK_HQVDP0_SYS,          &gated,         &vclk_clocks[CLK_TX_ICN_DMU]), /* Src=C0-9 */
_CLK_P(CLK_HQVDP1_SYS,          &gated,         &vclk_clocks[CLK_TX_ICN_DMU]), /* Src=C0-9 */
_CLK_P(CLK_HQVDP2_SYS,          &gated,         &vclk_clocks[CLK_TX_ICN_DMU]), /* Src=C0-9 */
_CLK_P(CLK_HQVDP0_REG,          &gated,         &vclk_clocks[CLK_ICN_REG]), /* Src=C0-13 */
_CLK_P(CLK_HQVDP1_REG,          &gated,         &vclk_clocks[CLK_ICN_REG]), /* Src=C0-13 */
_CLK_P(CLK_HQVDP2_REG,          &gated,         &vclk_clocks[CLK_ICN_REG]), /* Src=C0-13 */
_CLK_P(CLK_HQVDP0_IQI,          &gated,         &vclk_clocks[CLK_MAIN_DISP]), /* Src=C0-29 */
_CLK_P(CLK_HQVDP1_IQI,          &gated,         &vclk_clocks[CLK_MAIN_DISP]), /* Src=C0-29 */
_CLK_P(CLK_HQVDP2_IQI,          &gated,         &vclk_clocks[CLK_MAIN_DISP]), /* Src=C0-29 */
_CLK_P(CLK_HQVDP0_PROC,         &gated,         &vclk_clocks[CLK_MAIN_DISP]), /* Src=C0-29 */
_CLK_P(CLK_HQVDP1_PROC,         &gated,         &vclk_clocks[CLK_MAIN_DISP]), /* Src=C0-29 */
_CLK_P(CLK_HQVDP2_PROC,         &gated,         &vclk_clocks[CLK_AUX_DISP]), /* Src=C0-30 */
_CLK_P(CLK_HQVDP0_PIX,          &gated,         &vclk_clocks[CLK_PROC_MIXER]), /* Src=C0-39 */
_CLK_P(CLK_HQVDP1_PIX,          &gated,         &vclk_clocks[CLK_PROC_MIXER]), /* Src=C0-39 */
_CLK_P(CLK_HQVDP2_PIX,          &gated,         &vclk_clocks[CLK_PROC_MIXER]), /* Src=C0-39 */
_CLK_P(CLK_HVA_GATED,           &gated,         &vclk_clocks[CLK_HVA]), /* Src=C0-3 */
_CLK_P(CLK_HVA_ICNDDR,          &gated,         &vclk_clocks[CLK_TX_ICN_DMU]), /* Src=C0-9 */
_CLK_P(CLK_HVA_ICNESRAM,        &gated,         &vclk_clocks[CLK_TX_ICN_DMU]), /* Src=C0-9 */
_CLK_P(CLK_HVA_ICNPERIPH,       &gated,         &vclk_clocks[CLK_ICN_REG]), /* Src=C0-13 */
};

int vclk_nb = (sizeof(vclk_clocks) / sizeof(struct vclk));

int vclk_soc_init(void)
{
    vclk_debug(1, "H418 vclkd %s, %u known clocks\n", VCLK_SOC_VERSION, vclk_nb);

    /* Flexiclockgen lib init */
    if (flexgen_init(vclk_clocks, vclk_nb) != 0)
        return -1;
    /* System config lib init */
    if (vclk_syscfg_init(sysconf_groups) != 0)
        return -1;

    return 0;
}
