/*
* This file is part of Validation CLK Driver (VCLKD).
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
* Author(s): fabrice.charpentier@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* VCLKD may alternatively be licensed under GPL V2 license from ST.
*/

/* LLA version: YYYYMMDD */
#define VCLK_SOC_VERSION "20151203"

enum {
    /* SOC input clocks */
    CLK_REF,            // MIPHY USB2 25Mhz

    /* CA9 PLL4600 */
    CLK_A9_PHI,         /* PLL4600 PHI0 (FVCOBY2/ODF0) */
    CLK_A9,             /* CA9 clock */
    CLK_A9_PERIPHS,     /* CA9.gt CA9.twd clock */

    /* Clockgen A0 (DDR) */
    CLK_A0_REF,         /* refclkin[0] */
    CLK_A0_REF1,        /* refclkin[1] */
    CLK_A0_SRCIN0,      /* srcclkin[0] */
    CLK_A0_SRCIN1,      /* srcclkin[1] */
    CLK_A0_SRCIN2,      /* srcclkin[2] */

    CLK_A0_REFOUT0,     /* refclkout[0] = gfg0 in */
    CLK_A0_REFOUT1,     /* refclkout[1] = gfg1 in */
    CLK_A0_PLL0_VCO,    /* GFG0: PLL3200 FVCOBY2 */
    CLK_A0_PLL0_PHI0,   /* GFG0: PLL3200 PHI0 (FVCOBY2/ODF0) */

    CLK_ICN_LMI,        /* Div0 */

    /* Clockgen A1 */
    CLK_A1_REF,         /* refclkin[0] */
    CLK_A1_REF1,        /* refclkin[1] */
    CLK_A1_SRCIN0,      /* srcclkin[0] */
    CLK_A1_SRCIN1,      /* srcclkin[1] */
    CLK_A1_SRCIN2,      /* srcclkin[2] */

    CLK_A1_REFOUT0,     /* refclkout[0] = gfg0 in */
    CLK_A1_REFOUT1,     /* refclkout[1] = gfg1 in */
    CLK_A1_PLL0_VCO,    /* GFG0: PLL3200 FVCOBY2 */
    CLK_A1_PLL0_PHI0,   /* GFG0: PLL3200 PHI0 (FVCOBY2/ODF0) */

    CLK_2X_DEMOD,       /* Div0 */
    CLK_ADC_IQ,
    CLK_FSK,
    CLK_ADC_DISEQC,     /* Div3 */

    /* Clockgen C0 */
    CLK_C0_REF,         /* refclkin[0] */
    CLK_C0_REF1,        /* refclkin[1] */
    CLK_C0_SRCIN0,      /* srcclkin[0] */
    CLK_C0_SRCIN1,      /* srcclkin[1] */
    CLK_C0_SRCIN2,      /* srcclkin[2] */

    CLK_C0_REFOUT0,     /* refclkout[0] = gfg0 in */
    CLK_C0_REFOUT1,     /* refclkout[1] = gfg1 in */
    CLK_C0_REFOUT2,     /* refclkout[2] = gfg2 in */
    CLK_C0_PLL0_VCO,    /* GFG0=PLL3200 FVCOBY2 */
    CLK_C0_PLL0_PHI0,   /* GFG0=PLL3200: PHI0 (FVCOBY2/ODF0) */
    CLK_C0_PLL0_PHI1,   /* GFG0=PLL3200: PHI1 (FVCOBY2/ODF1) */
    CLK_C0_PLL0_PHI2,   /* GFG0=PLL3200: PHI2 (FVCOBY2/ODF2) */
    CLK_C0_PLL0_PHI3,   /* GFG0=PLL3200: PHI3 (FVCOBY2/ODF3) */
    CLK_C0_PLL1_VCO,    /* GFG1=PLL3200 FVCOBY2 */
    CLK_C0_PLL1_PHI0,   /* GFG1=PLL3200: PHI0 (FVCOBY2/ODF0) */
    CLK_C0_PLL1_PHI1,   /* GFG1=PLL3200: PHI1 (FVCOBY2/ODF1) */
    CLK_C0_PLL1_PHI2,   /* GFG1=PLL3200: PHI2 (FVCOBY2/ODF2) */
    CLK_C0_PLL1_PHI3,   /* GFG1=PLL3200: PHI3 (FVCOBY2/ODF3) */
    CLK_C0_FS0_VCO,     /* GFG2=FS660: FS embedded VCOCLK */
    CLK_C0_FS0_CH0,     /* GFG2=FS660: FS channel 0 */
    CLK_C0_FS0_CH1,     /* GFG2=FS660: FS channel 1 */
    CLK_C0_FS0_CH2,     /* GFG2=FS660: FS channel 2 */
    CLK_C0_FS0_CH3,     /* GFG2=FS660: FS channel 3 */

    CLK_SLIM_CC,        /* Div0 */
    CLK_SPI,
    CLK_FLASH,
    CLK_MMC_0,
    CLK_PERIPH_TS,
    CLK_EXT2F_A9,       /* Div5 */
    CLK_ICN_TS,
    CLK_NAND,
    CLK_MMC_1,
    CLK_ST231_GP_2,
    CLK_ST231_VDEC,     /* Div10 */
    CLK_ST231_GP_0,
    CLK_ST231_GP_1_AUDIO,
    CLK_ETH_GMAC,
    CLK_GMAC_PHYCLK,
    CLK_RCS_RECOVERED_0,/* Div15 */
    CLK_RCS_RECOVERED_1,
    CLK_STBE,
    CLK_HEVC_H264,
    CLK_AVSP,
    CLK_TIMER_A9,       /* Div20 */
    CLK_SLIM_VDEC,
    CLK_TSOUT_0,
    CLK_TSOUT_1,
    CLK_RX_ICN_L0L1,
    CLK_FRC_1,          /* Div25 */
    CLK_FRC_1_PAD,
    CLK_PWM_REAR,
    CLK_TX_ICN_L0L1,
    CLK_SPARE_CLK_OUT,
    CLK_SYSTEM_STFE,    /* Div30 */
    CLK_C0_SPARE_31,
    CLK_SLIM_FDMA_1,
    CLK_ATB,
    CLK_TRACE,
    CLK_BDISP,          /* Div35 */
    CLK_PROC_VDP,
    CLK_DSS,
    CLK_C0_SPARE_38,
    CLK_LPC,
    CLK_GPU,            /* Div40 */
    CLK_C0_SPARE_41,
    CLK_ICN_DISP,
    CLK_ICN_CPU,
    CLK_PWM_FRONT,
    CLK_SLIM_FDMA_0,    /* Div45 */
    CLK_PERIPH_DISP,
    CLK_PERIPH_CPU,     /* Div47 */

    CLK_EXT2F_A9_BY2,   /* CLK_EXT2F_A9 / 2 */

    /* Clockgen D0 */
    CLK_D0_REF,         /* refclkin[0] */
    CLK_D0_REF1,        /* refclkin[1] */
    CLK_D0_SRCIN0,      /* srcclkin[0] */
    CLK_D0_SRCIN1,      /* srcclkin[1] */
    CLK_D0_SRCIN2,      /* srcclkin[2] */
    CLK_D0_SRCIN3,      /* srcclkin[3] */

    CLK_D0_REFOUT0,     /* refclkout[0] = gfg0 in */
    CLK_D0_FS0_VCO,     /* GFG0=FS660: FS embedded VCOCLK */
    CLK_D0_FS0_CH0,     /* GFG0=FS660: FS channel 0 */
    CLK_D0_FS0_CH1,     /* GFG0=FS660: FS channel 1 */
    CLK_D0_FS0_CH2,     /* GFG0=FS660: FS channel 2 */
    CLK_D0_FS0_CH3,     /* GFG0=FS660: FS channel 3 */

    CLK_PCM_0,          /* Div0 */
    CLK_PCM_1,
    CLK_PCM_2,
    CLK_SPDIF,
    CLK_PCMR_0_MASTER,
    CLK_PCMIN0_MCLK_OUT,/* Div5 */
    CLK_D0_SPARE_6,
    CLK_D0_SPARE_7,
    CLK_D0_SPARE_8,
    CLK_D0_SPARE_9,
    CLK_D0_SPARE_10,
    CLK_D0_SPARE_11,
    CLK_D0_SPARE_12,
    CLK_D0_SPARE_13,
    CLK_D0_SPARE_14,
    CLK_D0_SPARE_15,
    CLK_D0_SPARE_16,
    CLK_D0_SPARE_17,
    CLK_D0_SPARE_18,
    CLK_D0_SPARE_19,
    CLK_D0_SPARE_20,
    CLK_D0_SPARE_21,
    CLK_D0_SPARE_22,
    CLK_D0_SPARE_23,
    CLK_D0_SPARE_24,
    CLK_D0_SPARE_25,
    CLK_D0_SPARE_26,
    CLK_D0_SPARE_27,
    CLK_D0_SPARE_28,
    CLK_D0_SPARE_29,
    CLK_D0_SPARE_30,
    CLK_D0_SPARE_31,
    CLK_D0_SPARE_32,
    CLK_D0_SPARE_33,
    CLK_D0_SPARE_34,
    CLK_D0_SPARE_35,
    CLK_D0_SPARE_36,
    CLK_D0_SPARE_37,
    CLK_D0_SPARE_38,
    CLK_D0_SPARE_39,
    CLK_HDMI_PHASE_REG, /* Div 40 */
    CLK_D0_SPARE_41,
    CLK_D0_SPARE_42,
    CLK_D0_SPARE_43,
    CLK_D0_SPARE_44,
    CLK_D0_SPARE_45,
    CLK_D0_SPARE_46,
    CLK_TO_CLKGENC_SRC0,/* Div47 */

    /* Clockgen D1 */
    CLK_D1_REF,         /* refclkin[0] */
    CLK_D1_REF1,        /* refclkin[1] */
    CLK_D1_SRCIN0,      /* srcclkin[0] = HDMITX_TMDSCK_OUT_FROM_PHY */
    CLK_D1_SRCIN1,      /* srcclkin[1] */
    CLK_D1_SRCIN2,      /* srcclkin[2] */
    CLK_D1_SRCIN3,      /* srcclkin[3] */

    CLK_D1_REFOUT0,     /* refclkout[0] = gfg0 in */
    CLK_D1_FS0_VCO,     /* GFG0=FS660: FS embedded VCOCLK */
    CLK_D1_FS0_CH0,     /* GFG0=FS660: FS channel 0 */
    CLK_D1_FS0_CH1,     /* GFG0=FS660: FS channel 1 */
    CLK_D1_FS0_CH2,     /* GFG0=FS660: FS channel 2 */
    CLK_D1_FS0_CH3,     /* GFG0=FS660: FS channel 3 */

    CLK_PIX_MAIN_DISP,  /* Div0 */
    CLK_PIX_AUX_DISP,
    CLK_PIX_HDDAC,
    CLK_HDDAC,
    CLK_D1_SPARE_4,
    CLK_D1_SPARE_5,     /* Div5 */
    CLK_SDDAC,
    CLK_PIX_HDMI,
    CLK_DENC,
    CLK_REF_HDMIPHY,
    CLK_TMDS_HDMI,      /* Div10 */
    CLK_PROC_HQVDP,
    CLK_D1_SPARE_12,
    CLK_PROC_MIXER,
    CLK_PROC_GDP_COMPO,
    CLK_D1_SPARE_15,    /* Div15 */
    CLK_FRC_0,
    CLK_FRC_0_PAD,
    CLK_FRC_2,
    CLK_D1_SPARE_19,
    CLK_NCR_SAT_DEMOD,  /* Div20 */
    CLK_D1_SPARE_21,
    CLK_D1_SPARE_22,
    CLK_D1_SPARE_23,
    CLK_D1_SPARE_24,
    CLK_D1_SPARE_25,
    CLK_D1_SPARE_26,
    CLK_D1_SPARE_27,
    CLK_D1_SPARE_28,
    CLK_D1_SPARE_29,
    CLK_D1_SPARE_30,
    CLK_D1_SPARE_31,
    CLK_D1_SPARE_32,
    CLK_D1_SPARE_33,
    CLK_D1_SPARE_34,
    CLK_D1_SPARE_35,
    CLK_D1_SPARE_36,
    CLK_D1_SPARE_37,
    CLK_D1_SPARE_38,
    CLK_D1_SPARE_39,
    CLK_D1_SPARE_40,
    CLK_D1_SPARE_41,
    CLK_D1_SPARE_42,
    CLK_D1_SPARE_43,
    CLK_D1_SPARE_44,
    CLK_D1_SPARE_45,
    CLK_D1_SPARE_46,
    CLK_TO_CLKGENC_SRC1,/* Div47 */

    /* Functional clusters gated clocks */
    /* DEMOD */
    CLK_2X_DEMOD_GATED,
    CLK_ADC_IQ_GATED,
    CLK_FSK_GATED,
    CLK_ADC_DISEQC_GATED,
    /* VDEC */
    CLK_PP_AVSP_GATED,      // c8had_pp_supertop_0/clk_avsp, src=CLK_AVSP
    CLK_PP_BUS_GATED,       // c8had_pp_supertop_0/clk_bus, src=CLK_ST231_VDEC
    CLK_PP_HEVC_GATED,      // c8had_pp_supertop_0/clk_hevc, src=CLK_AVSP
    CLK_PP_H264_GATED,      // c8had_pp_supertop_0/clk_h264, src=CLK_AVSP
    CLK_PP_TARG_GATED,      // c8had_pp_supertop_0/clk_targ, src=CLK_PERIPH_TS
    CLK_VDEC_BUS_GATED,     // c8vdec_top_0/clk_bus, src=CLK_ICN_TS
    CLK_VDEC_CPU_GATED,     // c8vdec_top_0/clk_cpu, src=CLK_ST231_VDEC
    CLK_VDEC_SLIM_GATED,    // c8vdec_top_0/clk_slim, src=CLK_SLIM_VDEC
    CLK_VDEC_TARG_GATED,    // c8vdec_top_0/clk_targ, src=CLK_PERIPH_TS
    CLK_DMU_IC_GATED,       // mbx_dmu_0/clk_ic, src=CLK_PERIPH_TS
    CLK_DMU_BUS_GATED,      // st231_dmu_0/clk_bus, src=CLK_ST231_VDEC
    CLK_DMU_CPU_GATED,      // st231_dmu_0/clk_cpu, src=CLK_ST231_VDEC
    /* DISPLAY cluster */
    CLK_AUDIO_PCM_0, // audio_glue_0/i_clk_fsyn0 src=CLK_PCM_0 SYSTEM_CONFIG6015
    CLK_AUDIO_PCM_1, // audio_glue_0/i_clk_fsyn1 src=CLK_PCM_1 SYSTEM_CONFIG6006
    CLK_AUDIO_PCM_2, // audio_glue_0/i_clk_fsyn2 src=CLK_PCM_2 SYSTEM_CONFIG6007
    CLK_AUDIO_SPDIF, // audio_glue_0/i_clk_fsyn3 src=CLK_PCM_3 SYSTEM_CONFIG6008
    CLK_BDISP_CK, // bdisp_0/bdisp_ck src=CLK_BDISP SYSTEM_CONFIG6010
    CLK_BDISP_CPU, // bdisp_0/cpu_ck src=CLK_PERIPH_DISP SYSTEM_CONFIG6010
    CLK_BDISP_IC, // bdisp_0/ic_ck src=CLK_ICN_DISP SYSTEM_CONFIG6010
    CLK_COMPO_CPU, // compo_0/cpu_ck src=CLK_PERIPH_DISP SYSTEM_CONFIG6002
    CLK_COMPO_PROC, // compo_0/gdp_proc_ck src=CLK_PROC_GDP_COMPO SYSTEM_CONFIG6002
    CLK_COMPO_MIX, // compo_0/mix_proc_ck src=CLK_PROC_MIXER SYSTEM_CONFIG6002
    CLK_COMPO_MIX1, // compo_0/mix1_ck src=CLK_PIX_MAIN_DISP SYSTEM_CONFIG6002
    CLK_COMPO_MIX2, // compo_0/mix2_ck src=CLK_PIX_AUX_DISP SYSTEM_CONFIG6002
    CLK_COMPO_ST, // compo_0/st_ck src=CLK_ICN_DISP SYSTEM_CONFIG6002
    CLK_FDMA0_SLIM, // fdma_0/clk_slim src=CLK_SLIM_FDMA_0 SYSTEM_CONFIG6004
    CLK_FDMA0_STBUS_T1, // fdma_0/clk_stbus_t1 src=CLK_PERIPH_DISP SYSTEM_CONFIG6004
    CLK_FDMA0_STBUS_T3_0, // fdma_0/clk_stbus_t3_0 src=CLK_PERIPH_DISP SYSTEM_CONFIG6004
    CLK_FDMA0_STBUS_T3_1, // fdma_0/clk_stbus_t3_1 src=CLK_ICN_DISP SYSTEM_CONFIG6004
    CLK_FDMA1_SLIM, // fdma_1/clk_slim src=CLK_SLIM_FDMA_1 SYSTEM_CONFIG6005
    CLK_FDMA1_STBUS_T1, // fdma_1/clk_stbus_t1 src=CLK_PERIPH_DISP SYSTEM_CONFIG6005
    CLK_FDMA1_STBUS_T3_0, // fdma_1/clk_stbus_t3_0 src=CLK_PERIPH_DISP SYSTEM_CONFIG6005
    CLK_FDMA1_STBUS_T3_1, // fdma_1/clk_stbus_t3_1 src=CLK_ICN_DISP SYSTEM_CONFIG6005
    CLK_HDTVOUT_DENC, // hdtvout_0/clk_denc src=CLK_DENC SYSTEM_CONFIG6003
    CLK_HDTVOUT_IC, // hdtvout_0/clk_ic src=CLK_PERIPH_DISP SYSTEM_CONFIG6003
    CLK_HDTVOUT_HDDAC, // hdtvout_0/clk_out_hddac src=CLK_HDDAC SYSTEM_CONFIG6003
    CLK_HDTVOUT_SDDAC, // hdtvout_0/clk_out_sddac src=CLK_SDDAC SYSTEM_CONFIG6003
    CLK_HDTVOUT_PIX_AUX, // hdtvout_0/clk_pix_aux src=CLK_PIX_AUX_DISP SYSTEM_CONFIG6003
    CLK_HDTVOUT_PIX_HDDAC, // hdtvout_0/clk_pix_hddac src=CLK_PIX_HDDAC SYSTEM_CONFIG6003
    CLK_HDTVOUT_PIX_HDMI, // hdtvout_0/clk_pix_hdmi src=CLK_PIX_HDMI SYSTEM_CONFIG6003
    CLK_HDTVOUT_PIX_MAIN, // hdtvout_0/clk_pix_main src=CLK_PIX_MAIN_DISP SYSTEM_CONFIG6003
    CLK_HDTVOUT_TMDS, // hdtvout_0/clk_tmds_hdmi src=CLK_TMDS_HDMI SYSTEM_CONFIG6003
    CLK_HQVDP_COMPO, // hqvdp_0/clk_compo src=CLK_PROC_MIXER SYSTEM_CONFIG6000
    CLK_HQVDP_PROC, // hqvdp_0/clk_proc src=CLK_PROC_HQVDP SYSTEM_CONFIG6000
    CLK_HQVDP_REG, // hqvdp_0/clk_reg src=CLK_PERIPH_DISP SYSTEM_CONFIG6000
    CLK_HQVDP_SYS, // hqvdp_0/clk_sys src=CLK_ICN_DISP SYSTEM_CONFIG6000
    // spdif_formatter_0/mclk src=- SYSTEM_CONFIG6008
    // t1_apb_debug_bridge_0/clk src=CLK_PERIPH_DISP SYSTEM_CONFIG6000
    // uplayer_0/clk_appl src=- SYSTEM_CONFIG6015
    // uplayer_0/clk_stbus src=CLK_PERIPH_DISP SYSTEM_CONFIG6015
    // uplayer_1/clk_appl src=- SYSTEM_CONFIG6006
    // uplayer_1/clk_stbus src=CLK_PERIPH_DISP SYSTEM_CONFIG6006
    // uplayer_2/clk_appl src=- SYSTEM_CONFIG6007
    // uplayer_2/clk_stbus src=CLK_PERIPH_DISP SYSTEM_CONFIG6007
    // uplayer_3/clk_appl src=- SYSTEM_CONFIG6008
    // uplayer_3/clk_stbus src=CLK_PERIPH_DISP SYSTEM_CONFIG6008
    // ureader_0/clk_appl src=CLK_PCMR_0_MASTER
    // ureader_0/clk_stbus src=CLK_PERIPH_DISP SYSTEM_CONFIG6009
    CLK_VDP_PIX, // vdp_aux_0/clk_pix src=CLK_PROC_MIXER SYSTEM_CONFIG6001
    CLK_VDP_PROC, // vdp_aux_0/clk_proc src=CLK_PROC_VDP SYSTEM_CONFIG6001
    CLK_VDP_REG, // vdp_aux_0/clk_reg src=CLK_PERIPH_DISP SYSTEM_CONFIG6001
    CLK_VDP_SYS, // vdp_aux_0/clk_sys src=CLK_ICN_DISP SYSTEM_CONFIG6001
    CLK_VDAC_UVW, // video_dac_0/CLKUVW src=CLK_HDDAC SYSTEM_CONFIG6003
    CLK_VDAC_X, // video_dac_0/CLKX src=CLK_SDDAC SYSTEM_CONFIG6003

    CLK_LAST_ID
};
