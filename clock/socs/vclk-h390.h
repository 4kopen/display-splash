/*
* This file is part of Validation CLK Driver (VCLKD).
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
* Author(s): fabrice.charpentier@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* VCLKD may alternatively be licensed under GPL V2 license from ST.
*/

/* LLA version: YYYYMMDD */
#define VCLK_SOC_VERSION "20150910"

enum {
    /* SOC input clocks */
    CLK_MIPHY_30,       // MIPHY internal 30Mhz
    CLK_WIFI_40,        // WIFI external 40Mhz
    CLK_REF,            // Mux output: CLK_MIPHY_30 | CLK_WIFI_40

    /* CA9 PLL4600 */
    CLK_A9_PHI,            /* PLL4600 PHI (FVCOBY2/ODF) */
    CLK_A9,                 /* CA9 clock */
    CLK_A9_PERIPHS,         /* CA9.gt CA9.twd clock */

    /* Clockgen A0- DDR*/
    CLK_A0_REF,         /* refclkin[0] */
    CLK_A0_REF1,        /* refclkin[1] = 'clk_osc_alt' */
    CLK_A0_SRCIN0,      /* srcclkin[0] = 'clk_src_0_ckga_0' */
    CLK_A0_SRCIN1,      /* srcclkin[1] = 'clk_src_1_ckga_0' */
    CLK_A0_SRCIN2,      /* srcclkin[2] = 'clk_src_2_ckga_0' */
    CLK_A0_REFOUT0,     /* refclkout[0] = gfg0 in */
    CLK_A0_REFOUT1,     /* refclkout[1] = gfg1 in */

    CLK_A0_PLL0_VCO,    /* GFG0: PLL3200 FVCOBY2 */
    CLK_A0_PLL0_PHI0,   /* GFG0: PLL3200 PHI0 (FVCOBY2/ODF0) */

    CLK_IC_LMI,         /* Div0 */

    /* Clockgen C0 */
    CLK_C0_REF,         /* refclkin[0] */
    CLK_C0_REF1,        /* refclkin[1] = 'clk_osc_alt' */
    CLK_C0_SRCIN0,      /* srcclkin[0] = 'clk_src_0_ckgc_0' */
    CLK_C0_SRCIN1,      /* srcclkin[1] = 'clk_src_1_ckgc_0' */
    CLK_C0_SRCIN2,      /* srcclkin[2] = 'clk_src_2_ckgc_0' */
    CLK_C0_REFOUT0,     /* refclkout[0] = gfg0 in */
    CLK_C0_REFOUT1,     /* refclkout[1] = gfg1 in */
    CLK_C0_REFOUT2,     /* refclkout[2] = gfg2 in */

    CLK_C0_PLL0_VCO,    /* GFG0=PLL3200 FVCOBY2 */
    CLK_C0_PLL0_PHI0,   /* GFG0=PLL3200: PHI0 (FVCOBY2/ODF0) */
    CLK_C0_PLL0_PHI1,   /* GFG0=PLL3200: PHI1 (FVCOBY2/ODF1) */
    CLK_C0_PLL0_PHI2,   /* GFG0=PLL3200: PHI2 (FVCOBY2/ODF2) */
    CLK_C0_PLL0_PHI3,   /* GFG0=PLL3200: PHI3 (FVCOBY2/ODF3) */
    CLK_C0_PLL1_VCO,    /* GFG1=PLL3200 FVCOBY2 */
    CLK_C0_PLL1_PHI0,   /* GFG1=PLL3200: PHI0 (FVCOBY2/ODF0) */
    CLK_C0_PLL1_PHI1,   /* GFG1=PLL3200: PHI1 (FVCOBY2/ODF1) */
    CLK_C0_PLL1_PHI2,   /* GFG1=PLL3200: PHI2 (FVCOBY2/ODF2) */
    CLK_C0_PLL1_PHI3,   /* GFG1=PLL3200: PHI3 (FVCOBY2/ODF3) */
    CLK_C0_FS0_VCO,     /* GFG2=FS660: FS embedded VCOCLK */
    CLK_C0_FS0_CH0,     /* GFG2=FS660: FS channel 0 */
    CLK_C0_FS0_CH1,     /* GFG2=FS660: FS channel 1 */
    CLK_C0_FS0_CH2,     /* GFG2=FS660: FS channel 2 */
    CLK_C0_FS0_CH3,     /* GFG2=FS660: FS channel 3 */

    CLK_SLIM_CC,        /* Div0 */
    CLK_SPI,            /* Div1 */
    CLK_FLASH,          /* Div2 */
    CLK_MMC_0,          /* Div3 */
    CLK_PERIPH_TS,      /* Div4 */
    CLK_EXT2F_A9,       /* Div5 */
    CLK_ICN_TS,         /* Div6 */
    CLK_NAND,           /* Div7 */
    CLK_MMC_1,          /* Div8 */
    CLK_ST231_GP_2,     /* Div9 */
    CLK_ST231_VDEC,     /* Div10 */
    CLK_ST231_GP_0,     /* Div11 */
    CLK_ST231_GP_1_AUDIO,  /* Div12 */
    CLK_ETH_GMAC_S,     /* Div13 */
    CLK_GMAC_S_PHYCLK,  /* Div14 */
    CLK_ETH_GMAC_W,     /* Div15 */
    CLK_GMAC_W_PHYCLK,  /* Div16 */
    CLK_STBE,           /* Div17 */
    CLK_HEVC_H264,      /* Div18 */
    CLK_AVSP,           /* Div19 */
    CLK_TIMER_A9,       /* Div20 */
    CLK_SLIM_VDEC,      /* Div21 */
    CLK_DEC_JPEG,       /* Div22 */
    CLK_WIFI,           /* Div23 */
    CLK_RX_ICN_L0L1,    /* Div24 */
    CLK_FRC_1,          /* Div25 */
    CLK_FRC_1_PAD,      /* Div26 */
    CLK_DVP_PROC,       /* Div27 */
    CLK_TX_ICN_L0L1,    /* Div28 */
    CLK_SYS_CLK_OUT,    /* Div29 */
    CLK_SYSTEM_STFE,    /* Div30 */
    CLK_C0_SPARE_31,    /* Div31 */
    CLK_SLIM_FDMA_1,    /* Div32 */
    CLK_ATB,            /* Div33 */
    CLK_TRACE,          /* Div34 */
    CLK_BDISP,          /* Div35 */
    CLK_PROC_VDP,       /* Div36 */
    CLK_DSS,            /* Div37 */
    CLK_C0_SPARE_38,    /* Div38 */
    CLK_LPC,            /* Div39 */
    CLK_GPU,            /* Div40 */
    CLK_ICN_WIFI,       /* Div41 */
    CLK_ICN_DISP,       /* Div42 */
    CLK_ICN_CPU,        /* Div43 */
    CLK_PERIPH_WIFI,    /* Div44 */
    CLK_SLIM_FDMA_0,    /* Div45 */
    CLK_PERIPH_DISP,    /* Div46 */
    CLK_PERIPH_CPU,     /* Div47 */

    CLK_EXT2F_A9_BY2,   /* C0-5 / 2 */

    /* Clockgen D0- Audio */
    CLK_D0_REF,         /* refclkin[0] */
    CLK_D0_REF1,        /* refclkin[1] = 'clk_osc_alt' */
    CLK_D0_SRCIN0,      /* srcclkin[0] = 'clk_mclk_hdmi_rx' */
    CLK_D0_SRCIN1,      /* srcclkin[1] = 'clk_mclk_dp_rx' */
    CLK_D0_SRCIN2,      /* srcclkin[2] = 'clk_src_2_ckgd_0' */
    CLK_D0_SRCIN3,      /* srcclkin[3] = 'clk_src_3_ckgd_0' */
    CLK_D0_REFOUT0,     /* refclkout[0] = gfg0 in */

    CLK_D0_FS0_VCO,     /* GFG0=FS660: FS embedded VCOCLK */
    CLK_D0_FS0_CH0,     /* GFG0=FS660: FS channel 0 */
    CLK_D0_FS0_CH1,     /* GFG0=FS660: FS channel 1 */
    CLK_D0_FS0_CH2,     /* GFG0=FS660: FS channel 2 */
    CLK_D0_FS0_CH3,     /* GFG0=FS660: FS channel 3 */

    CLK_PCM_0,          /* Div0 */
    CLK_PCM_1,          /* Div1 */
    CLK_PCM_2,          /* Div2 */
    CLK_SPDIF,          /* Div3 */
    CLK_PCMR_0_MASTER,  /* Div4 */
    CLK_MCLK_OUT,       /* Div5 */

    /* Clockgen D1- Video */
    CLK_D1_REF,         /* refclkin[0] */
    CLK_D1_REF1,        /* refclkin[1] = 'clk_osc_alt' */
    CLK_D1_SRCIN0,      /* srcclkin[0] = HDMITX_TMDSCK_OUT_FROM_PHY */
    CLK_D1_SRCIN1,      /* srcclkin[1] = Not connected */
    CLK_D1_SRCIN2,      /* srcclkin[2] = Not connected */
    CLK_D1_SRCIN3,      /* srcclkin[3] = Not connected */
    CLK_D1_REFOUT0,     /* refclkout[0] = gfg0 in */

    CLK_D1_FS0_VCO,     /* GFG0=FS660: FS embedded VCOCLK */
    CLK_D1_FS0_CH0,     /* GFG0=FS660: FS channel 0 */
    CLK_D1_FS0_CH1,     /* GFG0=FS660: FS channel 1 */
    CLK_D1_FS0_CH2,     /* GFG0=FS660: FS channel 2 */
    CLK_D1_FS0_CH3,     /* GFG0=FS660: FS channel 3 */

    CLK_PIX_MAIN_DISP,  /* Div0 */
    CLK_PIX_AUX_DISP,   /* Div1 */
    CLK_PIX_HDDAC,      /* Div2 */
    CLK_HDDAC,          /* Div3 */
    CLK_TVOUT_DEBUG,    /* Div4 */
    CLK_TVOUT_PIX_DEBUG,/* Div5 */
    CLK_SDDAC,          /* Div6 */
    CLK_PIX_HDMI,       /* Div7 */
    CLK_DENC,           /* Div8 */
    CLK_REF_HDMIPHY,    /* Div9 */
    CLK_TMDS_HDMI,      /* Div10 */
    CLK_PROC_HQVDP,     /* Div11 */
    CLK_IQI_HQVDP,      /* Div12 */
    CLK_PROC_MIXER,     /* Div13 */
    CLK_PROC_GDP_COMPO, /* Div14 */
    CLK_PWM,            /* Div15 */
    CLK_FRC_0,          /* Div16 */
    CLK_FRC_0_PAD,      /* Div17 */
    CLK_FRC_2,          /* Div18 */
    CLK_D1_SPARE_19,
    CLK_D1_SPARE_20,
    CLK_D1_SPARE_21,
    CLK_D1_SPARE_22,
    CLK_D1_SPARE_23,
    CLK_D1_SPARE_24,
    CLK_D1_SPARE_25,
    CLK_D1_SPARE_26,
    CLK_D1_SPARE_27,
    CLK_D1_SPARE_28,
    CLK_D1_SPARE_29,
    CLK_D1_SPARE_30,
    CLK_D1_SPARE_31,
    CLK_D1_SPARE_32,
    CLK_D1_SPARE_33,
    CLK_D1_SPARE_34,
    CLK_D1_SPARE_35,
    CLK_D1_SPARE_36,
    CLK_D1_SPARE_37,
    CLK_D1_SPARE_38,
    CLK_D1_SPARE_39,
    CLK_HDMI_PHASE_REG, /* Div 40 */

    /* Functional clusters gated clocks */
    CLK_HQVDP0_REG,         // Src=clk_periph_display, C0-46
    CLK_HQVDP0_SYS,         // Src=clk_icn_display, C0-42
    CLK_HQVDP0_PIX,         // Src=clk_pix_hqvdp_0; ???
    CLK_HQVDP0_IQI,         // Src=clk_iqi_hqvdp_0, D1-12
    CLK_HQVDP0_PROC,        // Src=clk_proc_hqvdp_0, D1-11
    CLK_VDPAUX_PIX,         // Src=clk_proc_hqvdp_0, D1-11
    CLK_VDPAUX_PROC,        // Src=clk_proc_vdp_0, C0-36
    CLK_VDPAUX_REG,         // Src=clk_periph_display, C0-46
    CLK_VDPAUX_SYS,         // Src=clk_icn_display, C0-42
    CLK_COMPO_CPU,          // Src=clk_periph_display, C0-46
    CLK_COMPO_ST,           // Src=clk_icn_display, C0-42
    CLK_COMPO_GDP_PROC,     // Src=clk_proc_gdp_compo_0, D1-14
    CLK_COMPO_MIX_PROC,     // Src=clk_proc_hqvdp_, D1-11
    CLK_COMPO_MIX1,         // Src=clk_mix1_compo_0, D1-0
    CLK_COMPO_MIX2,         // Src=clk_mix2_compo_0, D1-1
    CLK_HDTVOUT_IC,         // Src=clk_periph_display, C0-46
    CLK_HDTVOUT_DENC,       // Src=clk_tvout_denc, D1-8
    CLK_HDTVOUT_DVO_1,      // Src=clk_tvout_dvo_1, D1-4
    CLK_HDTVOUT_OUT_HDDAC,  // Src=clk_tvout_hddac, D1-3
    CLK_HDTVOUT_PIX_HDDAC,  // Src=clk_tvout_pix_hddac, D1-2
    CLK_HDTVOUT_OUT_SDDAC,  // Src=clk_tvout_sddac, D1-6
    CLK_HDTVOUT_PIX_AUX,    // Src=clk_mix2_compo_0, D1-1
    CLK_HDTVOUT_PIX_DVO_1,  // Src=clk_tvout_pix_dvo_1, D1-5
    CLK_HDTVOUT_PIX_HDMI,   // Src=clk_tvout_pix_hdmi, D1-7
    CLK_HDTVOUT_PIX_MAIN,   // Src=clk_mix1_compo_0, D1-0
    CLK_HDTVOUT_TMDS_HDMI,  // Src=clk_tmds_hdmi_hdtvout_0, D1-10
    CLK_FDMA0_T1,           // Src=clk_periph_display, C0-46
    CLK_FDMA0_T3_0,         // Src=clk_periph_display, C0-46
    CLK_FDMA0_T3_1,         // Src=clk_icn_display, C0-42
    CLK_FDMA0_SLIM,         // Src=clk_slim_fdma_0, C0-45
    CLK_FDMA1_T1,           // Src=clk_periph_display, C0-46
    CLK_FDMA1_T3_0,         // Src=clk_periph_display, C0-46
    CLK_FDMA1_T3_1,         // Src=clk_icn_display, C0-42
    CLK_FDMA1_SLIM,         // Src=clk_slim_fdma_1, C0-32
    CLK_BDISP_CPU_CK,       // Src=clk_periph_display, C0-46
    CLK_BDISP_IC_CK,        // Src=clk_icn_display, C0-42
    CLK_BDISP_CK,           // Src=clk_bdisp_ck, C0-35
    CLK_HDMIRX_IC_100,      // Src=clk_periph_display, C0-46
    CLK_DVPIN_CPU,          // Src=clk_periph_display, C0-46
    CLK_DVPIN_PROC,         // Src=clk_dvp_proc, C0-27
    CLK_DVPIN_ST,           // Src=clk_icn_display, C0-42
    CLK_GMAC_AXI,           // Src=clk_icn_wifi, C0-41
    CLK_GMAC_T1,            // Src=clk_periph_wifi, C0-44
    CLK_GMAC_PTP_REF,       // Src=clk_periph_wifi, C0-44
    CLK_GMAC_TX,            // Src=clk_250_125_wifi, C0-15
    CLK_WIFI_AXI,           // Src=clk_icn_wifi, C0-41
    CLK_WIFI_T1,            // Src=clk_periph_wifi, C0-44
    CLK_WIFI_500,           // Src=clk_500_wifi, C0-23
    // CLK_WIFI_640,           // Src=clk_flexgen_640, ???

    CLK_LAST_ID
};
