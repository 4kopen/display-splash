/*
* This file is part of Validation CLK Driver (VCLKD).
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
* Author(s): fabrice.charpentier@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* VCLKD may alternatively be licensed under GPL V2 license from ST.
*/

#include <stddef.h>
#include "vclk.h"
#include "vclk_flexgen.h"
#include "vclk-h410.h"
#include "vclk_algos.h"
#include "vclk_private.h"

#define CKG_A0_BASE_ADDRESS     0x90ff000
#define CKG_C0_BASE_ADDRESS     0x9103000
#define CKG_D0_BASE_ADDRESS     0x9104000
#define CKG_D2_BASE_ADDRESS     0x9106000
#define CKG_D3_BASE_ADDRESS     0x9107000

#define SYSCFG_PIO_FRONT_BASE   0x9280000
#define SYSCFG_PIO_REAR_BASE    0x9290000
#define SYSCFG_5000_5999_BASE   0x92B0000

struct vclk vclk_clocks[];

/*
 * Local functions
 */

struct sysconf_grp {
    int first;  /* First sysconf ID */
    int last;   /* Last sysconf ID */
    void *base; /* Sysconf base addr */
};

/* Sysconf base addresses declarations */
static struct sysconf_grp sysconf_groups[] = {
    { 1000, 1999, (void*)SYSCFG_PIO_FRONT_BASE },
    { 2000, 2999, (void*)SYSCFG_PIO_REAR_BASE },
    { 5000, 5999, (void*)SYSCFG_5000_5999_BASE },
    { -1 }
};

/* Temp sysconf support */
unsigned long SYSCONF_READU(num, lsb, msb)
{
    int i, bits;
    unsigned long addr, data;

    for (i = 0; sysconf_groups[i].first != -1; i++)
        if ((num >= sysconf_groups[i].first) && (num <= sysconf_groups[i].last))
            break;
    if (sysconf_groups[i].first == -1) {
        vclk_error("ERROR: Unknown sysconf %d\n", num);
        return 12;
    }
    addr = (unsigned long)sysconf_groups[i].base + (num - sysconf_groups[i].first) * 4;

    bits = msb - lsb + 1;
    data = vclk_peek_p((void*)addr);
    if (bits != 32) {
        data = data >> lsb;
        data &= (1 << bits) - 1;
    }

    return data;
}

void SYSCONF_WRITEU(num, lsb, msb, value)
{
    int i, bits;
    unsigned long addr;

    for (i = 0; sysconf_groups[i].first != -1; i++)
        if ((num >= sysconf_groups[i].first) && (num <= sysconf_groups[i].last))
            break;
    if (sysconf_groups[i].first == -1) {
        vclk_error("ERROR: Unknown sysconf %d\n", num);
        return;
    }
    addr = (unsigned long)sysconf_groups[i].base + (num - sysconf_groups[i].first) * 4;

    bits = msb - lsb + 1;
    if (bits == 32)
        vclk_poke_p((void*)addr, value);
    else {
        unsigned long reg_mask;
        unsigned long tmp;

        reg_mask = ~(((1 << bits) - 1) << lsb);

        tmp = vclk_peek_p((void*)addr);
        tmp &= reg_mask;
        tmp |= value << lsb;
        vclk_poke_p((void*)addr, tmp);
    }
}

/* ========================================================================
   Name:        identify_parent
   Description: Read HW status to identify clock source.
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int identify_parent(struct vclk *clk)
{
    if (!clk)
        return -1;

    vclk_debug(1, "identify_parent(%s)\n", clk->name);

    /* Single source clocks */
    if (clk->flags & VCLK_SINGLESRC)
        return 0;

    /* Flexiclocken lib handled clocks, except refclkinX */
    if ((clk->flags & VCLK_FG) && !(clk->flags2 & FG_REFIN))
        return flexgen_identify_parent(clk);

    /* Other clocks are unexpected */
    return -1;
}

/* ========================================================================
   Name:        recalc
   Description: Read HW programmed clock value
   Returns:     'clk_err_t' error code
   ======================================================================== */

static int recalc(struct vclk *clk)
{
    unsigned long prate = 0;

    if (!clk)
        return -1;

    vclk_debug(1, "recalc(%s)\n", clk->name);

    prate = vclk_get_rate(clk->parent);
    switch (clk->id) {
    /* Special cases */
    case CLK_EXT2F_A9_DIV2:
        clk->rate = prate / 2;
        break;
    case CLK_TMDSOUT_HDMI:
        /* =TMDS_HDMI/some_ratio in HDMI PHY
           but not supported by clock LLA */
    case CLK_PIX_HD_P_IN:
        /* It's a pad input. No way to get its freq */
    case CLK_13_FROM_D0 ... CLK_USB_UTMI:
        /* 'je_clk_input' special clocks. */
        clk->flags &= ~VCLK_OFF;
        if (!clk->parent)
            clk->flags |= VCLK_NORATE;
        else
            clk->rate = prate;
        break;

    /* Flexiclocken or unexpected clocks */
    default:
        /* Flexiclockgen lib handled clocks */
        if (clk->flags & VCLK_FG) {
            return flexgen_recalc(clk);
        }

        return -1;
    }

    clk->flags = 1; /* Always enabled */
    return 0;
}

/* ========================================================================
   Name:        xable
   Description: Enable clock
   Returns:     'clk_err_t' error code
   ======================================================================== */

static int xable(struct vclk *clk, int enable)
{
    int err=0;

    if (!clk)
        return -1;

    vclk_debug(1, "xable(%s, en=%d)\n", clk->name, enable);

    /* Flexiclockgen lib handled clocks */
    if (clk->flags & VCLK_FG) {
        /* Special treatment for audio clockgen (D0).
           Enabling source (FS) then audio clock */
           if ((clk->id >= CLK_PCM_0) && (clk->id <= CLK_PCMR0_MASTER)) {
            if (enable) {
                err = flexgen_xable(clk->parent, 1);
                if (!err)
                    err = flexgen_xable(clk, 1);
            } else {
                err = flexgen_xable(clk, 0);
                if (!err)
                    err = flexgen_xable(clk->parent, 0);
            }
            return err;
           }

        return flexgen_xable(clk, enable);
    }

    switch (clk->id) {
    case CLK_A0_REF:
    case CLK_C0_REF:
    case CLK_D0_REF:
    case CLK_D2_REF:
    case CLK_D3_REF:
        break;

    /* Special cases: no Xable feature */
    case CLK_EXT2F_A9_DIV2:
        return 0;

    /* Unexpected clocks */
    default:
        return -1;
    }

    /* Recalc is now done by upper layer */
    return err;
}

/* ========================================================================
   Name:        set_rate
   Description: Flexiclockgen: Set clock frequency
   Returns:     'clk_err_t' error code
   ======================================================================== */

static int set_rate(struct vclk *clk, unsigned long freq, unsigned long prate)
{
    int err;

    if (!clk)
        return -1;

    vclk_debug(1, "set_rate(%s, %lu, prate=%lu)\n", clk->name, freq, prate);

    switch (clk->id) {
    /* Flexiclockgen lib handled clocks */
    case CLK_A0_REF ... CLK_IC_LMI0:
    case CLK_C0_REF ... CLK_FC_HADES:
    case CLK_D0_REF ... CLK_D0_FS3:
    case CLK_USB2_PHY:
    case CLK_D2_REF ... CLK_REF_HDMIPHY:
    case CLK_D3_REF ... CLK_LPC_1:
#if defined(CONFIG_MACH_STM_COEMUL_MEMIN)
        /* If clock is injected, no way to set it */
        if (!vp_get_rate(clk, 0))
            return 0;
#endif
        return flexgen_set_ratep(clk, freq, prate);

    /* Special treatment for audio clockgen (D0).
       Changing clock at FS output, keeping div in bypass */
    case CLK_PCM_0 ... CLK_PCMR0_MASTER:
        err = flexgen_set_ratep(clk, freq, prate);
        return err;

    /* Special cases: no programmable rate */
    case CLK_EXT2F_A9_DIV2:
        break;

    /* Unexpected clocks */
    default:
        return -1;
    }

    /* Recalc is now done by upper layer */
    return 0;
}

/* ========================================================================
   Name:        set_parent
   Description: Set clock parent
   Returns:     'clk_err_t' error code
   ======================================================================== */

static int set_parent(struct vclk *clk, struct vclk *src)
{
    if (!clk)
        return -1;

    vclk_debug(1, "set_parent(%s)\n", clk->name);

    switch (clk->id) {
    /* Single source clocks not handled by flexiclockgen */
    case CLK_SYSIN:
    case CLK_A0_REF:
    case CLK_C0_REF:
    case CLK_EXT2F_A9_DIV2:
    case CLK_D0_REF:
    case CLK_USB2_PHY:
    case CLK_D2_REF ... CLK_D2_REF1:
    case CLK_TMDSOUT_HDMI ... CLK_USB_UTMI: /* srcclkinX + je_inX */
    case CLK_D3_REF:
        break;

    /* Flexiclocken lib handled clocks */
    default:
        return flexgen_set_parent(clk, src);
    }

    return 0;
}

/******************************************************************************
CA9 PLL
******************************************************************************/

/* ========================================================================
   Name:        a9_recalc
   Description: Get clocks frequencies (in Hz)
   Returns:     'clk_err_t' error code
   ======================================================================== */

static int a9_recalc(struct vclk *clk)
{
    unsigned long odf;
    unsigned long prate = 0;

    if (!clk)
        return -1;

    vclk_debug(1, "a9_recalc(%s)\n", clk->name);

    clk->flags &= ~VCLK_OFF; // Assuming clock enabled
    prate = vclk_get_rate(clk->parent);
    switch (clk->id) {
    case CLK_A9_REF:
    case CLK_A9:
        clk->rate = prate;
        break;
    case CLK_A9_PERIPHS:
        clk->rate = prate / 2;
        break;
    case CLK_A9_PHI0:
        odf = SYSCONF_READU(5108, 8, 13);
        if (odf == 0)
            odf = 1;
        clk->rate = prate / odf;
        break;
    case CLK_A9_FVCOBY2:
        {
            struct vclk_pll pll = {
                .type = VCLK_PLL3200C32,
            };
            int err;

            if (SYSCONF_READU(5106, 0, 0))
                /* A9_PLL_PD=1 => PLL disabled */
                clk->flags |= VCLK_OFF;

            pll.idf = SYSCONF_READU(5106, 25, 27);
            pll.ndiv = SYSCONF_READU(5108, 0, 7);
            err = vclk_pll_get_rate(clk->parent->rate,
                &pll, &clk->rate);
            if (err)
                return -1;
        }
        break;
    default:
        return -1;   /* Unknown clock */
    }

    vclk_debug(1, "  rate=%lu, status=%d\n", clk->rate, clk->flags);
    return 0;
}

/* ========================================================================
   Name:        a9_identify_parent
   Description: Identify parent clock for clockgen A9 clocks.
   Returns:     'clk_err_t' error code
   ======================================================================== */

static int a9_identify_parent(struct vclk *clk)
{
    vclk_debug(1, "a9_identify_parent(%s)\n", clk->name);

    if (clk->id != CLK_A9) /* Other clocks have static parent */
        return 0;

    /* Is CA9 clock sourced from PLL or C0-13 ? */
    if (SYSCONF_READU(5105, 1, 1)) {
        /* Yes. Div by 1 or div by 2 ? */
        if (SYSCONF_READU(5105, 0, 0))
            clk->parent = &vclk_clocks[CLK_EXT2F_A9];
        else
            clk->parent = &vclk_clocks[CLK_EXT2F_A9_DIV2];
    } else
        clk->parent = &vclk_clocks[CLK_A9_PHI0];
    vclk_debug(1, "  parent=%s\n", clk->parent->name);

    return 0;
}

/* ========================================================================
   Name:        a9_set_rate
   Description: Set clock frequency
   Returns:     'clk_err_t' error code
   ======================================================================== */

static int a9_set_rate(struct vclk *clk, unsigned long freq, unsigned long prate)
{
    unsigned long odf;
    struct vclk_pll pll = {
        .type = VCLK_PLL3200C32,
    };
    int err = 0;

    if (!clk)
        return -1;
    if (!clk->parent)
        return -1;

    vclk_debug(1, "a9_set_rate(%s, %lu, prate=%lu)\n", clk->name, freq, prate);

    switch (clk->id) {
    case CLK_A9:    // CLK_A9 = mux output
        return a9_set_rate(clk->parent, freq, clk->parent->parent->rate);
    case CLK_A9_FVCOBY2:
        {
            err = vclk_pll_get_params(prate, freq, &pll);
            vclk_debug(1, "  idf=%u ndiv=%u cp=%u\n", pll.idf, pll.ndiv, pll.cp);
            if (err != 0)
                return err;

            SYSCONF_WRITEU(5105, 1, 1, 1);      /* CLK_CA9=C0-13 clock */

            SYSCONF_WRITEU(5106, 0, 0, 1);      /* Disabling PLL */
            SYSCONF_WRITEU(5106, 25, 27, pll.idf);  /* IDF */
            SYSCONF_WRITEU(5108, 0, 7, pll.ndiv);   /* NDIV */
            SYSCONF_WRITEU(5106, 1, 5, pll.cp); /* Charge Pump */

            SYSCONF_WRITEU(5106, 0, 0, 0);      /* Reenabling PLL */

            while (!SYSCONF_READU(5543, 0, 0))  /* Wait for lock */
                ;
            /* Can't put any delay because may rely on a clock that is currently
               changing (running from CA9 case). */

            SYSCONF_WRITEU(5105, 1, 1, 0);      /* CLK_CA9=PLL */
        }
        break;
    case CLK_A9_PHI0:
        odf = vclk_best_div(prate, freq);
        SYSCONF_WRITEU(5108, 8, 13, odf);   /* ODF */
        vclk_debug(1, "  odf=%lu\n", odf);
        break;
    default:
        return -1;
    };

    /* Recalc is now done by upper layer */
    return 0;
}

/* ========================================================================
   Name:        a9_set_parent
   Description: Identify parent clock for clockgen A9 clocks.
   Returns:     'clk_err_t' error code
   ======================================================================== */

static int a9_set_parent(struct vclk *clk, struct vclk *src)
{
    vclk_debug(1, "a9_set_parent(%s, %s)\n", clk->name, src->name);

    if (clk->id != CLK_A9) /* Other clocks have static parent */
        return 0;

    if (src->id == CLK_A9_PHI0) {
        SYSCONF_WRITEU(5105, 1, 1, 0);
        clk->parent = &vclk_clocks[CLK_A9_PHI0];
    } else {
        SYSCONF_WRITEU(5105, 1, 1, 1);
        if (src->id == CLK_EXT2F_A9) {
            SYSCONF_WRITEU(5105, 0, 0, 1);
            clk->parent = &vclk_clocks[CLK_EXT2F_A9];
        } else {
            SYSCONF_WRITEU(5105, 0, 0, 0);
            clk->parent = &vclk_clocks[CLK_EXT2F_A9_DIV2];
        }
    }

    /* Recalc done by upper layer */
    return 0;
}

/*
 * Clocks declarations
 * Groups, flexgens, then clocks
 */

/* Clock groups declarations */
_CLK_GROUP(grouptop,
    "TOP",
    identify_parent,
    recalc,
    NULL,   // xable,
    NULL,   // set_ratep,
    NULL,   // set_parent,
    NULL,   // observe
    NULL,   // obs point
    NULL    // get_measure
);
_CLK_GROUP(groupa0,
    "A0",
    identify_parent,
    recalc,
    xable,
    set_rate,
    set_parent,
    NULL,   // observe
    NULL,   // obs point
    NULL    // get_measure
);
_CLK_GROUP(groupc0,
    "C0",
    identify_parent,
    recalc,
    xable,
    set_rate,
    set_parent,
    NULL,   // observe
    NULL,   // obs point
    NULL    // get_measure
);
_CLK_GROUP(groupd0,
    "D0",
    identify_parent,
    recalc,
    xable,
    set_rate,
    set_parent,
    NULL,   // observe
    NULL,   // obs point
    NULL    // get_measure
);
_CLK_GROUP(groupd2,
    "D2",
    identify_parent,
    recalc,
    xable,
    set_rate,
    set_parent,
    NULL,   // observe
    NULL,   // obs point
    NULL    // get_measure
);
_CLK_GROUP(groupd3,
    "D3",
    identify_parent,
    recalc,
    xable,
    set_rate,
    set_parent,
    NULL,   // observe
    NULL,   // obs point
    NULL    // get_measure
);
_CLK_GROUP(clkgena9,
    "A9",
    a9_identify_parent,
    a9_recalc,
    NULL,
    a9_set_rate,
    a9_set_parent,
    NULL,   // observe
    NULL,   // obs point
    NULL    // get_measure
);

/* Cross bar sources in connected order */
static int xbar_src_A0[] = {
    CLK_A0_PLL0_PHI0,   /* PLL3200 PHI0 */
    CLK_A0_REF,         /* refclkin[0] */
    -1 /* Keep this last */
};
static int xbar_src_C0[] = {
    CLK_C0_PLL0_PHI0,   /* PLL3200 PHI0 */
    CLK_C0_PLL1_PHI0,   /* PLL3200 PHI0 */
    CLK_C0_FS0,         /* FS660 chan 0 */
    CLK_C0_FS1,
    CLK_C0_FS2,
    CLK_C0_FS3,
    CLK_C0_REF,         /* refclkin[0] */
    -1 /* Keep this last */
};
static int xbar_src_D0[] = {
    CLK_D0_FS0,
    CLK_D0_FS1,
    CLK_D0_FS2,
    CLK_D0_FS3,
    CLK_D0_REF,         /* refclkin[0] */
    -1 /* Keep this last */
};
static int xbar_src_D2[] = {
    CLK_D2_FS0,
    CLK_D2_FS1,
    CLK_D2_FS2,
    CLK_D2_FS3,
    CLK_D2_REF,         /* refclkin[0] */
    CLK_D2_REF1,        /* refclkin[1], no source */
    CLK_TMDSOUT_HDMI,   /* srcclkin[0] */
    CLK_PIX_HD_P_IN,    /* srcclkin[1] */
    CLK_13_FROM_D0,     /* clk_je_input[0] */
    -1 /* Keep this last */
};
static int xbar_src_D3[] = {
    CLK_D3_FS0,
    CLK_D3_FS1,
    CLK_D3_FS2,
    CLK_D3_FS3,
    CLK_D3_REF,     /* refclkin[0] */
    -1 /* Keep this last */
};

/* Flexiclockgen hierarchical description
   - Clockgen name (ex: "A0")
   - Physical base address
   - Group the flexgen belongs to
   - Number of 'refclkin' (even if not connected)
   - Refclkout config (muxmap, divmap)
   - Xbar inputs list (in cross bar input order)
*/
static struct flexgen flexgens[] = {
    {
        "A0",
        (void*)CKG_A0_BASE_ADDRESS,
        &groupa0,
        2,          /* Nb of 'refclkin' */
        0x7, 0x6,   /* refclkout config */
        xbar_src_A0,
    },
    {
        "C0",
        (void*)CKG_C0_BASE_ADDRESS,
        &groupc0,
        2,      /* Nb of 'refclkin' */
        0x1f, 0x18, /* refclkout config */
        xbar_src_C0,
    },
    {
        "D0",
        (void*)CKG_D0_BASE_ADDRESS,
        &groupd0,
        2,      /* Nb of 'refclkin' */
        0x7, 0x6,   /* refclkout config */
        xbar_src_D0,
    },
    {
        "D2",
        (void*)CKG_D2_BASE_ADDRESS,
        &groupd2,
        2,      /* Nb of 'refclkin' */
        0x7, 0x6,   /* refclkout config */
        xbar_src_D2,    /* Xbar inputs */
    },
    {
        "D3",
        (void*)CKG_D3_BASE_ADDRESS,
        &groupd3,
        2,      /* Nb of 'refclkin' */
        0x7, 0x6,   /* refclkout config */
        xbar_src_D3,
    },
    {
        NULL /* Keep this last */
    }
};

/* Clocks declarations */
struct vclk vclk_clocks[] = {
/* Top level clocks */
_CLK_FIXED(CLK_SYSIN, &grouptop, 30000000),

/* Clockgen A0 */
_CLK_FG_P(CLK_A0_REF,       &flexgens[0], FG_REFIN, 0, 0, &vclk_clocks[CLK_SYSIN]),
_CLK_FG(CLK_A0_REFOUT0,     &flexgens[0], FG_REFOUT, 0, 0),
_CLK_FG_P(CLK_A0_PLL0_VCO,  &flexgens[0], FG_GFGOUT | FG_PLL3200C32 | FG_FVCOBY2, 0, 0, &vclk_clocks[CLK_A0_REFOUT0]),
_CLK_FG_P(CLK_A0_PLL0_PHI0, &flexgens[0], FG_GFGOUT | FG_PLL3200C32 | FG_PHI, 0, 0, &vclk_clocks[CLK_A0_PLL0_VCO]),
_CLK_FG(CLK_IC_LMI0,        &flexgens[0], FG_DIV, 0, 0),

/* Clockgen C0 */
_CLK_FG_P(CLK_C0_REF,       &flexgens[1], FG_REFIN, 0, 0, &vclk_clocks[CLK_SYSIN]),
_CLK_FG(CLK_C0_REFOUT0,     &flexgens[1], FG_REFOUT, 0, 0),
_CLK_FG(CLK_C0_REFOUT1,     &flexgens[1], FG_REFOUT, 1, 0),
_CLK_FG(CLK_C0_REFOUT2,     &flexgens[1], FG_REFOUT, 2, 0),
_CLK_FG_P(CLK_C0_PLL0_VCO,  &flexgens[1], FG_GFGOUT | FG_PLL3200C32 | FG_FVCOBY2, 0, 0, &vclk_clocks[CLK_C0_REFOUT0]),
_CLK_FG_P(CLK_C0_PLL0_PHI0, &flexgens[1], FG_GFGOUT | FG_PLL3200C32 | FG_PHI, 0, 0, &vclk_clocks[CLK_C0_PLL0_VCO]),
_CLK_FG_P(CLK_C0_PLL1_VCO,  &flexgens[1], FG_GFGOUT | FG_PLL3200C32 | FG_FVCOBY2, 1, 0, &vclk_clocks[CLK_C0_REFOUT1]),
_CLK_FG_P(CLK_C0_PLL1_PHI0, &flexgens[1], FG_GFGOUT | FG_PLL3200C32 | FG_PHI, 1, 0, &vclk_clocks[CLK_C0_PLL1_VCO]),
_CLK_FG_P(CLK_C0_FS_VCO,    &flexgens[1], FG_GFGOUT | FG_FS660C32 | FG_FVCO, 2, 0, &vclk_clocks[CLK_C0_REFOUT2]),
_CLK_FG_P(CLK_C0_FS0,       &flexgens[1], FG_GFGOUT | FG_FS660C32 | FG_CHAN, 2, 0, &vclk_clocks[CLK_C0_FS_VCO]),
_CLK_FG_P(CLK_C0_FS1,       &flexgens[1], FG_GFGOUT | FG_FS660C32 | FG_CHAN, 2, 1, &vclk_clocks[CLK_C0_FS_VCO]),
_CLK_FG_P(CLK_C0_FS2,       &flexgens[1], FG_GFGOUT | FG_FS660C32 | FG_CHAN, 2, 2, &vclk_clocks[CLK_C0_FS_VCO]),
_CLK_FG_P(CLK_C0_FS3,       &flexgens[1], FG_GFGOUT | FG_FS660C32 | FG_CHAN, 2, 3, &vclk_clocks[CLK_C0_FS_VCO]),
_CLK_FG_DIV(CLK_ICN_GPU,    &flexgens[1], 0),
_CLK_FG_DIV(CLK_FDMA,       &flexgens[1], 1),
_CLK_FG_DIV(CLK_NAND,       &flexgens[1], 2),
_CLK_FG_DIV(CLK_HVA,        &flexgens[1], 3),
_CLK_FG_DIV(CLK_PROC_STFE,  &flexgens[1], 4),
_CLK_FG_DIV(CLK_TP,         &flexgens[1], 5),
_CLK_FG_DIV(CLK_RX_ICN_DMU, &flexgens[1], 6),
_CLK_FG_DIV(CLK_RX_ICN_HVA, &flexgens[1], 7),
_CLK_FG_DIV(CLK_ICN_CPU,    &flexgens[1], 8),
_CLK_FG_DIV(CLK_TX_ICN_DMU, &flexgens[1], 9),
_CLK_FG_DIV(CLK_MMC_0,      &flexgens[1], 10),
_CLK_FG_DIV(CLK_MMC_1,      &flexgens[1], 11),
_CLK_FG_DIV(CLK_JPEGDEC,    &flexgens[1], 12),
_CLK_FG_DIV(CLK_EXT2F_A9,   &flexgens[1], 13),
_CLK_FG_DIV(CLK_IC_BDISP_0, &flexgens[1], 14),
_CLK_FG_DIV(CLK_IC_BDISP_1, &flexgens[1], 15),
_CLK_FG_DIV(CLK_PP_DMU,     &flexgens[1], 16),
_CLK_FG_DIV(CLK_VID_DMU,    &flexgens[1], 17),
_CLK_FG_DIV(CLK_DSS_LPC,    &flexgens[1], 18),
_CLK_FG_DIV(CLK_ST231_AUD_0,&flexgens[1], 19),
_CLK_FG_DIV(CLK_ST231_GP_1, &flexgens[1], 20),
_CLK_FG_DIV(CLK_ST231_DMU,  &flexgens[1], 21),
_CLK_FG_DIV(CLK_ICN_LMI,    &flexgens[1], 22),
_CLK_FG_DIV(CLK_TX_ICN_DISP_0,  &flexgens[1], 23),
_CLK_FG_DIV(CLK_ICN_SBC,    &flexgens[1], 24),
_CLK_FG_DIV(CLK_STFE_FRC2,  &flexgens[1], 25),
_CLK_FG_DIV(CLK_ETH_PHY,    &flexgens[1], 26),
_CLK_FG_DIV(CLK_ETH_PHYREF, &flexgens[1], 27),
_CLK_FG_DIV(CLK_FLASH_PROMIP,   &flexgens[1], 28),
_CLK_FG_DIV(CLK_MAIN_DISP,  &flexgens[1], 29),
_CLK_FG_DIV(CLK_AUX_DISP,   &flexgens[1], 30),
_CLK_FG_DIV(CLK_COMPO_DVP,  &flexgens[1], 31),

_CLK_FG_DIV(CLK_TX_ICN_HADES,   &flexgens[1], 32),
_CLK_FG_DIV(CLK_RX_ICN_HADES,   &flexgens[1], 33),
_CLK_FG_DIV(CLK_ICN_REG_16, &flexgens[1], 34),
_CLK_FG_DIV(CLK_PP_HADES,   &flexgens[1], 35),
_CLK_FG_DIV(CLK_CLUST_HADES,&flexgens[1], 36),
_CLK_FG_DIV(CLK_HWPE_HADES, &flexgens[1], 37),
_CLK_FG_DIV(CLK_FC_HADES,   &flexgens[1], 38),

/* Clockgen D0 */
_CLK_FG_P(CLK_D0_REF,       &flexgens[2], FG_REFIN, 0, 0, &vclk_clocks[CLK_SYSIN]),
_CLK_FG(CLK_D0_REFOUT0,     &flexgens[2], FG_REFOUT, 0, 0),
_CLK_FG_P(CLK_D0_FS_VCO,    &flexgens[2], FG_GFGOUT | FG_FS660C32 | FG_FVCO, 0, 0, &vclk_clocks[CLK_D0_REFOUT0]),
_CLK_FG_P(CLK_D0_FS0,       &flexgens[2], FG_GFGOUT | FG_FS660C32 | FG_CHAN, 0, 0, &vclk_clocks[CLK_D0_FS_VCO]),
_CLK_FG_P(CLK_D0_FS1,       &flexgens[2], FG_GFGOUT | FG_FS660C32 | FG_CHAN, 0, 1, &vclk_clocks[CLK_D0_FS_VCO]),
_CLK_FG_P(CLK_D0_FS2,       &flexgens[2], FG_GFGOUT | FG_FS660C32 | FG_CHAN, 0, 2, &vclk_clocks[CLK_D0_FS_VCO]),
_CLK_FG_P(CLK_D0_FS3,       &flexgens[2], FG_GFGOUT | FG_FS660C32 | FG_CHAN, 0, 3, &vclk_clocks[CLK_D0_FS_VCO]),
_CLK_FG_DIV(CLK_PCM_0,      &flexgens[2], 0),
_CLK_FG_DIV(CLK_PCM_1,      &flexgens[2], 1),
_CLK_FG_DIV(CLK_PCM_2,      &flexgens[2], 2),
_CLK_FG_DIV(CLK_SPDIFF,     &flexgens[2], 3),
_CLK_FG_DIV(CLK_PCMR0_MASTER,   &flexgens[2], 4),
_CLK_FG_DIV(CLK_D0_SPARE_5, &flexgens[2], 5),
_CLK_FG_DIV(CLK_D0_SPARE_6, &flexgens[2], 6),
_CLK_FG_DIV(CLK_D0_SPARE_7, &flexgens[2], 7),
_CLK_FG_DIV(CLK_D0_SPARE_8, &flexgens[2], 8),
_CLK_FG_DIV(CLK_D0_SPARE_9, &flexgens[2], 9),
_CLK_FG_DIV(CLK_D0_SPARE_10,&flexgens[2], 10),
_CLK_FG_DIV(CLK_D0_SPARE_11,&flexgens[2], 11),
_CLK_FG_DIV(CLK_D0_SPARE_12,&flexgens[2], 12),
_CLK_FG_DIV(CLK_D0_SPARE_13,&flexgens[2], 13),

_CLK_FG(CLK_USB2_PHY,       &flexgens[2], FG_EREFOUT, 0, 0),

/* Clockgen D2 */
_CLK_FG_P(CLK_D2_REF,       &flexgens[3], FG_REFIN, 0, 0, &vclk_clocks[CLK_SYSIN]),
_CLK_FG_P(CLK_D2_REF1,      &flexgens[3], FG_REFIN, 1, 0, &vclk_clocks[CLK_SYSIN]),
_CLK_FG(CLK_D2_REFOUT0,     &flexgens[3], FG_REFOUT, 0, 0),
_CLK_FG_P(CLK_D2_FS_VCO,    &flexgens[3], FG_GFGOUT | FG_FS660C32 | FG_FVCO, 0, 0, &vclk_clocks[CLK_D2_REFOUT0]),
_CLK_FG_P(CLK_D2_FS0,       &flexgens[3], FG_GFGOUT | FG_FS660C32 | FG_CHAN | FG_BEAT | FG_NSDIV1, 0, 0, &vclk_clocks[CLK_D2_FS_VCO]),
_CLK_FG_P(CLK_D2_FS1,       &flexgens[3], FG_GFGOUT | FG_FS660C32 | FG_CHAN | FG_BEAT | FG_NSDIV1, 0, 1, &vclk_clocks[CLK_D2_FS_VCO]),
_CLK_FG_P(CLK_D2_FS2,       &flexgens[3], FG_GFGOUT | FG_FS660C32 | FG_CHAN | FG_BEAT | FG_NSDIV1, 0, 2, &vclk_clocks[CLK_D2_FS_VCO]),
_CLK_FG_P(CLK_D2_FS3,       &flexgens[3], FG_GFGOUT | FG_FS660C32 | FG_CHAN | FG_BEAT | FG_NSDIV1, 0, 3, &vclk_clocks[CLK_D2_FS_VCO]),

_CLK_FG_DIV2(CLK_PIX_MAIN_DISP, &flexgens[3], FG_SYNC, 0),
_CLK_FG_DIV2(CLK_PIX_PIP,       &flexgens[3], FG_SYNC, 1),
_CLK_FG_DIV2(CLK_PIX_GDP1,      &flexgens[3], FG_SYNC, 2),
_CLK_FG_DIV2(CLK_PIX_GDP2,      &flexgens[3], FG_SYNC, 3),
_CLK_FG_DIV2(CLK_PIX_GDP3,      &flexgens[3], FG_SYNC, 4),
_CLK_FG_DIV2(CLK_PIX_GDP4,      &flexgens[3], FG_SYNC, 5),
_CLK_FG_DIV2(CLK_PIX_AUX_DISP,  &flexgens[3], FG_SYNC, 6),
_CLK_FG_DIV2(CLK_DENC,          &flexgens[3], FG_SYNC, 7),
_CLK_FG_DIV2(CLK_PIX_HDDAC,     &flexgens[3], FG_SYNC, 8),
_CLK_FG_DIV2(CLK_HDDAC,         &flexgens[3], FG_SYNC, 9),
_CLK_FG_DIV2(CLK_SDDAC,         &flexgens[3], FG_SYNC, 10),
_CLK_FG_DIV2(CLK_PIX_DVO,       &flexgens[3], FG_SYNC, 11),
_CLK_FG_DIV2(CLK_DVO,           &flexgens[3], FG_SYNC, 12),
_CLK_FG_DIV2(CLK_PIX_HDMI,      &flexgens[3], FG_SYNC, 13),
_CLK_FG_DIV(CLK_TMDS_HDMI,      &flexgens[3], 14),
_CLK_FG_DIV(CLK_REF_HDMIPHY,    &flexgens[3], 15),

_CLK_FG(CLK_TMDSOUT_HDMI,   &flexgens[3], FG_SRCIN | FG_BEAT, 0, 0), /* srcclkin0 */
_CLK_FG(CLK_PIX_HD_P_IN,    &flexgens[3], FG_SRCIN | FG_BEAT, 1, 0), /* srcclkin1 */

_CLK_FG(CLK_13_FROM_D0,     &flexgens[3], FG_JEIN, 0, 0), /* clk_je_input[0] */
_CLK_FG(CLK_USB_480,        &flexgens[3], FG_JEIN, 2, 0), /* clk_je_input[2] */
_CLK_FG(CLK_USB_UTMI,       &flexgens[3], FG_JEIN, 5, 0), /* clk_je_input[5] */

/* Clockgen D3 */
_CLK_FG_P(CLK_D3_REF,       &flexgens[4], FG_REFIN, 0, 0, &vclk_clocks[CLK_SYSIN]),
_CLK_FG(CLK_D3_REFOUT0,     &flexgens[4], FG_REFOUT, 0, 0),
_CLK_FG_P(CLK_D3_FS_VCO,    &flexgens[4], FG_GFGOUT | FG_FS660C32 | FG_FVCO, 0, 0, &vclk_clocks[CLK_D3_REFOUT0]),
_CLK_FG_P(CLK_D3_FS0,       &flexgens[4], FG_GFGOUT | FG_FS660C32 | FG_CHAN, 0, 0, &vclk_clocks[CLK_D3_FS_VCO]),
_CLK_FG_P(CLK_D3_FS1,       &flexgens[4], FG_GFGOUT | FG_FS660C32 | FG_CHAN, 0, 1, &vclk_clocks[CLK_D3_FS_VCO]),
_CLK_FG_P(CLK_D3_FS2,       &flexgens[4], FG_GFGOUT | FG_FS660C32 | FG_CHAN, 0, 2, &vclk_clocks[CLK_D3_FS_VCO]),
_CLK_FG_P(CLK_D3_FS3,       &flexgens[4], FG_GFGOUT | FG_FS660C32 | FG_CHAN, 0, 3, &vclk_clocks[CLK_D3_FS_VCO]),
_CLK_FG_DIV(CLK_STFE_FRC1,  &flexgens[4], 0),
_CLK_FG_DIV(CLK_TSOUT_0,    &flexgens[4], 1),
_CLK_FG_DIV(CLK_TSOUT_1,    &flexgens[4], 2),
_CLK_FG_DIV(CLK_MCHI,       &flexgens[4], 3),
_CLK_FG_DIV(CLK_VSENS_COMPO,&flexgens[4], 4),
_CLK_FG_DIV(CLK_FRC1_REMOTE,&flexgens[4], 5),
_CLK_FG_DIV(CLK_LPC_0,      &flexgens[4], 6),
_CLK_FG_DIV(CLK_LPC_1,      &flexgens[4], 7),

/* CA9 PLL */
_CLK_P(CLK_A9_REF,          &clkgena9,  &vclk_clocks[CLK_SYSIN]),
_CLK_P(CLK_A9_FVCOBY2,      &clkgena9,  &vclk_clocks[CLK_A9_REF]),
_CLK_P(CLK_A9_PHI0,         &clkgena9,  &vclk_clocks[CLK_A9_FVCOBY2]),
_CLK(CLK_A9,                &clkgena9),
_CLK_P(CLK_A9_PERIPHS,      &clkgena9,  &vclk_clocks[CLK_A9]),
};

int vclk_nb = (sizeof(vclk_clocks) / sizeof(struct vclk));

int vclk_soc_init(void)
{
    vclk_debug(1, "H410 vclkd %s, %u known clocks\n", VCLK_SOC_VERSION, vclk_nb);

    /* Flexiclockgen lib init */
    if (flexgen_init(vclk_clocks, vclk_nb) != 0)
        return -1;

    return 0;
}
