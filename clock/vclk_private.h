/*
* This file is part of Validation CLK Driver (VCLKD).
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
* Author(s): fabrice.charpentier@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* VCLKD may alternatively be licensed under GPL V2 license from ST.
*/

#ifndef __VCLK_PRIVATE_H
#define __VCLK_PRIVATE_H

/*
 * Clock registration macros (used by clocks-<soc>.c)
 */

/* Clock group registration */
#define _CLK_GROUP(_name, _desc, _identp, _recalc, _xable, _setratep, _setparent, _obs, _obspoint, _getmeasure) \
static struct vclk_group _name = { \
    .name = _desc,              \
    .identify_parent = _identp, \
    .recalc_rate = _recalc,     \
    .xable = _xable,            \
    .set_ratep = _setratep,     \
    .set_parent = _setparent,   \
    .observe = _obs,            \
    .obs_point = _obspoint,     \
    .get_measure = _getmeasure, \
}

/* Fixed source clock registration */
#define _CLK_FIXED(_id, _group, _rate) \
[_id] = (struct vclk){          \
    .name = #_id,               \
    .id = (_id),                \
    .group = ((void*)_group),   \
    .rate = (_rate),            \
    .flags = VCLK_FIXED         \
}

/* Standard clock registration */
#define _CLK(_id, _group)       \
[_id] = (struct vclk){          \
    .name = #_id,               \
    .id = (_id),                \
    .group = ((void*)_group),   \
}

/* Standard clock with fixed parent */
#define _CLK_P(_id, _group, _parent)    \
[_id] = (struct vclk){          \
    .name = #_id,               \
    .id = (_id),                \
    .group = ((void*)_group),   \
    .parent = (_parent),        \
    .flags = VCLK_SINGLESRC     \
}

/* Flexiclockgen clock registration macro */
#define _CLK_FG(_id, _fgen, _fgflags, _idx0, _idx1)   \
[_id] = (struct vclk){          \
    .name = #_id,               \
    .id = (_id),                \
    .group = ((void*)_fgen),    \
    .flags = VCLK_FG,           \
    .flags2 = (_fgflags),       \
    .idx0 = (_idx0),            \
    .idx1 = (_idx1)             \
}

/* Flexiclockgen clock registration macro
   _id/_flags = Clock id & flags
   _fgen = Flexgen group
   _fgflags/_fgidx0/_gfidx1 = Flexgen specifics
 */
#define _CLK_FG2(_id, _fgen, _flags, _fgflags, _fgidx0, _fgidx1)   \
[_id] = (struct vclk){          \
    .name = #_id,               \
    .id = (_id),                \
    .group = ((void*)_fgen),    \
    .flags = (VCLK_FG|_flags),  \
    .flags2 = (_fgflags),       \
    .idx0 = (_fgidx0),          \
    .idx1 = (_fgidx1)           \
}

/* Flexiclockgen clock registration macro
   Clock with frozen parent. */
#define _CLK_FG_P(_id, _fgen, _fgflags, _idx0, _idx1, _parent)    \
[_id] = (struct vclk){                  \
    .name = #_id,                       \
    .id = (_id),                        \
    .group = ((void*)_fgen),            \
    .flags = (VCLK_FG|VCLK_SINGLESRC),  \
    .flags2 = (_fgflags),               \
    .idx0 = (_idx0),                    \
    .idx1 = (_idx1),                    \
    .parent = (_parent)                 \
}

/* Flexiclockgen clock registration macro
   Final divider output */
#define _CLK_FG_DIV(_id, _fgen, _idx0)  \
[_id] = (struct vclk){          \
    .name = #_id,               \
    .id = (_id),                \
    .group = ((void*)_fgen),    \
    .flags = VCLK_FG,           \
    .flags2 = FG_DIV,           \
    .idx0 = (_idx0)             \
}

#define _CLK_FG_DIV2(_id, _fgen, _fgflags, _idx0)  \
[_id] = (struct vclk){          \
    .name = #_id,               \
    .id = (_id),                \
    .group = ((void*)_fgen),    \
    .flags = VCLK_FG,           \
    .flags2 = (FG_DIV|_fgflags),\
    .idx0 = (_idx0)             \
}

/*
 * vclk common variables (WARNING.. may need to remove)
 */

/* Per SOC known clocks table */
extern struct vclk vclk_clocks[];
extern int vclk_nb;

/* Returns parent clock if parent exists or 0 */
static inline unsigned long vclk_get_prate(struct vclk *clk)
{
    if (!clk || !clk->parent)
        return 0;
    return clk->parent->rate;
}

/*
 * SOC specific code
 */

int vclk_soc_init(void);

#endif // __VCLK_PRIVATE_H
