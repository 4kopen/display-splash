/*
* This file is part of Validation CLK Driver (VCLKD).
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
* Author(s): fabrice.charpentier@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* VCLKD may alternatively be licensed under GPL V2 license from ST.
*/

#include "vclk_flexgen.h"
#include "vclk_algos.h"
#include <stddef.h>
#include "vclk_private.h"
#include <stdint.h>

/*
 * Privates
 */

#define FCG_CONFIG(_num)            (0x0 + (_num) * 0x4)
#define GFG_CONFIG(_gfg_id, _num)   (0x2a0 + (_num) * 0x4 + (_gfg_id) * 0x28)

/*
 * Local functions
 */

/* ========================================================================
Name:        flexgen_get_fcg
Description: Update FCG_CONFIG reg offset & shift for given 'idx'
Returns:     Nothing
======================================================================== */

static void flexgen_get_fcg(int idx, unsigned long *regoffs, int *shift)
{
    *regoffs = (idx / 4) * 4;
    *shift = (idx % 4) * 8;
}

/* ========================================================================
   Name:        flexgen_xbar_set
   Description: Configure XBAR
     out_idx = output index
     fg_base = flexiclockgen VIRTUAL base address
     in_idx  = input index
   Returns:     0=OK
   ======================================================================== */

static int flexgen_xbar_set(int out_idx, void *fg_base, int in_idx)
{
    unsigned long data;
    void *vaddr;
    unsigned long regoffs;
    int shift;

    flexgen_get_fcg(out_idx, &regoffs, &shift);
    vaddr = fg_base + FCG_CONFIG(6) + regoffs;
    data = vclk_peek_v(vaddr);
    data &= ~(0x3f << shift);
    data |= (((1 << 6) | in_idx) << shift);
    vclk_poke_v(vaddr, data);

    return 0;
}

/* ========================================================================
   Name:        flexgen_xbar_reroute
   Description: Reconfigure Xbar so that inputs 'in' are rerouted to
                'refclkin[0]'. Or restore previous rerouting.
     fg      = flexgen
     in      = 1 bit per input to avoid (0 to 63)
     restore = if 1, restore previous rerouting
   Returns:     0=OK
   ======================================================================== */

static int flexgen_xbar_reroute(struct flexgen *fg, unsigned long long in,
                   int restore)
{
    int o, i, change;
    static unsigned long long rerouted; /* Rerouted outputs */
    static unsigned char input[64];
    unsigned long regoffs;
    int shift;

    if (!restore) {
        int ref_idx;

        vclk_debug(1, "  Rerouting inputs 0x%llx\n", in);

        /* What is xbar input of 'refclkin0' ? */
        for (ref_idx = 0; fg->xbar_src[ref_idx] != -1; ref_idx++) {
            if (!(vclk_clocks[fg->xbar_src[ref_idx]].flags2 & FG_REFIN))
                continue; /* Not a 'refclkin' */
            if (!vclk_clocks[fg->xbar_src[ref_idx]].idx0)
                break; /* Found refclkin[0] */
        }
        vclk_debug(1, "    refclkin0 found on xbar input %d\n", ref_idx);

        /* TO BE COMPLETED
        Should raise an error if REFIN0 not found
        */

        /* Routing matching childs to 'refclkin0' */
        rerouted = 0ll;
        for (o = 0; o < (fg->divlast - fg->div0 + 1); o++) {
            if (vclk_clocks[fg->div0 + o].name && !vclk_clocks[fg->div0 + o].parent) {
                /* Could be spare clock */
                vclk_debug(1, "No parent for xbar output %d (id=%d) ... IGNORING\n", o, fg->div0 + o);
                continue;
            }

            /* Getting FCG_CONFIG proper offset & shift */
            flexgen_get_fcg(o, &regoffs, &shift);

            /* Identifying crossbar source */
            i = (vclk_peek_v(fg->virt_addr + FCG_CONFIG(6) + regoffs) >> shift) & 0x3f;
            change = (in >> i) & 1ll;
            if (!change)
                continue;

            vclk_debug(1, "    Rerouting '%s' (out %d)\n", vclk_clocks[fg->div0 + o].name, o);
            flexgen_xbar_set(o, fg->virt_addr, ref_idx);

            /* Storing route infos for restore */
            rerouted |= (1ll << o);
            input[o] = i;
        }
        vclk_debug(1, "    Rerouted xbar outputs=0x%llx\n", rerouted);

    } else {
        /* Restoring childs routing */
        for (o = 0; o < (fg->divlast - fg->div0 + 1); o++) {
            change = (rerouted >> o) & 1ll;
            if (!change)
                continue;
            vclk_debug(1, "  Restoring '%s' route\n", vclk_clocks[fg->div0 + o].name);
            flexgen_xbar_set(o, fg->virt_addr, input[o]);
        }
    }

    return 0;
}

/* ========================================================================
   Name:        flexgen_refout_recalc
   Description: Read HW status for 'refclkout' or 'ext_refclkout' clocks
   Returns:     0=OK, or -1
   ======================================================================== */

static int flexgen_refout_recalc(struct vclk *clk)
{
    int idx;
    unsigned long parent_rate;
    struct flexgen *fg;

    /* Reading 'muxdiv' ratio (1 to 16). Order is
       MuxDiv0   => refclkout[0] => GFG0 ref
       ...
       MuxDivX   => refclkout[X] => GFGX ref
       MuxDivX+1 => ext_refclkout0
       MuxDivX+2 => ext_refclkout1
       ... etc
     */
    fg = (struct flexgen *)clk->group;
    if (clk->flags2 & FG_REFOUT)
        idx = clk->idx0;
    else /* 'ext_refclkout' case */
        idx = fg->nb_gfg + clk->idx0;

    /* Any div capability ? */
    parent_rate = vclk_get_prate(clk);
    if (fg->refdivmap & (1 << idx)) {
        unsigned long data;

        /* Yes */
        data = vclk_peek_v(fg->virt_addr + FCG_CONFIG(3 + (idx / 8)));
        /* Fields are: [3:0], [7:4], [11:8], ...etc */
        data = ((data >> ((idx % 8) * 4)) & 0xf) + 1;

        clk->rate = parent_rate / data;
    } else
        /* No. Direct connection to parent */
        clk->rate = parent_rate;

    clk->flags &= ~VCLK_OFF; // Always ON

    return 0;
}

/* ========================================================================
   Name:        flexgen_pll3200_recalc
   Description: Read PLL3200 C28/C32 GFG HW status
   Returns:     0=OK, or -1
   ======================================================================== */

int flexgen_pll3200_recalc(struct vclk *clk)
{
    unsigned long data;
    struct vclk_pll pll;
    int err, gfg_id;
    unsigned long parent_rate;
    struct flexgen *fg;

    if (!clk) {
        vclk_error("ERROR: lost in flexgen_pll3200_recalc()\n");
        return -1;
    }

    fg = (struct flexgen *)clk->group;
    gfg_id = clk->idx0;
    parent_rate = vclk_get_prate(clk);
    vclk_debug(1, "flexgen_pll3200_recalc(%s), gfg_id=%d, prate=%lu\n",
        clk->name, gfg_id, parent_rate);
    if (clk->flags2 & FG_PLL3200C32) {
        pll.type = VCLK_PLL3200C32;
    } else {
        pll.type = VCLK_PLL3200C28;
    }

    clk->flags &= ~VCLK_OFF; // Assuming enabled

    if (clk->flags2 & FG_FVCOBY2) {
        data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0));
        if (data & (1 << 8)) {
            vclk_debug(1, "  PLL is powered down\n");
            clk->flags |= VCLK_OFF; /* PLL is powered DOWN */
        }

        /* Reading FVCOBY2 clock */
        data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 1));
        pll.idf = data & 0x7;           // gfgx_config1[2:0]
        pll.ndiv = (data >> 16) & 0xff; // gfgx_config1[23:16]
        data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 2));
        if (data & (1 << 21)) { // FRAC_CONTROL = 1 ?
            pll.mode = 1;
            pll.frac = data & 0xffff;
        } else
            pll.mode = 0;
        err = vclk_pll_get_rate(parent_rate, &pll, &clk->rate);
        if (err != 0) {
            vclk_error("ERROR: Can't compute PLL3200Cxx '%s' "
                   "freq with idf/ndiv=%lu/%lu\n",
                   clk->name, pll.idf, pll.ndiv);
            return -1;
        }
        vclk_debug(1, "  idf=%u ndiv=%u mode=%d frac=%u => FVCOBY2=%lu\n",
              pll.idf, pll.ndiv, pll.mode, pll.frac, clk->rate);
    } else if (clk->flags2 & FG_PHI) {
        /* Reading ODFx ratio (1 to 63) */
        data = vclk_peek_v(fg->virt_addr
                + GFG_CONFIG(gfg_id, 5 + clk->idx1))
                & 0x3f;
        if (!data)
            data = 1;
        clk->rate = parent_rate / data;
        vclk_debug(1, "  odf=%lu => PHI rate=%lu\n", data, clk->rate);
    } else {
        vclk_error("ERROR: flexgen_pll3200_recalc(), neither VCO nor PHI\n");
        return -1;
    }

    return 0;
}

/* ========================================================================
   Name:        flexgen_fs660_recalc
   Description: Read FS660 C28/C32 GFG HW status
   Returns:     0=OK, or -1=ERROR
   ======================================================================== */

int flexgen_fs660_recalc(struct vclk *clk)
{
    int err = 0;
    unsigned long data;
    struct vclk_fs fs_vco;
    struct vclk_fs fs;
    int channel;
    unsigned long parent_rate;
    int gfg_id;
    struct flexgen *fg;

    if (!clk) {
        vclk_error("ERROR: lost in flexgen_fs660_recalc()\n");
        return -1;
    }

    if (clk->flags2 & FG_FS660C32) {
        fs_vco.type = VCLK_FS660C32VCO;
        fs.type = VCLK_FS660C32;
    } else {
        fs_vco.type = VCLK_FS660C28VCO;
        fs.type = VCLK_FS660C28;
    }
    gfg_id = clk->idx0;
    channel = clk->idx1;
    parent_rate = vclk_get_prate(clk);
    vclk_debug(1, "flexgen_fs660_recalc(%s), gfg_id=%d, prate=%lu, chan=%d\n",
          clk->name, gfg_id, parent_rate, channel);
    fg = (struct flexgen *)clk->group;

    clk->flags &= ~VCLK_OFF; // Assuming enabled

    if (clk->flags2 & FG_FVCO) {
        /* Checking FSYN analog part (~NPDPLL) */
        data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0));
        if ((data & (1 << 12)) != 0) {
            vclk_debug(1, "  FS is powered down (NPDPLL=0)\n");
            clk->flags |= VCLK_OFF;
        }

        /* Computing freq */
        data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 1));
        if (clk->flags2 & FG_FS660C32)
            fs_vco.ndiv = (data >> 16) & 0x7;
        else
            fs_vco.ndiv = (data >> 16) & 0xf;
        err = vclk_fs_get_rate(parent_rate, &fs_vco, &clk->rate);
        if (err != 0) {
            vclk_error("ERROR: Can't compute FS660 VCO "
                   "freq with ndiv=%lu\n", fs_vco.ndiv);
            return -1;
        }
        vclk_debug(1, "  ndiv=%lu => FVCO rate=%lu\n",
              fs_vco.ndiv, clk->rate);
        parent_rate = clk->rate;
    } else if (clk->flags2 & FG_CHAN) {
        /* Checking FSYN digital part (~NRSTx) */
        data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0));
        if ((data & (1 << channel)) != 0) {
            vclk_debug(1, "  FS in digital reset (NRSTx=0)\n");
            clk->flags |= VCLK_OFF;
        }
        /* Checking FSYN digital part (~NSBx) */
        data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0));
        if ((data & (0x100 << channel)) != 0) {
            vclk_debug(1, "  FS in digital power down (NSBx=0)\n");
            clk->flags |= VCLK_OFF;
        }

        /* FSYN up & running. Computing frequency */
        data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, (5 + channel)));
        fs.mdiv = (data >> 15) & 0x1f;  /* MD[19:15] */
        fs.pe = (data & 0x7fff);        /* PE[14:0] */
        fs.sdiv = (data >> 20) & 0xf;   /* SDIV[23:20] */
        fs.nsdiv = (data >> 24) & 0x1;  /* NSDIV[24] */
        err = vclk_fs_get_rate(parent_rate, &fs, &clk->rate);
        if (err != 0) {
             vclk_error("ERROR: Can't compute FS660 '%s' "
                   "freq with md/pe/sd/nsd=%lu/%lu/%lu"
                   "/%lu\n", clk->name,
                   fs.mdiv, fs.pe, fs.sdiv, fs.nsdiv);
            return -1;
        }
        vclk_debug(1, "  md/pe/sd/nsd=%lu/%lu/%lu/%lu => CHAN rate=%lu\n",
              fs.mdiv, fs.pe, fs.sdiv, fs.nsdiv, clk->rate);
    }

    return 0;
}

/* ========================================================================
   Name:        flexgen_div_recalc
   Description: Read dividers HW status
   Returns:     0=OK, or -1
   ======================================================================== */

int flexgen_div_recalc(struct vclk *clk)
{
    unsigned long regoffs, data, ratio;
    int shift;
    unsigned long parent_rate;
    struct flexgen *fg;

    if (!clk) {
        vclk_error("ERROR: lost in flexgen_div_recalc()\n");
        return -1;
    }

    vclk_debug(1, "flexgen_div_recalc(%s)\n", clk->name);

    clk->flags &= ~VCLK_OFF; // Assuming enabled
    fg = (struct flexgen *)clk->group;

    /* Is clock enabled at crossbar output ?
       Note that this disable feature should normally not be used
       prefering disable feature of final divider instead. */
    flexgen_get_fcg(clk->idx0, &regoffs, &shift);
    data = vclk_peek_v(fg->virt_addr + FCG_CONFIG(6) + regoffs) >> (shift + 6);
    if ((data & 1) == 0) { /* Bit 6 = enable */
        vclk_debug(1, "  cross bar output DISABLED\n");
        clk->flags |= VCLK_OFF;
    }

    /* Reading predivider */
    data = vclk_peek_v(fg->virt_addr + FCG_CONFIG(22 + clk->idx0));
    ratio = (data & 0x3FF) + 1;
    parent_rate = vclk_get_prate(clk);
    vclk_debug(1, "  prate=%lu Hz, prediv=%lu\n", parent_rate, ratio);
    clk->rate = parent_rate / ratio;

    /* Reading final divider */
    data = vclk_peek_v(fg->virt_addr + FCG_CONFIG(89 + clk->idx0));
    if ((data & (1 << 6)) == 0) {
        vclk_debug(1, "  final div output DISABLED\n");
        clk->flags |= VCLK_OFF;
    }

    ratio = (data & 0x3F) + 1;
    clk->rate /= ratio;
    vclk_debug(1, "  final div ratio=%lu => rate=%lu\n", ratio, clk->rate);

    return 0;
}

/* ========================================================================
   Name:        flexgen_pll3200_set_rate
   Description: Set PLL3200 C28/C32 GFG clock
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

int flexgen_pll3200_set_rate(struct vclk *clk, unsigned long freq)
{
    int err = 0;
    int gfg_id;
    unsigned long parent_rate, data;
    struct vclk_pll pll;
    unsigned long flags2, odf = 0;
    unsigned long vcoby2_rate = 0;
    struct flexgen *fg;

    if (!clk) {
        vclk_error("ERROR: lost in flexgen_pll3200_set_rate()\n");
        return -1;
    }

    gfg_id = clk->idx0;
    parent_rate = vclk_get_prate(clk);
    flags2 = clk->flags2;
    fg = (struct flexgen *)clk->group;
    if (flags2 & FG_PLL3200C32) {
        pll.type = VCLK_PLL3200C32;
    } else {
        pll.type = VCLK_PLL3200C28;
        pll.idf = 0xff; // Let the algo choose
    }
    if (flags2 & FG_FRAC)
        pll.mode = 1; // 1=fractional
    else
        pll.mode = 0; // 0=integer

    /* Computing parameters.
       Either FVCO and/or ODF */

    if (flags2 & FG_PHI) {
        if (flags2 & FG_FVCOBY2) {
            /* Computing ODF: min FVCOBY2=800Mhz.
               Using ODF to go below. */
            if (freq < 800000000) {
                odf = 800000000 / freq;
                if (800000000 % freq)
                    odf += 1;
            } else
                odf = 1;
            vcoby2_rate = freq * odf;
        } else {
            /* Computing params of PHIx only*/
            odf = vclk_best_div(parent_rate, freq);
        }
    } else
        vcoby2_rate = freq;
    if (flags2 & FG_FVCOBY2) {
        /* Computing params for FVCOBY2 */
        err = vclk_pll_get_params(parent_rate, vcoby2_rate, &pll);
        if (err != 0) {
            vclk_error("ERROR: Can't compute 'PLL3200Cxx' params for clk %s\n", clk->name);
            return -1;
        }
    }

    /* Parameters computed.
       Let's program FVCOBY2 and/or PHIx */

    if (flags2 & FG_FVCOBY2) {
        int i;
        unsigned long long in;

        /* Before shutting down VCO, need to reroute all childs
           clocks (those from same GFG) to 'refclkin0' */
        for (i = 0, in = 0ll; fg->xbar_src[i] != -1; i++) {
            int id;

            id = fg->xbar_src[i];
            if (!(vclk_clocks[id].flags2 & FG_GFGOUT))
                continue; /* Not a GFG output */
            if (vclk_clocks[id].idx0 != gfg_id)
                continue; /* Not the correct GFG */
            in |= (1 << i);
        }
        flexgen_xbar_reroute(fg, in, 0);

        /* PLL power down */
        data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0));
        vclk_poke_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0), data | (1 << 8));

        /* Configure ndiv and idf */
        data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 1)) & 0xff00fff8;
        vclk_poke_v(fg->virt_addr + GFG_CONFIG(gfg_id, 1),
            (data | (pll.ndiv << 16) | pll.idf));

        /* Configure cp */
        data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0)) & ~(0xff << 16);
        vclk_poke_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0),
            (data | (pll.cp << 16)));

        /* Configure FRAC_INPUT, FRAC_CONTROL & DITHER_DISABLE */
        data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 2));
        if (flags2 & FG_FRAC)
            data |= (3 << 23) | (1 << 21) | pll.frac;
        else
            data &= ~((1 << 21) | 0xffff);
        vclk_poke_v(fg->virt_addr + GFG_CONFIG(gfg_id, 2), data);
    }

    if (flags2 & FG_PHI) {
        /* Configure ODF */
        data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 5)) & ~(0x3f);
        vclk_poke_v(fg->virt_addr + GFG_CONFIG(gfg_id, 5), data | odf);
    }

    if (flags2 & FG_FVCOBY2) {
        /* PLL power up */
        data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0));
        vclk_poke_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0), data & ~(1 << 8));

        /* Check PLL Lock */
        while ((data & (1 << 24)) != (1 << 24)) {
            data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0));
        }

        /* Rerouting child clocks to original parent */
        flexgen_xbar_reroute(fg, 0ll, 1);
    }

    /* Recalc done by upper layer */
    return 0;
}

/* ========================================================================
   Name:        flexgen_fs660_set_rate
   Description: Set FS660 C28/C32 GFG clock.
        freq = channel freq if FG_CHAN set, else FVCO freq
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

int flexgen_fs660_set_rate(struct vclk *clk, unsigned long freq)
{
    int err;
    struct vclk_fs fs_vco;
    struct vclk_fs fs = {
        .nsdiv = 0xff
    };
    unsigned long channel, parent_rate, data, flags2;
    int gfg_id;
    struct flexgen *fg;

    if (!clk) {
        vclk_error("ERROR: lost in flexgen_fs660_set_rate()\n");
        return -1;
    }

    fg = (struct flexgen *)clk->group;
    parent_rate = vclk_get_prate(clk);
    flags2 = clk->flags2;
    if (flags2 & FG_FS660C32) {
        fs_vco.type = VCLK_FS660C32VCO;
        fs.type = VCLK_FS660C32;
    } else {
        fs_vco.type = VCLK_FS660C28VCO;
        fs.type = VCLK_FS660C28;
    }

    if (flags2 & FG_FVCO)
        err = vclk_fs_get_params(parent_rate, freq, &fs_vco);
    else {
        if (flags2 & FG_NSDIV0)
            fs.nsdiv = 0;
        else if (flags2 & FG_NSDIV1)
            fs.nsdiv = 1;
        err = vclk_fs_get_params(parent_rate, freq, &fs);
    }
    if (err != 0) {
        vclk_error("ERROR: Can't compute FS660 params for "
               "'%s'=%lu Hz with prate=%lu\n", clk->name,
               freq, parent_rate);
        return err;
    }

    gfg_id = clk->idx0;
    channel = clk->idx1;

    if (flags2 & FG_FVCO) { /* FVCO prog required */
        int i;
        unsigned long long in;

        vclk_debug(1, "  Programming FVCO with ndiv=0x%lx\n", fs_vco.ndiv);

        /* Before shutting down FVCO, need to reroute all its childs
           clocks (those for same GFG) to 'refclkin0' */
        for (i = 0, in = 0ll; fg->xbar_src[i] != -1; i++) {
            int id;

            id = fg->xbar_src[i];
            if (!(vclk_clocks[id].flags2 & FG_GFGOUT))
                continue; /* Not a GFG output */
            if (vclk_clocks[id].idx0 != gfg_id)
                continue; /* Not the correct GFG */
            in |= (1 << i);
        }
        flexgen_xbar_reroute(fg, in, 0);

        /* Digital reset (NRST) */
        data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0));
        data |= 0xf;
        vclk_poke_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0), data);

        /* PLL power down (~NPDPLL=1) */
        data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0));
        vclk_poke_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0), data | (1 << 12));

        /* 'ndiv' programmation */
        if (flags2 & FG_FS660C32)
            data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 1)) & ~(0x7 << 16);
        else // FG_FS660C28
            data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 1)) & ~(0xf << 16);
        vclk_poke_v(fg->virt_addr + GFG_CONFIG(gfg_id, 1),
              data | (fs_vco.ndiv << 16));

        /* PLL power up (~NPDPLL=0)*/
        data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0));
        vclk_poke_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0), data & ~(1 << 12));

        /* Check PLL Lock */
        while ((data  & (1 << 24)) != (1 << 24)) {
            data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0));
        }

        /* Release digital resets */
        data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0));
        vclk_poke_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0), (data & ~0xf));

        /* Rerouting child clocks to original parent */
        flexgen_xbar_reroute(fg, 0ll, 1);
    }

    if (flags2 & FG_CHAN) { /* Channel prog required */
        vclk_debug(1, "  Programming chan %lu with md/pe/sd/nsd="
              "%lu/%lu/%lu/%lu\n",
              channel, fs.mdiv, fs.pe, fs.sdiv, fs.nsdiv);

        /* Release standby (/NSBi): note that this means forcing
           clock enable but mandatory to allow EN_PROGx */
        data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0));
        vclk_poke_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0), (data & ~(0x100 << channel)));

        /* FS programming */
        data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, (channel + 5)));
        data &= ~(0x1 << 24 | 0xf << 20 | 0x1f << 15 | 0x7fff);
        data |= (fs.nsdiv << 24 | fs.sdiv << 20 | fs.mdiv << 15 | fs.pe);
        vclk_poke_v(fg->virt_addr + GFG_CONFIG(gfg_id, (channel + 5)), data);

        /* Enable channel EN_PROGx */
        data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 3));
        vclk_poke_v(fg->virt_addr + GFG_CONFIG(gfg_id, 3), (data | (1 << channel)));

        /* Disable channel EN_PROGx to stop further changes */
        data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 3));
        vclk_poke_v(fg->virt_addr + GFG_CONFIG(gfg_id, 3), (data & ~(1 << channel)));
    }

    /* No more doubt on reg returned freq since sequence applied */
    clk->flags &= ~VCLK_RATEDOUBT;

    /* Recalc done by upper layer */
    return 0;
}

/* ========================================================================
   Name:        flexgen_div_set
   Description: Set pre and/or final dividers ratios
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int flexgen_div_set(struct vclk *clk, unsigned long *div)
{
    unsigned long pdiv = 0, fdiv = 0; /* Pre & final divs */
    int sync = 0, bc_nb = -1;
    struct flexgen *fg;
    int i;
    unsigned long data;

    if (!clk || !(clk->flags2 & FG_DIV)) {
        vclk_error("ERROR: lost in flexgen_div_set()\n");
        return -1;
    }

    fg = (struct flexgen*)clk->group;

    /* Async or sync ? */
    sync = (clk->flags2 & FG_SYNC) ? 1 : 0;
     vclk_debug(1, "flexgen_div_set(%s, div_idx=%d, div=%lu) => sync=%d\n",
          clk->name, clk->idx0, *div, sync);

    /* Computing predivider & final divider ratios */
    if (*div <= 64) {
        pdiv = 0; /* bypass prediv */
        fdiv = *div - 1;
    } else if (*div <= (1024 * 64)) {
        unsigned long dev = 0xffffffff, new_dev; /* deviation */
        unsigned long f, p, new_div;

        for (f = 64; f; f--) {
            p = *div / f;
            if (!p || (p > 1024))
                continue;
            new_div = f * p;
            if (new_div > *div)
                new_dev = new_div - *div;
            else
                new_dev = *div - new_div;
            if (new_dev < dev) {
                /* Best case */
                fdiv = f - 1;
                pdiv = p - 1;
                dev = new_dev;
            }
        }
        if (dev == 0xffffffff) /* No solution found */
            return -1;
    } else
        return -1;

    /* If sync mode, may need to reprogram beat counter */
    if (sync) {
        /* Which beat counter number for this clock ? */
        for (i = 0, bc_nb = -1; fg->xbar_src[i] != -1; i++) {
            /* A beatcounter on this source ? */
            if (vclk_clocks[fg->xbar_src[i]].flags2 & FG_BEAT)
                bc_nb++;
            if (fg->xbar_src[i] == clk->parent->id)
                break; /* Source found */
        }
        if (fg->xbar_src[i] == -1 ||
            !(vclk_clocks[fg->xbar_src[i]].flags2 & FG_BEAT)) {
            vclk_debug(1, "  '%s' has NO source with beatcounter => ASYNC\n", clk->name);
            sync = 0;
        } else
            vclk_debug(1, "  '%s' is assigned to beatcounter %d\n", clk->name, bc_nb);
    }

    vclk_debug(1, "  requested div=%lu => prediv=%lu, findiv=%lu, sync=%d\n",
          *div, pdiv, fdiv, sync);

    /* Pre divider config */
    vclk_poke_v(fg->virt_addr + FCG_CONFIG(22) + (clk->idx0 * 4), pdiv);
    /* Final divider config */
    data = vclk_peek_v(fg->virt_addr + FCG_CONFIG(89) + (clk->idx0 * 4));
    data &= ~((1 << 7) | 0x3f);
    vclk_poke_v(fg->virt_addr + FCG_CONFIG(89) + (clk->idx0 * 4), data | (!sync << 7) | fdiv);

    if (sync) {
        unsigned long beat, div, max = 0;
        void *addr;
        int bc_changed = 0, j;

        /* Computing new beat counter value taking highest final ratio
           applied on all sync clocks.
           WARNING: This makes sense only for 1, 2, 4, 8, 16.. div series */
        for (i = 0; i < (fg->divlast - fg->div0 + 1); i++) {
            if (!vclk_clocks[fg->div0 + i].name)
                continue; // Invalid clock entry

            if (vclk_clocks[fg->div0 + i].parent != clk->parent)
                continue; // Not the same source

            // sync = (i < 32) ? (fg->syncl >> i) & 1 :
                // (fg->synch >> (i - 32)) & 1;
            if (!(vclk_clocks[fg->div0 + i].flags2 & FG_SYNC))
                continue; // Same source but not semi-sync

            /* Taking max final div ratio */
            div = vclk_peek_v(fg->virt_addr + FCG_CONFIG(89) + (i * 4)) & 0x3f;
            if (div > max)
                max = div;
        }

        /* Current beat counter value ? */
        beat = vclk_peek_v(fg->virt_addr + FCG_CONFIG(155 + (bc_nb / 4)));
        beat = (beat >> (8 * (bc_nb % 4))) & 0xff;
        vclk_debug(1, "  Cur. beat count=%lu, max ratio=%lu\n", beat, max);

        /* If beat counter != max ratio, need to reprogram it */
        if (beat != max) {
            beat = vclk_peek_v(fg->virt_addr + FCG_CONFIG(155 + (bc_nb / 4)));
            beat &= ~(0xff << (8 * (bc_nb % 4)));
            beat |= (max << (8 * (bc_nb % 4)));
            vclk_poke_v(fg->virt_addr + FCG_CONFIG(155 + (bc_nb / 4)), beat);
            bc_changed = 1;
        } else
            bc_changed = 0;

        /* To force resync, restarting
           - all SYNC clocks with same source if beat counter has changed
           - or only current clock */
        if (bc_changed) {
            i = 0;
            j = fg->divlast - fg->div0 + 1;
        } else {
            i = clk->idx0;
            j = i + 1;
        }
        for (; i < j; i++) {
            if (!vclk_clocks[fg->div0 + i].name)
                continue; // Invalid clock entry

            if (vclk_clocks[fg->div0 + i].parent != clk->parent)
                continue; // Not the same source

            // sync = (i < 32) ? (fg->syncl >> i) & 1 :
                // (fg->synch >> (i - 32)) & 1;
            // if (!sync)
            if (!(vclk_clocks[fg->div0 + i].flags2 & FG_SYNC))
                continue; // Same source but not semi-sync

            /* If clock not active, skip */
            addr = fg->virt_addr + FCG_CONFIG(89) + (i * 4);
            data = vclk_peek_v(addr);
            if ((data & (1 << 6)) == 0)
                continue; // OFF

            /* Disable, then reenable */
            vclk_debug(1, "  Restarting '%s' (idx %d)\n", vclk_clocks[fg->div0 + i].name, i);
            vclk_poke_v(addr, data & ~(1 << 6));
            vclk_poke_v(addr, data | (1 << 6));
        }
    }

    /* Recalc done by upper layer */
    return 0;
}

/* ========================================================================
   Name:        flexgen_pll3200_xable
   Description: Enable/disable clocks from PLL3200 C28/C32
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int flexgen_pll3200_xable(struct vclk *clk, int enable)
{
    int gfg_id;
    unsigned long data, flags2;
    struct flexgen *fg;

    if (!clk) {
        vclk_error("ERROR: lost in flexgen_pll3200_xable()\n");
        return -1;
    }

    vclk_debug(1, "flexgen_pll3200_xable(%s, %d)\n", clk->name, enable);

    gfg_id = clk->idx0;
    flags2 = clk->flags2;
    fg = (struct flexgen *)clk->group;

    if (enable) {
        if (flags2 & FG_FVCOBY2) {
            /* PLL power up */
            data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0));
            vclk_poke_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0), data & ~(1 << 8));

            /* Wait for lock */
            while ((data  & (1 << 24)) != (1 << 24)) {
                data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0));
            }
        }

        if (flags2 & FG_PHI) {
            /* ODF enable */
            data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 5));
            vclk_poke_v(fg->virt_addr + GFG_CONFIG(gfg_id, 5), data & ~(1 << 6));
        }
    } else {
        if (flags2 & FG_PHI) {
            /* ODF disable */
            data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 5));
            vclk_poke_v(fg->virt_addr + GFG_CONFIG(gfg_id, 5), data | (1 << 6));
        }

        if (flags2 & FG_FVCOBY2) {
            /* PLL power down */
            data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0));
            vclk_poke_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0), data | (1 << 8));
        }
    }

    /* Recalc is now done by upper layer */
    return 0;
}

/* ========================================================================
   Name:        flexgen_fs660_xable
   Description: Enable/disable clocks from FS660 C28/C32
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int flexgen_fs660_xable(struct vclk *clk, int enable)
{
    int gfg_id, channel;
    unsigned long data;
    void *addr;
    struct flexgen *fg;

    if (!clk) {
        vclk_error("ERROR: lost in flexgen_fs660_xable()\n");
        return -1;
    }

     vclk_debug(1, "flexgen_fs660_xable(%s, %d)\n", clk->name, enable);

    fg = (struct flexgen *)clk->group;
    gfg_id = clk->idx0;
    channel = clk->idx1;
    addr = fg->virt_addr + GFG_CONFIG(gfg_id, 0); // GFGx_CONFIG0

    if (enable) {
        if (clk->flags2 & FG_FVCO) {
            data = vclk_peek_v(addr);

            /* PLL power up */
            data &= ~(1 << 12);
            vclk_poke_v(addr, data);

            /* Wait for lock */
            while ((data  & (1 << 24)) != (1 << 24)) {
                data = vclk_peek_v(addr);
            }

            /* Deactivating all NRSTi */
            data &= ~(0xf);
            vclk_poke_v(addr, data);
        } else if (channel != -1) {
            /* If rate not valid (no setrate() prior to enable())
               let's perform PROG_EN seq */
            if (clk->flags & VCLK_RATEDOUBT) {
                int i;

                /* Release standby (/NSBi): note that this means forcing
                   clock enable but mandatory to allow EN_PROGx */
                data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0));
                vclk_poke_v(fg->virt_addr + GFG_CONFIG(gfg_id, 0), (data & ~(0x100 << channel)));

                /* Enable channel EN_PROGx */
                data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 3));
                vclk_poke_v(fg->virt_addr + GFG_CONFIG(gfg_id, 3), (data | (1 << channel)));

                for (i = 0; i < 6000; i++);

                /* Disable channel EN_PROGx to stop further changes */
                data = vclk_peek_v(fg->virt_addr + GFG_CONFIG(gfg_id, 3));
                vclk_poke_v(fg->virt_addr + GFG_CONFIG(gfg_id, 3), (data & ~(1 << channel)));
            }

            /* If channel, deactivating NSBi */
            data = vclk_peek_v(addr);
            vclk_poke_v(addr, data & ~(1 << (8 + channel)));
        }

        /* No more doubt on reg returned freq since sequence applied */
        clk->flags &= ~VCLK_RATEDOUBT;
    } else {
        data = vclk_peek_v(addr);

        /* If channel, activating NSBi */
        if (channel != -1) {
            data |= (1 << (8 + channel));
            vclk_poke_v(addr, data);
        }

        /* PLL power down if no channel=VCO, or all channels OFFs */
        if (channel == -1 || ((data & (0xf << 8)) == (0xf << 8))) {
            vclk_poke_v(addr, data | (1 << 12));
        }
    }

    /* Recalc is now done by upper layer */
    return 0;
}

/* ========================================================================
   Name:        flexgen_div_xable
   Description: Enable/disable clocks at final divider stage
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

static int flexgen_div_xable(struct vclk *clk, int enable)
{
    unsigned long regoffs, data;
    int shift;
    void *addr;
    struct flexgen *fg;

    if (!clk || !(clk->flags2 & FG_DIV)) {
        vclk_error("ERROR: lost in flexgen_div_xable()\n");
        return -1;
    }

    vclk_debug(1, "flexgen_div_xable(%s, en=%d)\n", clk->name, enable);

    flexgen_get_fcg(clk->idx0, &regoffs, &shift);
    fg = (struct flexgen *)clk->group;

    if (enable) {
        /* Just in case because not supposed to be OFF,
           enabling clock at cross bar stage. */
        addr = fg->virt_addr + FCG_CONFIG(6) + regoffs;
        data = vclk_peek_v(addr);
        vclk_poke_v(addr, data | (1 << (shift + 6)));

        /* Enabling clock at final divider stage */
        addr = fg->virt_addr + FCG_CONFIG(89) + (clk->idx0 * 4);
        data = vclk_peek_v(addr);
        vclk_poke_v(addr, data | (1 << 6));
    } else {
        /* Disabling clock at final divider stage */
        addr = fg->virt_addr + FCG_CONFIG(89) + (clk->idx0 * 4);
        data = vclk_peek_v(addr);
        vclk_poke_v(addr, data & ~(1 << 6));
    }

    /* Recalc is now done by upper layer */
    return 0;
}

/*
 * Exported functions
 */

/* ========================================================================
   Name:        flexgen_init
   Description: Flexgen lib init function
   Returns:     0=OK
   ======================================================================== */

int flexgen_init(struct vclk *soc_clocks, int nbclocks)
{
    struct flexgen *fg = NULL;
    int i, j;
    struct vclk *clk;

    if (!soc_clocks) {
        vclk_error("ERROR: lost in flexgen_init()\n");
        return -1;
    }

    vclk_debug(1, "Initializing flexiclockgen library\n");

    /* Going thru all clocks */
    for (clk = &soc_clocks[0], i = 0; i < nbclocks; i++, clk = &soc_clocks[i]) {
        if (!clk->name)
            continue; // Not a valid entry

        if (!(clk->flags & VCLK_FG))
            continue; // Not a flexiclockgen clock

        if (fg != (struct flexgen *)clk->group) {
            fg = (struct flexgen *)clk->group;
            vclk_debug(2, "  New flexiclockgen '%s' @%p\n", fg->name, fg->phys_addr);

            /* Registering flexiclockgen virtual base addresses */
            fg->virt_addr = vclk_phys2virt(fg->phys_addr, 0x1000);
            vclk_debug(2, "    virt_addr=%p\n", fg->virt_addr);

            /* Computing number of GFGs. */
            fg->nb_gfg = 0;
            for (j = i; (j < nbclocks) && (soc_clocks[j].group == (void*)fg); j++) {
                if (!(soc_clocks[j].flags2 & FG_GFGOUT))
                    continue; /* Not GFG output */
                if ((soc_clocks[j].idx0 + 1) > fg->nb_gfg)
                    fg->nb_gfg = soc_clocks[j].idx0 + 1;
            }

            /* Computing number of xbar sources */
            for (fg->xbar_in_size = 0; fg->xbar_src[fg->xbar_in_size] != -1;
                fg->xbar_in_size++);
            vclk_debug(2, "    xbar_in_size=%d\n", fg->xbar_in_size);

            /* First & last final div indexes to be computed */
            fg->div0 = -1;
            fg->divlast = -1;
        }

        /* Updating div0 & divlast fields */
        if (clk->flags2 & FG_DIV) {
            if (fg->div0 == -1)
                fg->div0 = clk->id; // Assuming IDs always in ascending order
            if ((fg->divlast == -1) || (clk->idx0 > (fg->divlast - fg->div0)))
                fg->divlast = clk->id;
        }

        /* Rate doubt is set only for FS where EN_PROG stuff may mean
           that current reg settings do not reflect reality */
        if ((clk->flags2 & FG_FS660C32) && (clk->flags2 & FG_CHAN))
            clk->flags |= VCLK_RATEDOUBT;
    }

    return 0;
}

/* ========================================================================
   Name:        flexgen_identify_parent
   Description: Read HW to identify source clock (GFG out, refclkin,
        srcclkin, ..)
   Returns:     0=OK
   ======================================================================== */

int flexgen_identify_parent(struct vclk *clk)
{
    struct flexgen *fg;

    vclk_debug(1, "flexgen_identify_parent(%s)\n", clk->name);
    // vclk_debug(1, "  phys=0x%p virt_addr=0x%p\n", fg->phys_addr, fg->virt_addr);
    // vclk_debug(1, "  idx0=%d\n", clk->idx0);

    fg = (struct flexgen *)clk->group;

    /* Final div output => possible sources = xbar inputs */
    if (clk->flags2 & FG_DIV) {
        unsigned long regoffs;
        int shift;
        int enable;
        uint32_t val, src_sel;

        /* Getting FCG_CONFIG proper offset & shift */
        flexgen_get_fcg(clk->idx0, &regoffs, &shift);

        /* Identifying crossbar source */
        val = vclk_peek_v(fg->virt_addr + FCG_CONFIG(6) + regoffs) >> shift;
        src_sel = val & 0x3f;
        enable = (val >> 6) & 1;
        if (!enable)
            vclk_debug(1, "  WARNING: Xbar DISABLED\n");
        if (src_sel >= (uint32_t)fg->xbar_in_size) {
            /* Source currently on unsupported clock input */
            vclk_error("ERROR: flexgen_identify_parent(). No source @ xbar in %d (last=%d)\n", src_sel, fg->xbar_in_size - 1);
            return -1;
        }
        clk->parent = &vclk_clocks[fg->xbar_src[src_sel]];
        vclk_debug(1, "  Final div output %d => xbar in %d => %s\n", clk->idx0, src_sel, clk->parent->name);
    }

    /* Is it a 'refclkout' or 'ext_refclkout' clock ? */
    else if ((clk->flags2 & FG_REFOUT) || (clk->flags2 & FG_EREFOUT)) {
        int muxdiv_idx, refclkin_idx, i;

        /* Reading 'muxdiv' mux select. Order is
           MuxDiv0   => refclkout0 = GFG0 input
           MuxDivX   => refclkoutX = GFGX input
           MuxDivX+1 => ext_refclkout0
           MuxDivX+2 => ext_refclkout1
           MuxDivX+3 => srcclkin0
           MuxDivX+4 => srcclkin1
           ... etc
         */
        if (clk->flags2 & FG_REFOUT) /* GFG ref inputs */
            muxdiv_idx = clk->idx0;
        else if (clk->flags2 & FG_EREFOUT)
            muxdiv_idx = fg->nb_gfg + clk->idx0;
        else
            muxdiv_idx = fg->nb_gfg + fg->nb_refin + clk->idx0;

        /* Mux capability for this clock ? */
        if (fg->refmuxmap & (1 << muxdiv_idx)) {
            /* Yes. Identifying parent */
            refclkin_idx = vclk_peek_v(fg->virt_addr + FCG_CONFIG(0 + (muxdiv_idx / 8)));
            /* 4 bits per muxdiv, so 8 muxdiv per register */
            refclkin_idx = (refclkin_idx >> ((muxdiv_idx % 8) * 4)) & 0xf;
        } else
            refclkin_idx = clk->idx0; /* No mux = direct connection. refclkout[x]=refclkin[x] */

        if (refclkin_idx >= fg->nb_refin) {
            vclk_error("ERROR: %d refclkin ONLY while refout source is %d\n", fg->nb_refin, refclkin_idx);
            return -1;
        }

        /* Looking for 'refclkinX' source */
        for (i = 0; i < vclk_nb; i++) {
            if (vclk_clocks[i].group != (void*)fg)
                continue; /* Not the same flexiclockgen */
            if (!(vclk_clocks[i].flags2 & FG_REFIN))
                continue; /* Not a 'refclkin' */
            if (vclk_clocks[i].idx0 == refclkin_idx)
                break; /* Correct 'refclkin' found */
        }
        if (i == vclk_nb) {
            vclk_error("ERROR: flexgen_identify_parent(%s), no refclkin[%d]\n", clk->name, refclkin_idx);
            return -1;
        }
        clk->parent = &vclk_clocks[i];
        vclk_debug(1, "  (ext)refout[%d] sourced from refclkin[%d]=%s\n", muxdiv_idx, refclkin_idx, clk->parent->name);
    }

    /* Don't know what to do with this clock, so assuming static parent */
    else {
        // vclk_error("ERROR: lost in flexgen_identify_parent(%s)\n", clk->name);
        // return -1;
    }

    return 0;
}

/* ========================================================================
   Name:        flexgen_set_parent
   Description: Set cross bar source clock
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

int flexgen_set_parent(struct vclk *clk, struct vclk *src)
{
    int xbar_in;
    struct flexgen *fg;

    if (!clk || !src) {
        vclk_error("ERROR: lost in flexgen_set_parent()\n");
        return -1;
    }

    vclk_debug(1, "flexgen_set_parent(%s, %s)\n", clk->name, src->name);

    fg = (struct flexgen *)clk->group;

    /* Is it div output ? */
    if (clk->flags2 & FG_DIV) {
        /* Which xbar input is the requested source ? */
        for (xbar_in = 0; (fg->xbar_src[xbar_in] != -1) &&
             (src->id != fg->xbar_src[xbar_in]); xbar_in++);
        if (fg->xbar_src[xbar_in] == -1) {
            /* Invalid source */
            vclk_error("ERROR: Invalid source in flexgen_set_parent()\n");
            return -1;
        }

        /* Configuring xbar */
        flexgen_xbar_set(clk->idx0, fg->virt_addr, xbar_in);

        clk->parent = &vclk_clocks[src->id];
        return 0;
    }

    /* refclkout or ext_refclkout ? */
    if (clk->flags2 & FG_REFOUT || clk->flags2 & FG_EREFOUT) {
        /* TO BE COMPLETED !!! */
        vclk_error("ERROR: REFOUT/EREFOUT not supported yet in flexgen_set_parent()\n");
    }

    /* Other clocks do not have any source mux capability */
    return -1;
}

/* ========================================================================
   Name:        flexgen_recalc
   Description: Read flexiclockgen HW infos to fill in 'clk->rate'
   Returns:     0=OK, or -1
   ======================================================================== */

int flexgen_recalc(struct vclk *clk)
{
    /* In several cases, a clock source is mandatory. */
    if ((clk->flags2 & FG_DIV) || (clk->flags2 & FG_GFGOUT) ||
        (clk->flags2 & FG_REFOUT) || (clk->flags2 & FG_EREFOUT)) {
        if (!clk->parent) {
            vclk_error("ERROR: flexgen_recalc(%s). No source\n",
                   clk->name);
            return -1;
        }
    }

    /* Is it a final divider output clock ? */
    if (clk->flags2 & FG_DIV)
        return flexgen_div_recalc(clk);

    /* Is it a GFG output clock ? */
    if (clk->flags2 & FG_GFGOUT) {
        /* PLL3200 C28/C32 clock ? */
        if ((clk->flags2 & FG_PLL3200C28) || (clk->flags2 & FG_PLL3200C32))
            return flexgen_pll3200_recalc(clk);
        /* FS660 C28/C32 clock ? */
        if ((clk->flags2 & FG_FS660C28) || (clk->flags2 & FG_FS660C32))
            return flexgen_fs660_recalc(clk);

        /* Unsupported case */
        vclk_error("ERROR: '%s', unsupported flexiclockgen GFG "
               "type for recalc\n", clk->name);
        return -1;
    }

    /* Is it a 'refclkout' or 'ext_refclkout' clock ? */
    if ((clk->flags2 & FG_REFOUT) || (clk->flags2 & FG_EREFOUT))
        return flexgen_refout_recalc(clk);

    /* For other clocks, taking parent rate if exists or setting NORATE */
    if (clk->parent)
        clk->rate = vclk_get_prate(clk);
    else
        clk->flags |= VCLK_NORATE;
    clk->flags &= ~VCLK_OFF;

    return 0;
}

/* ========================================================================
   Name:        flexgen_set_ratep
   Description: Set Flexiclockgen clock.
                Note: 'struct vclk' is updated by upper layer
   Returns:     0=OK
   ======================================================================== */

int flexgen_set_ratep(struct vclk *clk, unsigned long freq, unsigned long prate)
{
    if (!clk || !freq) {
        vclk_error("ERROR: lost in flexgen_set_rate()\n");
        return -1;
    }

    vclk_debug(2, "flexgen_set_rate(%s, %lu, prate=%lu)\n", clk->name, freq, prate);

    /* Is it a divider output clock ? */
    if (clk->flags2 & FG_DIV) {
        unsigned long div;

        div = vclk_best_div(prate, freq);
        return flexgen_div_set(clk, &div);
    }

    /* Is it a GFG output clock ? */
    if (clk->flags2 & FG_GFGOUT) {
        /* PLL3200 C28/C32 clock ? */
        if ((clk->flags2 & FG_PLL3200C28) || (clk->flags2 & FG_PLL3200C32))
            return flexgen_pll3200_set_rate(clk, freq);
        /* FS660 C28/C32 clock ? */
        if ((clk->flags2 & FG_FS660C28) || (clk->flags2 & FG_FS660C32))
            return flexgen_fs660_set_rate(clk, freq);

        /* Unsupported case */
        vclk_error("ERROR: '%s', unsupported flexiclockgen GFG "
               "type for set_rate\n", clk->name);
        return -1;
    }

    /* Other clock types are considered not having any
       rate programmation feature */

    return 0;
}

/* ========================================================================
   Name:        flexgen_xable
   Description: Enable/disable clock at HW level.
                Note: 'struct vclk' update is done by VCLK top level layer
   Returns:     0=OK
   ======================================================================== */

int flexgen_xable(struct vclk *clk, int enable)
{
    /* Is it a divider output clock ? */
    if (clk->flags2 & FG_DIV)
        return flexgen_div_xable(clk, enable);

    /* Is it a GFG output clock ? */
    if (clk->flags2 & FG_GFGOUT) {
        /* PLL3200 C28/C32 clock ? */
        if ((clk->flags2 & FG_PLL3200C28) || (clk->flags2 & FG_PLL3200C32))
            return flexgen_pll3200_xable(clk, enable);
        /* FS660 C28/C32 clock ? */
        if ((clk->flags2 & FG_FS660C28) || (clk->flags2 & FG_FS660C32))
            return flexgen_fs660_xable(clk, enable);

        /* Unsupported case */
        vclk_error("ERROR: '%s', unsupported flexiclockgen GFG "
               "type for xable\n", clk->name);
        return -1;
    }

    /* Other clock types have no xable feature */

    return 0;
}

/* ========================================================================
   Name:        flexgen_shutdown
   Description: Disable each final div output whose 'disable' bit is set
   Params:      vbase=Flexgen VIRTUAL base addr
                disable=ORed combination of outputs to switch OFF
   Returns:     0=OK
   ======================================================================== */

void flexgen_shutdown(void *vbase, uint64_t disable)
{
    int i;
    uint32_t data;
    void *vaddr;

    for (i = 0; i < 64; i++) {
        if (((disable >> i) & 1 ) == 0)
            continue;
        vaddr = vbase + FCG_CONFIG(89) + (i * 4);
        data = vclk_peek_v(vaddr);
        vclk_poke_v(vaddr, data & ~(1 << 6));
    }
}

/* ========================================================================
   Name:        flexgen_observe
   Description: Configure JESS to observe flexiclockgen clocks
   Returns:     0=OK
   ======================================================================== */

int flexgen_observe(struct vclk *clk, unsigned long *div)
{
    unsigned long cfg160, flags2;
    unsigned char div_sel;
    struct flexgen *fg;

    vclk_debug(2, "flexgen_observe(%s)\n", clk->name);
    fg = (struct flexgen *)clk->group;

    flags2 = clk->flags2;
    if (flags2 & FG_DIV)             /* Final div output */
        cfg160 = (0x3 << 6) | clk->idx0;
    else if (flags2 & FG_REFIN)      /* refclkin ? */
        cfg160 = (0x0 << 6) | clk->idx0;
    else if (flags2 & FG_REFOUT)     /* refclkout ? */
        cfg160 = (0x0 << 6) | (fg->nb_refin + clk->idx0);
    else if (flags2 & FG_EREFOUT)    /* ext_refclkout ? */
        cfg160 = (0x0 << 6) | (fg->nb_refin + fg->nb_gfg + clk->idx0);
    else if (flags2 & FG_GFGOUT) {   /* GFG output clock ? */
        /* Info
           Observation index must be computed by removing all
           NON GFG output clocks from the Xbar input list */
        int i, j, id;
        for (i = 0, j = -1; fg->xbar_src[i] != -1; i++) {
            id = fg->xbar_src[i];
            if (!vclk_clocks[id].name) {
                vclk_error("ERROR: Clk id %u is an INVALID entry\n", id);
                continue;
            }
            if (!(vclk_clocks[id].flags2 & FG_GFGOUT))
                continue; // Not a GFG output

            if (j == -1)
                j = 0; // Let's start counting obs index
            else
                j++;

            if (clk->id == id)
                break;
        }
        if (fg->xbar_src[i] == -1)
            return -1;
        vclk_debug(1, "  GFG output to observe = Xbar in %d (obs idx %d)\n", i, j);
        cfg160 = (0x1 << 6) | j;
    } else if (flags2 & FG_SRCIN)    /* srcclkin ? */
        cfg160 = (0x2 << 6) | clk->idx0;
    else if (flags2 & FG_JEIN)       /* je_in ? */
        cfg160 = (1 << 16) | (clk->idx0 << 8);
    else
        return -1;

    /* Div selection: FCG_CONFIG160[24:22]
       Select the closest ratio equal or below the one requested.
    */
    for (div_sel = 0; div_sel < 7; div_sel++)
        if (*div <= (1 << div_sel)) break;
    *div = 1 << div_sel;

    /* Obs0 mux clock select & output enable */
    cfg160 |= (0x2 << 27) | (0x3 << 25) | (div_sel << 22);
    vclk_poke_v(fg->virt_addr + FCG_CONFIG(160), cfg160);

    return 0;
}

/* ========================================================================
   Name:        flexgen_get_measure
   Description: Perform HW measure and update '*rate'
     clk   = Clock to measure (currently final div ONLY)
     ref   = GFG ref clock to use (ex: CLK_B0_REFOUT0)
     *rate = returned measured rate
   Returns:     0=OK, -1=ERROR/UNSUPPORTED
   ======================================================================== */

int flexgen_get_measure(struct vclk *clk, struct vclk *ref, unsigned long *rate)
{
    struct flexgen *fg;
    uint32_t val;
    int clk_idx;
    uint32_t freq_cnt;

    vclk_debug(1, "flexgen_measure(%s, ref=%s)\n", clk->name, ref->name);

    if (!(clk->flags2 & FG_DIV))
        return -1;
    if (clk->flags & VCLK_OFF)
        return -1; // OFF

    fg = (struct flexgen *)clk->group;
    clk_idx = clk->id - fg->div0;
    vclk_debug(1, "  clk_idx=%d\n", clk_idx);

            // FCG_CONFIG161 (0x284) JE/OBs mux 1 and dynout mux setting bits
            // [27] JE_enable: Defines JE enable control bit <= 0x1
            // uvh_write_register_field_32(CLOCKGEN_BASE_ADDR + 0x284, 27, 0x08000000, 0x1);
    val = vclk_peek_v(fg->virt_addr + 0x284);
    val |= (1 << 27);
    vclk_poke_v(fg->virt_addr + 0x284, val);

            //inff_pll muxing
            /* FCG_CONFIG168 (0x288) JE PLL phase and lock analysis control bits
             * [3:0] JEinffsel: Defines JE inff clock mux selection bits
            */
            // uvh_write_register_field_32(CLOCKGEN_BASE_ADDR + 0x288, 0, 0x0000000F, gfg_index);
    val = vclk_peek_v(fg->virt_addr + 0x288);
    val &= ~(0xf); // Clear [3:0]
    val |= 0; // Select GFG0 ref clock (ext_refclk0)
    vclk_poke_v(fg->virt_addr + 0x288, val);

            // je_ref muxing
            /* FCG_CONFIG160 (0x280) JE/OBs mux 0 and dynout mux setting bits
             * [15] JEintext0sel: Defines JE mux 0 internal or external clock selection bit <= 0x0 (Internal clocks)
             * [17] JEmux0invert: Defines JE mux 0 output divider clock inversion bit <= 0x0 (No inversion)
             * [21:19] JEmux0div: Defines JE mux 0 output divider clock division bits <= 0x0 (Division by 1)
             * [25] JEmux0enable: Defines JE mux 0 output enable bit <= 0x1 (Enable)
            */
            // uvh_write_register_field_32(CLOCKGEN_BASE_ADDR + 0x280, 15, 0x023C8000, 0x0);
            // uvh_write_register_field_32(CLOCKGEN_BASE_ADDR + 0x280, 25, 0x02000000, 0x1);
    val = vclk_peek_v(fg->virt_addr + 0x280);
    val &= ~(1 << 15); // Clear [15]
    val |= (1 << 25);
    vclk_poke_v(fg->virt_addr + 0x280, val);

                //gfg_clks[gfgClockIndex] selection
            //[7:0] JEOBSint0sel: Defines JE/OBs mux 0 internal clock selection bits <= out_clks[ClockIndex]
            // uvh_write_register_field_32(CLOCKGEN_BASE_ADDR + 0x280, 6, 0x000000C0, 0x3);
            // uvh_write_register_field_32(CLOCKGEN_BASE_ADDR + 0x280, 0, 0x0000003F, ClockIndex);
    val &= ~(0xff); // Clear [7:0]
    val |= (3 << 6) | clk_idx; // Select fin div
    vclk_poke_v(fg->virt_addr + 0x280, val);

            // FCG_CONFIG165 (0x294) JE BIST control register 3 bits
            // [21:17] JE_select_out: Defines JE select out bits <= 0x0C (frequency value)
            // [14:11] JE_bec_code_stop: Defines JE bec code stop bits <= 0x9 (1023)
            // [7:5] JE_visu_mode: Defines JE visu mode bits <= 0x1 (BEND is reported on o_JE_dyn)
            // [4:3] JE_stop_pulse_mode: Defines JE stop pulse mode bits <= 0x3 (application mode)
            // [0] JE_edge_mode: Defines JE edge mode bits <= 0x1
            // uvh_write_register_field_32(CLOCKGEN_BASE_ADDR + 0x294, 17, 0x003E0000, 0x0C);
            // uvh_write_register_field_32(CLOCKGEN_BASE_ADDR + 0x294, 11, 0x00007800, 0x9);
            // uvh_write_register_field_32(CLOCKGEN_BASE_ADDR + 0x294, 5, 0x000000E0, 0x1);
            // uvh_write_register_field_32(CLOCKGEN_BASE_ADDR + 0x294, 3, 0x00000018, 0x3);
            // uvh_write_register_field_32(CLOCKGEN_BASE_ADDR + 0x294, 0, 0x00000001, 0x1);
    val = vclk_peek_v(fg->virt_addr + 0x294);
    val &= ~(0x1f << 17); // Clear [21:17]
    val |= (0xC << 17);
    val &= ~(0xf << 11); // Clear [14:11]
    val |= (0x9 << 11);
    val &= ~(0x7 << 5); // Clear [7:5]
    val |= (0x1 << 5);
    val &= ~(0x3 << 3); // Clear [4:3]
    val |= (0x3 << 3);
    val &= ~(0x3 << 0); // Clear [1:0]
    val |= (0x1 << 0);
    vclk_poke_v(fg->virt_addr + 0x294, val);

            // FCG_CONFIG163 (0x28C) JE BIST control register 1 bits
            // [0] JE_pll_bist_run: Defines JE pll bist run bit <= 0x1
            // uvh_write_register_field_32(CLOCKGEN_BASE_ADDR + 0x28C, 0, 0x00000001, 0x1);
    val = vclk_peek_v(fg->virt_addr + 0x28C);
    val |= (1 << 0);
    vclk_poke_v(fg->virt_addr + 0x28C, val);

    do {
        val = vclk_peek_v(fg->virt_addr + 0x298);
    } while (((val >> 17) & 0x7) != 0x4);

            // Check je_status[2] = BSTART & BEND when JE_pll_bist_run_start=1
            // if(uvh_wait_until_reg((uintptr_t)CLOCKGEN_BASE_ADDR + 0x298,0x00080000,0x00080000,100)!=UVH_SUCCESS_CODE)
            // {
                // uvh_error("JESS","BIST not ended in 100�s");
            // }
            // else
            // {
                // uvh_info("JESS",0,"BIST ended");
                // FCG_CONFIG163 (0x28C) JE BIST control register 1 bits
                // [0] JE_pll_bist_run: Defines JE pll bist run bit <= 0x0
                // uvh_write_register_field_32(CLOCKGEN_BASE_ADDR + 0x28C, 0, 0x00000001, 0x0);
    val = vclk_peek_v(fg->virt_addr + 0x28C);
    val &= ~(1 << 0);
    vclk_poke_v(fg->virt_addr + 0x28C, val);

                // uint32_t overflow;
                // [21:17] JE_select_out: Defines JE select out bits <= 0x0C (frequency value LSB)
                // uvh_write_register_field_32(CLOCKGEN_BASE_ADDR + 0x294, 17, 0x003E0000, 0x0C);
    val = vclk_peek_v(fg->virt_addr + 0x294);
    val &= ~(0x1f << 17); // Clear [21:17]
    val |= (0xC << 17);
    vclk_poke_v(fg->virt_addr + 0x294, val);
                // overflow = uvh_read_register_field_32(CLOCKGEN_BASE_ADDR + 0x298, 16, 0x00010000);
    val = vclk_peek_v(fg->virt_addr + 0x298);

                // if(overflow == 0x1)
                // {
                    // uvh_error("ERROR","Overflow detected during frequency BIST sequence in %s",clkgenName);
                        //return UVH_ERROR_CODE;
                // }
    if (val & (1 << 16))
        vclk_error("ERROR: JE overflow\n");

                // uint32_t freq_cnt;
                // [21:17] JE_select_out: Defines JE select out bits <= 0x0C (frequency value LSB)
                // uvh_write_register_field_32(CLOCKGEN_BASE_ADDR + 0x294, 17, 0x003E0000, 0x0C);
    val = vclk_peek_v(fg->virt_addr + 0x294);
    val &= ~(0x1f << 17); // Clear [21:17]
    val |= (0xC << 17);
    vclk_poke_v(fg->virt_addr + 0x294, val);
                // freq_cnt = uvh_read_register_field_32(CLOCKGEN_BASE_ADDR + 0x298, 0, 0x0000FFFF);
    freq_cnt = vclk_peek_v(fg->virt_addr + 0x298) & 0xffff;
                // uvh_info("INFO",0,"freq_cnt[15:0] is %d",freq_cnt);
    vclk_debug(1, "  freq_cnt=0x%x\n", freq_cnt);

                // uint32_t frequency;
                // frequency = (unsigned long) ((double)(freq_cnt)*(double)(gfg_clk_freq)/1024);
    *rate = (unsigned long)((uint64_t)freq_cnt * ref->rate / 1024);
                // uvh_info("INFO",0,"Frequency BIST (application mode) check pass on %s/%s[%d] - frequency is %d kHz",clkgenName,"clk_div_",ClockIndex,frequency);
            // }
    // vclk_debug(1, "rate=%lu\n", *rate);

            // FCG_CONFIG161 (0x284) JE/OBs mux 1 and dynout mux setting bits
            // [27] JE_enable: Defines JE enable control bit <= 0x0
            // uvh_write_register_field_32(CLOCKGEN_BASE_ADDR + 0x284, 27, 0x08000000, 0x0);
    val = vclk_peek_v(fg->virt_addr + 0x284);
    val &= ~(1 << 27);
    vclk_poke_v(fg->virt_addr + 0x284, val);
        // }
        // else {
            // uvh_info("INFO",0,"Clock frequency of %s/%s[%d] is null", clkgenName, "clk_div_", ClockIndex);
        // }
    // }
    // uvh_info("END_TEST",0,"Ending %s JESS frequency test", clkgenName);

    return 0;
}
