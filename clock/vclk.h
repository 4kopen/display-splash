/*
* This file is part of Validation CLK Driver (VCLKD).
*
* Copyright 2014, STMicroelectronics - All Rights Reserved
* Author(s): fabrice.charpentier@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* VCLKD may alternatively be licensed under GPL V2 license from ST.
*/

#ifndef __VCLK_H
#define __VCLK_H

#include <stddef.h>
#include <stdint.h>

/* Prototypes */
struct vclk;

/*
 * Constants, structures & types
 */

/* Clock group & associated operations */
typedef struct vclk_group {
    const char  *name; /* Group name (ex: "A0") */

    // Enable/disable clock
    int         (*xable)(struct vclk *clk, int enable);
    // Read HW & compute current rate
    int         (*recalc_rate)(struct vclk *clk);
    // Set clock rate according to given parent rate
    int         (*set_ratep)(struct vclk *clk,
                        unsigned long rate, unsigned long prate);
    // Identify parent clock
    int         (*identify_parent)(struct vclk *clk);
    // Set parent clock
    int         (*set_parent)(struct vclk *clk, struct vclk *src);
    // Route clock to IO for observation purpose
    int         (*observe)(struct vclk *clk, unsigned long *div);
    // Observation point (usually which PIO)
    const char  *obs_point;
    // HW measure feature
    int         (*get_measure)(struct vclk *clk, unsigned long *);
} vclk_group_t;

/* Possible clock infos (to be ORed) */
enum vclk_infos {
    VCLK_OFF = 1 << 0,          // Disabled at this level
    VCLK_POFF = 1 << 1,         // Disabled at parent level
    VCLK_NORATE = 1 << 2,       // Rate compute unsupported
    VCLK_UPD = 1 << 3,          // Updated from HW (source + rate)
    VCLK_FG = 1 << 4,           // This clock belongs to a flexiclockgen
    VCLK_SINGLESRC = 1 << 5,    // This clock has a single source
    VCLK_RATEDOUBT = 1 << 6,    // Set if rate might be wrong (FS case due to ENPROG)
    VCLK_NOSOURCE = 1 << 7,     // This clock is not driven (ex: unconnected alt ref input)
    VCLK_FIXED = 1 << 8         // OSC source clock like
};

/* Clock declaration */
typedef struct vclk {
    const char          *name;      // Clock name (NULL = invalid entry)
    int                 id;         // Clock ID
    void                *group;     // Group/flexgen the clock belongs to

    struct vclk         *parent;    // Parent clock
    unsigned long       rate;       // Frequency
    unsigned short      flags;      // Some clocks infos

    unsigned int        flags2;     // Flexgen specific flags
    char                idx0;       // Flexgen refID, srcID, gfgID, or divID
    char                idx1;       // Flexgen channelID, PHIID

    /* Optional: for clock tree creation */
    struct vclk         *next;      // Next clock at same level
    struct vclk         *child;     // Child clock
} vclk_t;

/*
 * 'vclk' exported functions
 */

/* Clock driver init.
   To be done prior to any vclk API use
   If 'read_hw' is set, HW is read to update internal infos.
   Returns: 0=ok, -1=ERROR
 */
int vclk_init(int read_hw);

/* Look for clock name and return 'struct vclk'
   Returns: pointer to 'struct vclk' or NULL if unknown
 */
struct vclk *vclk_get(char *name);

/* Look for clock identifier and return 'struct vclk'
   Returns: pointer to 'struct vclk' or NULL if unknown
 */
struct vclk *vclk_get_id(int id);

/* A clock can belong to a flexiclockgen, that itself belongs
   to a clock group.
   Return: the top level group for this clock or NULL if none */
struct vclk_group *vclk_get_group(struct vclk *clk);

/* Update clock infos & rate from HW
   - identify source
   - then read programmed rate & xable infos
   mode: 0=update parent first if VCLK_UPD flag not set
         1=always update parent first
   Returns: 0=OK, -1=ERROR
 */
int vclk_update(struct vclk *clk, int mode);

/* Get 'clk' programmed rate even if disabled.
   Note that vclk_update() must be called first if
     vclkd is not the exclusive clock driver.
   Returns: rate or 0 if ERROR */
unsigned long vclk_get_rate(struct vclk *clk);

/* Check if 'clk' clock is running.
   Note that vclk_update() must be called first if
     vclkd is not the exclusive clock driver.
   Return: 1=TRUE, 0=FALSE/ERROR */
int vclk_is_running(struct vclk *clk);

/* Check if 'clkname' clock is running.
   Note that vclk_update() must be called first if
     vclkd is not the exclusive clock driver.
   Return: 1=TRUE, 0=FALSE/ERROR */
int vclk_is_running_name(char *clkname);

/* Enable/disable clock at HW level, update infos,
   then propagate to childs.
   Returns: 0=OK, -1=ERROR
 */
int vclk_xable(struct vclk *clk, int enable);

/* Set 'clk' rate with given parent rate.
   Useful to set a clock prior to its parent.
   Returns: 0=OK, -1=ERROR */
int vclk_set_ratep(struct vclk *clk, unsigned long rate, unsigned long prate);

/* Set 'clk' rate with CURRENT parent rate.
   Returns: 0=OK, -1=ERROR */
int vclk_set_rate(struct vclk *clk, unsigned long rate);

/* Route clock to IO for observation if possible
   Returns: 0=ok, -1=error
 */
int vclk_observe(struct vclk *clk, unsigned long *div);

/* Change clock source (when possible)
   Returns: 0=ok, -1=error
 */
int vclk_set_parent(struct vclk *clk, struct vclk *src);

/* Returns current parent or NULL */
static inline struct vclk *vclk_get_parent(struct vclk *clk)
{
    if (!clk) return NULL;

    return clk->parent;
}

/* Parse clocks list and call 'func' for each clock
 */
int vclk_for_each(void (*func)(struct vclk *clk, void *data), void *data);

/* Returns length of longest clock name
   or -1 if ERROR */
int vclk_max_len(void);

/* Parse clocks list and create a clock tree
   updating 'next' & 'child' fields.
   Note that full update from HW is done.
   Returns: -1=ERROR, 0=OK
 */
int vclk_tree(struct vclk **tree, char *clkname);

/*
 * OS/application specific functions that must be
 * provided by the wrapper on top of validation clock driver.
 */

/* Errors print function */
int vclk_error(const char *Fmt, ...);

/* Debug print function */
int vclk_debug(int dbglevel, const char *Fmt, ...);

/* Physical to virtual addr mapping */
void *vclk_phys2virt(void *paddr, unsigned long size);

/* Register read function with virtual address */
unsigned long vclk_peek_v(void *vaddr);

/* Register write function with virtual address */
void vclk_poke_v(void *vaddr, unsigned long data);

/* Register read function with physical address */
unsigned long vclk_peek_p(void *paddr);

/* Register write function with physical address */
void vclk_poke_p(void *paddr, unsigned long data);

#endif // __VCLK_H
