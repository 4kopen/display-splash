/*
* This file is part of Validation CLK Driver (VCLKD).
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
* Author(s): fabrice.charpentier@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* VCLKD may alternatively be licensed under GPL V2 license from ST.
*/

#ifndef __VCLK_FLEXGEN_H
#define __VCLK_FLEXGEN_H

#include "vclk.h"
#include <stdint.h>

/* Clocks flags. Some can be "ORed".
    Examples:
      FS660C32 single CHAN output => FG_GFGOUT|FG_FS660C32|FG_FVCO|FG_CHAN
    This means there is no FVCO dedicated clock. Changing this clock
    rate will also change FVCO accordingly.
      PLL3200C32 FVCOBY2 => FG_GFGOUT|FG_PLL3200C32|FG_FVCOBY2
    This is a VCOby2 clock only
      PLL3200C32 PHI => FG_GFGOUT|FG_PLL3200C32|FG_FVCOBY2|FG_PHI
    No FVCO dedicated clock. Changing PHI will update FVCO
    accordingly.
*/
enum {
    /* GFG types [7:0] */
    FG_PLL3200C28 = 1 << 0,
    FG_PLL3200C32 = 1 << 1,
    FG_FS660C28 = 1 << 2,
    FG_FS660C32 = 1 << 3,

    /* GFG outputs types [15:8] */
    FG_FVCO = 1 << 8,       // VCO freq (fs660c32 case)
    FG_FVCOBY2 = 1 << 9,    // VCO freq by 2 (pll3200c32 case)
    FG_PHI = 1 << 10,       // PHIx freq (pll3200c32 case)
    FG_CHAN = 1 << 11,      // CHANNELx freq (fs660c32 case)
    FG_FRAC = 1 << 12,      // Fractional mode (some PLL)
    /* RESERVED TYPES = [15:13] */

    /* Clock types */
    FG_SRCIN = 1 << 16,     // Source clock input (srcclkin)
    FG_REFIN = 1 << 17,     // Ref clock input (refclkin)
    FG_REFOUT = 1 << 18,    // Ref clock output (refclkout)
    FG_EREFOUT = 1 << 19,   // Ext ref clock output (ext_refclkout)
    FG_GFGOUT = 1 << 20,    // GFG output
    FG_DIV = 1 << 21,       // Final div output
    FG_JEIN = 1 << 22,      // clk_je_input

    /* Misc */
    FG_NSDIV0 = 1 << 28,    // NSDIV frozen to 0
    FG_NSDIV1 = 1 << 29,    // NSDIV frozen to 1
    FG_BEAT = 1 << 30,      // Beat counter on this source
    FG_SYNC = 1 << 31       // Semi-sync final div clock
};

/* Flexiclockgen infos */
struct flexgen {
    /* Flexiclockgen general infos */
    const char *name;           /* Clockgen name (ex: "A0") */
    void *phys_addr;            /* Physical base addr */
    struct vclk_group *group;   /* Group the flexgen belongs to */

    /* Ref clocks (refclkin) */
    int nb_refin;               /* Number of refclkin */

    /* 'refclkout/extrefclkout' path config */
    unsigned short refmuxmap;   /* 1=mux present, 0=bypass */
    unsigned short refdivmap;   /* 1=div present, 0=bypass */

    /* Cross bar inputs (clock IDs) listed from 0 to MSB. */
    int *xbar_src;              /* Terminated with -1 */

    /* Lib internal vars */
    void *virt_addr;            // Virtual base addr
    int nb_gfg;                 // Number of GFGs = nb of refclkout
    int div0;                   // Div0 clock ID (-1=undef)
    int divlast;                // DivLast clock ID (-1=undef)
    int xbar_in_size;           // Number of Xbar sources
};

/* Flexiclockgens declaration
   - Clockgen name (ex: "A0")
   - Physical base address
   - Group the flexgen belongs to
   - Number of 'refclkin' (even if not connected)
   - Refclkout config (muxmap, divmap)
   - Xbar inputs list (in cross bar input order)
*/
#define _FLEXGEN(_name, _paddr, _group, _nbrefin, _muxmap, _divmap, _xbarsrc) \
{ \
    .name = _name,          \
    .phys_addr = _paddr,    \
    .group = _group,        \
    .nb_refin = _nbrefin,   \
    .refmuxmap = _muxmap,   \
    .refdivmap = _divmap,   \
    .xbar_src = _xbarsrc    \
}

#define _FLEXGEN_END() \
{ \
    .name = NULL            \
}

/*
 * Flexiclockgen generic functions
 */

/* ========================================================================
   Name:        flexgen_init
   Description: Flexgen lib init function
   Returns:     0=OK
   ======================================================================== */

int flexgen_init(struct vclk *soc_clocks, int nbclocks);

/* ========================================================================
   Name:        flexgen_identify_parent
   Description: Read HW to identify source clock (GFG out, refclkin,
        srcclkin, ..)
   Returns:     0=OK
   ======================================================================== */

int flexgen_identify_parent(struct vclk *clk);

/* ========================================================================
   Name:        flexgen_set_parent
   Description: Set cross bar source clock
   Returns:     0=OK, -1=ERROR
   ======================================================================== */

int flexgen_set_parent(struct vclk *clk, struct vclk *src);

/* ========================================================================
   Name:        flexgen_recalc
   Description: Read flexiclockgen HW status to fill in 'clk->rate'
   Returns:     0=OK, or CLK_ERR_BAD_PARAMETER
   ======================================================================== */

int flexgen_recalc(struct vclk *clk);

/* ========================================================================
   Name:        flexgen_set_ratep
   Description: Set Flexiclockgen clock & update status
   Returns:     0=OK
   ======================================================================== */

int flexgen_set_ratep(struct vclk *clk, unsigned long freq, unsigned long prate);

/* ========================================================================
   Name:        flexgen_xable
   Description: Enable/disable clock & update status
   Returns:     0=OK
   ======================================================================== */

int flexgen_xable(struct vclk *clk, int enable);

/* ========================================================================
   Name:        flexgen_shutdown
   Description: Disable each final div output whose 'disable' bit is set
   Params:      base=Flexgen base addr
                disable=ORed combination of outputs to switch OFF
   Returns:     0=OK
   ======================================================================== */

void flexgen_shutdown(void *base, uint64_t disable);

/* ========================================================================
   Name:        flexgen_observe
   Description: Configure JESS to observe flexiclockgen clocks
   Returns:     0=OK
   ======================================================================== */

int flexgen_observe(struct vclk *clk, unsigned long *div);

/* ========================================================================
   Name:        flexgen_get_measure
   Description: Perform HW measure and update '*rate'
     clk   = Clock to measure (currently final div ONLY)
     ref   = GFG ref clock to use (ex: CLK_B0_REFOUT0)
     *rate = returned measured rate
   Returns:     0=OK, -1=ERROR/UNSUPPORTED
   ======================================================================== */

int flexgen_get_measure(struct vclk *clk, struct vclk *ref, unsigned long *rate);

#endif // __VCLK_FLEXGEN_H
