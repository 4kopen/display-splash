/*
* This file is part of Splash Screen
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
* Author(s): karim.ben-belgacem@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* Splash Screen may alternatively be licensed under GPL V2 license from ST.
*/

#ifndef __VCLK_DEBUG_H
#define __VCLK_DEBUG_H

//#define DEBUG

#ifdef DEBUG

#define CLK_DEBUG_LEVEL 0

#define PRINT_MAXLEN 2048
#define NOCOL      "\033[0m"
#define REDCOL     "\033[0;31m"

enum
{
    CLK_DEBUG_ERROR = 0,
    CLK_DEBUG_TRACES,
};

/* Errors print function */
#define vclk_error(...)     vclk_print(CLK_DEBUG_ERROR, __func__, __VA_ARGS__)
/* Debug print function */
#define vclk_debug(l, ...)  vclk_print(l, __func__, __VA_ARGS__)

#else // DEBUG

#define vclk_error(m, ...)
#define vclk_debug(l, ...)

#endif // DEBUG

#endif // __VCLK_DEBUG_H
