/*
* This file is part of Validation CLK Driver (VCLKD).
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
* Author(s): fabrice.charpentier@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* VCLKD may alternatively be licensed under GPL V2 license from ST.
*/

#ifndef __VCLK_SYSCONF_H
#define __VCLK_SYSCONF_H

struct sysconf_grp {
    int first;  // First sysconf ID
    int last;   // Last sysconf ID
    void *base; // Sysconf base addr
    int inc;    // Increment. Default=4Bytes
};

int vclk_syscfg_init(struct sysconf_grp *soc_sysconf_groups);
void vclk_syscfg_write(int num, int lsb, int msb, int value);
unsigned long vclk_syscfg_read(int num, int lsb, int msb);

#endif /* #ifndef __VCLK_SYSCONF_H */
