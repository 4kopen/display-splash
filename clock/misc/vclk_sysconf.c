/*
 * OS independant Validation CLocK Driver
 * sysconf library
 * 2014-15, F. Charpentier, UPD Functional Validation
 */

#include "vclk.h"
#include "vclk_sysconf.h"

/*
 * Local variables
 */

static struct sysconf_grp *sysconf_groups = NULL;
static int init_done = 0;

/*
 * Local functions
 */

/* Compute sysconf address and updates '*addr'.
   Return: 0=OK, -1=ERR
 */
static int get_syscfg_addr(int syscfg, void **addr)
{
    int i;

    for (i = 0; sysconf_groups[i].first != -1; i++)
        if ((syscfg >= sysconf_groups[i].first) && (syscfg <= sysconf_groups[i].last))
            break;
    if (sysconf_groups[i].first == -1) {
        vclk_error("ERROR: Unknown sysconf %d\n", syscfg);
        return -1;
    }
    *addr = sysconf_groups[i].base + (syscfg - sysconf_groups[i].first) * sysconf_groups[i].inc;

    return 0;
}

/*
 * Exported functions
 */

int vclk_syscfg_init(struct sysconf_grp *soc_sysconf_groups)
{
    int i;

    sysconf_groups = soc_sysconf_groups;

    /* If 'inc' is undef (0) defaulting to 4 bytes */
    for (i = 0; sysconf_groups[i].first != -1; i++)
        if (!sysconf_groups[i].inc)
            sysconf_groups[i].inc = 4;

    init_done = 1;
    return 0;
}

void vclk_syscfg_write(int num, int lsb, int msb, int value)
{
    int bits;
    void *addr;

    if (!init_done) {
        vclk_error("ERROR: Sysconf lib not initialized\n");
        return;
    }

    if (get_syscfg_addr(num, &addr) != 0)
        return;

    bits = msb - lsb + 1;
    if (bits == 32)
        vclk_poke_p(addr, value);
    else {
        unsigned long reg_mask;
        unsigned long tmp;

        reg_mask = ~(((1 << bits) - 1) << lsb);

        tmp = vclk_peek_p(addr);
        tmp &= reg_mask;
        tmp |= value << lsb;
        vclk_poke_p(addr, tmp);
    }
}

unsigned long vclk_syscfg_read(int num, int lsb, int msb)
{
    int bits;
    unsigned long data;
    void *addr;

    if (get_syscfg_addr(num, &addr) != 0)
        return 12;

    bits = msb - lsb + 1;
    data = vclk_peek_p(addr);
    if (bits != 32) {
        data = data >> lsb;
        data &= (1 << bits) - 1;
    }

    return data;
}
