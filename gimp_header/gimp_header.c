#include "splash.h"
#include "bare-tools.h"
#include "gimp_header.h"
#include "exported_image.h"

void splash_read_gimp_header(stm_img_t *imgInfo)
{
  unsigned int x,y,i;
  unsigned char *input;
  U32 *output;
  unsigned char pixel[3];

  imgInfo->Palette = NULL;
  imgInfo->pixel_format = PIXEL_FORMAT_RGB_AUTO;
  imgInfo->bits_per_pixel = 32; /* implies ARGB8888 */
  imgInfo->height = height; /* from exported_image.h */
  imgInfo->width = width; /* from exported_image.h */

  imgInfo->fbuffer = splash_malloc(imgInfo->height*imgInfo->width*imgInfo->bits_per_pixel/8);

  input = header_data; /* from exported_image.h */
  output = (U32 *) imgInfo->fbuffer;

  for(i=0; i<(imgInfo->width*imgInfo->height); ++i) {
    HEADER_PIXEL(input, pixel);
    *(output++) = SET_ARGB_PIXEL(0xff, pixel[0], pixel[1], pixel[2]);
  }
}
