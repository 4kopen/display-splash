/*
* This file is part of Splash Screen
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
* Author(s): karim.ben-belgacem@st.com for
* STMicroelectronics.
*
* License terms: MIT.
* See full license terms in LICENSE file.
*
* Splash Screen may alternatively be licensed under GPL V2 license from ST.
*/

#include <stddef.h>
#include <stdarg.h>
#include "sys.h"
#include "vclk.h"
#include "modes.h"

#define N_ELEMENTS(array)   (sizeof (array) / sizeof ((array)[0]))

//#define DEBUG
static int clocks_initialized = 0;

static const sp_clock_def_t fixed_clock_names[] =
{
#if defined (stx410)
  /* Clockgen C0 */
  { "CLK_MAIN_DISP"       , "CLK_C0_PLL1_PHI0",   400000000 }
, { "CLK_AUX_DISP"        , "CLK_C0_PLL1_PHI0",   400000000 }
, { "CLK_COMPO_DVP"       , "CLK_C0_PLL1_PHI0",   400000000 }
, { "CLK_TMDS_HDMI"       , "CLK_TMDSOUT_HDMI",           0 }
  /* ClockGen D2 */
, { "CLK_D2_FS_VCO"       , ""                ,   660000000 }
#elif defined(stx418) || defined(stx419)
/* Clockgen C0 */
  { "CLK_MAIN_DISP"       , "CLK_C0_PLL1_PHI0",   400000000 }
, { "CLK_AUX_DISP"        , "CLK_C0_PLL1_PHI0",   400000000 }
, { "CLK_PROC_MIXER"      , "CLK_C0_PLL1_PHI0",   400000000 }
, { "CLK_TMDS_HDMI"       , "CLK_TMDSOUT_HDMI",           0 }
, { "CLK_TMDS_HDMI_DIV2"  , "CLK_TMDSOUT_HDMI",           0 }
#if defined(stx419)
, { "CLK_PROC_GDP"        , "CLK_C0_PLL1_PHI0",   400000000 }
#else
, { "CLK_COMPO_DVP"       , "CLK_C0_PLL1_PHI0",   400000000 }
#endif
/* ClockGen D2 */
, { "CLK_D2_FS0_VCO"      , ""                ,   660000000 }
#elif defined (stx337) || defined (stx390)
/* ClockGen D1 */
  { "CLK_D1_SRCIN0"       , ""                ,           0 }
, { "CLK_TMDS_HDMI"       , "CLK_D1_SRCIN0"   ,           0 }
, { "CLK_D1_FS0_VCO"      , ""                ,   660000000 }
, { "CLK_D1_FS0_CH2"      , ""                ,   200000000 }
, { "CLK_D1_FS0_CH3"      , ""                ,   240000000 }
#if defined (stx390) && !defined(CUT_2)
, { "CLK_PROC_MIXER"      , "CLK_D1_FS0_CH2"  ,   200000000 }
#else
, { "CLK_PROC_MIXER"      , "CLK_D1_FS0_CH3"  ,   240000000 }
#endif
, { "CLK_PROC_GDP_COMPO"  , "CLK_D1_FS0_CH3"  ,   240000000 }
, { "CLK_HDMI_PHASE_REG"  , "CLK_D1_FS0_CH2"  ,   100000000 }
#endif
};

/* Clock name mapping */
#define SPLASH_CLK_NONE           ""
#if defined(stx410)
#define SPLASH_CLK_FS0_VCO       "CLK_D2_FS_VCO"
#define SPLASH_CLK_FS0_CH0       "CLK_D2_FS0"
#define SPLASH_CLK_FS0_CH1       "CLK_D2_FS1"
#define SPLASH_CLK_PIX_MAIN_DISP "CLK_PIX_MAIN_DISP"
#define SPLASH_CLK_REF_HDMIPHY   "CLK_REF_HDMIPHY"
#define SPLASH_CLK_PIX_HDMI      "CLK_PIX_HDMI"
#define SPLASH_CLK_PIX_HDDAC     "CLK_PIX_HDDAC"
#define SPLASH_CLK_HDDAC         "CLK_HDDAC"
#define SPLASH_CLK_PIX_AUX_DISP  "CLK_PIX_AUX_DISP"
#define SPLASH_CLK_DENC          "CLK_DENC"
#define SPLASH_CLK_SDDAC         "CLK_SDDAC"
#define SPLASH_CLK_PIX_GDP3      "CLK_PIX_GDP3"
#define SPLASH_CLK_PIX_GDP4      "CLK_PIX_GDP4"
#elif defined(stx418) || defined(stx419)
#define SPLASH_CLK_FS0_VCO       "CLK_D2_FS0_VCO"
#define SPLASH_CLK_FS0_CH0       "CLK_D2_FS0_CH0"
#define SPLASH_CLK_FS0_CH1       "CLK_D2_FS0_CH1"
#define SPLASH_CLK_PIX_MAIN_DISP "CLK_PIX_MAIN_DISP"
#define SPLASH_CLK_REF_HDMIPHY   "CLK_REF_HDMIPHY"
#define SPLASH_CLK_TMDSOUT       "CLK_TMDSOUT_HDMI"
#define SPLASH_CLK_PIX_HDMI      "CLK_PIX_HDMI"
#define SPLASH_CLK_PIX_HDDAC     "CLK_PIX_HDDAC"
#define SPLASH_CLK_HDDAC         "CLK_HDDAC"
#define SPLASH_CLK_PIX_AUX_DISP  "CLK_PIX_AUX_DISP"
#define SPLASH_CLK_DENC          "CLK_DENC"
#define SPLASH_CLK_SDDAC         "CLK_SDDAC"
#elif defined(stx390) || defined(stx337)
#define SPLASH_CLK_FS0_VCO       "CLK_D1_FS0_VCO"
#define SPLASH_CLK_FS0_CH0       "CLK_D1_FS0_CH0"
#define SPLASH_CLK_FS0_CH1       "CLK_D1_FS0_CH1"
#define SPLASH_CLK_FS0_CH2       "CLK_D1_FS0_CH2"
#define SPLASH_CLK_FS0_CH3       "CLK_D1_FS0_CH3"
#define SPLASH_CLK_PIX_MAIN_DISP "CLK_PIX_MAIN_DISP"
#define SPLASH_CLK_REF_HDMIPHY   "CLK_REF_HDMIPHY"
#define SPLASH_CLK_TMDSOUT       "CLK_D1_SRCIN0"
#define SPLASH_CLK_TMDS_HDMI     "CLK_TMDS_HDMI"
#define SPLASH_CLK_PIX_HDMI      "CLK_PIX_HDMI"
#define SPLASH_CLK_PIX_HDDAC     "CLK_PIX_HDDAC"
#define SPLASH_CLK_HDDAC         "CLK_HDDAC"
#define SPLASH_CLK_PIX_AUX_DISP  "CLK_PIX_AUX_DISP"
#define SPLASH_CLK_SDDAC         "CLK_SDDAC"
#define SPLASH_CLK_DENC          "CLK_DENC"
#define SPLASH_CLK_HDMI_PHASE_REG "CLK_HDMI_PHASE_REG"
#endif

static const sp_clock_def_t fhd_clocks[] =
{
  {SPLASH_CLK_FS0_CH0       , SPLASH_CLK_FS0_VCO, 297000000},
  {SPLASH_CLK_FS0_CH1       , SPLASH_CLK_FS0_VCO, 108000000},
  {SPLASH_CLK_PIX_MAIN_DISP , SPLASH_CLK_FS0_CH0, 148500000},
  {SPLASH_CLK_REF_HDMIPHY   , SPLASH_CLK_FS0_CH0, 148500000},
#if defined(stx337)
  {SPLASH_CLK_TMDS_HDMI     , SPLASH_CLK_FS0_CH0, 148500000},
#endif
  {SPLASH_CLK_PIX_HDMI      , SPLASH_CLK_FS0_CH0, 148500000},
#ifdef STM_HD_COMPONENT
  {SPLASH_CLK_PIX_HDDAC     , SPLASH_CLK_FS0_CH0, 148500000},
  {SPLASH_CLK_HDDAC         , SPLASH_CLK_FS0_CH0, 148500000},
#else
  {SPLASH_CLK_PIX_HDDAC     , SPLASH_CLK_FS0_CH1,  13500000},
  {SPLASH_CLK_HDDAC         , SPLASH_CLK_FS0_CH1, 108000000},
#endif
  {SPLASH_CLK_PIX_AUX_DISP  , SPLASH_CLK_FS0_CH1,  13500000},
  {SPLASH_CLK_DENC          , SPLASH_CLK_FS0_CH1,  27000000},
  {SPLASH_CLK_SDDAC         , SPLASH_CLK_FS0_CH1, 108000000},
#ifdef stx410
  {SPLASH_CLK_PIX_GDP3      , SPLASH_CLK_FS0_CH0, 148500000},
  {SPLASH_CLK_PIX_GDP4      , SPLASH_CLK_FS0_CH1,  13500000},
#endif
};

static const sp_clock_def_t hd_clocks[] =
{
  {SPLASH_CLK_FS0_CH0       , SPLASH_CLK_FS0_VCO, 297000000},
  {SPLASH_CLK_FS0_CH1       , SPLASH_CLK_FS0_VCO, 108000000},
  {SPLASH_CLK_PIX_MAIN_DISP , SPLASH_CLK_FS0_CH0,  74250000},
  {SPLASH_CLK_REF_HDMIPHY   , SPLASH_CLK_FS0_CH0,  74250000},
#if defined(stx337)
  {SPLASH_CLK_TMDS_HDMI     , SPLASH_CLK_FS0_CH0,  74250000},
#endif
  {SPLASH_CLK_PIX_HDMI      , SPLASH_CLK_FS0_CH0,  74250000},
#ifdef STM_HD_COMPONENT
  {SPLASH_CLK_PIX_HDDAC     , SPLASH_CLK_FS0_CH0,  74250000},
  {SPLASH_CLK_HDDAC         , SPLASH_CLK_FS0_CH0, 148500000},
#else
  {SPLASH_CLK_PIX_HDDAC     , SPLASH_CLK_FS0_CH1,  13500000},
  {SPLASH_CLK_HDDAC         , SPLASH_CLK_FS0_CH1, 108000000},
#endif
  {SPLASH_CLK_PIX_AUX_DISP  , SPLASH_CLK_FS0_CH1,  13500000},
  {SPLASH_CLK_DENC          , SPLASH_CLK_FS0_CH1,  27000000},
  {SPLASH_CLK_SDDAC         , SPLASH_CLK_FS0_CH1, 108000000},
#ifdef stx410
  {SPLASH_CLK_PIX_GDP3      , SPLASH_CLK_FS0_CH0,  74250000},
  {SPLASH_CLK_PIX_GDP4      , SPLASH_CLK_FS0_CH1,  13500000},
#endif
};

static const sp_clock_def_t ed_clocks[] =
{
  {SPLASH_CLK_FS0_CH0       , SPLASH_CLK_FS0_VCO, 108000000},
  {SPLASH_CLK_FS0_CH1       , SPLASH_CLK_FS0_VCO, 108000000},
  {SPLASH_CLK_PIX_MAIN_DISP , SPLASH_CLK_FS0_CH0,  27000000},
  {SPLASH_CLK_REF_HDMIPHY   , SPLASH_CLK_FS0_CH0,  27000000},
#if defined(stx337)
  {SPLASH_CLK_TMDS_HDMI     , SPLASH_CLK_FS0_CH0,  27000000},
#endif
  {SPLASH_CLK_PIX_HDMI      , SPLASH_CLK_FS0_CH0,  27000000},
#ifdef STM_HD_COMPONENT
  {SPLASH_CLK_PIX_HDDAC     , SPLASH_CLK_FS0_CH0,  27000000},
  {SPLASH_CLK_HDDAC         , SPLASH_CLK_FS0_CH0, 108000000},
#else
  {SPLASH_CLK_PIX_HDDAC     , SPLASH_CLK_FS0_CH1,  13500000},
  {SPLASH_CLK_HDDAC         , SPLASH_CLK_FS0_CH1, 108000000},
#endif
  {SPLASH_CLK_PIX_AUX_DISP  , SPLASH_CLK_FS0_CH1,  13500000},
  {SPLASH_CLK_DENC          , SPLASH_CLK_FS0_CH1,  27000000},
  {SPLASH_CLK_SDDAC         , SPLASH_CLK_FS0_CH1, 108000000},
#ifdef stx410
  {SPLASH_CLK_PIX_GDP3      , SPLASH_CLK_FS0_CH0,  27000000},
  {SPLASH_CLK_PIX_GDP4      , SPLASH_CLK_FS0_CH1,  13500000},
#endif
};

static const stm_clock_t clock_configuration[] = {
  { (1<<STM_1920X1080P60) | (1<<STM_1920X1080P50)
  , N_ELEMENTS(fhd_clocks)
  , fhd_clocks
  },

  { (1<<STM_1280X720P60)  | (1<<STM_1280X720P50)
                    | (1<<STM_1920X1080I60) | (1<<STM_1920X1080I50)
  , N_ELEMENTS(hd_clocks)
  , hd_clocks
  },

  { (1<<STM_720X480P60)   | (1<<STM_720X576P50)
  , N_ELEMENTS(ed_clocks)
  , ed_clocks
  }
};

/* Debug print function */
#ifdef DEBUG

#define MAX_LENGTH_ARG  255
#define CLK_DEBUG_ERROR 0
#define CLK_DEBUG_LEVEL CLK_DEBUG_ERROR
#define PRINT_MAXLEN    2048
#define NOCOL           "\033[0m"
#define REDCOL          "\033[0;31m"

int __attribute__ ((format (printf, 2, 3)))
vclk_debug(int level, const char *fmt, ...)
{
  va_list args;
  int     printed=0;
  char    buf[MAX_LENGTH_ARG];

  if (level > CLK_DEBUG_LEVEL)
    return 0;

  va_start(args, fmt);
  printed += vsnprintf(buf, MAX_LENGTH_ARG, fmt, args);
  va_end(args);

  printf(NOCOL "%s" NOCOL, buf);
  return printed;
}

int __attribute__ ((format (printf, 1, 2)))
vclk_error(const char *fmt, ...)
{
  va_list args;
  int     printed=0;
  char    buf[MAX_LENGTH_ARG];

  va_start(args, fmt);
  printed += vsnprintf(buf, MAX_LENGTH_ARG, fmt, args);
  va_end(args);

  printf(REDCOL " %s" NOCOL, buf);
  return printed;
}

#else
int vclk_debug(int dbglevel, const char *fmt, ...)
{
	return 0;
}
int vclk_error(const char *fmt, ...)
{
	return 0;
}
#endif

void *vclk_phys2virt(void *addr, unsigned long size)
{
  /*
   * This mapping function was exported by VCLKD driver
   * and shall be defined here.
   * In our case Virtual and Physical adresses are the same
   * So we don't need to do any remap, hence size is not used.
   */
  return addr;
}

unsigned long vclk_peek(unsigned long addr)
{
  return STSYS_ReadRegDev32LE(addr);
}

void vclk_poke(unsigned long addr, unsigned long data)
{
  STSYS_WriteRegDev32LE(addr, data);
}

unsigned long vclk_peek_p(void *paddr)
{
  return vclk_peek((unsigned long)paddr);
}

void vclk_poke_p(void *paddr, unsigned long data)
{
  vclk_poke((unsigned long)paddr, data);
}

unsigned long vclk_peek_v(void *vaddr)
{
  return vclk_peek((unsigned long)vclk_phys2virt(vaddr, 4));
}

void vclk_poke_v(void *vaddr, unsigned long data)
{
  vclk_poke((unsigned long)vclk_phys2virt(vaddr, 4), data);
}

static int vclk_init_clocks(void)
/**********************/
{
  int i = 0;
  struct vclk *clk  = NULL;
  struct vclk *clkp = NULL;

  if(clocks_initialized)
    return 0;

  if(vclk_init(1))
    return -1;

  /* fixed clocks */
  for (i = 0; i < N_ELEMENTS(fixed_clock_names); i++)
  {
    vclk_debug(0, "Initializing clock '%s' with parent = '%s' and rate=%ld\n"
                , fixed_clock_names[i].clk_name, fixed_clock_names[i].parent_name
                , fixed_clock_names[i].clk_rate);

    clk = vclk_get(fixed_clock_names[i].clk_name);

    if (!clk || clk < 0) {
      vclk_error("Failed to get Clock '%s'\n", fixed_clock_names[i].clk_name);
      continue;
    }

    if (fixed_clock_names[i].parent_name[0] != '\0') {
      clkp = vclk_get(fixed_clock_names[i].parent_name);
      if (!clkp || clkp < 0) {
        vclk_error("Failed to get Clock '%s'\n", fixed_clock_names[i].parent_name);
        continue;
      }
      if (vclk_set_parent(clk, clkp)) {
        vclk_error("Failed to set parent '%s' for Clock '%s'\n", fixed_clock_names[i].parent_name, fixed_clock_names[i].clk_name);
        continue;
      }
    }

    if (vclk_xable(clk, 1)) {
      vclk_error("Failed to Enable Clock '%s'\n", fixed_clock_names[i].clk_name);
      return -2;
    }

    if (fixed_clock_names[i].clk_rate) {
      if (vclk_set_rate(clk, fixed_clock_names[i].clk_rate)) {
        vclk_error("Failed to set rate %lld for Clock '%s'\n", fixed_clock_names[i].clk_rate, fixed_clock_names[i].clk_name);
        return -3;
      }
    }
  }

  clocks_initialized = 1;
  return 0;
}

/* ========================================================================
   Name:        clk_get_clock_configuration
   Description: Get Splash Screen clocks configuration for specific mode
   ======================================================================== */
int clk_get_clock_configuration(splash_modes_t main_mode, stm_clock_t *clock_cfg)
{
  int i = 0;

  for (i = 0; i < N_ELEMENTS(clock_configuration);i++)
  {
    if ((1L<<main_mode)&(clock_configuration[i].main_modes))
    {
      *clock_cfg = clock_configuration[i];
      vclk_debug(0, "Using clock configuration id=%d addr=%p\n", i, clock_cfg);
      return 0;
    }
  }

  vclk_error("ERROR: clocks are not defined for this mode\n");
  return -1;
}

/* ========================================================================
   Name:        clk_set_clocks
   Description: Set Splash Screen clocks
   ======================================================================== */
int clk_set_clocks(const stm_clock_t* clock_cfg)
{
  int i = 0;
  struct vclk *clk  = NULL;
  struct vclk *clkp = NULL;

  if(!clock_cfg) {
    vclk_error("ERROR: Invalid clock configuration\n");
    return -1;
  }

  vclk_debug(0, "Starting clock module for Mode 0x%x\n", clock_cfg->main_modes);

  if(!clocks_initialized)
  {
    vclk_debug(0, "Initializing clock module\n");
    if (vclk_init_clocks()) {
        vclk_error("ERROR: failed to initialize clock module\n");
        return -1;
    }
  }

  /* Dynamic clocks */
  for (i = 0; i < clock_cfg->nb_clocks; i++)
  {
    if ((!clock_cfg->clocks[i].clk_name) || (clock_cfg->clocks[i].clk_name[0] == '\0'))
      break;

    clk = vclk_get(clock_cfg->clocks[i].clk_name);
    if(!clk || clk < 0) {
        vclk_error("Failed to get Clock '%s'\n", clock_cfg->clocks[i].clk_name);
        break;
    }

    vclk_debug(0, "Starting clock '%s' with parent='%s' and rate=%ld\n"
                , clock_cfg->clocks[i].clk_name, clock_cfg->clocks[i].parent_name
                , clock_cfg->clocks[i].clk_rate);

    if (clock_cfg->clocks[i].parent_name[0] != '\0') {
      clkp = vclk_get(clock_cfg->clocks[i].parent_name);
      if (!clkp || clkp < 0) {
        vclk_error("Failed to get Clock '%s'\n", clock_cfg->clocks[i].parent_name);
        continue;
      }
      vclk_set_parent(clk,clkp);
    }

    if (vclk_xable(clk, 1)) {
      vclk_error("Failed to Enable Clock '%s'\n", clock_cfg->clocks[i].clk_name);
      return -2;
    }

    if(clock_cfg->clocks[i].clk_rate) {
      if (vclk_set_rate(clk, clock_cfg->clocks[i].clk_rate)) {
        vclk_error("Failed to set rate %lld for Clock '%s'\n", clock_cfg->clocks[i].clk_rate, clock_cfg->clocks[i].clk_name);
        return -3;
      }
    }
  }

  return 0;
}


/* ========================================================================
   Name:        clk_disable_clocks
   Description: Disable Splash Screen clocks
   ======================================================================== */
int clk_disable_clocks(void)
{
  int i = 0;
  const stm_clock_t* clock_cfg = &clock_configuration[0];
  struct vclk *clk  = NULL;

  if(!clocks_initialized)
  {
    vclk_debug(0, "Initializing clock module\n");
    if (vclk_init_clocks()) {
        vclk_error("ERROR: failed to initialize clock module\n");
        return -1;
    }
  }

  /* Disable Dynamic clocks */
  for (i = 0; i < clock_cfg->nb_clocks; i++)
  {
    if ((!clock_cfg->clocks[i].clk_name) || (clock_cfg->clocks[i].clk_name[0] == '\0'))
      break;

    clk = vclk_get(clock_cfg->clocks[i].clk_name);

    if(!clk || clk < 0) {
        vclk_error("Failed to get Clock '%s'\n", clock_cfg->clocks[i].clk_name);
        continue;
    }

    vclk_xable(clk, 0);
  }

  /* Disable Fixed Clocks */
  for (i = 0; i < N_ELEMENTS(fixed_clock_names); i++)
  {
    clk = vclk_get(fixed_clock_names[i].clk_name);

    if(!clk || clk < 0) {
        vclk_error("Failed to get Clock '%s'\n", fixed_clock_names[i].clk_name);
        continue;
    }

    if (vclk_xable(clk, 1)) {
      vclk_error("Failed to Enable Clock '%s'\n", fixed_clock_names[i].clk_name);
      return -2;
    }
  }

  return 0;
}
