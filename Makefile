BUILD_SPLASH	?= $(shell pwd)

ECHO 	?= @echo
MAKE	?= make

ifeq ($(ARCH),arm)
CC    	:= arm-none-eabi-gcc
AR    	:= arm-none-eabi-ar
STRIP 	:= arm-none-eabi-strip
else
$(error Unspecified build architecture, set ARCH=arm)
endif

CFLAGS := -Wno-format -Wall -fno-short-enums -Werror

ifeq ($(DISPLAY_SPLASH_DEBUG),y)
CFLAGS += -g -DDEBUG
else
CFLAGS += -Os -g
endif

ifneq ($(SDK2_SOURCE_VCLKD),)
CLOCK_PATH := $(SDK2_SOURCE_VCLKD)
endif
CLOCK_PATH ?= $(BUILD_SPLASH)/clock

CLOCK := $(addprefix $(CLOCK_PATH)/,  \
			vclk.c                    \
			vclk_algos.c              \
			vclk_flexgen.c            \
			misc/vclk_sysconf.c)

# BMP format type (default image type)
BMP_CFLAGS 	:= -DSPLASH_IMAGE_BMP
BMP_SRC		:= $(addprefix bmp/,      \
				bmp-decoder.c)

# GIMP header format type
GIMP_CFLAGS	:= -DSPLASH_IMAGE_GIMP_HEADER
GIMP_SRC	:= $(addprefix gimp_header/, \
				gimp_header.c)

ifneq (,$(filter $(STSPLASH_CHIP),h301 h305 h310 h407 h410))
 CFLAGS +=-Dstx410
 CLOCK += $(addprefix $(CLOCK_PATH)/, socs/vclk-h410.c)
else ifneq (,$(filter $(STSPLASH_CHIP),h314 h318 h414 h418 h31A))
 CFLAGS +=-Dstx418
 CLOCK += $(addprefix $(CLOCK_PATH)/, socs/vclk-h418.c)
else ifneq (,$(filter $(STSPLASH_CHIP),h419 h319 ))
 CFLAGS +=-Dstx418 -Dstx419
 CLOCK += $(addprefix $(CLOCK_PATH)/, socs/vclk-h419.c)
else ifneq (,$(filter $(STSPLASH_CHIP),h390 h39D))
CFLAGS +=-Dstx390
ifeq ($(SOC_CUT), 2)
 CFLAGS +=-DCUT_2
endif
CLOCK += $(addprefix $(CLOCK_PATH)/, socs/vclk-h390.c)
else ifneq (,$(filter $(STSPLASH_CHIP),h337))
 CFLAGS +=-Dstx390 -Dstx337
 CLOCK += $(addprefix $(CLOCK_PATH)/, socs/vclk-h337.c)
else
$(error Platform not supported)
endif

BUILD_EXPORT_DIR				?= $(BUILD_SPLASH)/armv7/bare
SPLASH_IMAGE					?= bmp
SPLASH_DISPLAY_STANDARD 		?= PAL

DISPLAY_SPLASH_IMG_TYPE 		?= $(SPLASH_IMAGE)
DISPLAY_SPLASH_MEM_BASE 		?= $(VIBE_SPLASH_MEM_BASE)
DISPLAY_SPLASH_MEM_SIZE 		?= $(VIBE_SPLASH_MEM_SIZE)
DISPLAY_SPLASH_STANDARD 		?= $(SPLASH_DISPLAY_STANDARD)
ifeq ($(DISPLAY_SPLASH_STANDARD),PAL)
DISPLAY_SPLASH_MAIN_MODE 		?= STM_MAIN_MODE_1280X720P50
DISPLAY_SPLASH_AUX_MODE  		?= STM_AUX_MODE_PAL
endif
ifeq ($(DISPLAY_SPLASH_STANDARD),NTSC)
DISPLAY_SPLASH_MAIN_MODE 		?= STM_MAIN_MODE_1280X720P60
DISPLAY_SPLASH_AUX_MODE  		?= STM_AUX_MODE_NTSC
endif
DISPLAY_SPLASH_ENABLE_YPBPR 	?= y
DISPLAY_SPLASH_HDMI_YCBCR 		?= n
DISPLAY_SPLASH_TEST_ALL_MODES	?= n
DISPLAY_SPLASH_TEST_ALL_FORMATS	?= n

ifeq ($(DISPLAY_SPLASH_MEM_BASE),)
$(error DISPLAY_SPLASH_MEM_BASE is not defined !!)
endif
ifeq ($(DISPLAY_SPLASH_MEM_SIZE),)
$(error DISPLAY_SPLASH_MEM_SIZE is not defined !!)
endif

CFLAGS +=-DDISPLAY_SPLASH_MEM_BASE=$(DISPLAY_SPLASH_MEM_BASE)
CFLAGS +=-DDISPLAY_SPLASH_MEM_SIZE=$(DISPLAY_SPLASH_MEM_SIZE)
CFLAGS +=-D$(DISPLAY_SPLASH_MAIN_MODE) -D$(DISPLAY_SPLASH_AUX_MODE)

ifeq ($(DISPLAY_SPLASH_ENABLE_YPBPR),y)
CFLAGS +=-DSTM_HD_COMPONENT
endif

ifeq ($(DISPLAY_SPLASH_HDMI_YCBCR),y)
CFLAGS +=-DSTM_HDMI_YCBCR_ENABLE
endif

ifeq ($(DISPLAY_SPLASH_TEST_ALL_MODES),y)
CFLAGS +=-DTEST_ALL_MODES
endif

ifeq ($(DISPLAY_SPLASH_TEST_ALL_FORMATS),y)
CFLAGS += -DTEST_ALL_FORMATS
endif

INC        += -I$(BUILD_SPLASH)
INC        += -I$(CLOCK_PATH) -I$(CLOCK_PATH)/misc
SRC_FILES  := $(CLOCK) bare-tools.c clock.c splash.c

ifeq ($(DISPLAY_SPLASH_IMG_TYPE),bmp)
INC        += -I$(BUILD_SPLASH)/bmp
SRC_FILES  += $(BMP_SRC)
CFLAGS     += $(BMP_CFLAGS)
else ifeq ($(DISPLAY_SPLASH_IMG_TYPE),gimp_header)
INC        += -I$(TOPDIR)/gimp_header
CFLAGS     += $(GIMP_CFLAGS)
SRC_FILES  += $(GIMP_SRC)
else
$(warning **** Please specify image/bitmap decoder otherwise you will not get display until calling splash_enable ****)
endif

ifneq ($(USE_SPLASH_SHELL),)
SRC_FILES  += test/shell.c
endif

export CC INC CFLAGS BUILD_EXPORT_DIR

show_configuration:
	$(ECHO) "--- SplashScreen driver build configuration"
	$(ECHO) " * BUILD_EXPORT_DIR                 = $(BUILD_EXPORT_DIR)"
	$(ECHO) " * DISPLAY_SPLASH_IMG_TYPE          = $(DISPLAY_SPLASH_IMG_TYPE)"
	$(ECHO) " * DISPLAY_SPLASH_MEM_BASE          = $(DISPLAY_SPLASH_MEM_BASE)"
	$(ECHO) " * DISPLAY_SPLASH_MEM_SIZE          = $(DISPLAY_SPLASH_MEM_SIZE)"
	$(ECHO) " * DISPLAY_SPLASH_MAIN_MODE         = $(DISPLAY_SPLASH_MAIN_MODE)"
	$(ECHO) " * DISPLAY_SPLASH_AUX_MODE          = $(DISPLAY_SPLASH_AUX_MODE)"
	$(ECHO) " * DISPLAY_SPLASH_ENABLE_YPBPR      = $(DISPLAY_SPLASH_ENABLE_YPBPR)"
	$(ECHO) " * DISPLAY_SPLASH_HDMI_YCBCR        = $(DISPLAY_SPLASH_HDMI_YCBCR)"
	$(ECHO) " * DISPLAY_SPLASH_TEST_ALL_MODES    = $(DISPLAY_SPLASH_TEST_ALL_MODES)"
	$(ECHO) " * DISPLAY_SPLASH_TEST_ALL_FORMATS  = $(DISPLAY_SPLASH_TEST_ALL_FORMATS)"

export:
	$(ECHO) "--- Exporting Header files"
	$(shell mkdir -p $(BUILD_EXPORT_DIR)/lib)
	$(shell mkdir -p $(BUILD_EXPORT_DIR)/include)
	$(shell cp $(BUILD_SPLASH)/splash.h $(BUILD_EXPORT_DIR)/include/)
	$(shell cp $(BUILD_SPLASH)/sys.h $(BUILD_EXPORT_DIR)/include/)

splash: | export
	$(ECHO) "--- Building splash bare library"
	$(CC) $(INC) $(CFLAGS) -c $(SRC_FILES)
	$(AR) ruv $(BUILD_EXPORT_DIR)/lib/libsplash.a *.o
ifneq ($(DISPLAY_SPLASH_DEBUG),y)
	$(STRIP) --strip-debug --strip-unneeded $(BUILD_EXPORT_DIR)/lib/libsplash.a
endif

tests:
	$(Q)$(MAKE) -C $(BUILD_SPLASH)/test tests

debug:
	$(Q)$(MAKE) -C $(BUILD_SPLASH)/test dbg

dbgx:
	$(Q)$(MAKE) -C $(BUILD_SPLASH)/test dbgx

run:
	$(Q)$(MAKE) -C $(BUILD_SPLASH)/test run

clean:
	$(Q)rm -rf *.o $(BUILD_EXPORT_DIR)

clean_tests:
	$(Q)$(MAKE) -C $(BUILD_SPLASH)/test clean
